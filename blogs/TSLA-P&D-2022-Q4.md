---
title: 特斯拉（TSLA）發布 2022 Q4 生產及交付量，股價一年跌了 70%
description: '美股特斯拉|特斯拉 TSLA|特斯拉生產交車量 2022 Q4|特斯拉 Model 3 生產交車量 2022 Q4 第四季|特斯拉 Model Y 生產交車量 2022 Q4 第四季'
date: 2023-01-02
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 01/02（一）公佈了 2022 第四季的生產 & 交車數量 </br>

在 Q3 財報發布 → 公佈 2022 Q4 交車數量期間（10/19 → 01/02），特斯拉股價暴跌 44.5%，而 S&P 500 指數僅下跌 3% </br>

如果巨大的跌幅，除了受美聯儲加息的影響外，馬斯克為了購買推特＆籌備營運資金，而賣股的決策也加大了股價跌幅的程度 </br>

而在科技業開啟了裁員風潮後，金融業也準備在 2023 年初開始裁員，種種跡象都讓人擔心特斯拉是否仍能維持強勁的「交付量」</br>

下面來看看特斯拉在這 2022 的最後一季度表現如何

## 特斯拉 2022 Q4 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q3</th>
    <th align="center">2022 Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">19,935</td>
    <td align="center">20,613</td>
    <td align="center">15.42%</td>
    <td align="center">21.47%</td>
    <td align="center">3.4%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">345,988</td>
    <td align="center">419,088</td>
    <td align="center">-16.83%</td>
    <td align="center">42.87%</td>
    <td align="center">21.13%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">18,672</td>
    <td align="center">14,147</td>
    <td align="center">9.77%</td>
    <td align="center">15.53%</td>
    <td align="center">-24.23%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">325,158</td>
    <td align="center">388,131</td>
    <td align="center">-19.23%</td>
    <td align="center">36.32%</td>
    <td align="center">19.37%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">365,923</td>
    <td align="center">439,701</td>
    <td align="center">-15.33%</td>
    <td align="center">41.51%</td>
    <td align="center">20.16%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">343,830</td>
    <td align="center">402,278</td>
    <td align="center">-17.85%</td>
    <td align="center">35%</td>
    <td align="center">17%</td>
  </tr>
</table>

Q4 生產/交付量：
- Model S/X：生產量約 2 萬輛，與上季相比，成長了 3.4%，季成長率為 2022 年最低（前二季的成長率約 15.5% ＆ 21.5%）
- Model S/X：交付量約 1.4 萬輛，與上季相比，衰退 24%，是 2022 年，唯一的季度負成長
- Model 3/Y：生產量約 42 萬輛，季成長為 21%，雖不及上季的 43%，但因上季有許多僅差了關鍵零件即可完成生產的車，因此本季的產量仍符合預期
- Model 3/Y：交付量約 40 萬輛，季成長為 17%，雖不及上季的 35%，但在部分車主為了 2023 的能源車減免，而延遲購買的情況下，季成長 17% 已是不錯的成績

2022 總生產量約 137 萬輛車，年成長為 47%；總交付量約 130 萬輛，年成長為 40%；而 Model S/X 皆約佔了 5% </br>

雖沒成功達成「交付量 50% 年成長率」的目標，但若以生產量來說其實已十分接近了 </br>
且因美國在 2022 通過的 「通脹削减法案」的影響，在 2023 年購車的特斯拉車主，都能獲得 $7,500 的減免，也連帶的縮減特斯拉在第四季的交付量



## 股價暴跌回 2020 Q3 時期的特斯拉
特斯拉股價在 2022 的表現為 -69%（S&P 500下跌 20%），一舉將股價跌回 2020 Q3 的時期

- 當時，特斯拉的「年產量的目標」為 50 萬輛 → 現在，年產量為 137 萬輛
- 當時，一個季度僅能生產約 13 萬輛 Model 3/Y → 現在，一個季度就能生產 42 萬輛
- 當時，僅有加州工廠＆僅能生產 Model 3 的上海工廠 → 現在，有著生產量是當時 3 倍的上海工廠，更多了柏林＆德州工廠
- 當時，總營收約 88 億，總利潤 20.6 億 → 現在，總營收為 215 億，總利潤約 54 億
- 當時，特斯拉並未入選 S&P 500 指數 → 現在，已是 S&P 500 指數內，市值第 14 大的公司


回顧特斯拉 2020 Q3 財報：[特斯拉（Tesla）公布 Q3 季報，繳出了史上最好的成績單](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-Q3-2020.html)

## 後記
誰能想到特斯拉的股價在經歷了 2020 & 2021 的飆漲後，會在 2022 寫下 -70% 的成績 </br>
只要在 2020 的 9 月後買進特斯拉的投資人，一直放到現在的，帳上都是虧錢的 </br>

這是否代表著現階段不該再買入特斯拉？或是該直接買 TSLQ 來做空特斯拉呢？</br>
相反的，若對於特斯拉仍具信心，或是有在關注特斯拉財報的投資人，則會認為加碼的時候到了 </br>

二年前，仍未出社會，或是資金不夠的投資人，到了現在，薪資已有成長，也存了些積蓄 </br>
現在遇上特斯拉股價打折出清，雖有可能繼續下跌，但仍提供了不錯的進場機會 </br>

在 2022 年，我僅買入 73 股特斯拉，僅約 2021 的 25%，且這些都帶來了帳面上的虧損 </br>
但我仍希望它能在 2023 繼續下跌，讓我可在這新的一年持續買入更多特斯拉


::: warning
點擊觀看更多： </br>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
