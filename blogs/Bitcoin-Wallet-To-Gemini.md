---
title: 從 Bitcoin.com 錢包轉出 BCH 到 Gemini 交易所
description: 'Noise.Cash 介紹|Noise.Cash 教學|Noise.Cash 錢包|BCH 錢包|Bitcoin Cash 錢包|Noise.Cash 推薦錢包|Noise.Cash 轉出|Noise.Cash 出金|Bitcoin Cash 教學|使用 Bitcoin Cash 錢包|Gemini 介紹|Gemini 教學|Gemini 交易所|Gemini 推薦'
date: 2022-03-12
tags: [Noise.Cash, SocialFi, Crypto, Bitcoin.com, Gemini]
categories: Investment
---

## Gemini 介紹
前篇 [Noise.Cash 連接到 Bitcoin.com 錢包教學](https://ycjhuo.gitlab.io/blogs/NoiseCash-To-Bitcoin-Wallet.html) 提到我們在 Noise.Cash 中賺到的 BCH 可以直接從 Noise.Cash 轉到 Bitcoin.com 錢包上，但有沒有更好的選擇呢？

這篇要介紹的就是 Gemini 這個總部在紐約的交易所，成立於 2014 年，且受到「紐約金融服務管理局」的監管（NYSDFS），且是少數有提供利息給美國使用者的交易所，而這也是我選擇 Gemini 的原因 </br>
因為你若是在美國的話，像是幣安（Binance）、FTX、Coinbase 等等皆沒有提供 BCH 的利息，唯一能選擇且規模較大的交易所就只剩下 Gemini 了

Gemini 目前針對 BCH 有提供 5.12% 的利息，更多幣種利息可參考 [Gemini Earn](https://www.gemini.com/earn)
Gemini 的法幣出金幣種則有、提供美金、歐元、英鎊、港幣，新加坡幣、澳幣、加幣 </br>

接下來就開始介紹如何將 BCH 從 Bitcoin.com 轉到 Gemini

## 在 Gemini 交易所建立 BCH 收款地址
下面顯示的操作是在網頁版的 Gemini
1. 點擊右上方的 Transaction，選擇 Deposit into Gemini

![Noise.Cash](../images/166-1.png)

2. 進入轉帳頁面（Transfer funds） 後，在下方的 Currency 中，選擇 BCH，接著按下 Continue，就會產生 BCH 的收款地址了
若是要把其它加密貨幣轉到 Gemini 的話，則這邊要選擇相對應的加密貨幣

![Noise.Cash](../images/166-2.png)

## 從 Bitcoin.com 傳送 BCH 到 Gemini
取得 Gemini 的 BCH 收款地址後，我們就要到 Bitcoin.com 來把 BCH 轉過去了 </br>
這邊我是用手機版的 Bitcoin.com App

1. 打開 Bitcoin.com App，點選 Send 來到轉帳頁面後，我們可以選擇把剛剛在 Gemini 產生的收款地址貼到最上方的搜尋欄，或是直接點擊 Scan QR Code 來掃描剛剛在 Gemini 產生的 QR Code

![Noise.Cash](../images/166-3.png)

2. 輸入 Gemini 的 BCH 收款地址後，畫面會跳轉到轉帳金額的頁面，我們決定好要轉帳到金額後，按下 Continue，之後再將 SLIDE TO SENT 的按鈕滑到右邊，轉帳就成功了

![Noise.Cash](../images/166-4.png)

::: tip
Gemini 官方有特別說明：</br>
因為 BCH 的交易特性，也就是要在區塊鏈上要達到 15 個節點的確認，交易才會成功，所以通常要等 2 - 2.5 小時 </br>
但以我的經驗，轉出後大概等 15 分鐘，即可在 Gemini 帳戶內看到剛剛轉出的 BCH 了  </br>
:::

若不放心的，可至 [Blockchair](https://blockchair.com/) 這裡查詢自己 BCH 的交易紀錄 </br>
點擊連結後，在上方搜尋欄輸入自己的 BCH 轉出地址，即可找到該地址的轉出紀錄
![Noise.Cash](../images/166-7.png)


## 在 Gemini 上賺取利息
在 BCH 轉到 Gemini 後，我們就可以開始用 BCH 賺利息囉，不過 Gemini 並不會自動把你帳戶上的 BCH 拿去賺利息，因此我們須再做下面幾個步驟，才能讓 BCH 開始幫我們賺利息

1. 首先回到 Gemini 的 Earn 頁面，點擊 BCH 旁邊的 Earn，之後選擇要用來賺利息的 BCH 金額，按下 Continue 即可開始賺取利息

![Noise.Cash](../images/166-5.png)

2. 完成後，我們可以看到下面這個提示訊息，告訴我們說我們剛剛輸入的 BCH 會在下一個工作日開始幫我們賺取利息

![Noise.Cash](../images/166-6.png)



## Gemini 手續費 & 推薦碼
Gemini 的手續費與 Coinbase 一樣，都是以交易金額來算：
- 金額 < $10 的話：收取 $0.99 的手續費（約 9.9%）
- 金額介於 $10 - $24.99 的話：收取 $1.49 的手續費（約 5.96%）
- 金額介於 $25 - $49.99 的話：收取 $1.99 的手續費（約 3.98%）
- 金額介於 $50 - $199.99 的話：收取 $2.99 的手續費（約 1.495%）
- 金額 > $200 的話：收取 1.49% 的手續費

由此可知，交易金額越高，手續費就越低

對於 Gemini 交易所感興趣的，可以用我的 [Gemini 推薦連結](https://www.gemini.com/share/wax7vgvue) 註冊 </br>
註冊成功後，我們二個都會得到 $10 美元等值的比特幣（BTC），不過要先交易 $100 以上，才可拿到獎勵

## 後記
這次介紹了把從 Noise.Cash 賺到的 Bitcoin Cash（BCH）出金的流程：</br>

[Noise.Cash → Bitcoin.com 錢包](https://ycjhuo.gitlab.io/blogs/NoiseCash-To-Bitcoin-Wallet.html) → Gemini 交易所 -> 出金 </br>

1. 一開始從 Noise.Cash 轉出 0.06664095 的 BCH 到 Bitcoin.com 錢包
2. 因為轉到 Bitcoin.com 錢包不用手續費，所以這時 BCH 的金額仍是 0.06664095
3. 從 Bitcoin.com 錢包轉出 0.06664095 BCH 到 Gemini 交易所
4. 在 Gemini 交易所，收到了 0.06663818 的 BCH，在這中間被收了手續費 0.00000277 BCH

手續費是一次性的，跟轉出金額無關，因此若要省下手續費的話，也可以直接將 BCH 從 Noise.Cash 轉到 Gemini

