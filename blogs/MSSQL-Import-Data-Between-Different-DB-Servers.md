---
title:  在不同伺服器的資料庫 (MS SQL)中匯入資料
description: '不同 Server 匯資料|不同資料庫匯入|連接不同資料庫|MS SQL匯入資料到另一台資料庫|MS SQL 匯另一台 MS MQL||MS SQL 不同 MS MQL 匯入|MS SQL Linked Server 教學||MS SQL Linked Server 連接'
date: 2021-03-10 01:04:20
tags: [MSSQL, Database]
categories: Programming
---

若我們想要將 A 資料庫中特定 table 的資料（每天）匯入到不同伺服器中的 B 資料庫中，該怎麼做呢？

這個範例可以讓我們將 A, B 資料庫連接，並藉由設定排程來達到這個目的。

## 設定資料庫連線
1. 首先，我們先在 A 資料庫中，建立與 B 資料庫的連線。</br>
點選在資料庫項下的 Linked Server -> New Linked Server -> General 裡面的 linked Server。</br>
linked Server 要填上 B 資料庫的名稱。</br>
![62-1](../images/62-1.png)
![62-2](../images/62-2.png)

資料庫的名稱可在資料庫中的 Properties 找到，如下圖紅框處：</br>
![62-3](../images/62-3.png)

2. 接著切換到 Security 頁籤，在下面填上 B 資料庫的登入 ID 及密碼。</br>
按下 OK 後即可在 A 資料庫中建立好與 B 資料庫的連線（若連線不到 B 資料庫的話，則有可能是防火牆沒開通）</br>

![62-4](../images/62-4.png)

## 設定匯入條件
這部分我們就要開始建立從 A 資料庫匯資料到 B 資料庫的條件了（若要設定其他條件也可以）</br>
步驟類似於我們在 Server 中建立排程（Task Schedule）

3. 在資料庫項下的 SQL Server Agent -> New -> Job </br>
![62-5](../images/62-5.png)
4. Step name 是這個排程的名稱，可依自己的需求取名，下方 Database 則是我們要進行操作的 database，command 的地方則填入要執行的代碼 </br>
因為我這邊是要匯入資料到 B 資料庫，因此我就填上這段匯入資料的 script
```sql
Insert Into [B DB Server Name].[B DB Name].[B Table]
SELECT * FROM [A DB Name].[A Table]
WHERE MESG_CREATE_DATE > convert(varchar(8), dateadd(day,-2,getdate()), 112)
AND  MESG_CREATE_DATE <  convert(varchar(8), getdate(), 112)
```
這邊的 WHERE 條件是因為我要匯入每天的資料，而 MESG_CREATE_DATE 則是我 Table 中的一個時間欄位
因此就可以用第 3, 4 行取得每天的資料。</br>

![62-6](../images/62-6.png)

5. 設定好匯入的條件後在切換到 Schedules 這個頁籤，選擇要執行程式的頻率及時間就完成了

![62-7](../images/62-7.png)