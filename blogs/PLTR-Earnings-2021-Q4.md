---
title: Palantir（PLTR），暴漲 250% 又回到上市價的現在，是否到買點？2021 財報解析
description: '美股投資 Palantir PLTR 2021 財報分析|AI 大數據分析的顧問公司|顧客包含美國政府|協助找到賓拉登|Palantir PLTR 買點｜Palantir PLTR 買入｜Palantir PLTR IPO 價格'
date: 2022-03-13
tags: [Stock, PLTR]
categories: Investment
---

曾打著協助美國政府擊殺賓拉登的 AI 大數據分析公司 Palantir（PLTR），也被譽為是矽谷最神秘的獨角獸，於 2004 年創立，至今已成立 19 年，仍未實現獲利 </br>

Palantir（PLTR）在 2020/09/30 上市（$10），在上市四個月後，股價暴漲了 250%，來到歷史高點（$35.18），而在 18 個月後的現在，股價又回到了當初的上市價格</br>

Palantir 的股價在 2021 時下跌了 22.68%，而同期間 S&P 500 上漲了 24.23%，一漲一跌，報酬率在 2021 年跟大盤差了近 50% </br>

而在 2022/2/17，Palantir 發佈了 2021 年的財報 ，來看看在目前股價（$11.39）回到接近 IPO 上市價格的時候，是否已經到買點了

## Palantir 2021 財務指標
[來源](https://investors.palantir.com/news-details/2022/Palantir-Reports-Revenue-Growth-of-41-for-FY-2021-US-Commercial-Revenue-up-102-YY-in-FY-2021)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2019</th>
    <th align="center">2020</th>
    <th align="center">2021</th>
    <th align="center">Chg（2020 vs 2019）</th>
    <th align="center">Chg（2021 vs 2020）</th>
  </tr>
  <tr>
    <td align="center">營收（千）</td>
    <td align="center">742,555</td>
    <td align="center">1,092,673</td>
    <td align="center">1,541,889</td>
    <td align="center">47.15%</td>
    <td align="center">41.11%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">67.36%</td>
    <td align="center">67.74%</td>
    <td align="center">77.99%</td>
    <td align="center">0.38%</td>
    <td align="center">10.25%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-77.63%</td>
    <td align="center">-107.41%</td>
    <td align="center">-22.66%</td>
    <td align="center">-29.78%</td>
    <td align="center">80.75%</td>
  </tr>
  <tr>
    <td align="center">EPS（diluted）</td>
    <td align="center">-1.02</td>
    <td align="center">-1.2</td>
    <td align="center">-0.27</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：2021 年營收為 15.4 億，年成長率為 41.11%，去年的成長率為 47.15%
- 毛利率：2021 的毛利率為 77.99%，相比去年＆前年的 67%，大幅上升了 10.25%
- 營業利潤率：2021 的營業利潤率為 -26.66%，雖比去年的 -107.41% 改善很多，但仍然是負數
- EPS：2021 年的 EPS 為 -0.27，與去年的 -1.2 相比，已大幅改善

Palantir 在 2021 年的淨虧損為 5.2 億，相比 2020 虧損的 11.6 億，已減少了一半，但仍處於虧損狀態 </br>
雖在今年已大幅縮減了虧損，但 Palantir 未來能否真的在現有的營運模式上實現盈利，仍是一個問號 </br>
不過除了上面的財務指標，財報中闡述了 2021 的重大成長以及給出了 2022 年的財務指引


## Palantir 2022 業務發展
若單以 Q4 的財報來看，Palantir 在 2021 Q4 增加了 34 個新客戶，2021 整年度的話：
- 政府機關類的營收成長 47%，帶來的營收為 8.79 億，佔了總營收的 57% </br>
- 商務型客戶的營收年成長則是 34%，但若以地區來分，美國地區商務型客戶所帶來的營收，年成長則達到了驚人的 102%
- 平均營收的留存率為 131%，其中美國地區的商務型客戶營收留存率為 150%，全球政府類的營收留存率為 146%

財報中著重於全球政府機關＆美國地區的商務型客戶，表示在政府＆商務這二種客群中，政府類客戶是營收成長的主力，接著才是美國地區的商務型客戶，而其他地區的商務型客戶，應是推展不順，財報並沒有磨墨太多 </br>

在預測 2022 的財報方面：
- Q1 的營收預估為 4.4 億，僅比 2021 Q4 的營收成長了 2.34%
- 全年的營收則預估會有 30% 左右（或更高）的成長服務，並保持這個速度至 2025 年
- 營業利益率方面，Q1 時可達到 23%，整年度的話則是 27%

雖說年成長 30% 很高，但與前二年的 47% & 41% 相比下，仍是下降不少，唯一的優點就是營業利益率在 2022 年總算可以轉正（2021 的營益率為 -22.66%）</br>
這表示說，若沒有其它因素干擾的話，Palantir 將會在 2022 年首次實現盈利


## 後記
本來在看財務數據時，想說 Palantir 雖然年營收成長的速度很快，但受限於本身的營運費用過高，要在短時間內實現盈利其實不容易 </br>
但沒想到在財報中，Palantir 給出了 2022 Q1 的營業利益率可到達 23% 的預測，相比 2021 的 -26.66%，大大改善了 50% </br>
若能成功達成這個目標的話，則可證明 Palantir 的商業模式是可以賺到錢的

且由於 Palantir 的客戶分為政府機關＆大型商業公司二類（因為 Palantir 高昂的收費，普通的小公司也負擔不起），其實都算是穩定且長期的客戶，不太會有專案做到一半沒收到錢，或是客戶輕易流失等狀況 </br>

由於政府機關＆大型公司的招標過程都是很繁複的，在導入廠商前，要做的事前調查其實不少，因此一旦導入 Palantir 的服務就不會輕易的中斷使用，這點從財報中的營收留存率 > 130% 也可看出，表示客戶每年都會付出比原本高 30% 的費用來給 Palantir；但也可能是第一年 Palantir 給的折扣比較高，第二年就回到原價，客戶自然越付越多 </br>


Palantir 的財報在 2021 的表現，的確比 2020 好很多，但仍不足讓它脫離虧損的狀態，而唯一能讓 Palantir 股價反轉的因素，只能寄望於 2022 Q1 的盈利 </br>

在去年寫的這篇 [PLTR ，用大數據協助美國政府擊殺賓拉登的公司，第四季及 2020 全年財報](https://ycjhuo.gitlab.io/blogs/PLTR-Earnings-2021-Q4.html)，我不太看好 Palantir 的，而在過去一年，Palantir 的股價也的確一若千丈 </br>

但過了一年的現在，整體而言，Palantir 在財報＆未來發展都是往好的方向前進，唯獨股價是越來越低，若能忍受一定程度的虧損的話，長期來說，Palantir 肯定會帶來不錯的報酬率
