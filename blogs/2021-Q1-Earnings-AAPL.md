---
title: Apple 公佈 2020 Q1 財報，營收與 EPS 皆創新高
description: '美股投資|蘋果 AAPL 2020 Q1 第一季財報分析|蘋果 營收 EPS 創新高'
date: 2021-02-16 00:47:02
tags: [Stock, AAPL]
categories: Investment
---

蘋果在 2021/01/27 發佈了 2020 Q1 財報，營收與 EPS 雙雙破了歷史紀錄。</br>
在財報的第一行就寫著：營收成長 21%，EPS 成長了 35%。顯示了蘋果在接連突破 1 兆，2 兆市值後，成長動能仍然足夠強勁，而穩居美股最高市值企業的位置。
::: tip
Revenue up 21 percent and EPS up 35 percent to new all-time records
iPhone, Wearables, and Services set new revenue records
:::

## Apple 2021 Q1 季報
蘋果 的 Q1 季報範圍是 2020/10 - 2020/12，這個季度受益於新 iPhone 的推出，因此也是蘋果一年中表現最好的季度。

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q1</th>
    <th align="center">Change(%)</th>
  </tr>
  <tr>
    <td align="center">產品營收 (百萬)</td>
    <td align="center">95,678</td>
    <td align="center">79,104</td>
    <td align="center">20.95%</td>    
  </tr>
  <tr>
    <td align="center">服務營收 (百萬)</td>
    <td align="center">15,761</td>
    <td align="center">12,715</td>
    <td align="center">23.96%</td>
  </tr>
  <tr>
    <td align="center">總營收 (百萬)</td>
    <td align="center">111,439</td>
    <td align="center"> 91,819</td>
    <td align="center">21.37%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">39.78%</td>
    <td align="center">38.35%</td>
    <td align="center"> 3.71%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">25.80%</td>
    <td align="center">24.22%</td>
    <td align="center"> 6.55%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">1.7</td>
    <td align="center">1.26</td>
    <td align="center">34.92%</td>
  </tr>
</table>

從表中可看到，產品跟服務這二個類別的營收都達到了 20% 以上的成長，且毛利跟營利率相比去年也有所提升。</br>
服務類別因為軟體的邊際效益低的原因，利潤較容易成長。但產品方面，若要提升利潤，不外乎是提高產品售價或是從節省材料下手。讓我們來看看蘋果是從哪個地方提升了自己的毛利。</br>

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q1</th>
    <th align="center">Change(%)</th>
  </tr>
  <tr>
    <td align="center">產品開銷 (百萬)</td>
    <td align="center">62,130</td>
    <td align="center">52,075</td>
    <td align="center">19.31%</td>    
  </tr>
  <tr>
    <td align="center">服務開銷 (百萬)</td>
    <td align="center">4,981</td>
    <td align="center">4,257</td>
    <td align="center">10.03%</td>
  </tr>
  <tr>
    <td align="center">總開銷 (百萬)</td>
    <td align="center">67,111</td>
    <td align="center">52,075</td>
    <td align="center">18.57%</td>
  </tr>
</table>

蘋果的營收跟去年同期相比上升了 20.95%，開銷上升了 19.31%，二者差了 1.64％，而毛利率上升了 3.71%，表示蘋果增加的毛利，44% 是由供應商貢獻，而 56% 則是消費者買單。
但這也不代表我們買的 iPhone 12 系列變貴了，也有可能是因為其他毛利更高的產品銷量上升所致。

## Apple 2021 Q1 產品銷量

單位：百萬 </br>

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q1</th>
    <th align="center">Change(%)</th>
  </tr>
  <tr>
    <td align="center">iPhone</td>
    <td align="center">65,597</td>
    <td align="center">55,957</td>
    <td align="center">17.23%</td>    
  </tr>
  <tr>
    <td align="center">Mac</td>
    <td align="center">8,675</td>
    <td align="center">7,160</td>
    <td align="center">21.16%</td>
  </tr>
  <tr>
    <td align="center">iPad</td>
    <td align="center">8,435</td>
    <td align="center">5,977</td>
    <td align="center">41.12%</td>
  </tr>
  <tr>
    <td align="center">穿戴式裝置</td>
    <td align="center">12,971</td>
    <td align="center">10,010</td>
    <td align="center">29.58%</td>
  </tr>
  <tr>
    <td align="center">服務</td>
    <td align="center">15,761</td>
    <td align="center">12,715</td>
    <td align="center">23.96%</td>
  </tr>
  <tr>
    <td align="center">總計</td>
    <td align="center">111,439</td>
    <td align="center">91,819</td>
    <td align="center">21.37%</td>
  </tr>
</table>

不出意料的，iPhone 仍是佔了總銷量的六成左右，iPad 在這季銷量則躍升了 41%，在總銷量的佔比從去年的 6.5% 升到了 7.6%，幾乎能確定明年 Mac 就會成為銷量最低的產品。</br>

而穿戴式裝置的成長速度也幾乎到達了 30%，目前雖佔總銷量的 12% 左右，但很快就會超越軟體服務（14%），成為僅次於 iPhone 的營收主力。</br>

我們在上一段提到了蘋果毛利上升了 3.7%，很大可能也是因為 iPad, AirPods, Apple Watch 銷量上漲的幫助。這些看似單價低的產品，但其實毛利可能更勝於 iPhone。

::: tip
蘋果銷售類別佔比：iPhone 58.86%， Mac 7.78%， iPad 7.57%， 穿戴式裝置 11.64%， 服務為 14.14%
:::

## Apple 2021 Q1 地區銷量

單位：百萬 </br>

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q1</th>
    <th align="center">Change(%)</th>
  </tr>
  <tr>
    <td align="center">美洲</td>
    <td align="center">46,310</td>
    <td align="center">41,367</td>
    <td align="center">11.95%</td>    
  </tr>
  <tr>
    <td align="center">歐洲</td>
    <td align="center">27,306</td>
    <td align="center">23,273</td>
    <td align="center">17.33%</td>
  </tr>
  <tr>
    <td align="center">大中國區</td>
    <td align="center">21,313</td>
    <td align="center">13,578</td>
    <td align="center">56.97%</td>
  </tr>
  <tr>
    <td align="center">日本</td>
    <td align="center">8,285</td>
    <td align="center">6,223</td>
    <td align="center">33.14%</td>
  </tr>
  <tr>
    <td align="center">其他亞太區</td>
    <td align="center">8,225</td>
    <td align="center">7,378</td>
    <td align="center">11.48%</td>
  </tr>
</table>

以銷售地區來看的話，大中國區（Great China）跟去年相比，銷量暴增了 57％，若再加上日本的話，銷量直接超越歐洲市場。難怪蘋果越來越注重亞太市場。

## 後記
蘋果在這次財報中的強勁表現，也讓它在近一年的表現中，領先其他 S&P 500 指數中的第 2-5 名。

![55-01](../images/55-01.png)

創新高的不只是營收及 EPS，就連本益比也是五年內的新高。目前蘋果的本益比為 36.7，而 2016 - 現在的本益比平均數為 19.25，中位數只有 16.25。</br>
若將時間縮短為三年內（ 2018  - 現在）的話，本益比平均數為 22.48，中位數為 18.65。雖然蘋果在三年內，本益比上升了不少，但現在的本益比仍大於平均數 63%。顯示了投資人仍極度看好蘋果後續的發展。

很大原因是大家對於蘋果將要切入電動車市場的期待。希望蘋果能藉由 Apple Car 來重現如特斯拉一樣的漲幅。</br>

且以 Apple Car 這種價位的產品，以及現有用戶對於蘋果的信賴感及忠誠度，都讓人認為蘋果的確具有挑戰特斯拉的能力。

說了這麼多優點，那缺點呢？

還記得在 2018 年底到 2019 年初，蘋果因為 iPhone 銷量放緩，許多人認為蘋果已喪失了創新的能力。那時本益比只有 12.5 左右（我自己也是在那時加碼買入蘋果）</br>

![55-02](../images/55-02.png)

若去除 Apple Car 的因素，蘋果要維持這個本益比我認為是有難度的，且在電動車領域中，最重要的自動駕駛功能，特斯拉已經在收集數據賽道中佔據絕對的領先位置。當其他車廠還在進行上路前的測試時，特斯拉已經在目前售出的車輛中，取得源源不絕的數據資料。</br>

因為有著這個優勢，其他車廠要在自動駕駛上趕上特斯拉，是不太可能的。而若少了自動駕駛這個功能，Apple Car 是否還能成為消費者購車的首選仍有待考驗。




## 參考資料
[蘋果官網](https://www.apple.com/newsroom/2021/01/apple-reports-first-quarter-results/)