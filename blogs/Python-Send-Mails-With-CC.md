---
title: 如何在 Python 寄送郵件時加上不同副本收件人 ( CC )
description: 'Python 寄信教學 | Python 寄信介紹 | Python email 用法 | Python email cc 收件人'
date: 2021-01-21 10:00:58
tags: [Python, Pandas, Sendmail]
categories: Programming
---

在前二篇: [如何用 Python 寄送郵件](hhttps://ycjhuo.gitlab.io/blogs/Python-How-To-Send-Mails.html)，[用 Python 寄送客制化郵件](https://ycjhuo.gitlab.io/blogs/Python-Mutiple-And-Customize-Mail-Message.html) 寫到了如何寄發通知信給我們從 dataframe 篩選出來的人， 以及如何在通知信內加上 dataframe 中的資訊。

這篇來介紹該怎麼在寄送 mail 時，一併加上副本收件人。<br/>
程式碼如下：
```python
import pandas as pd
import smtplib

### Email Server Settings 
smtp_server = "XXXXX"
port = 25
server = smtplib.SMTP(smtp_server, port)
sender = "XXX@XXXX.com"
receiver = ""
cc = []


message_subject = "Subject: This is a mail subject"

message_text = """
Mail Content
"""

message = "From: %s\r\n" % sender \
        + "To: %s\r\n" % receiver \
        + "CC: %s\r\n" % ",".join(cc) \
        + "Subject: %s\r\n" % message_subject \
        + "\r\n" \
        + message_text


toAddress = [receiver] + cc
server.sendmail(sender, toAddress, message)
```

上面 5 - 17 行都是要發送 Mail 的一些設定，包含 mail server，寄件人，主旨，郵件內容等等。<br/>
19 行在用 message 這個變數將上述的所有設定包含進來。<br/>

27 行這邊要先將我們從 dataframe 中取得的收件人 以及要 cc 的對象合起來作為一個 list。<br/>
最後在用```server.sendmail(sender, toAddress, message)``` 將信發送出去，而這個方法裡面的三個參數分別是寄件人，收件人及郵件的所有內容。<br/>

這裡要注意，即使我們在 message 中已經設定好，寄信人，收件人，cc等等，但我們仍須另外設一個變數將收件人跟 CC 合併起來，並放到 ```sendmail``` 的第 2 個參數，才可成功將信發出。

:::tip
要注意收件者跟 CC 在合併時都要是 list 的形式才可，否則 CC 會收不到信。這也是為什麼我們一開始要先將 cc 宣告成 list。
:::

而收件人為什麼不一開始也設定成 list，而是在最後合併時（toAddress）才改成 list 呢？
這是因為若一開始就將收件人設為 list，那麼在發出信件時，收件者的位置就會顯示 [xxx@xxxx.com]，雖然還是可以成功讓收件人收到信，但在郵件中，並不好看。

若不想將信箱地址直接寫死在程式內的話，可參考這篇：[如何讓 Python 讀取外部 ini 設定檔](https://ycjhuo.gitlab.io/blogs/Python-Read-Ini-File.html) </br>
讓程式透過外部的 ini 檔案讀取相信箱地址