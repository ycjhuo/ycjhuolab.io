---
title: HackerRank Python Triangle Quest 解法
description: 'HackerRank 教學|HackerRank 解答|HackerRank Python|HackerRank Python Triangle Quest 解答'
date: 2021-03-14 14:08:47
tags: [Python, HackerRank, Medium]
categories: HackerRank
---
## Problem
### Task
You are given a positive integer ```N```. Print a numerical triangle of height ```N - 1``` like the one below:

::: tip
1</br>
22</br>
333</br>
4444</br>
55555</br>
......</br>
:::

Can you do it using only ```arithmetic operations, a single for loop and print statement?``` </br>
Use no more than two lines. The first line (the for statement) is already written for you. You have to complete the print statement.</br>

Note: Using anything related to strings will give a score of ```0```.</br>

### Input Format 
A single line containing integer, ```N```.

### Constraints 
```1 <= N <= 9```

### Output Format 
Print ```N - 1``` lines as explained above.

## Answer
這題看起來是考驗迴圈熟悉度，但其實上是考對於數學的熟悉度。 </br>
題目雖然限制了僅能使用算術運算及不超過二行的的程式碼來執行，但實際上也限制了型態轉換 </br>
```using only arithmetic operations, a single for loop and print statement?``` </br>

本來可以單純地將 i 轉為 str，再運用 Python 的字串相乘特性就可成功得到解答（如 Source Code 行 2）</br>
但在 Run Code 後，卻會得到下面這個錯誤訊息（就算我們將最後輸出的答案再轉型回 int 一樣會得到這個訊息）</br>
::: tip
Compiler Message </br>
Wrong Answer </br>
str is not allowed </br>
:::

而在討論區，有人給出了正統的解法（Source Code 行 3）</br>
將 i 作為 10 的 i 次方，再除上 9，最後乘上 i，就可得出題目要的答案。
::: tip
i = 1，算式會是：10 / 9 = 1，==> 1 * 1 = 1 </br>
i = 2，算式會是：100 / 9 = 11，==> 11 * 2 = 22 </br>
i = 3，算式會是：1000 / 9 = 1，==> 111 * 3 = 333 </br>
i = 4，算式會是：10000 / 9 = 1，==> 1111 * 4 = 4444 </br>
:::

### Source Code
```python {2}
for i in range(1,int(input())): # More than 2 lines will result in 0 score. Do not leave a blank line also
    print (str(i) * i)
    print( 10 ** i // 9 * i)
```

::: tip
在 Python 3 中，使用 / 得出的值會是浮點數，// 則是得到整數</br>
在 Python 2 中，使用 / 與 // 則沒有區別
:::