---
title: 啟動 VuePress 時，出現 error:0308010C 錯誤訊息
description: 'VuePress error:0308010C|VuePress envelope routines::unsupported|VuePress openssl-legacy-provider|NODE openssl-legacy-provider'
date: 2023-02-07
tags: [VuePress]
categories: Blog
---

在 Node.js 版本升上 17 後，因 Node.js 將原本使用的 OpenSSL 1.0.2 升為 OpenSSL 3.0，會讓某些功能無法在 OpenSSL 3.0 被正確運行，而在編譯時出現 ```error:0308010C``` 這個錯誤訊息，如下圖：

```powershell
Error: error:0308010C:digital envelope routines::unsupported

  opensslErrorStack: [ 'error:03000086:digital envelope routines::initialization error' ],
  library: 'digital envelope routines',
  reason: 'unsupported',
  code: 'ERR_OSSL_EVP_UNSUPPORTED'
```

![VuePress error:0308010C](../images/212-01.png)

要解決這個問題，除了將 Node.js 降版成 16 以下外，還可透過下面二個方式


## 解決 error:0308010C
### 方法一：set NODE_OPTIONS=--openssl-legacy-provider
1. 在 Command Prompt (命令提示字元) 中，先輸入：```set NODE_OPTIONS=--openssl-legacy-provider```
2. 之後再輸入原本的編譯指令即可 (如 ``` yarn dev ```) 即可

::: tip
每次開啟新的 Command Prompt 時，都須重新輸入 ```set NODE_OPTIONS=--openssl-legacy-provider```
:::

### 方法二：packages.json
開啟 ```packages.json``` 這個檔案，將原本的 script，如下
```json
"scripts": {
    "dev": "vuepress dev . --open --host \"localhost\"",
    "build": "vuepress build ."
  },
```

改為下面這樣後，再我們輸入平常啟動運行的 ```yarn dev```，系統就會自動加上 ```set NODE_OPTIONS=--openssl-legacy-provider``` <br/>
而不須我們在每次開啟新的 Command Prompt 後，都要再重新輸入 ```set NODE_OPTIONS=--openssl-legacy-provider``` 了

```json
"scripts": {
    "dev": "set NODE_OPTIONS=--openssl-legacy-provider & vuepress dev . --open --host \"localhost\"",
    "build": "set NODE_OPTIONS=--openssl-legacy-provider & vuepress build ."
  },
```