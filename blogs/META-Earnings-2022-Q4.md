---
title: Meta 2022 Q4 財報，股價在今年已上漲 46%
description: 'Meta FB 2022 Q4 第四季財報｜Facebook FB 2022 Q4 第四季財報｜'
date: 2023-02-06
tags: [Stock, Meta]
categories: Investment
---

社交龍頭 Meta 在 2/1 公佈了 2022 第四季的財報，在 Q3 財報 - Q4 財報之間（10/26 - 2/1）</br>
Meta 股價從 $129.82 → $152.32，上漲了 17%（同時期 S&P 500 上漲 7.5%）</br>

而在財報公布後隔天，股價直接跳漲 24% ！ 2023 才剛進入二月，Meta 股價已上漲 46% </br>
如此強勁的表現，是否代表 Meta 已擺脫「元宇宙」燒錢的困境？或是原有的「廣告業務」有特殊的表現？ </br>

下面就來看看 Meta 2022 Q4 財報

## Meta 2022 Q4 財報
[資料來源](https://s21.q4cdn.com/399680738/files/doc_financials/2022/q4/Meta-12.31.2022-Exhibit-99.1-FINAL.pdf)
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">2022 Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">28,822</td>
    <td align="center">27,714</td>
    <td align="center">32,165</td>
    <td align="center">3.28%</td>
    <td align="center">-3.84%</td>
    <td align="center">16.06%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">8,186</td>
    <td align="center">5,576</td>
    <td align="center">6,149</td>
    <td align="center">-8.11%</td>
    <td align="center">-31.88%</td>
    <td align="center">10.28%</td>
  </tr>
  <tr>
    <td align="center">廣告營利率</td>
    <td align="center">39.35%</td>
    <td align="center">34.04%</td>
    <td align="center">33.97%</td>
    <td align="center">-2.85%</td>
    <td align="center">-5.31%</td>
    <td align="center">-0.07%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">29%</td>
    <td align="center">20.44%</td>
    <td align="center">19.89%</td>
    <td align="center">-1.54%</td>
    <td align="center">-8.56%</td>
    <td align="center">-0.54%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">2.47</td>
    <td align="center">1.64</td>
    <td align="center">1.76</td>
    <td align="center">-9.85%</td>
    <td align="center">-33.6%</td>
    <td align="center">7.32%</td>
  </tr>
</table>

- 營收：本季總算突破到 322 億，是 2022 唯一突破 300 億的季度，季增率 16%，與去年同期的 -4% 相比，大有改善  </br>
細分的話：廣告業務季增 15％，虛擬實境業務（Reality Labs）季增 155%（佔營收的比例從上季的 1% 增至 2%，也是 2022 增速最快的季度）
- 稅前淨利：季增率為 10%，達到 62.5 億，但僅是去年同期的 48%，主因是營業成本的增加（較上季增加了 17%），故將營收吐回
- 廣告營利率：與上季持平，維持在 34%，在連三季的下跌後，總算維持住一定的毛利率，但仍與去年的 80% 有著巨大的落差
- 營業利益率：一樣維持在與上季相同的 20%，不僅低於 Q1 的 31%，也低於去年的 40％，而原因仍是營業成本＆行政費用的增加
- EPS：為 1.76，較上季成長 7%，是本季的唯一亮點，但年增率卻衰退了 52%

Meta 的營收在連續下跌後，在今年的最後季度總算有著大幅度的成長，將營收拉回到與去年同期相同，可惜由於營運成本過高，而導致利潤無法維持 </br>
值得注意的是，虛擬實境業務在營收持續成長的同時，虧損的幅度有著明顯的減緩，若下個季度能繼續保持，則可代表 Meta 已脫離最燒錢的階段

![Meta 2022 Q4 第四季 營收](../images/211-01.png)
![Meta 2022 Q4 第四季 利潤](../images/211-02.png)

## Meta 財報重點
Facebook 每日活躍用戶（DAUs）在本季突破了 20 億（季增率 4%），主要是由於短影音 Reels 帶動了使用者人數的成長 </br>
Reels 目前在 Facebook ＆ Instagram 上的播放量快速增長，僅花了不到半年的時間，播放量已增加一倍 </br>
但由於短影音的盈利效果較差，因此 Meta 正專注於加強短影音的貨幣轉化率＆運用 AI 來提升廣告的轉化率 </br>

Meta 在 2023 將專注在提高效率在各方面上，這點可以在 CFO 對於 2023 的預測，著重在調降營業成本＆各項資本支出來看出 </br>
而本季大幅增加的營運成本，主要有部分是組織重組的費用，若扣除組織重組的費用，營業利益率則會增加到 33%，一躍成為 2022 表現最好的季度 </br>

在 2022/12/31 時，Meta 共有約 8.6 萬名員工（包含約 1.1 萬被裁的員工），因此在 2023，Meta 將會減少約 13% 的員工數 </br>
儘管還須加上重組的費用，但相信 Meta 能在 2023 再提高營業利潤

## 後記
Meta 在 2022 的股價表現為 -64%，對比大盤的 -20%，可說是慘輸了三倍之多 </br>
但從這次公布財報後，股價暴漲 25% 來看，投資人對於 Meta 這次財報＆未來的方向相當滿意 </br>

Meta 執行長在財報會議上明確指出要使公司運作更有效率，且讓公司的獲利面變得更強，並專注於交出更好的財報成績 </br>
也因此，在本季已投入近 70 億來買回庫藏股（2022 共投入約 110 億）後，而最近也準備再投入 400 億來買回庫藏股 </br>

加上 Meta 透過裁員、調降建設資料中心等各項支出，讓我們相信 Meta 將更注重成本管控＆獲利 </br>
不像之前一昧的投入人力＆設備在「元宇宙」上，相信多數投資者＆股東也是看到了這點，才以實際行動推升 Meta 股價


::: warning
點擊觀看更多：
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::