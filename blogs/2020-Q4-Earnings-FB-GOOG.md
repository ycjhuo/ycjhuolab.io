---
title: Facebook & Google 公佈第四季財報，股價卻不同步？
description: '美股投資|Facebook 與 Google 2020 財報比較|FB vs. GOOG|Facebook Google 哪個好|Facebook Google 比較'
date: 2021-02-06 14:58:58
tags: [Stock, FB, GOOG]
categories: Investment
---

上週三，Google 公佈了 2020 Q4 及 2020 全年財報，財報公佈後，Google 股價在上週四、五，共漲了 8.8%；相對的，Facebook 在財報公佈後（01/27）的二天，股價則下跌了 5.3%。

延伸閱讀：[公佈第四季及 2020 全年財報，在企業縮減開支的一年下，Facebook 表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-FB.html)

下面來分析這二間身為數量廣告的龍頭企業，在財報的表現是否真的如股價反應的一樣有著不小的差距。

## 2020 Q4 Facebook vs. Google
首先來看看 Facebook 與 Google 在 Q4 的表現：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">Facebook Q3 </th>
    <th align="center">Facebook Q4 </th>
    <th align="center">Facebook Change(%)</th>
    <th align="center">Google Q3</th>
    <th align="center">Google Q4</th>
    <th align="center">Google Change(%)</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">21,470</td>
    <td align="center">28,072</td>
    <td align="center">23.52%</td>
    <td align="center">47,173</td>
    <td align="center">56,898</td>
    <td align="center">17.09%</td>
  </tr>
  <tr>
    <td align="center">廣告業務營收（百萬）</td>
    <td align="center">21,221</td>
    <td align="center">27,187</td>
    <td align="center">21.94%</td>
    <td align="center">43,573</td>
    <td align="center">52,873</td>
    <td align="center">17.59%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">37%</td>
    <td align="center">46%</td>
    <td align="center">19.57%</td>
    <td align="center">24%</td>
    <td align="center">28%</td>
    <td align="center">14.29%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">2.71</td>
    <td align="center">3.88</td>
    <td align="center">30.15%</td>
    <td align="center">16.4</td>
    <td align="center">22.3</td>
    <td align="center">26.46%</td>
  </tr>
</table>

::: tip
在 Google 的財報中，營收來源分為：Google Service, Google Cloud, Other Bets 和 Hedging gains <br/>
Facebook 的營收來源則較為單純，只分為：Advertising 及 Other </br>
上表的 Google 及 Facebook 的廣告業務營收分別取自 Google Service 及 Advertising<br/>
:::
::: tip
Google Service 在 Q3 時佔總營收的比重為 92.37%；Q4 為 92.93%。<br/>
Facebook 的 Advertising 在 Q3 時佔總營收的 98.84%；Q4 為 96.85% <br/>
因此若單純比較二家企業的廣告業務，並不會有太大落差。
:::

在成長力度表現上，Facebook 的各項數值中都領先 Google，但為什麼二者的股價表現差這麼多呢？將財報時間拉長，來看看他們在 2020 全年的表現。

## 2020 Full Year Facebook vs. Google

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">Facebook 2019 </th>
    <th align="center">Facebook 2020 </th>
    <th align="center">Facebook Change(%)</th>
    <th align="center">Google 2019</th>
    <th align="center">Google 2020</th>
    <th align="center">Google Change(%)</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">70,697</td>
    <td align="center">85,965</td>
    <td align="center">17.76%</td>
    <td align="center">161,857</td>
    <td align="center">182,527</td>
    <td align="center">11.32%</td>
  </tr>
  <tr>
    <td align="center">廣告業務營收（百萬）</td>
    <td align="center">69,655</td>
    <td align="center">84,169</td>
    <td align="center">17.24%</td>
    <td align="center">151,825</td>
    <td align="center">168,635</td>
    <td align="center">9.97%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">34%</td>
    <td align="center">48%</td>
    <td align="center">10.53%</td>
    <td align="center">21%</td>
    <td align="center">23%</td>
    <td align="center">6.36%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">6.43</td>
    <td align="center">10.09</td>
    <td align="center">36.27%</td>
    <td align="center">49.16</td>
    <td align="center">58.70</td>
    <td align="center">16.25%</td>
  </tr>
</table>

Facebook 目前市值為 0.76 兆，Google 為 1.42 兆，目前 Facebook 市值約是 Google 的 0.53 倍，而二者市值比率在 Q3 財報公佈時，約是 0.68。顯示出 Google 的市值（股價）成長幅度在這三個月高出 Facebook 很多。

取自：[Facebook & Google 公佈第三季財報，線上廣告的市場是否能持續成長？](https://ycjhuo.gitlab.io/blogs/2020-Q3-Earnings-FB-GOOG.html)。

且不只是三個月，在這六個月中，Facebook 的成長速度一直都落後 Google 很多（近半年 Facebook 股價成長 1.06% vs. Google +39.86%）。

![54-01](../images/54-01.png)

不管從季報或年報的比較來看，Facebook 的成長速度一直都是領先 Google，但從股價表現來說卻不是如此。是因為 Facebook 股價本來就高估，就算公佈財報後，仍無法得到投資人的認同嗎？ 我們接著從本益比的角度來觀察。<br/> 

## 本益比
比較近五年（2016-2020）的本益比資料，可得出下表： 
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">Facebook</th>
    <th align="center">Google</th>
  </tr>
  <tr>
    <td align="center">現在本益比（截至 02/05/2021）</td>
    <td align="center">26.57</td>
    <td align="center">35.58</td>
  </tr>
  <tr>
    <td align="center">平均本益比</td>
    <td align="center">34.07</td>
    <td align="center">33.56</td>
  </tr>
    <tr>
    <td align="center">本益比中位數</td>
    <td align="center">31.87</td>
    <td align="center">29.52</td>
  </tr>
    <tr>
    <td align="center">現在本益比處於歷史百分位</td>
    <td align="center">25%</td>
    <td align="center">75%</td>
  </tr>
</table>

Facebook 的本益比不管是以自己的歷史角度，或是目前跟 Google 對比，都是屬於偏低的階段。說明了目前 Facebook 並沒有股價高估的問題，倒是 Google 目前的本益比，在近五年來是位在 75% 的高位。

若想自己計算目前股價位在歷史本益比的哪個位置，可參考這篇：[如何用 Python 進行統計分析](https://ycjhuo.gitlab.io/blogs/Statistics-In-Python.html)

## 後記
市場願意給 Google 較高的本益比，說明大家普遍看好 Google 未來的成長潛力，畢竟 Google 的業務眾多，相比 Facebook 卻只能依賴線上廣告，但真的是這樣嗎？

::: tip
Google Cloud 營收季成長（Q3 vs. Q4）為 10.1%，而 Facebook 的 other 業務營收季成長（Q3 vs. Q4）為 71.86% <br/>
年成長分別為 31.71% (Google Cloud) vs. 41.98% (Facebook Others)<br/>
:::
雖然目前 Facebook 的 Other 規模仍小於 Google Cloud 很多（年營收約 18 億 vs. 130 億），Facebook 去除線上廣告業務後的營收規模約是 Google Cloud 營收的 14% 左右。<br/>

但 VR 設備（Facebook Oculus）對比雲端平台（Google Cloud）來說，二個市場都有足夠大的成長空間，但 VR 領域目前不像雲端平台領域已經有像 Amazon AWS, Microsoft Azure 這些已佔據先發優勢且資源充足的對手。在 VR 市場 Facebook 仍有機會依靠著本身的通路及資源搶佔 PS 及 HTC 的市場份額。

我本身仍是看好 Facebook 接下來的發展，雖然最近因蘋果新增的隱私權保護計畫吵得沸沸揚揚。但這影響的不只是 Facebook，Google 的線上廣告業務一樣會受到影響。

因此，我認為現在是買入 Facebook 的好時機，在財報公佈的前後，我少量買入了 8 股 Facebook，目前總股數來到 37 股。

## 參考來源
[Facebook Q3 財報](https://investor.fb.com/investor-news/press-release-details/2020/Facebook-Reports-Third-Quarter-2020-Results/default.aspx) <br/>

[Facebook Q4 財報](https://investor.fb.com/investor-news/press-release-details/2021/Facebook-Reports-Fourth-Quarter-and-Full-Year-2020-Results/default.aspx)<br/>

[Google Q3 財報](https://abc.xyz/investor/static/pdf/2020Q3_alphabet_earnings_release.pdf?cache=514fb58)<br/>

[Google Q4 財報](https://abc.xyz/investor/static/pdf/2020Q4_alphabet_earnings_release.pdf?cache=9e991fd)<br/>