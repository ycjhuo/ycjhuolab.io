---
title: 2021 Q2 財報，Facebook vs. Google，誰更具投資價值
description: '美股投資 Facebook Google 比較|Facebook Google 2021 Q2 第二季財報|FB GOOG 哪個好|Facebook vs. Google 線上廣告'
date: 2021-08-14
tags: [Stock, FB, GOOG]
categories: Investment
---

在 Facebook & Google 均公布 2021 Q2 財報後，我們來對比一下，誰才是線上廣告的霸主，以及在現階段，誰更具投資價值

## 獲利能力 Facebook vs. Google

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">FB Q2 </th>
    <th align="center">GOOG Q2</th>
    <th align="center">FB vs. Q1 Chg(%)</th>
    <th align="center">GOOG vs. Q1 Chg(%)</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">29,077</td>
    <td align="center">61,880</td>
    <td align="center">11.1%</td>
    <td align="center">11.87</td>
  </tr>
  <tr>
    <td align="center">廣告業務營收（百萬）</td>
    <td align="center">28,580</td>
    <td align="center">42,847</td>
    <td align="center">12.35%</td>
    <td align="center">13.1%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">12,513</td>
    <td align="center">21,985</td>
    <td align="center">8.78%</td>
    <td align="center">3.3%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">81.43%</td>
    <td align="center">68.71%</td>
    <td align="center">12.96%</td>
    <td align="center">9.37%</td>
  </tr>
  <tr>
    <td align="center">營業利潤率</td>
    <td align="center">42.53%</td>
    <td align="center">31.29%</td>
    <td align="center">-0.94%</td>
    <td align="center">1.57%</td>
  </tr>
  <tr>
    <td align="center">淨利率</td>
    <td align="center">35.75%</td>
    <td align="center">29.94%</td>
    <td align="center">-0.54%</td>
    <td align="center">-2.48%</td>
  </tr>
  <tr>
    <td align="center">Dilited EPS</td>
    <td align="center">3.61</td>
    <td align="center">27.26</td>
    <td align="center">9.39%</td>
    <td align="center">-6.93</td>
  </tr>
</table>

- 總營收：Google 營收規模為 Facebook 的 212.81%
- 廣告業務營收：Google 為 Facebook 的 149.92%
- 稅前淨利：Google 為 Facebook 的 175.7 %
- 毛利率：Google 因為業務涵蓋範圍較廣，毛利率不如專注於廣告業務的 Facebook 來的高，約相差 12.72%
- 營利率：Google 與 Facebook 約相差 11.24%
- 淨利率：Google 與 Facebook 約相差 5.81%

從上表可看出，Google 雖然營收是 Facebook 的 2 倍以上，但各項業務貢獻度較為平均，廣告業務僅佔總營收的 69% </br>
相反的，Facebook 的廣告營收佔了總營收的 98%，代表目前發展的 VR 及其它業務僅佔了 2% </br>

也因為業務集中的關係，Facebook 在各項利潤率上，均領先 Google </br>
但可以看出在淨利率上二者的差距逐漸減少，表示 Google 相比 Facebook 有著較多的業外收入 </br>

## 庫藏股 Facebook vs. Google

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">FB Q2 </th>
    <th align="center">GOOG Q2</th>
    <th align="center">FB vs. Q1 Chg(%)</th>
    <th align="center">GOOG vs. Q1 Chg(%)</th>
  </tr>
  <tr>
    <td align="center">回購金額（百萬）</td>
    <td align="center">7,079</td>
    <td align="center">12,796</td>
    <td align="center">11.1%</td>
    <td align="center">11.87%</td>
  </tr>
  <tr>
    <td align="center">股價變化（%）</td>
    <td align="center">21.60%</td>
    <td align="center">18.59%</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

近年來，Facebook 與 Google 都不約而同的實施了庫藏股的政策，將多餘的現金用於改善公司體質以及拉抬股價 </br>

Google 在 Q2 的回購金額，約為 Facebook 的 1.8 倍，但股價上漲並不如 Facebook 來得多 </br>
部分原因為：Google 的回購金額會同時用在購買 GOOG 與 GOOGL 上，而 Facebook 的回購金額則是完全用在購買 FB 上，因此，上漲幅度會較為明顯 </br>

它們在 Q2 的回購金額都比 Q1 多出 11% 左右，但 Google 股價在 Q1 時上漲了 19.69%（Facebook 則是上漲了 9.5%），可以看出並不是回購金額越高，股價就漲的越多

## 後記
雖然 Facebook 已是社交領域的龍頭，也在這個領域上靠著線上廣告，讓公司成長到現在的規模（S&P 500 指數中權重排行第 4），但跟稱霸搜尋領域的 Google 相比下還是小了許多 </br>
僅以廣告業務的營收來比，Facebook 僅是 Google 的 67% </br>

一般來說，規模越小的企業，成長的速度就越快，但目前 Google 除了搜尋領域外，Youtube，Cloud 這些業務，也有季成長約 15% 的速度，且目前已佔了總營收約 7 - 11% 的比重 </br>
相比 98% 營收都是線上廣告的 Facebook，Google 的獲利能力更為多樣，目前也更受到投資人的青睞，GOOG 本益比為 27.74，FB 為 26.93（截至 8/14 ）</br>

相較之下，Facebook 雖然全力發展 VR，短語音以及 Facebook Pay，但目前都還沒成長到一定的市佔率，在獲利引擎只有一部的情況下，要維持高成長性，以及描繪給投資人未來的藍圖，也較為困難


延伸閱讀： </br>
[Facebook（FB）2021 Q2 季報，EPS 同期成長 100%](https://ycjhuo.gitlab.io/blogs/FB-Earnings-2021-Q2.html) </br>
[Google（GOOG）2021 Q2 季報，近一年來表現最好的科技巨頭](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q2.html) </br>