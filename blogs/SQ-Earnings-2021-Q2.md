---
title: Square（SQ）公佈 2021 Q2 財報，股價直接上漲 10%
description: 'Square SQ 2021 Q2 第二季財報|美股 Square SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報'
date: 2021-08-20
tags: [Stock, SQ]
categories: Investment
---

Square（股票代號：SQ），從 2021 Q1 財報發布（05/06），到 2021 Q2 財報發布（08/01）期間，股價上漲了 10.4% （ $223.96 → $247.26 ）</br>
而在 2021 Q2 財報公佈後，隔天股價更是直接跳升 10.16%，讓我們從 Square 2021 第二季財報中分析大漲的原因

複習 Square 2021 Q1 財報：</br>
[Square（SQ）公佈 2021 Q1 財報，比特幣收入暴增一倍](https://ycjhuo.gitlab.io/blogs/SQ-Earnings-2021-Q1.html)

## 財務指標
[來源](https://s27.q4cdn.com/311240100/files/doc_financials/2021/q2/Q2-2021-Shareholder-Letter.pdf)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">QoQ</th>
    <th align="center">QoQ (Previous)</th>
  </tr>
  <tr>
    <td align="center">營收（億）</td>
    <td align="center">50.58 億</td>
    <td align="center">46.81 億</td>
    <td align="center">-7.45%</td>
    <td align="center">60.09%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">19.05%</td>
    <td align="center">24.38%</td>
    <td align="center">5.33%</td>
    <td align="center">-6.39%</td>
    
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">1.34%</td>
    <td align="center">2.67%</td>
    <td align="center">1.33%</td>
    <td align="center">-0.09%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.08</td>
    <td align="center">0.4</td>
    <td align="center">400%</td>
    <td align="center">-86.15%</td>
  </tr>
</table>

- 營收：Square 這次的季成長為 -7.45%（前一次的季成長為 60.09%），主要是受到 Bitcoin 營收下降 22.41% 的影響
- 毛利率：因利潤最低的 Bitcoin 營收下滑，讓 Square 的毛利率回升到 24.38%（前四季平均為 25.43%）
- 營業利潤率：創下了歷史新高的 2.67%（前四季平均為 0.8%）
- EPS：也創下了歷史新高的 0.4，季成長 400%（前四季平均為 0.18）

## 營收來源
Square 的收入可分為四個項目：
<table style="width:100%">
  <tr>
    <th align="center">收入組成</th>
    <th align="center">營收佔比</th>
    <th align="center">QoQ</th>
    <th align="center">QoQ (Previous)</th>
    <th align="center">毛利率 (2021 Q2)</th>
    <th align="center">毛利率 (vs. 2021 Q1)</th>
  </tr>
  <tr>
    <td align="center">交易</td>
    <td align="center">18.98%</td>
    <td align="center">27.9%</td>
    <td align="center">-10.43%</td>
    <td align="center">44.21%</td>
    <td align="center">-0.9%</td>
  </tr>
  <tr>
    <td align="center">訂閱</td>
    <td align="center">11.03%</td>
    <td align="center">22.86%</td>
    <td align="center">-3.2%</td>
    <td align="center">81.94%</td>
    <td align="center">-1.85%</td>
  </tr>
    <tr>
    <td align="center">硬體</td>
    <td align="center">0.57%</td>
    <td align="center">51.89%</td>
    <td align="center">-0.2%</td>
    <td align="center">-40.43%</td>
    <td align="center">0.19%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin</td>
    <td align="center">69.43%</td>
    <td align="center">-22.41%</td>
    <td align="center">13.84%</td>
    <td align="center">2.01%</td>
    <td align="center">-0.13%</td>
  </tr>
</table>

本季除了 Bitcoin 收入大幅減少 22.41% 之外，其他三項收入均實現了雙位數的成長 </br>
相比 2021 Q1 的季成長，除了 Bitcoin 收入增加 13.84%，其餘收入均呈現負成長的狀態，是否表示 Bitcoin 熱潮已逐漸衰退呢？

:::tip
若以 Square 2021 Q1 與 Q2 財報這段區間（05/26 - 08/01）來看 </br>
Bitcoin 上漲了 12%（ $35,678.13 → $39,974.89），成交量則衰退了 15.67%（31,646,080,921 → 26,688,438,115）
:::

## Seller vs. Cash App ecosystem
接著我們將 Square 分為 B2B 業務（Seller）以及 B2C 業務（Cash App），來比較哪個業務對於 Square 的獲利更為重要 </br>
（假設 Seller 跟 Cash App 在同樣項目的毛利率一樣）

<table style="width:100%">
  <tr>
    <th align="center">淨利佔比</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q1</th>
    <th align="center">Change（%）</th>
  </tr>
  <tr>
    <td align="center">交易淨利（Seller）</td>
    <td align="center">44%</td>
    <td align="center">41%</td>
    <td align="center">3%</td>
  </tr>
  <tr>
    <td align="center">訂閱淨利（Seller）</td>
    <td align="center">11%</td>
    <td align="center">11%</td>
    <td align="center">--</td>
  </tr>
  <tr>
    <td align="center">硬體淨利（Seller）</td>
    <td align="center">-2%</td>
    <td align="center">-1%</td>
    <td align="center">-1%</td>
  </tr>
  <tr>
    <td align="center">交易淨利（Cash App）</td>
    <td align="center">4%</td>
    <td align="center">4%</td>
    <td align="center">--</td>
  </tr>
  <tr>
    <td align="center">訂閱淨利（Cash App）</td>
    <td align="center">36%</td>
    <td align="center">38%</td>
    <td align="center">-2%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin 淨利（Cash App）</td>
    <td align="center">5%</td>
    <td align="center">8%</td>
    <td align="center">-3%</td>
  </tr>
</table>

大部分的業務淨利相比前一次的季成長都不變，而 Bitcoin 收入的下滑則由 Seller 的交易業務補上 </br>
而硬體淨利（刷卡機銷售）一直以來都是負的，可推算出：</br>
因刷卡機銷售的數量變多，導致硬體虧損變大，但隨著越來越多店家導入刷卡機，Square 可以收的交易手續費收入也就變高了

## 後記
Square 雖然被稱作為比特幣概念股，但實際上最賺錢的業務還是在於跟商家收取的交易手續費＆相關的軟體訂閱服務，毛利分別為 45% 與 84%，而 Bitcoin 的毛利僅為 2.13% </br>
隨著美國疫情逐漸趨緩，餐廳紛紛恢復營業後，也幫助 Square 的交易營收實現了季成長 27.9%，這也是本季 Square 的 EPS 能達到季成長 400% 的最大功臣 </br>

Square 雖然在2021 Q2 財報上表現良好，但在這段時間，股價起伏很大，最低曾跌到 $197.13，後來才又回升到 $247.26 </br>
在 Q1 財報發佈後即開始下跌（距離最低點約為 -12%），隨後在 Q2 財報發布前，股價上漲了約 25% </br>

而我也因為在分析完[ Q1 財報](https://ycjhuo.gitlab.io/blogs/SQ-Earnings-2021-Q1.html)後，認為 Square 能隨著經濟復甦而在 Q2 財報有著不錯的表現，趁機加碼了一些 Square，而幸運跟到這波漲幅</br>
我目前跟 Square 的管理層一樣，仍是持續看好 Square 下半年的發展

![SQ 2021-Q2 交易紀錄](../images/SQ-Transactions-In-2021-Q2.png)

看更多[財報分析](https://ycjhuo.gitlab.io/tag/Stock/)</br>
