---
title: 開箱！羚羊峽谷（Antelope Canyon）＆馬蹄灣（Horseshoe Bend）拉斯維加斯 當天來回
description: '羚羊峽谷介紹|羚羊峽谷推薦|拉斯維加斯 羚羊峽谷|拉斯維加斯 必去|拉斯維加斯 景點|拉斯維加斯 推薦|馬蹄灣 介紹|馬蹄灣 推薦|Antelope Canyon 介紹|Antelope Canyon 推薦|Horseshoe Bend 介紹|Horseshoe Bend 推薦|Las Vegas Horseshoe Bend|Las Vegas Antelope Canyon|Horseshoe Bend Antelope Canyon'
date: 2021-10-03
tags: [Las Vegas, Arizona]
categories: Travel
---

2014 年，以世界上最高價賣出的照片（Phantom by Peter Lik，以 650 萬美元）拍攝地：羚羊峽谷（Antelope Canyon），位於美國中西部的亞利桑那州（Arizona）
![Phantom by Peter Lik](../images/117-01.png) 一直是攝影愛好者中的必去之地，雖然位於亞利桑那州，但其實若是跟團的話，旅行團大都從拉斯維加斯（Las Vegas）出發，下面就來介紹開箱這次的行程吧

## 羚羊峽谷旅行團
這次我們參加的是 Get Your Guide 網站上的羚羊峽谷＋馬蹄灣的行程（Las Vegas: Antelope Canyon & Horseshoe Bend Tour with Pickup）</br>
連結在此：[Get Your Guide](https://www.getyourguide.com/las-vegas-l58/antelope-canyon-and-horseshoe-bend-tour-from-las-vegas-t169954/?utm_source=getyourguide&utm_medium=sharing&utm_campaign=activity_details&visitor_id=LIQMMS3HSYZST4AK8F33SLT674NW97XG) </br>

之所以會找到這個網站純粹是之前在找羚羊峽谷的資料時，邊滑 Facebook，就出現這家旅行社的廣告了，而點進這個行程後，看了它的評價（4.8 顆星，390 評價），參加過的人看起來都很滿意，就訂了 </br>

## 羚羊峽谷行程介紹
Get Your Guide 其實只是一個旅遊仲介網站，真正帶我們去的會是當地跟他們配合的旅行社，而這個團最大的好處是，他們會到 Vegas 大道上的酒店接你，然後就直接開始行程 </br>

這一日來回的行程就是：Vegas 酒店接送（05:00 - 05:30） -> 停 3 個休息站 -> 馬蹄灣（11:00 - 12:00） -> 午餐（Subway）（12:30 - 13:00）-> 羚羊谷（13:30 - 15:30） -> 停 3 個休息站 -> 回飯店（19:30 - 20:30） </br>

整個行程約 15 小時，司機會在出發的前一天詢問我們是住在 Vegas 大道上（Strip）的哪個飯店，依順序來接送，若住的飯店不在 Strip，就得自己先到 Strip 上的其中一家等候才可 </br>

當天的交通工具是一台 15 人座的小巴，我們當天參加的人只有 8 個人，除了一對情侶外，其他都是一個女生就自己就來參加的（可見羚羊峽谷在女孩中的熱門度有多高）</br>

![羚羊峽谷（Antelope Canyon）交通工具](../images/117-02.jpg)

整個行程就我們 8 位旅客，加上一位司機安東尼（Anthony），可以看出安東尼是一位很專業的導遊 </br>
除了會講解路上經過的一些景點外，最重要的是他每隔 1 - 1.5 小時就會停麥當勞 / 超市 / 加油站讓我們可以採買及上廁所 </br>
而他也會在每個休息站買一袋冰塊放在後車廂的小冰櫃，裡面冰著瓶裝水、可樂、能量飲料給我們隨便取用，更貼心準備三個車充給旅客充手機（Andorid & iPhone 的線都有！）

## 馬蹄灣（Horseshoe Bend）
在開了 5 個小時後，我們到了第一個景點：馬蹄灣（Horseshoe Bend），可俯瞰科羅拉多河（Colorado River）被切割成馬蹄鐵造型而聞名的景點 </br>
![馬蹄灣（Horseshoe Bend）](../images/117-03.jpg)

這個景點，司機會在園區停一個小時，讓我們自己走的 15-20 分鐘的步道到可以俯瞰馬蹄灣的地方，一小時後在自己原路返回到停車場集合 </br>

參觀完馬蹄灣後，司機就會帶我們到大概半小時車程的 Subway 吃午餐，由於這個 Tour 是有包含午餐的，所以安東尼就跟我們說，我們可以隨便點自己想吃的，他最後再幫我們買單，但其實在 33 度的高溫下，大家也都吃不太下，吃完後就繼續出發到羚羊峽谷了

接下篇：[開箱！羚羊峽谷Ｘ（Antelope Canyon X）](https://ycjhuo.gitlab.io/blogs/Tour-Vegas-Antelope-X.html)