---
title: 如何讓 Python 讀取外部 ini 設定檔
description: 'Python 讀取 INI 檔 | Pyhton ini 教學 | Python 讀取網路磁碟 | Pyhton 存取網路資料夾'
date: 2021-07-22
tags: [Python, Sendmail, ini]
categories: Programming
---

有些時候，我們會遇到須頻繁更動程式內某些數值，而這時若將這些數值寫死在程式中，在程式已經打包的情況下，每更新一次程式就要重新打包 </br>

這時，如果程式能藉由讀取外部檔案的數值，取得它所需要元素，我們就不須一直重複打包，而只要修改外部檔案即可 </br>

常見的設定檔有很多，這篇就用 Windows 系統上，最常見的 ini 檔案來舉例，介紹如何用 Pyhton 的 ```configparser``` 直接讀取 ini 檔案  </br>

下面這篇算是最常見的案例：[如何用 Python 寄送 HTML 郵件（Email）](https://ycjhuo.gitlab.io/blogs/Python-How-To-Send-HTML-Mails.html) </br>
若將收件者的信箱寫死在程式內的話，若之後人員有變更，我們就可以直接透過修改 ini 檔的方式來更新收件者，而不須動到程式 </br>

---

## ini 檔
ini 檔分為二個區塊，用 [ ] 包起來的是 section，而下方的黃色部分則是變數（在 ini 檔中稱作 key）</br>
可以看到在這個 ini 檔，有二個 section，一個是 File 區塊，另一個是 Mail 區塊 </br>
section 跟 key 都是由我們自己的意思設定的

```ini
[File]
# The path for placing FFCDB files
Path = \\8.8.8.8\d$\Public_Data\Private\02.B&C\Daily FFCDB

[Mail]
Sender =  NYB.IT.System@megaicbc.com
Receiver = NYB.IT@megaicbc.com
Cc = NYB.IT.System@megaicbc.com, y.c.jhuo@megaicbc.com
```

---

## 讀取 ini 檔中的設定值
下面是我們原本在程式中寫的設定：
```python
sender = "sender@XXX.com"
receiver = "receiver@XXX.com"
cc = ["cc@XXX.com"]
```


而這是透過讀取 ini 檔的設定：
```python
import configparser

config = configparser.ConfigParser()
config.read('config.ini', encoding='utf-8')
sender = config['Mail']['Sender']
receiver = config['Mail']['Receiver']
cc = config['Mail']['Cc']
# Convert string to list for sending mail
cc = list(cc.split(','))
```

可以看到在讀取 ini 檔後（```config.read('config.ini', encoding='utf-8')```） </br>
要同時填上 section 跟 key 值才能讓程式讀到我們所須的資料

::: tip
因為 ini 檔裡面的值都是字串，因此若有轉型的需求，則須在程式內進行
:::

## 在 ini 檔寫入分享資料夾/網路磁碟機（shared folder / network location）的路徑

若要程式取得非本機端（localhost）的檔案，也可以將路徑直接寫在 ini 檔中</br>

如下面這個 Path 就是到 8.8.8.8 的 server 中的 D 槽下找到所指定的資料夾

```ini
[File]
# The path for placing FFCDB files
Path = \\8.8.8.8\d$\Public_Data\Private\02.B&C\Daily FFCDB
```

若程式須進行打包處理（```Pyinstaller```），或是 ini 檔裡面有中文字的話，可參考這篇：</br>
[如何讓 Python 在打包後讀取外部 ini 設定檔（含有中文字）](https://ycjhuo.gitlab.io/blogs/Python-Pyinstaller-Read-Ini-File-Chinese.html)