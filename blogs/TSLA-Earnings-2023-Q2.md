---
title: 特斯拉（TSLA）2023 Q2 季報｜Dojo 潛力如何，帶動股價大漲 60%
description: '特斯拉 TSLA 財報|特斯拉 TSLA 2023 Q2 第二季財報|特斯拉 TSLA 生產量 交車量 2023 Q2|特斯拉 2023 大降價|'
date: 2023-09-21
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 7/19（三）公佈了 2023 的第二季財報 </br>
在 Q1 - Q2 的財報期間（4/19 - 7/19），特斯拉的股價從 $180.59 → $291.26，大漲 61%（同期 S&P 500 上漲 10%）

截至今日（9/21），特斯拉今年的股價上漲了 137%，對比 S&P 500 的 13%，領先大盤 10 倍

在上季財報，特斯拉犧牲了毛利＆利潤，維持住營收＆市占率，進而帶動股價成長（上漲 25%）</br>
這季財報是否有將毛利＆利潤拉上來，或是繼續降價促銷呢？下面來看看特斯拉 2023 Q2 財報

## 特斯拉 2023 Q2 財報
[來源](https://digitalassets.tesla.com/tesla-contents/image/upload/IR/TSLA-Q2-2023-Update.pdf)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q1</th>
    <th align="center">2023 Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">Q1 vs. Q4</th>
    <th align="center">Q2 vs. Q1</th>
  </tr>
  <tr>
    <td align="center">電動車營收</td>
    <td align="center">199.6 億</td>
    <td align="center">212.7 億</td>
    <td align="center">12%</td>
    <td align="center">-7%</td>
    <td align="center">6%</td>
  </tr>
  <tr>
    <td align="center">總營收</td>
    <td align="center">233.3 億</td>
    <td align="center">249.3 億</td>
    <td align="center">12%</td>
    <td align="center">-4%</td>
    <td align="center">6%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">19%</td>
    <td align="center">18%</td>
    <td align="center">-1%</td>
    <td align="center">-4%</td>
    <td align="center">-1%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">11%</td>
    <td align="center">10%</td>
    <td align="center">-1%</td>
    <td align="center">-5%</td>
    <td align="center">-2%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.73</td>
    <td align="center">0.78</td>
    <td align="center">11%</td>
    <td align="center">-47%</td>
    <td align="center">6%</td>
  </tr>
</table>

- 電動車營收：本季電動車營收約 213 億，季成長為 6%，回升到與去年 Q4 相同的水準
- 總營收：本季總營收為 249 億，季成長 6%，而前二季度的季成長分別為 21% & 12%（電動車佔了總營收的 86%）
- 毛利率：本季毛利率較上季降低 1%，為 18%，已連續二季低於 20%（2022 平均毛利率為 29%）
- 營業利益率：本季營利率較上季降低 1%，為 10%，已連續三季減少（2022 平均營利率為 17%）
- EPS：本季 EPS 為 0.78，季增 6%

本季由於電動車營收比上季增加了 6%，帶動總營收也季增 6%，在毛利率與上季差不多的狀況下，推動本季 EPS 較上季成長 6%

為了保持營收的成長性，本季延續了上季的降價策略，犧牲了利潤，導致毛利率＆營利率繼續降低

所幸，跟上季相比，本季下降的幅度已逐漸縮小，顯示特斯拉並沒有因過度追求市佔率而不斷降價促銷

## 財報重點
本季營益利益率下降的因素，除了降價促銷外，還有因升級款的 4680 電池產生的相關費用， 以及 Cybertrunk & AI 的支出

以下為各車廠近況：
- 德州：除了原有的 Model Y 外，也正進行 Cybertrunk 的生產設備安裝，並期望在今年開始交付車輛
- 上海：作為主要的出口中心，目前產量已接近滿載（年產能 75 萬輛）
- 柏林：在本季開始生產「標準版」的 Model Y，年產能將從 35 萬輛略增至 37.5 萬輛

目前特斯拉的服務＆其他業務的毛利率在 8% 左右，隨著福特、豐田、賓士等車廠相繼加入特斯拉的充電協定（NACS）後，將可再提升這部分的毛利率

## 後記
特斯拉在今年的股價勢如破竹，至今已上漲了 136%，而最主要的漲幅就在這一財報季度

本季除了生產/交車量持續成長外，最大亮點就是 Dojo AI 運算，在財報中預估 Dojo 將在 2024/02 時，成為全球前五強的超級電腦

由於特斯拉本身擁有的海量駕駛資料，讓用來計算這些資料，發展自動駕駛，特斯拉內部開發的運算工具 Dojo 應運而生

聽起來就如同 Amazon 當初發展 AWS 時的狀況，先是為了解決公司內部的問題，後來發展出成功的商業模式，現在更成為了 Amazon 的主要獲利來源

也就是這個原因，讓投資人重新審視特斯拉除了電動車＆自動駕駛之外，另一個會在未來創造出巨大價值的業務之一，促成特斯拉近期股價大漲的原因

::: warning
點擊觀看更多： </br>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::