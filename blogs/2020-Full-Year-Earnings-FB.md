---
title: 公佈第四季及 2020 全年財報，在企業縮減開支的一年下，Facebook 表現如何？
description: '美股投資 Facebook FB 2020 財報分析|Facebook 疫情表現如何|社交網站龍頭'
date: 2021-01-29 01:29:52
tags: [Stock, FB]
categories: Investment
---

Facebook 在本週三（01/27）的盤後公佈了 2020 的第四季度財報及 2020 的全年財報。首先來複習一下 Facebook 在上個季度（2020 Q3）表現如何：<br/>
以下資料來自 [Facebook & Google 公佈第三季財報，線上廣告的市場是否能持續成長？](https://ycjhuo.gitlab.io/blogs/2020-Q3-Earnings-FB-GOOG.html)

::: tip
營收：與去年同期相比，成長為 22%；去年的年成長為 28% <br/>
營業利潤率：與去年同期相比，從 41% 下滑到 37% <br/>
EPS：年成長 28%；去年同期則是 20% <br/>
日活用戶（ daily active users ）及月活用戶（ monthly active users ）均為 12% 的年成長 <br/>
:::

可以看出，除了利潤率小幅下降之外，其餘指標都有二位數的成長。接著我們來看看在第四季是否能保持這個成長。

## Facebook Q4 季報 及全年財報
資料取自：[Facebook Q4 季報 及全年財報](https://investor.fb.com/investor-news/press-release-details/2021/Facebook-Reports-Fourth-Quarter-and-Full-Year-2020-Results/default.aspx)

### 財務指標

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">與 2019 Q4 比較</th>
    <th align="center">與 2019 整年比較</th>
  </tr>
  <tr>
    <td align="center">營收成長率</td>
    <td align="center">+33%</td>
    <td align="center">+22%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率增長率</td>
    <td align="center">+4%（46%）</td>
    <td align="center">+4%（38%）</td>
  </tr>
  <tr>
    <td align="center">EPS 成長率</td>
    <td align="center">+52%</td>
    <td align="center">+57%</td>
  </tr>
</table>

上表可以看出，儘管在 2020 各企業都大幅縮減廣告開銷的情況下，但 Facebook 在營收增速上與去年相比仍有 22% 的成長（去年增速為 28%）。且營業利潤率與去年相比，也小幅上升了 4%，表示公司並沒有因為營收成長而犧牲了利潤。
</br>

### 用戶活動數
在用戶方面，雖然因為隱私問題，讓不少人紛紛棄用 Facebook，但相比去年仍能維持 2 位數的成長。</br>
- Facebook 每日活動用戶 DAUs（daily active users）年成長為 11%，人數為 18.4 億 </br>
- 整個家族（包含 Facebook, Instagram, What's app）年成長則是 15%，人數為 26 億

- 月活用戶相比去年，成長了 12%，人數為 28 億</br>
- 家族的月活用戶則是成長了 14% ，人數為 33 億

## 股票回購
在這次財報中，Facebook 宣布了要進行最多 250 億的股票回購。但回購真的能有效帶動股價嗎？</br>
先前 Facebook  也說過要回購 340 億股票，直到 2020 年底，之前的回購現金剩下 86 億。表示在這段時間，公司已經用了 254 億進行回購。

讓我們來看看回購的效果如何：</br>
若是取去年的年中來看，07/06 時股價是 $245.07，到了 12/28 時股價為 273.16，成長了 11.46%。看起來好像很不錯？</br>
接著我們來對比以科技股為主的納斯達克（Nasdaq）指數從 10617.44 成長到 12888.28，共上漲了 21.39%。

::: tip 
想了解美股指數的可參考這篇：[追蹤美股三大市場的指數型 ETF](https://ycjhuo.gitlab.io/blogs/US-Stock-Markets-Index-ETF.html)</br>
:::

可以看出，儘管有回購的加持，但 Facebook 的表現仍遠低於大盤，漲幅比率大概只有大盤的 53.6%。而這次宣布的回購金額，相比上次並沒有太大差別。

若我們將回購的時間縮短為三個月的話，從去年的 08/31 開始，表現又如何呢？</br>
Facebook 在 08/31 的股價為 282.73，與年底時股價相比，下跌了 3.38%。而納斯達克在同樣期間內則是成長了 13.92%。

但我們若來看一下上面財務指標這份表，EPS 成長率相比去年，成長了 57％。這可是四年前才有的成長率。</br>
表示了股票回購在提升 EPS 還是有著一定的功效。
::: tip
2017 年成長為 54.44%，2018 年成長為 40.45%，2019 年成長為 15.06%
:::

## 後記
雖然 Facebook 在去年的報酬率跑輸了納斯達克（18.71% vs 43.79%），近五年的報酬率也輸了（136.16% vs 189.06%）。但 Facebook 作為一家盈利能力強大的企業（營業利潤率 38%），且身為社交龍頭（具有 Facebook, Instagram, What's App）三種產品，掌握了絕大多數的流量入口。若能從中找到貨幣化的新方式，即可讓營收再度成長。

且因 Facebook 目前仍被隱私及拆分等問題纏繞，在 2020 年度財報出來後，本益比目前從原先的 32 降到了 26 左右（疫情最嚴重時約為 23.5，相比目前股價約有 80% 的漲幅），我認為仍是個可以投資的價位。