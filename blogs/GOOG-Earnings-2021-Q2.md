---
title: Google（GOOG）2021 Q2 季報，近一年來表現最好的科技巨頭
description: '美股投資 Google GOOG|Google GOOG 介紹|Google GOOG 2021 Q2 第二季財報分析'
date: 2021-08-09
tags: [Stock, GOOG]
categories: Investment
---

搜尋龍頭 Google（GOOG）在 07/27 公佈了 2021 第二季的財報，從 2021 Q1 季報公佈後（04/27 盤後），到公佈 Q2 季報（07/27 盤後），三個月的時間，股價從 2307.12 → 2735.93，上漲了 18.6% </br>

同期間，納斯達克指數（Nasdaq）上漲了 5.1%

---

Facebook 2021 Q1 財報回顧：[Google（GOOG）公布 2021 Q1 季報，近半年中表現最好的權值股](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q1.html) </br>

## Google 2021 Q2 財報
[來源](https://abc.xyz/investor/static/pdf/2021Q2_alphabet_earnings_release.pdf?cache=4db52a1)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q1 vs. 2020 Q4</th>
    <th align="center">2022 Q2 vs. 2022 Q1</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">55,314</td>
    <td align="center">61,880</td>
    <td align="center">-2.78%</td>
    <td align="center">11.87%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">21,283</td>
    <td align="center">21,985</td>
    <td align="center">13.88%</td>
    <td align="center">3.3%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">70.28%</td>
    <td align="center">68.71%</td>
    <td align="center">-2.21%</td>
    <td align="center">-1.57%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">29.72%</td>
    <td align="center">31.29%</td>
    <td align="center">2.21%</td>
    <td align="center">1.57%</td>
  </tr>
    <tr>
    <td align="center">Diluted EPS</td>
    <td align="center">29.29</td>
    <td align="center">27.26</td>
    <td align="center">31.35%</td>
    <td align="center">-6.93%</td>
  </tr>
</table>

- 營收：Google 的營收不像 Facebook 一樣，有著明顯的季節性 ; 本季（Q2）相比 Q1 增加了 11.87%
- 稅前淨利：相比上季的雙位數成長（13.88%），本季僅上漲了 3.3% ; 同時，營業收入上漲了 17.79%（上季成長率為 5.02%）
- 毛利率＆營益率：毛利率從 2020 Q3 的 75.7%，下降到本季的 68.71%  ; 但由於成本管控得宜，以及將設備的折舊年限增加二年，營益率從 2020 Q3 的 24% 增加到本季的 31%
- EPS：這一季 EPS 增加了 3.98%，相比 Q1 的 18.15%，成長速度下降許多

## 營收來源
Google 在本季營收正式突破 600 億（約 619 億，上季為 553 億），而也是以線上廣告為主的 Facebook，本季營收為 291 億 </br>
在營收已經這麼高的情形下，Google 營收的成長率仍能與營收僅約 Google 40% 的 Facebook 差不多（Google 為 11.87%，Facebook 11.1%）</br>
若將營收細分來看，分為以下三類：
- Google Service：包含線上廣告，Google Search，Android，YouTube 等我們常用的 Google 服務 ; 相比 Q1，成長了 11.51%
- Google Cloud：以 Google 雲端平台，以及提供給企業用的相關服務 ; 相比 Q1，成長了 14.36%
- Other Bets：其他收入來源，僅占總營收的 0.3%，占比太小，可忽略不看

若再將 Google Service 細分，成長速度最快的是 YouTube 廣告（與 Q1 相比，成長了16.6%），約佔總營收的 13% 左右 </br>

而成長速度第二快的是：Google Cloud 從 2020 Q3 以來，皆占總營收 7% 左右（與 Q1 相比，成長了14.36%），但這項業務目前仍在虧損中，預計 Q4 可轉虧為盈 </br>

## 庫藏股
在本次財報中，Google 提及了在 Q1 宣布的 500 億的股票回購計劃，原本只能用於回購 Class C 股票（代號：GOOG），現在被修改為也可用於回購 Class A 股票（代號：GOOGL）</br>

而本季回購的金額相比 Q1，增加了 12.29%，以下為這期間的股價表現，可看出將一部分資金用於回購 GOOGL，不太影響 GOOG 的上漲幅度 </br>

- 2021 Q1：Google 股價上漲 19.69%，S&P 500 上漲 9.42%，回購金額 114 億
- 2021 Q2：Google 股價上漲 18.56%，S&P 500 上漲 5.13%，回購金額 128 億

## 後記
Google 不論在近一個月，近一年，及今年的股價表現均在科技五巨頭（蘋果，微軟，亞馬遜，Facebook）中排行第一 </br>
近一年的表現（+83.19%）上更是遙遙領先排名第二的微軟（+39%）二倍以上 </br>

而 Google 目前的本益比（27.46）在五巨頭中也是偏低（蘋果 28.62，微軟 35.96，亞馬遜 58.3，Facebook 26.95）</br>

![FAANG 近一年走勢圖](../images/105-01.png) <br/>

我們取 Google 近五年的本益比資料（2017 - 2021），目前的本益比 27.46，剛好就是近五年的中位數（平均數為 33.72），表示雖然股價在近一年上漲了許多，但就本益比的角度來看，仍是屬於合理的價位 </br>

今年以來，我多次買入 FB，想說就成長性來看，規模較小的 Facebook 應該會有較大的漲幅 ; 且在疫情經濟下，大家減少了外出的頻率，進而增加了使用手機的時間，而 YouTube 僅佔 Google 營收的一部分，但 Facebook 卻是 FB 的主要營收來源 </br>

乍看之下，Facebook 應該是較好的選擇，現在就股價的漲幅來看，卻不是如此，或許將股價表現從近一年再拉長到三年，這個差距應該會逐漸縮小 </br>

延伸閱讀： </br>
[Facebook（FB）2021 Q2 季報，EPS 同期成長 100%](https://ycjhuo.gitlab.io/blogs/FB-Earnings-2021-Q2.html) </br>