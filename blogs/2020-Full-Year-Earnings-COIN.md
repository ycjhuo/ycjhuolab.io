---
title: Coinbase 第一家上市的加密貨幣交易所，2020 財報分析
description: '美股投資 Coinbase COIN 介紹|Coinbase 2020 財報分析|第一家上市的加密貨幣 Crypto 交易所|比特幣 Bitcoin 概念股|加密貨幣 Crypto 投資'
date: 2021-04-18 22:42:27
tags: [Stock, COIN]
categories: Investment
---
Coinbase，成立於 2012 年，是美國最大的加密貨幣交易所，於上週三（04/14）在納斯達克上市，為首家上市（美國）的加密貨幣交易所 （代號：COIN）</br>

上市首日，開盤價為 $381，當日最高點為 $429.54（上漲 12.74%）；截至上週五（04/16）收盤價為 $342（與開盤價相比，下跌了 10.24%）</br>

讓我們從下面的財報中，看看 Coinbase 是否為一個值得投資的公司

## Coinbase Q4 季報 及全年財報
資料取自：[SEC Coinbase](https://www.sec.gov/Archives/edgar/data/1679788/000162828021003168/coinbaseglobalincs-1.htm)

### 財務指標
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
    <th align="center">Growth(%)(Q4 vs. Q3 / 2020 vs. 2019)</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">5.85 億 vs. 3.15 億</td>
    <td align="center">12.78 億 vs. 5.3 億</td>
    <td align="center">85.54% / 139.35%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">84.96% vs. 90.9%</td>
    <td align="center">89.33% vs. 90.48%</td>
    <td align="center">-5.94% / -1.16%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">38.73% vs. 32.19%</td>
    <td align="center">32.01% vs. -8.58%</td>
    <td align="center">6.54% vs. 40.59%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center"></td>
    <td align="center">1.58 vs. -0.5</td>
    <td align="center"></td>
  </tr>
</table>

- 營收方面：不管跟去年或是上季相比，成長幅度都相當驚人
- 毛利率除了在 2020 Q4 稍微降低（84.96%），其餘期間都維持在 90% 左右
- 營業利益率在 2020 為 32.01%；Q4 為 38.73%；2019 則是 -8.58%
    - 2020 營業費用在各項的比例跟 2019 差異不大
    - 交易費用佔了：15.6%；研發 31.29%；行銷 6.5%；行政費用 32%；其他費用 14%
- 與 2019 相比，2020 營收成長了 139.35%，費用僅上升 49.87%，也是這個原因讓營業利益率在 2020 一舉轉正

### 用戶活動數
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 vs. 2019</th>
    <th align="center">Growth(%)</th>
  </tr>
  <tr>
    <td align="center">已驗證用戶（Verified Users）</td>
    <td align="center">4,300 萬 vs. 3,200 萬</td>
    <td align="center">34.38%</td>
  </tr>
  <tr>
    <td align="center">月交易用戶（Monthly Transacting Users）</td>
    <td align="center">280 萬 vs. 100 萬</td>
    <td align="center">180%</td>
  </tr>
    <tr>
    <td align="center">平台規模（Asset on Platform）</td>
    <td align="center">903 億 vs. 170 億</td>
    <td align="center">432.19%</td>
  </tr>
  <tr>
    <td align="center">交易量（Trading Volume）</td>
    <td align="center">1931 億 vs. 799 億</td>
    <td align="center">141.66%</td>
  </tr>
  <tr>
    <td align="center">淨收入（Net Income）</td>
    <td align="center">3.22 億 vs. -0.3 億</td>
    <td align="center"></td>
  </tr>
</table>

比特幣（Butcoin）從 2019 年的 $3457.79, 到 2020 底的 $33,114.36，上漲了 858%，而這個風潮也讓 Coinbase 的月交易用戶及交易量有著三位數的成長 </br>

![加密貨幣與美股市場比較圖](../images/Crypto-vs-Stock-Market.png)



## 高階主管及大股東釋股
[來源 Yahoo Finance](https://finance.yahoo.com/news/coinbase-global-inc-coin-chairman-031502372.html)

Coinbase 共有 Class A（一股 = 1 票）及 Class B （一股 = 20 票）二種股票，而目前在市場中交易的種類為 Class A，二者僅在投票權有所不同 </br>

若依據 SEC 於 02/25 的資料來看，CEO 持有 2,753,924（Ａ股），36,851,833（Ｂ股）</br>
- 若僅計算Ａ股的話，CEO 在上週出脫了 27.23% 的持股，加計Ｂ股的話，則僅售出 1.89%

將該來源的釋出股份加總，約 603 萬股，平均賣出價為 $386（開盤價為 $381）</br>
開盤前二天的交易量為 1.2 億，本次高階主管及相關股東約佔總交易量的 5% </br>

而依據 ARK（方舟投資）上週的交易紀錄，他們總共買進了 127.7 萬股的 COIN，約是高階主管釋出股份的 21%
- ARKF 買進 142,280 股，佔總 ETF 的 1.16%
- ARKK 買進 907,968 股，佔總 ETF 的 1.28%
- ARKW 買進 227,221 股，佔總 ETF 的 1.13%

若想了解方舟投資，可參考這二篇文：</br>
[方舟投資（ARK Invest），著重於破壞式創新的主動型 ETF](https://ycjhuo.gitlab.io/blogs/Ark-Invest.html)</br>
[ARKX（太空探索 ETF），方舟投資（ARK Invest）新成立的主動型 ETF](https://ycjhuo.gitlab.io/blogs/ARK-Invest-ARKX.html)</br>

## 後記
在分析前，本來以為 Coinbase 只是個加密貨幣的交易所，毛利率應該不高，結果居然高達 90%，營業利潤率也有 38.73%

對比目前用戶最多的股票交易公司，嘉信理財（代號：SCHW），市值為 1220.47 億，營業利潤率為 36.78，表現不差

而目前市值 672.92 億的 Coinbase 約是嘉信理財市值的 45%，在目前加密貨幣交易所並不是一個寡占市場的情況下，Coinbase 該如何維持高毛利，與增長速度則是投資人該考慮的

雖說 Coinbase 可交易的加密貨幣種類較多，但根據 Coinbase 提供給 SEC 的資料中顯示，2020 的交易量中 BTC 佔了 41%，ETH 佔了 15%，其餘加密貨幣則佔了 44%

主流加密貨幣就佔了交易量的 56%，而 Fintech 巨頭如 Paypal（美股代號：PYPL，市值 3168.98 億）及 Square（美股代號：SQ，市值 1164.27 億），目前也都有交易 BTC 的功能，未來也可能新增交易其他幣種的功能

在這情形下，目前 Coinbase 仍缺乏自己的護城河，但隨著加密貨幣目前的熱潮，Coinbase 仍有上漲的空間