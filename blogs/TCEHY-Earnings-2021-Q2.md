---
title: 騰訊（TCEHY）2021 Q2 財報，半年跌了 30% 後是否能進場
description: '騰訊 2021 Q2 第二季財報|TCEHY 2021 Q2 第二季財報|騰訊 TCEHY 抄底|騰訊 TCEHY 進場'
date: 2021-08-26
tags: [Stock, TCEHY]
categories: Investment
---
近半年已跌了 30% 的騰訊（TCEHY），在 08/08 公佈了 Q2 財報後，股價仍不見起色 </br>
下面來看看這次財報是否能幫助騰訊股價止跌回升

## 騰訊（TCEHY）2021 Q2 財報
[資料來源](https://static.www.tencent.com/uploads/2021/08/18/94ef94c81d2bd9306b83d0afe3a125c9.pdf)
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">QoQ</th>
  </tr>
  <tr>
    <td align="center">營收（RMB in million）</td>
    <td align="center">135,303</td>
    <td align="center">138,259</td>
    <td align="center">2.18%</td>
  </tr>
  <tr>
    <td align="center">營業利潤（RMB in million）</td>
    <td align="center">56,273</td>
    <td align="center">52,487</td>
    <td align="center">-6.73%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">46.29%</td>
    <td align="center">45.38%</td>
    <td align="center">-0.91%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">41.59%</td>
    <td align="center">37.96%</td>
    <td align="center">-3.63%</td>
  </tr>
  <tr>
    <td align="center">EPS（diluted）</td>
    <td align="center">4.92</td>
    <td align="center">4.39</td>
    <td align="center">-10.78%</td>
  </tr>
</table>

- 營收：本季營收來到 1,383 億（季成長 2.18%），其中季成長最快的營收來源為金融服務（7.34%），佔總營收的 30.3%
- 營業利潤：約 525 億，受到毛利率＆營業利益率雙雙下降的影響，季成長為 -6.73%
- 毛利率＆營業利益率：分別為 45.38% ＆ 37.96%，與 Q1 相比，下降了 0.91% & 3.63%，主要是受到銷售＆行政成本大幅增加
- EPS：受到合資公司虧損 38.6 億所影響（上季為獲利 13.5 億），本季 EPS 為 4.39，季成長為 -10.78%

## 使用者月活數（Monthly active users）
單位百萬（In millions）:
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2 vs. 2021 Q1</th>
    <th align="center">用戶增減（％）</th>
  </tr>
  <tr>
    <td align="center">微信 + WeChat 月活數（MAU）</td>
    <td align="center">1,251.4 vs. 1,241.6</td>
    <td align="center">0.8%</td>
  </tr>
  <tr>
    <td align="center">QQ 智慧裝置月活數（Smart device MAU of QQ）</td>
    <td align="center">590.9 vs. 606.4</td>
    <td align="center">-2.6%</td>
  </tr>
  <tr>
    <td align="center">收費服務訂閱數（Fee-based VAS registered subscriptions）</td>
    <td align="center">229.4 vs. 225.7</td>
    <td align="center">1.64%</td>
  </tr>
</table>

若將中國人口估為 14 億，以目前微信使用數 12.51 億（含海外版微信 WeChat）來算，微信的滲透率為 89.35%（季成長 0.8%）

而社交龍頭 Facebook 月活數為 29 億（季成長 1.75%），若加上 Instagram ＆ What's App，則月活數為 35.1 億（季成長 1.74%）

考慮到微信全球化的困難程度，目前季成長已接近停滯，未來僅能朝著增加每月用戶的貢獻價值前進 </br>
所幸騰訊除了社交領域外，遊戲也是公司的主要獲利引擎（ 目前加值服務佔了總營收的 52.09% ），在提高客單價方面，對騰訊來說並不算困難

## 後記
騰訊因受到大陸官方稱網路遊戲是「精神鴉片」，以及政府加強監管的影響，近半年股價跌了 30% </br>
而 Q2 財報揭露了，騰訊 16 歲以下的使用者僅佔 2.6%，而 12 歲以下的更只有 0.3% </br>

考慮到低年齡層的使用者並沒有什麼消費能力，佔的比率又低，可推測股價不振的原因大部分為監管因素 </br>
但也因這個因素，讓騰訊目前的本益比達到近五年的最低點 20.4（疫情時最低為 27.69，2018 最低點為 23.79）</br>

雖然本季騰訊的財報季成長皆落後 Q1，但若考慮到騰訊已在社交＆遊戲領域建立起強大的護城河 </br>
若有長期持有的打算，我認為現在正是投資騰訊的好機會，而我也趁這時小幅提高了騰訊在投資組合的比重

![2021 Q2 TCEHY](../images/108-01.png)

若想了解美股購買騰訊的 ADR 費用及股利，可參考這篇：</br>
[公佈第四季及 2020 全年財報，遭到大股東拋售的騰訊表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-TCEHY.html)