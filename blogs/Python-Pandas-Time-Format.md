---
title: Pandas 的時間格式轉換及如何在 Excel 中正確顯示時間
description: 'Python 時間格式 | Python Pandas 時間格式 | Python Pandas Timedelta 介紹 | Python Pandas Excel 顯示 | Python 格式轉換'
date: 2021-01-17 00:27:36
tags: [Python, Pandas]
categories: Programming
---

在用 Pandas 統計資料時，時常會遇到從 Excel 讀取進來的資料，時間的資料格式是 object。 

如果要將這些時間料做加/減等運算的話，就會需要將格式從 object。 轉換為 timedelta。

下面來介紹如何在 Pandas 中作時間格式的轉換

## 從 Object 轉換為 Timedelta

這次的例子是在 summary 這個 dataframe 裡面，有二個欄位 VPN Hours 及 Office Hours，這二個欄位目前格式都是 object.

![44-01](../images/44-01.png)

接著我們用 ``` pd.to_timedelta ``` 來將這二個欄位的格式從 object 轉換為 timedelta。

timedelta 這個格式常用於二個時間的比較、加減運算等等，使用時要先記得先 import
``` from datetime import timedelta ```

<br/>
關於 datetime，這裡有個小知識：

::: tip
datetime 是一個模組（module），這個模組中有很多操作日期或時間的類別（class），例如：date, time, datetime, timedelta 等等。

對，datetime 這個模組裡面也有一個叫做 datetime 的類別，因此若是只有 ```import datetime```，那麼在要使用 datetime 這個類別時，則須要打 ```datetime.datetime```才能成功使用

而若想直接引用 datetime 這個類別的話，則可用 ```from datetime import datetime``` 來直接使用 datetime 這個類別
:::

<br/>

```python
summary['VPN Hours'] = pd.to_timedelta(summary['VPN Hours'])
summary['Office Hours'] = pd.to_timedelta(summary['Office Hours'])
```

當轉換到 timedelta 格式後，雖然從畫面上看，除了類型已成功轉換為 timedelta 之外，資料並沒有什麼改變。



但事實上資料已經從 11:25:45 這種顯示方式，變成了 0 days 11:25:45.000000000

雖然在畫面上仍舊顯示 00:55:19，也不妨礙我們觀看。但若要將資料存成 excel 的話。在 excel 中則會顯示如下：

![44-02](../images/44-02.png)

若要在 Excel 中讓資料正確顯示，有二種方法：
第一種是：<br/>
選取這二列資料後，按右鍵選擇 Format Cells... 手動將格式改為 Time 中的 hh:mm:dd

如下圖：
![44-03](../images/44-03.png)

第二種則是：<br/>
直接在 pandas 中把資料的格式 timedelta 再轉換回 object 即可。

## 從 Timedelta 轉換為 Object

這裡我們定義一個 removeDayAndTail 方法來達成這個目的：
1. 將先傳入的 series 轉換成字串
2. 用 lambda 匿名函數來切字串：
	先用 index 來定位 days 的位置，再將位置往後推 5 格，到時間的開頭
	再用 index(".") 定位字串中 . 的位置切掉後面的 0，就可成功取得我們所需要的 hh:mm:dd 這段。


```python
# Remove days for Series
# eg. 0 days 00:55:19.000000000  -> 00:55:19
def removeDaysAndTail(series):
	# Convert series format from timedelta to object
	series = series.astype(str)
	seriesToStr = series.apply(str)
	series = seriesToStr.apply(lambda x : x[x.index("days")+5 : x.index(".")]).str.strip()
	return series

summary['VPN Hours'] = removeDaysAndTail(summary['VPN Hours'])
summary['Office Hours'] = removeDaysAndTail(summary['Office Hours'])
```
