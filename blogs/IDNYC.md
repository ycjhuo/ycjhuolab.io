---
title: IDNYC 申請攻略，紐約市民必備神卡
description: 'IDNYC 紐約市民卡介紹|IDNYC 紐約市民卡 用途|IDNYC 紐約市民卡 推薦|紐約 博物館 動物園 免費|IDNYC 紐約市民卡 福利|紐約 必辦 紐約市民卡|IDNYC 紐約市民卡 怎麼辦|IDNYC 紐約市民卡 申請教學|IDNYC 紐約市民卡 申請資格|IDNYC 紐約市民卡 申辦'
date: 2021-07-30
tags: [New York, IDNYC, WCS Membership]
categories: Travel
---

最近辦了 IDNYC 這張卡，也記錄下我的申辦流程，有關 IDNYC 的福利可到官網查詢： </br>
[IDNYC Benefits Guide](https://www1.nyc.gov/assets/idnyc/downloads/pdf/cardholder-benefits-guide-brc-971-mlf.pdf)

下面列出較為常用的福利：
- Citi Bike 年費會員：15% off（原價：$179），若持有花旗信用卡或 Debit Card 僅有 10% off
- Costco 會員：送 $20 Gift Card
- 往 Governors Island 的 Ferry 票：Free；原價 $3（13 - 64 歲）
- Zipcar 租車：第一年年費 50% off（原價：$70）
- 可在作為借書證在紐約市圖書館使用（New York, Brooklyn, & Queens）
- 享有各種圖書館及動物/植物園折扣，參考：[官網列表](https://www1.nyc.gov/site/idnyc/benefits/museums-and-cultural-institutions.page)

## 申請條件
申請 IDNYC 須同時符合身份及居住地證明這二個條件：
- 集齊 3 點身份證明
- 集齊 1 點居住證明

### 身份證明
可作為身份證明的有：
- 護照（3 點）
- 美國 VISA（2 點）
- Social Security Card（2 點）
- 美國駕照 / Permit（3 點）

### 居住證明
可作為居住證明的有：
- 水電費帳單（Utilities）（1 點）
- 銀行帳單（Bank Statement）（1 點）

可到官網的 [點數計算機](https://www1.nyc.gov/site/idnyc/card/documentation.page)，確認是否已準備好相關文件

## 申請流程
[IDNYC 官網 的相關說明](https://www1.nyc.gov/site/idnyc/card/how-to-apply.page)

### 預約
首先到 [IDNYC 官網](https://a069-idnyconlineportal.nyc.gov/IOPWeb/#/EN/Dashboard)，選擇 ```Make an appointment``` 預約自己想要的時間（可同時預約多個，再取消預約）</br>

### 前往辦理
攜帶身份&居住證明文件到預約的地點辦理，我是預約 Downtown 的 Manhattan Business Center - DOF 辦理  </br>
地址是：66 John St 2F, New York, NY 10038  </br>

到了後，會有服務人員接待，拿申請表給你填，申請表有各種語言，服務人員會詢問你要哪種語言的申請表  </br>

填完基本資料後，交給櫃台，在核對完資料後，櫃台會用旁邊的相機拍照幫你拍照 </br>

拍完照後，服務人員會給你一張確認單，確認單上面有申請的編號 </br>
這個編號可在 [IDNYC 官網](https://a069-idnyconlineportal.nyc.gov/IOPWeb/#/EN/Dashboard) 的 ```Check Your Application Status``` 查詢目前申辦的進度 </br>

並告知 2-3 個星期後，IDNYC 就會寄到你在申請表上填的地址了 </br>

![IDNYC](../images/IDNYC.png)

以上就是申辦 IDNYC 的流程，相比辦駕照要在 MVC 裡面排 2 - 3 個小時，IDNYC 僅花不到 10 分鐘就辦理完成 </br>

且服務人員也更為親切，推薦大家可去辦一下 </br>

辦完 IDNYC 的話，可參考這篇來免費暢遊紐約動物園 & 水族館：</br>
[WCS Membership，免費暢遊紐約動物園 & 水族館](https://ycjhuo.gitlab.io/blogs/WCS-Membership.html) </br>


若對辦理（紐澤西）駕照有興趣的話，可參考這篇：</br>
[台灣駕照在紐澤西免考試換駕照 @ Bayonne MVC](https://ycjhuo.gitlab.io/blogs/New-Jersey-Taiwan-Drive-License.html)


::: warning
點擊觀看更多： </br>
- [旅遊相關文章](https://ycjhuo.gitlab.io/categories/Travel/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::