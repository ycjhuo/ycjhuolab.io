---
title: 開箱！漢莎航空貴賓室（紐約-紐華克自由國際機場 NWR Airport）
description: '紐華克貴賓室 | Newark Lounge 貴賓室 | 漢莎貴賓室 | Lufthansa Senator Lounge 貴賓室介紹'
date: 2021-09-21
tags: [Lounge, New York]
categories: Travel
---

紐約-紐華克機場（Newark Airport，NWR）的漢莎航空貴賓室（Lufthansa Senator Lounge）位於 B 航廈，對於這次搭乘聯合航空（A 航廈）的我，其實有點不便，但因為紐華克機場有 Air Train 連結各個航廈，所以就算航廈不同，仍能方便地在各航廈間移動

::: tip
但因為 Newark 機場的貴賓室在檢查行李的後面，所以若要特別去不同航廈的貴賓室，之後要回到原本的航廈的登機門，仍須再經過一次檢查行李的程序
:::

漢莎航空貴賓室（Lufthansa Senator Lounge）相比其他貴賓室，其實並沒有太突出的地方，但在疫情期間，大部分貴賓室都關閉的現在，也不失為一個選擇

## 大門＆休息區
漢莎航空貴賓室的大門並不顯眼，我在找的途中其實經過了二次仍沒有發現，最後是問了機場的服務人員才找到
![漢莎航空貴賓室（Lufthansa Senator Lounge）大門](../images/115-01.jpg)

進入大門後就可以看到櫃檯，在這邊出示新貴通卡（Priority Pass）即可進入
![漢莎航空貴賓室（Lufthansa Senator Lounge）櫃檯](../images/115-02.jpg)

在大家幾乎都已接種完 2 劑疫苗的現在（2021/09），不僅是機場，連貴賓室也是人滿為患，幾乎已沒有任何空座位
![漢莎航空貴賓室（Lufthansa Senator Lounge）休息區](../images/115-03.jpg)

從人滿的程度來看，相信美國的旅遊業及航空業已幾乎恢復到疫情前的狀態了
![漢莎航空貴賓室（Lufthansa Senator Lounge）休息區](../images/115-04.jpg)

## 用餐區
漢莎航空的貴賓室餐點僅有沙拉、水果、三明治等冷食，沒有提供熱食 </br>
但零食部分就有提供不少種洋芋片、能量棒可以選擇
![漢莎航空貴賓室（Lufthansa Senator Lounge）用餐區](../images/115-05.jpg)
![漢莎航空貴賓室（Lufthansa Senator Lounge）用餐區](../images/115-06.jpg)

## 後記
因為還要回到聯合航空的航廈，因此在拿了三明治＆洋芋片後就匆匆離開貴賓室了，而三明治真的不好吃，特地花時間過去其實沒有必要（除非是已辦完 Pre TSA 的人，不用再排長長的隊伍檢查行李）</br>


點擊 [貴賓室開箱](https://ycjhuo.gitlab.io/tag/Lounge/) ，看更多貴賓室開箱</br>
