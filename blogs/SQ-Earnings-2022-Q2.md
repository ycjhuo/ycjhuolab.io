---
title: Block（SQ）公佈 2022 Q2 財報，近一個月股價大漲 40%
description: 'Block SQ 2022 Q1 第一季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報|Block 收購 Afterpay|Block 收購先買後付平台|Block buy now pay later” (BNPL)'
date: 2022-08-15
tags: [Stock, SQ]
categories: Investment
---

Block（股票代號：SQ），從 Q1 財報發布，到 Q2 財報發布期間（05/23 → 08/04），股價下跌了 6%（$95.55 → $89.7），同期間那斯達克指數（Nasdaq Index）僅下跌 1%</br>

這段期間，Block 的股價最低跌到了 $57.5，隨後開始一路上漲，截至上週五收盤（8/12），近一個月的漲幅來到了 41%

下面就來看看 Block（SQ）的第二季財報表現如何

## Block 財務指標
[來源](https://investors.block.xyz/financials/quarterly-results/default.aspx)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q4</th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
    <th align="center">2022 Q2 vs. 2022 Q1</th>
  </tr>
  <tr>
    <td align="center">營收（億）</td>
    <td align="center">40.79 億</td>
    <td align="center">39.61 億</td>
    <td align="center">44.05 億</td>
    <td align="center">-2.89%</td>
    <td align="center">11.21%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">28.98%</td>
    <td align="center">32.70%</td>
    <td align="center">33.37%</td>
    <td align="center">3.71%</td>
    <td align="center">0.67%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-1.34%</td>
    <td align="center">-5.73%</td>
    <td align="center">-4.85%</td>
    <td align="center">-4.39%</td>
    <td align="center">0.87%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">-0.17</td>
    <td align="center">-0.38</td>
    <td align="center">-0.36</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：受惠於交易業務的營收較上季成長了近 20%，推動了 Block 本季的總營收較上季成長了 11%
- 毛利率：比上季小幅增加 0.7%，本季為 33.4%，為 9 個季度以來（2020 Q3）的最高點 </br>
原因是利潤最低的 Bitcoin 業務，在總營收的佔比下滑至 40.6%（上季為 43.7%），而下滑的營收則是由交易業務補上
- 營業利潤率：因行政費用較上季降低了 11%，讓本季營利率小幅上漲 0.9%，來到 -4.9%，目前已連續三季為負
- EPS：本季 EPS 為 -0.36，與上季 -0.38 相差無幾，受營業利潤率影響，目前已連續三季為負

::: tip
雖 Q1 財報中表示 Q2 的虧損會增大，但從上面的指標看來，受惠於營收＆毛利率的成長，Q2 的虧損並未增加，還小幅減少了 1% </br>
:::

## Block 業務發展
本季 Block 的營收年成長為 29%（Square 與 Cash App 的成長速度一致），營運費用的年成長則是更高的 66%（包含收購 AfterPay 的相關費用）

而若是排除去年收購的先買後付平台 AfterPay 後，營收年成長則會由 29% → 16%，可看出 Block 雖然因收購的費用在短期內無法實現盈利，但就對營收的幫助來說，這樁交易仍是值得的

細看 Block 的二大業務，面向企業端的 Square 與 面向消費者的 Cash App：

- Square：本季營收＆利潤年成長分別為 32% & 29%，若扣除 AfterPay 則為 24% & 16% </br>
海外營收年成長也達到了 104%，佔了 Square 總營收的 13%（若扣除 Afterpay，年成長則是 40%）

- Cash App：營收＆利潤年成長為 -21% & 29%，若扣除 AfterPay 後，營收年成長則為 15% </br>
截至七月，4700 萬使用者中，有超過 100 萬的月活使用者在使用 Cash App 的借貸服務

## 後記
Block 本季的淨損失為 2.08 億，若是扣掉收購 AfterPay 與比特幣投資損失後，淨損失則僅剩 0.98 億 </br>
而 EPS 也從本來的 -0.36 降為 -0.17，大幅降低了一半的虧損


雖然今年 Block 因市場升息的影響，造成股價大幅崩落（今年表現至今為 -46%），且因收購 AfterPay 後的大幅支出而讓公司更難實現盈利，目前 Block 已連續四季 EPS 虧損，而若要在今年的 Q3 & Q4 就實現盈利仍是困難重重

由此可見，股價是否能在維持近一個月的漲勢，在沒有財報做保證的情況下，仍屬不易


::: warning
點擊觀看更多： </br>
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::