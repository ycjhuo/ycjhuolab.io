---
title: 開箱！迪士尼 Disney Genie Plus（Disney Genie+）介紹
description: '迪士尼快速通關|快速通關 Fast Pass 介紹|迪士尼 Genie Plus 介紹|什麼是 Genie Plus|Disney Genie+ 介紹|Disney Genie+ 推薦|Disney Genie+ 教學'
date: 2021-12-22
tags: [Orlando, Disney]
categories: Travel
---

迪士尼在今年八月多將原本的快速通關（FastPass）下線了後，推出新的 Genie Plus 功能來取代，這篇就來介紹該如何使用 Disney Genie Plus

首先，Disney Genie 是一個免費的服務，可讓我們事先在 App （My Disney Experience - Walt Disney World）內，選擇自己有興趣的設施，之後我們在當天，App 就會提醒我們目前哪個設施排隊人潮較少

但我們實際用過後，覺得完全無感，原本 App 在地圖就有顯示每個設施的等待時間，Disney Genie 服務只是再把我們事先勾選有興趣的設施再標記出來，但幫助不大

而 Genie+ 是須付費才可使用的服務，費用為一人（一天）$15，用途是：讓我們可以先預約想玩的設施，但也有著一些限制


## Disney Genie Plus 限制
1. 一次只能預約一個設施，等到玩完了第一個設施，才可再預約第二個設施 </br>
但預約的設施沒辦法選擇要幾點去玩，只能由系統安排，因此若你預約的時間是 2:10 PM（如下圖紅框）</br>
那麼你在 2:10 PM 之前就不能再預約其它設施 </br>

![迪士尼 Disney Genie Plus](../images/142-01.png)

2. 若發生了上面的情形，在預約第一個設施的 90 分鐘之後，系統就會再讓你再預約另外一種設施，但一樣無法選擇要幾點去玩，只能由系統安排（系統不會自動通知，要自己算時間後進去 App 預約）

## Lightning Lane
迪士尼快速通關在這次改版後，改名為：Lighting Lane（LL），而這 Lighting Lane 也分為二種：
1. 涵蓋在 Genie Plus 裡面，在 App 中顯示為 LL Disney Genie+ </br>
只要付費開通 Genie Plus 服務後即可使用，使用限制就如同上一段提到的

2. 特別獨立出來的 Individual Lightning Lane </br>
不管有沒有買 Genie Plus 服務，都可以獨立購買，一人一次的費用為 $7 - $14 </br>

Individual Lightning Lane 只在每個樂園中，最熱門的二個設施有賣，例如在動物王國（Animal Kingdom）就是：
- 阿凡達飛行歷險（Avatar Flight of Passage）
- 遠征聖母峰（Expedition Everest - Legend of the Forbidden Mountain）

如下圖所示，這二個設施的 Lightning Lane 分別為 $14 與 $7

![迪士尼 Individual Lightning Lane](../images/142-02.png)

值得注意的是，Lightning Lane 是有數量限制的，賣完了就沒了，因此若想要體驗這些設施的人，看到 Lightning Lane 出現了就不要猶豫，不然馬上就會賣完

依我這次的經驗（2021/12/19），超過 10:00 後，Lightning Lane 就會銷售一空，而這些有 Lightning Lane 的設施，排隊時間皆在 120 - 180 分鐘左右

::: tip
不論是 LL Disney Genie+ 或是 Individual Lightning Lane 都是在當天早上七點就可開始預約，不論你是否入園
:::

## 使用心得
不管是 Genie Plus 內含的 Lightning Lane 或是獨立購買的 Lightning Lane，再預約到該時段後，都會有一個小時的緩衝時間

也就是若上面時間顯示 2:00 PM，那麼在 2:00 PM - 3:00 PM 之間都可以去玩，所以不用擔心預約時間到了，但我們卻趕不上的問題

由於 Genie Plus 的限制，理論上我們預約設施的時間應該要越早越好，因為這樣我們玩完後就可以再馬上預約下一個設施 </br>
例如：先預約 10 點的 → 10 點馬上過去玩 → 玩完後再預約 11 點的 → 11 點再過去玩 → 依此類推

但實際上，每個設施的 Genie Plus 都是有時段限制的，這個時段滿了，就不會再讓你預約了 </br>
因此，為了能保證玩到自己想玩的設施，就算一早看到那個設施的預約時間是晚上，但若現在不預約，可能那個設施就不會再開放 Lightning Lane 預約了，因此也能只一早就預約下去，而我自己也是這麼用的

由於迪士尼有針對 Lightning Lane 的人數作管控，因此 Lightning Lane 的排隊的時間基本上不會超過 10 分鐘，但也就造成了就算購買了 Genie Plus，但一整天的使用次數可能就只有 1 - 2 次

而環球影城的快速通關（Express），不須事先預約，只要看到想玩的設施就能進去排，因此環球的 express 排隊時間也就遠超迪士尼的 Lightning Lane，就我個人的經驗，約 20 - 40 分鐘（約為正常排隊時間的 1/3）

::: tip
迪士尼快速通關（Genie Plus）一天為 $15，環球影城的快速通關（Express）一天為 $75 - $269.99（依每天入園人數而定）
:::

若想看更多迪士尼文章，可點選文章上方的 Disney Tag，或是 [Disney](https://ycjhuo.gitlab.io/tag/Disney/) 


::: warning
點擊觀看更多： </br>
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
:::