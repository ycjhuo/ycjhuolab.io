---
title: 特斯拉（TSLA）發布 2022 Q1 生產及交付量，成長率首見衰退
description: '美股特斯拉|特斯拉 TSLA|特斯拉生產交車量 2022 Q1|特斯拉 Model 3 生產交車量 2022 Q1|特斯拉 Model Y 生產交車量 2022 Q1|特斯拉 拆股 2022'
date: 2022-04-04
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 04/02（六）公佈了 2022 第一季的生產 & 交車數量

在 2021 Q4 財報發布（01/26）到 公佈 2022 Q1 交車數量（04/02）的期間內，特斯拉股價上漲了 15.7%，而同時 S&P 500 指數僅上漲 4.5%

特斯拉的股價表現再次超越大盤，但也可能是受到特斯拉即將再次拆股的議題，從而推升了股價

下面來看看特斯拉 2022 第一季的生產/交付量表現如何

## 特斯拉 2022 Q1 生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q4</th>
    <th align="center">2022 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">13,109</td>
    <td align="center">14,218</td>
    <td align="center">282.09%</td>
    <td align="center">46.62%</td>
    <td align="center">8.46%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">292,731</td>
    <td align="center">291,189</td>
    <td align="center">12.15%</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">11,750</td>
    <td align="center">14,724</td>
    <td align="center">390.18%</td>
    <td align="center">26.49%</td>
    <td align="center">25.14%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">296,850</td>
    <td align="center">295,324</td>
    <td align="center">16.39%</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">305,804</td>
    <td align="center">305,407</td>
    <td align="center">15.21%</td>
    <td align="center">28.6%</td>
    <td align="center">-0.14%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">308,660</td>
    <td align="center">310,048</td>
    <td align="center">19.91%</td>
    <td align="center">27.84%</td>
    <td align="center">0.45%</td>
  </tr>
</table>

本季度的生產/交付量：

- Model S/X：生產量約 1.4 萬台，在維持了二個季度的高成長幅度後，本季的成長力道僅為 8.5%；交付量則是維持跟前一季度差不多的 25% </br>
 身為特斯拉產品線的最頂級車款，交車數量能直接反應在財報上的毛利率

- Model 3/Y：身為全球最熱銷的車款，本季的生產/交付量皆與上個季度差不多，仍是維持 29 萬輛左右 </br>
從 2020 Q3 開始保持雙位數的成長性，再連續達成了六個季度後，首次呈現負成長（-0.53%）</br>
 但值得高興的是，特斯拉在本季終於實現了：用四個季度達成生產量 100 萬台的紀錄

## 特斯拉上海＆柏林工廠進度
由於近日上海疫情嚴重，而開始實施封城，特斯拉的上海超級工廠也被迫停工，進而影響了這季的生產量，若狀況嚴重的話，連帶的 2022 第二季也將受到影響 </br>
目前上海工廠的（Model 3/Y）年產能約為 45 萬左右

不過可喜的是，特斯拉位於德國柏林的超級工廠，已於 03/22 正式投入生產，而這座工廠主要以生產 Model Y 為主，目前特斯拉並未揭露柏林工廠的年產能，但相信在第二季，Model 3/Y 生產量的成長性將會再次上漲

## 後記
特斯拉今年的股價從 03/14 時的低點（$766），到了現在（04/01）的 $1,085，上漲了約 42% </br>
最主要的原因莫過於在 03/28 公佈的即將在股東大會上討論拆股的可能性，這則消息一出，直接讓股價跳漲了 8% </br>

::: tip
特斯拉拆股的消息不只讓股價大漲，連我在 2020 當時寫的舊文，點閱率也在這段時間成長不少，有興趣的連結如下： </br>
[半年漲了六倍的特斯拉（Tesla），是否該考慮賣出](https://ycjhuo.gitlab.io/blogs/How-Tesla-Soar-600-in-6-month.html)
:::

特斯拉上次的拆股日是 2020/08/31，當時特斯拉在宣布將拆股消息後，短短的 20 天內，股價就從 $956 (08/11) 躍升到 $2,213 (08/28)，漲幅約 231%
那時，特斯拉也仍未被納入 S&P 500 指數中，市值相比現在小很多

而現在，特斯拉已是一家市值破兆的企業，拆股後的漲幅是否能有上次的 50% 都很難說

但若市場因為特斯拉本季的生產量不如預期，導致股價大跌的話，我認為會是一個很好的買入點

::: warning
點擊觀看更多： </br>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
