---
title: 特斯拉（TSLA）2023 Q4 季報｜進入穩定發展，不會有太多爆點的 2024
description: '美股 特斯拉 Tesla TSLA 財報|特斯拉 TSLA 2023 Q4 第四季財報 分析 重點 介紹|'
date: 2024-02-04
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 01/24（三）公佈了 2023 的第四季財報 <br />

在 Q3 - Q4 財報期間（10/18 - 01/24），特斯拉的股價從 $242.68 → $207.83，下跌 14%（同時 S&P 500 上漲 13%），但在財報發佈後，隔天馬上大跌 12%（$207.83 → $182.63），今年才過完一個月，股價至今跌了 24%<br />

而在過了一星期後的現在，股價並沒有再次下跌，而是在 $180 - $190 之間徘徊，以這個財報成績，及特斯拉今年的展望，股價將會繼續下跌 <br />

我認為今年會是特斯拉沈潛的一年，蹲得深，才跳得高，而在今年進場／仍持有特斯拉的投資者，會在 2025 享受到豐厚的果實 <br />

下面就來看看特斯拉 2023 Q4 財報以及 2024 的規劃

## 特斯拉 2023 Q4 財報
[來源](https://digitalassets.tesla.com/tesla-contents/image/upload/IR/TSLA-Q4-2023-Update.pdf)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q3</th>
    <th align="center">2023 Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">電動車營收</td>
    <td align="center">196.3 億</td>
    <td align="center">215.6 億</td>
    <td align="center">6%</td>
    <td align="center">-8%</td>
    <td align="center">9%</td>
  </tr>
  <tr>
    <td align="center">總營收</td>
    <td align="center">233.5 億</td>
    <td align="center">251.7 億</td>
    <td align="center">6%</td>
    <td align="center">-7%</td>
    <td align="center">7%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">18%</td>
    <td align="center">18%</td>
    <td align="center">-1%</td>
    <td align="center">--</td>
    <td align="center">--</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">8%</td>
    <td align="center">8%</td>
    <td align="center">-2%</td>
    <td align="center">-2%</td>
    <td align="center">--</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.66</td>
    <td align="center">0.71</td>
    <td align="center">7%</td>
    <td align="center">-38%</td>
    <td align="center">7%</td>
  </tr>
</table>

- 電動車營收：本季電動車營收為 216 億，季成長為 9%（ 2023 全年成長 15%）
- 總營收：Q4 總營收為 252 億，為 2023 年最高峰，季成長為 7%（ 2023 全年成長 19%）
- 毛利率：Q4 毛利率為 18%，考量到物價通膨的影響，毛利沒有顯著下降，顯示出特斯拉優秀的控制成本能力（2023 全年毛利率也是 18%，2022 則是 26%）
- 營業利益率：Q4 營利率為 8%，比上季小幅成長 1%，已連續三季低於 10%（2023 全年營利率為 9%，2022 為 17%）
- EPS：本季 EPS 為 0.71，由於營利率上升的影響，季成長為 7%

特斯拉在 2023 年最後一次的財報，由於 Q3 的產線停工影響，本季的各項指標雖保持小幅成長，並不代表特斯拉在本季的成長率真的像數字顯示的一樣這麼強勁，與 Q2 比的話，也僅能算是持平，而 Q4 EPS 比 Q2 相比，還低了 28%

## 財報重點
下表為 Q4 與 2023 全年的年成長率比較，可看出成長速度逐季減緩，部分原因是，基期已墊高的情況下，繼續維持高速成長也不容易
- 財報成長的部分（生產／交車量 ＆ 總營收／電動車營收），Q4 的成長速度比 2023 平均速度低
- 財報衰退的部分（毛利率／營利率 ＆ 利潤／EPS），Q4 又比 2023 平均速度還嚴重

<tr>
    <th align="center"></th>
    <th align="center">Q4 年成長</th>
    <th align="center">2023 年成長</th>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量／交車量</td>
    <td align="center">14%／19%</td>
    <td align="center">37%／39%</td>
  </tr>
  <tr>
    <td align="center">總營收／電動車營收</td>
    <td align="center">3%／1%</td>
    <td align="center">19%／15%</td>
  </tr>
  <tr>
    <td align="center">毛利率／營業利益率</td>
    <td align="center">-6%／-8%</td>
    <td align="center">-7%／-8%</td>
  </tr>
    <tr>
    <td align="center">營業利潤／EPS（Diluted）</td>
    <td align="center">-47%／-40%</td>
    <td align="center">-35%／-23%</td>
  </tr>
</table>

- 電動車業務：佔總營收 86%，目前年產量已突破 200 萬（Model 3/Y 212.5萬，Model S/X 10 萬，Cybertruck 12.5 萬），但在各國的市佔率仍不到 5%（美加 4%，歐洲 3%，中國 2.5%）<br />
造車成本持續下降，由 Q3 的每輛 $37,500 降為 Q4 的 $36,500，但已接近所能下降的極限，今年以來，造車成本共下降了 $3,000（約 5.5%）

- 能源業務：佔總營收 6%，在 2023 年成長為 154%，但季成長卻下滑 8%，預計在 2024 保持成長趨勢
- 服務業務：佔總營收 8%，在 2022 年轉虧為盈，今年年成長為 37%，Q4 季成長為 27%，利潤組成是零件／二手車銷售，超級充電站等等，隨著越來越多的特斯拉上路，服務營收也會繼續維持高成長

特斯拉在今年將會有二大發展重點：
1. 將已是全球最受歡迎的 Model Y，以及剛完成改款的 Model 3，推向全球，擴大在各個市場的市佔率
2. 致力發展下一代車款（更便宜的車款），但因為這個原因，特斯拉在 2024 的產能成長將會低於 2023


## 後記
特斯拉在 Q4 的財報沒有表現出以往的高成長率，也預告了 2024 的產量成長速度會低於 2023，這雙重打擊下，財報發佈的隔天，股價就大跌了 12%（$207.83 → $182.63）<br />

與 2022 比較，2023，因為實施了一連串的降價策略，導致毛利率下滑了 7%，就算營收成長了 19%，但在高額的資本支出／研發擠壓下，利潤仍是少了 35%，在如此不利的狀況下，又宣佈 2024 產量會有明顯的降速，也難怪投資人紛紛賣股逃生 <br />

我卻認為，在 Model 3/Y 都已成功創下熱賣的紀錄後，接下來的新車款肯定能再重現榮景，只是這不會發生在 2024，量產化則可能要等到 2025 後 <br />

在 2024 既無法量產 Cybertruck，推出新車款的可能也不高，今年特斯拉要引發熱潮，只能著重在 AI／自動駕駛這些無法馬上在財報中看出成績的「軟」實力上

但我認為，這麽多壞消息籠罩特斯拉的情況下，今年股價肯定不會好，那正是趁著低檔，加碼股票的最好時機，到了明年，有著 Cybertruck 量產 ＆ 新車款推出等話題的特斯拉，將會再迎來爆發式的成長 <br />

::: tip
這次只在財報公佈隔天以 $187.57 的價位買進 5 股，預計跌破 $180 後再加碼
:::

::: warning
點擊觀看更多： <br />
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
