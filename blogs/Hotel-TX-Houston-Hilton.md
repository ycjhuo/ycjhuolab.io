---
title: 開箱！德州休士頓-希爾頓 C. Baldwin, Curio Collection（C. Baldwin, Curio Collection by Hilton）
description: '休士頓飯店介紹|休士頓飯店推薦|休士頓飯店|休士頓飯店 CP 值|休士頓希爾頓 Hilton 介紹|Hilton Curio Collectio|希爾頓 Curio Collection 系列|Hilton Curio Collection 介紹'
date: 2022-02-22
tags: [Hotel, Hilton, Texas, Houston]
categories: Travel
---
::: tip
入住時間：2022/02/18 - 02/20 </br>
房型：Room, 1 King Bed </br>
每晚價格（稅後）：$244.53 / 55,000 點 </br>
Google 評價：4.1 顆星（521 評價）</br>
:::

這次在休士頓的四天三夜行程，前二晚選擇了希爾頓的 C. Baldwin, Curio Collection，後一晚選擇了 Hyatt Regency Houston，二間步行距離為 3 分鐘，皆位於 Houston Downtown，距離著名的博物館區（Houston Museum District）車程僅 13 分鐘，非常適合來休士頓的旅客

希爾頓的 Curio Collection 品牌，是 2014 年才有的新品牌，以獨立運營＆獨有個性而聞名，僅是掛上 Hilton 的名字，皆是屬於 4、5 星級的高端酒店

截至 2021，全球僅有 129 間，下面就來開箱位於德州休士頓的 C. Baldwin, Curio Collection

## 飯店外觀＆大廳（Hotel Building & Lobby）
C. Baldwin 這棟建築非常的大，共有 20 個樓層，寬度目測應該有三棟建築，在進入大廳時，會覺得空間非常寬廣 </br>
![C. Baldwin, Curio Collection by Hilton 外觀](../images/160-01.jpg)

進入大廳後，右邊是 Check-In 櫃檯，中間則是可以直接通往酒吧的休息區長廊
![C. Baldwin, Curio Collection by Hilton Check-In 櫃檯](../images/160-02.jpg)
![C. Baldwin, Curio Collection by Hilton 休息區](../images/160-03.jpg)

## 房間內部（Room 1 King Bed）
這次的房間位於 19F，房型是 King-Size 床，開門進入到房間後，右邊是穿鞋＆衣櫥，左邊則是浴室
![C. Baldwin, Curio Collection by Hilton 房間](../images/160-04.jpg)

下圖是走到房間最裡面，往門口拍的角度，可看到房間內除了有床跟電視外，右邊還有一組長沙發＆桌子 </br>
雖然房間相當寬敞，但房間內並沒有微波爐，小冰箱，咖啡機或茶包等
![C. Baldwin, Curio Collection by Hilton 房間](../images/160-05.jpg)

## 衛浴（Bathroom）
C. Baldwin 的浴室非常有設計感，整體裝潢與房間有著一致的整體感 </br>
備品是 ROIL Amanda George 這個牌子
![C. Baldwin, Curio Collection by Hilton 浴室](../images/160-06.jpg)

比較特別的的是，它的淋浴間是沒有門的，但因為前端有玻璃擋住，因此不用怕水會濺出來 </br>
另外，大部分美國的浴室是沒有蓮蓬頭的（Shower head），而 C. Baldwin 提供了花灑＆活動式的水龍頭二種，只是水量有點偏小
![C. Baldwin, Curio Collection by Hilton 浴室](../images/160-07.jpg)

## 餐廳（Restaurant & Bar）
下圖為 C. Baldwin 一樓的吧台，除了提供酒類之外，也有提供 Pizza 等簡單的食物，下圖可以看到吧台生意非常的好
![C. Baldwin, Curio Collection by Hilton 吧台](../images/160-08.jpg)
而吧台之所以生意很好的原因是，C. Baldwin 樓下的餐廳似乎是當地非常有名的網紅餐廳，若沒提前一天預約的話，Walk-In 時，服務人員就會推薦你到旁邊的吧台用餐 </br>

餐廳跟酒吧的類型都是偏義式，除了有一客 $95 的帶骨牛排外，也有一客僅要 $36 的平價牛排

相比之下，吧台的食物選擇就很有限了，因為要用掉美國運通 THC 送的 $100 Credit，我們就在這邊點了二個 Pizza、Meatball Slider、炸花枝以及二杯調酒，這樣下來再加上稅也差不多 $100
![C. Baldwin, Curio Collection by Hilton 餐點](../images/160-09.jpg)

在餐廳櫃檯旁，好像很有名的網紅牆，在我們用餐的二小時內，看到多於 5-6 組的網紅們在進入前都會在這個牆拍照，而之所以會說他們是網紅，是因為她們妝容都很精緻，且都穿著正式服裝 </br>
![C. Baldwin, Curio Collection by Hilton 餐廳](../images/160-10.jpg)

## 後記
休士頓的消費與紐約相比之下並不高，一晚僅要 $250 左右的房價，其實跟 DoubleTree 差不多，但 C. Baldwin 給人的感覺，建築非常的新，裝潢也很新潮，在餐廳＆吧台也可以看到很多年輕人 </br>

但 C. Baldwin 裡面沒有便利商店，外面若不開車的話，也沒有便利商店，若臨時要買什麼東西的話，其實有點不便，不過每層樓都有自動販賣機可以買飲料 </br>

早餐的話，若是金卡會員，C. Baldwin 會給予每人 $15 的 Credit 可以用在飯店內類似星巴克的早餐店，雖然金額不多，但以可頌 $3，大拿鐵 $6 的價位來說，應該是足夠了

若有預算考量的話，也可以考慮 C. Baldwin 旁邊，走路只要三分鐘就可到達的 [Hyatt Regency](https://ycjhuo.gitlab.io/blogs/Hotel-TX-Houston-Hyatt-Regency.html)，一晚含稅價格為 $162，約是 C. Baldwin 的 65 折左右
