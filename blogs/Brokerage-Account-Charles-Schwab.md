---
title: 投資美股，最推薦的美股券商嘉信理財（Charles Schwab）
description: '美股投資券商|美股券商推薦|美股券商哪一家最好|美股零手續費|美國匯錢回台灣|花旗全球速匯|嘉信提款卡|嘉信提款卡 免手續費|美金匯台灣|ATM 0 手續費提款|花旗撤出台灣|TD 提款卡|美金轉台幣|美金轉外幣'
date: 2021-07-27
tags: [Brokerage account, Credit Cards, Global Transfers]
categories: Investment
---

前篇文章：[美股券商帳戶轉出教學 (從德美利證券 TD Ameritrade 轉到嘉信理財 Charles Schwab)](https://ycjhuo.gitlab.io/blogs/Transfer-Account-From-TD-To-Schwab.html) </br>

提到了如何將 TD 帳號裡面的股票及金額轉到嘉信理財（Charles Schwab）

這篇來介紹，為什麼我選擇了嘉信理財（Charles Schwab）作為投資美股的券商

## 選擇美股券商
投資美股時，可以選擇的券商有很多，如 Charles Schwab, TD Ameritrade, Firstrade 以及較為新興的 Robinhood 等 </br>

這些券商，交易美股的手續費都是 $0，而在這個狀態下，選擇哪一家券商其實沒有什麼分別

但嘉信理財 Charles Schwab)與其他券商的不同點在於： </br>

嘉信理財提供了一張 VISA 金融卡給有開設投資帳戶（Brokerage account）的人

這張卡能夠讓你在任何 （支援 VISA 提款）的 ATM 領取現金（當地貨幣），且沒有任何手續費 </br>

在台灣 ATM 用的話，提領出來的就會是台幣，在日本 ATM 用的話，提領出來的就是日幣 </br>

::: tip
這張 VISA 金融卡每天提款的上限為 $1,000 美金
:::

![Schwab-Brokerage-debit-card](../images/Schwab-Brokerage-debit-card.png) </br>
圖片來源：[secretflyer](https://www.secretflyer.com/the-best-way-to-access-your-money-while-traveling-internationally)

## ATM 提款匯率
下圖是嘉信的提款記錄以及 在 07/21 的 ATM 提款明細：

![Schwab-debit-ATM-匯率](../images/98-01.png)
![Schwab-debit-ATM-明細](../images/98-02.png)

可以看到，在提款的當下會被收取 ATM 跨國提款手續費 $100（新台幣）</br>
但因為嘉信的提款卡有跨國提款免手續費的優惠，因此，可以在嘉信的提款記錄中看到 Rebate ATM Surcharge waived $3.57 </br>
表示這 $100（新台幣）的手續費被嘉信減免了（手續費換算匯率為 28.011）</br>

而我們提領這 $1,000（新台幣）的匯率如下：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">提款金額（台幣）</th>
    <th align="center">嘉信扣款金額</th>
    <th align="center">換算匯率</th>
  </tr>
  <tr>
    <td align="center">2021/07/21</td>
    <td align="center">$1,000</td>
    <td align="center">$35.67</td>
    <td align="center">28.035</td>
  </tr>
</table>


提領當天的銀行匯率如下：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">即期買入</th>
    <th align="center">即期賣出</th>
    <th align="center">現金買入</th>
    <th align="center">現金賣出</th>
  </tr>
  <tr>
    <td align="center">台灣銀行</td>
    <td align="center">27.945</td>
    <td align="center">28.095</td>
    <td align="center">27.62</td>
    <td align="center">28.29</td>
  </tr>
  <tr>
    <td align="center">兆豐銀行</td>
    <td align="center">28.01</td>
    <td align="center">28.11</td>
    <td align="center">27.67</td>
    <td align="center">28.34</td>
  </tr>
  <tr>
    <td align="center">中國信託</td>
    <td align="center">28.017</td>
    <td align="center">28.117</td>
    <td align="center">27.711</td>
    <td align="center">28.328</td>
  </tr>
</table>

因為我們是從 ATM 提款新台幣，扣款嘉信帳戶裡面的美元，因此對應到的就是```現金買入```（等於銀行用新台幣跟我們買入美元） </br>
可以看到嘉信的匯率（28.035） 比這三家銀行的```現金買入```都好 </br>

就算是用```即期買入```來看，表示銀行用 NTD 跟我們買入 USD，只是拿到的錢不是現金，而是存入銀行帳戶，嘉信的匯率仍就比這三家銀行好 </br>

::: tip
現金 & 即期買入：數字越高對我們越好，表示銀行用較高的價格買我們手上的美元
:::

## 後記
為什麼嘉信的匯率會比銀行的好呢？這是因為嘉信給我的提款卡是 VISA 的，所以就會以 VISA 的匯率為主 </br>
VISA 的匯率可在官網的匯率計算機找到：[Currency Exchange Calculator](https://usa.visa.com/support/consumer/travel-support/exchange-rate-calculator.html) </br>

![嘉信理財匯率](../images/98-03.png) </br>

而在花旗銀行於 2021/04/15 宣布即將退出台灣市場後，從美國（免費）匯款到台灣的方法可能又會少一條 </br>
沒錯，就是花旗銀行的 ```全球速匯（Citibank Global Transfers）``` 服務  </br>

若不熟悉花旗銀行 ```全球速匯（Citibank Global Transfers）``` 這個免費跨國匯款服務的話，可參考這二篇： </br>
[台灣美國之間匯款利器，花旗全球速匯（美國匯款到台灣篇）](https://ycjhuo.gitlab.io/blogs/Citi-Global-Transfers-US-To-TW.html) </br>
[台灣美國之間匯款利器，花旗全球速匯（台灣匯款到美國篇）](https://ycjhuo.gitlab.io/blogs/Citi-Global-Transfers-TW-To-US.html) </br>


如果花旗銀行因為退出台灣市場而讓我們無法再使用 ```全球速匯``` 這個免費台灣 ⇄ 美國匯款服務的話 </br> 
有了嘉信理財這張 VISA 提款卡，一樣能幫我們做到：將在美國的美金以不錯的匯率轉成台幣，或是到各個國家旅遊時可以提領當地現金來使用 </br>

也算幫我們實現了將美國的錢，轉回台灣來使用

::: tip
雖然每天提領的金額上限為 $1,000 美金，但若金額不多的話，其實領個十天就是 $10,000 美金了，如果沒有臨時的大筆支出，一天一天慢慢領，也是個不錯的方法
:::

::: warning
點擊觀看更多： </br>
- [美國運通福利文](https://ycjhuo.gitlab.io/tag/Credit%20Cards/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::