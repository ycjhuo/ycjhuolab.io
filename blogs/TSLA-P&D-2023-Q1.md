---
title: 特斯拉（TSLA）發布 2023 Q1 生產及交付量，生產量＆交付量幾近停滯
description: '美股特斯拉|特斯拉 TSLA|特斯拉生產交車量 2023 Q1|特斯拉 Model 3 生產交車量 2023 Q1 第一季|特斯拉 Model Y 生產交車量 2023 Q1 第一季'
date: 2023-04-14
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 04/02（日）公佈了 2023 第一季的生產 & 交車數量

在 Q4 財報發布 → 公佈 2023 Q1 交車數量期間（1/2 → 4/2），特斯拉股價暴漲 44%，而 S&P 500 指數僅上漲 2% </br>
如此顯著的漲幅，將特斯拉今年以來的報酬率推升到了 92%（$108.1 → $207.5），也一舉消滅了市場上看空特斯拉的聲音

之前對於特斯拉受供應鏈影響的質疑，以及馬斯克為了購買推特，大賣特斯拉股票的新聞，在這幾天似乎已消聲匿跡


而特斯拉這個漲幅，是否能持續下去呢？下面來看看特斯拉在這 2022 的最後一季度表現如何

## 特斯拉 2023 Q1 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q4</th>
    <th align="center">2023 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2023 Q1 vs. Q4</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">20,613</td>
    <td align="center">19,437</td>
    <td align="center">21.47%</td>
    <td align="center">3.4%</td>
    <td align="center">-5.71%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">419,088</td>
    <td align="center">421,371</td>
    <td align="center">42.87%</td>
    <td align="center">21.13%</td>
    <td align="center">0.54%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">14,147</td>
    <td align="center">10,695</td>
    <td align="center">15.53%</td>
    <td align="center">-24.23%</td>
    <td align="center">-24.4%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">388,131</td>
    <td align="center">412,180</td>
    <td align="center">36.32%</td>
    <td align="center">19.37%</td>
    <td align="center">6.2%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">439,701</td>
    <td align="center">440,808</td>
    <td align="center">41.51%</td>
    <td align="center">20.16%</td>
    <td align="center">0.25%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">402,278</td>
    <td align="center">422,875</td>
    <td align="center">35%</td>
    <td align="center">17%</td>
    <td align="center">5.12%</td>
  </tr>
</table>

2023 Q1 生產/交付量：
- Model S/X：產量約 1.9 萬輛，與上季相比，衰退近 6%，是 2021 Q2 恢復生產後，第一次出現負成長
- Model S/X：交付量約 1.1 萬輛，與上季相比，衰退 24%（上個季度成長也是衰退 24%），一樣是從 2021 Q2 之後，第二次的負成長
- Model 3/Y：產量約 42 萬輛，季成長僅為 0.5%，相比上季的 21% 成長率，以往動輒二位數的成長率，幾乎已停滯
- Model 3/Y：交付量約 41 萬輛，季成長為 6%，比起前上季的 36% ＆ 19%，也有顯著的降低

本季總產量約 44 萬輛車，交付了 42.3 萬輛，季成長分別為 0.3% ＆ 5%，上季成長率為 20% ＆ 17％，可看出特斯拉以往引以為傲的成長性，在本季似乎已接近停止

若失去了成長動能的特斯拉是否仍值得投資呢？接下來用 2022 Q4 的財報中來分析特斯拉是否已到達目前的最大產能，抑或是僅因為供應鏈的問題而短暫放緩了產能

## 特斯拉最大產能
依據特斯拉 2022 Q4 財報所揭露，目前各大超級工廠的年產能如下：
- 加州：Model S＆X 為 10 萬輛，Model 3/Y 為 55 萬輛
- 上海：Model 3＆Y 為 75 萬輛以上
- 柏林：Model Y 為 25 萬輛以上
- 德州：Model Y 為 25 萬輛以上

若均以最低標準來算，合計為：
- Model S＆X 為 10 萬輛
- Model 3＆Y 為 180 萬輛

而上面這年產量若平均分為四個季度來看，Model S＆X 為 2.5 萬輛，Model 3＆Y 為 45 萬輛

對比本季的 1.9 萬輛（S/X）＆ 42 萬輛（3/Y），目前均已達最大產能的 76% ＆ 93%

如此一來，特斯拉若無法再提升產能，未來的成長動能就依靠其它業務來推動，抑或是以提升毛利率的方向來進行

## 後記
在這 2023 年的第一季，特斯拉並沒有因為「今年買車可獲得 $7,500 減免」的政策，出現交付量暴增的現象，同時在產能上也幾乎到達瓶頸

在財報中，各工廠最大產能來看，並無看到特斯拉在其它地區有 Model S/3/X/Y 的增產計畫，而同時，特斯拉也在去年財報中，也預期了 2023 的產量約會是 180 萬輛左右，若將本季的產量 x 4 倍，的確會是 180 輛左右，這是否代表特斯拉在產能上的成長已趨緩了呢？

不知大家是否還記得，在 2022 年底，特斯拉剛交付了第一輛的 Semi，而負責生產 Semi 的內華達工廠（Nevada）也將在今年進入生產階段，相信今年 Semi 的產能將邁入正軌，並在年底或是明年成為特斯拉的又一熱銷車款

::: warning
點擊觀看更多： </br>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
