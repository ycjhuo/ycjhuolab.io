---
title: WCS Membership，免費暢遊紐約動物園 & 水族館
description: '紐約 動物園 Bronx Zoo 門票 年票|紐約 動物園 水族館 免費|紐約市民卡 IDNYC WCS Membership 會員 免費|紐約 博物館 免費|中央公園 動物園 水族館 免費|'
date: 2022-08-30
tags: [New York, IDNYC, WCS Membership]
categories: Travel
---

## 用 IDNYC 申辦 WCS Membership
上篇 [IDNYC 申請攻略，紐約市民必備神卡](https://ycjhuo.gitlab.io/blogs/IDNYC.html) 介紹了在紐約市都應該要有的卡：IDNYC，這篇來介紹要如何用 IDNYC 來取得 WCS Membership（有效期限一年）

什麼是 WCS Membership 呢？它算是紐約動物園的一種會員方案，有了這個會員，就可免費進入紐約的 4 個動物園及 1 個水族館 </br>

如果 5 個景點都去的話，相當於幫我們省了 $112，下面列出 WCS 會員可免費進入的地方＆價格：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">成人（13+）</th>
    <th align="center">小孩（3 - 12）</th>
  </tr>
  <tr>
    <td align="center">Bronx Zoo</td>
    <td align="center">$41.95</td>
    <td align="center">$31.95</td>
  </tr>
  <tr>
    <td align="center">Central Park Zoo</td>
    <td align="center">$19.95</td>
    <td align="center">$14.95</td>
  </tr>
  <tr>
    <td align="center">Queens Zoo</td>
    <td align="center">$9.95</td>
    <td align="center">$6.95</td>
  </tr>
  <tr>
    <td align="center">Prospect Park Zoo</td>
    <td align="center">$9.95</td>
    <td align="center">$6.95</td>
  </tr>
  <tr>
    <td align="center">New York Aquarium</td>
    <td align="center">$29.95</td>
    <td align="center">$26.95</td>
  </tr>
</table>

要申辦 WCS Membership 須親自帶著 IDNYC 到下面三個地點的其中一個申辦：</br>
Bronx Zoo、Central Park Zoo、New York Aquarium，而每天申辦的時間是：10:00 - 15:30

我自己是到 Bronx Zoo 辦的，下面介紹要如何從曼哈頓搭乘大眾運輸到 Bronx Zoo

## 曼哈頓到 Bronx Zoo
1. 先到 Madison Ave. & 29 St. 的公車站搭乘快捷公車 Express Bus (BxM11)，這邊要注意 BxM11 跟 B11 是不同的

![紐約公車站牌 Express Bus BxM11](../images/191-01.jpg)

2. 快捷公車的票價為 $6.95，上車刷卡，可直接用 MTA 地鐵卡，整個車程約 40 - 50 分 </br>
下圖可看到，快捷公車比一般的公車舒適，車上也有 USB 插座供手機充電

![紐約快捷公車 Express Bus BxM11](../images/191-02.png)

3. 到站後，下車往左轉走就可看到 Bronx Zoo 的大門了

![紐約 New York Bronx Zoo 入口](../images/191-04.jpg)

4. 再往內走即可看到售票處＆我們這次的目標：辦理 WCS Membership 的櫃檯 </br>
只須出示 IDNYC 給櫃檯人員，並告知想申辦 WCS Membership，即可申辦完畢

![Bronx Zoo WCS Membership 櫃檯](../images/191-05.jpg)

::: tip
註冊好 WCS Membership 後不能直接進入動物園，而是須上網預約要去的日期＆時段
由於當天預約，通常會遇到已額滿的情況，所以申辦的當天有很大的機率無法進入動物園
:::

![IDNYC 申辦 WCS Membership](../images/191-03.jpg)


5. 申辦完後，若要離開動物園，則一樣走出大門後，右轉即可看到公車站牌（與來的時候，下車的地點不同）</br>
一樣搭乘 BxM11 回到曼哈頓
![Bronx Zoo 回曼哈頓公車站牌](../images/191-06.jpg)
![Bronx Zoo 回曼哈頓公車站牌 BxM11](../images/191-07.jpg)


## WCS Membership 預定門票
有了 WCS Membership 後，就可以直接在各個動物園的官網上，輸入自己的 WCS Membership 卡號＆ Last name 來預訂門票囉 </br>
下面列出 2 個動物園的官網，其它的也可以自行到官網預定：
- [Bronx Zoo 預定門票](https://bronxzoo.com/shop/ticket-options)
- [Central Park Zoo 預定門票](https://centralparkzoo.com/shop/ticket-options)


::: warning
點擊觀看更多： </br>
- [旅遊相關文章](https://ycjhuo.gitlab.io/categories/Travel/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::