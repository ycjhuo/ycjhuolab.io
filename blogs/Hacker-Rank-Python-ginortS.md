---
title: HackerRank Python ginortS 解法
description: 'HackerRank 教學|HackerRank 解答|HackerRank Python|HackerRank Python ginortS 教學|HackerRank Python ginortS 解答|Python 字串拆解排序'
date: 2021-03-15 23:57:41
tags: [Python, HackerRank, Medium]
categories: HackerRank
---
## Problem
### Task
You are given a string ```S```. </br>
```S``` contains alphanumeric characters only. </br>

Your task is to sort the string ```S``` in the following manner:
- All sorted lowercase letters are ahead of uppercase letters.
- All sorted uppercase letters are ahead of digits.
- All sorted odd digits are ahead of sorted even digits.

### Input Format
A single line of input contains the string ```S```. </br>

### Constraints
0 < len(```S```) < 1000

### Output Format
Output the sorted string ```S```.

### Sample Input
```Sorting1234```

### Sample Output
```ginortS1324```


## Answer
這題是將題目給出的字串逐一拆解，經過以下 4 個步驟而得出答案：
1. 取出字串中的小寫字母，接著依照字母順序重新排序
2. 取出字串中的大寫字母，接著依照字母順序重新排序
3. 取出字串中的奇數數字
4. 取出字串中的偶數數字
5. 將上面的 4 個字串重新組合成一個新字串

### Source Code
```python
# 取得 User 輸入的字串
# 題目範例為：Sorting1234
s = input()

# 宣告 4 個 list 來儲存 大寫字母，小寫字母，奇數數字，偶數數字
upperList, lowerList, oddList, evenList = [],[],[],[]
# 最後將上面 4 個 list 排序後依照題目需求存到這個 list
finalList = []
# 逐一取得 User 輸入字串的每個字元
for i in range(len(s)):
    # 判斷字元是否為字母
    if s[i].isalpha():
        # 判斷字元是否為 大寫 字母
        if s[i].isupper():
            # 是的話就存到 upperList
            upperList.append(s[i])
        # 否則就是小寫字母，並存到 lowerList
        else:
            lowerList.append(s[i])
    # 非字母的話，就是數字了
    else:
        # 數字可以被 2 整除的話就是奇數，存到 oddList
        if (int(s[i]) % 2) == 1:
            oddList.append(s[i])
        # 否則就是偶數，存到 evenList
        else:
            evenList.append(s[i])
# sorted 可以將 list 裡面的值依據字母順序排序（及數字大小）
# finalList =  ['g', 'i', 'n', 'o', 'r', 't', 'S', '1', '3', '2', '4']
finalList = sorted(lowerList) + sorted(upperList) + oddList + evenList

# 取得最後的 list 後，逐個取出 list 中的字元，並組成字串
finalStr = ''
for s in range(len(finalList)):
    finalStr += ''.join(finalList[s])

# finalStr = ginortS1324
print(finalStr)
```