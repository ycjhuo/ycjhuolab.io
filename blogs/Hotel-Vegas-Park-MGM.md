---
title: 開箱！拉斯維加斯-蒙地卡羅賭場度假村 (Park MGM Las Vegas)
description: '飯店開箱 拉斯維加斯 Las Vegas|Las Vegas 飯店推薦|Las Vegas 飯店介紹|Las Vegas 飯店禁菸|Vegas 飯店 CP 值|拉斯維加斯 飯店推薦|拉斯維加斯 飯店介紹|拉斯維加斯 飯店禁菸|拉斯維加斯 飯店 CP 值'
date: 2021-09-23
tags: [Hotel, MGM, Las Vegas]
categories: Travel
---
::: tip
入住時間：2021/09/20 - 09/24 </br>
房型：Park MGM Two Queen </br>
每晚價格（稅前）：$50，還要外加設施費 $39 / 一天</br>
Google 評價：4.2 顆星（19,112 評價）</br>
:::

這次在 Las Vegas 的五天四夜選擇了位於 Strip 大道上偏南的 Park MGM，Park MGM 每晚的房價差距很大，從淡季平日的 $38 到旺季假日的 $416 都有，這次我們是九月平日去的，只要 $50 一晚，就住到了這間 4 星級飯店

::: tip
Las Vegas 的飯店，除了房價外，都還要另加設施費（不管有沒有使用設施），而 Park MGM 的設施費是 $39 一天
:::

這次在訂房時，選擇了普通的 Two Queen 的房型，但入住時很幸運的被升級到 Strip View 房型，也就是可以看到拉斯維加斯大道（Strip）的房間 </br>

聽起來好像不錯對吧，但實際上並沒有差太多，因為房間外圍的玻璃都沒有擦，所以看出去的景其實也就打折扣了，如下：
![Park MGM Las Vegas Strip View](../images/113-09.jpg)

## 飯店外觀＆大廳
飯店外觀是米高梅（MGM）一貫的綠色設計，大樓緊鄰著另一家飯店 NoMad，飯店內除了賭場外，也有幾家餐廳可供選擇
![Park MGM Las Vegas 外觀](../images/113-01.jpg)

紐約也有的 EATALY 義式餐廳
![Park MGM Las Vegas 外觀](../images/113-02.jpg)

## 房間
房間是 2 Queen Bed 的設計，最旁邊還有一個沙發床
![Park MGM Las Vegas 房間](../images/113-04.jpg)

## 浴室
浴室是無浴缸的設計，沒有要泡澡的話，其實無浴缸的設計感覺比較方便
![Park MGM Las Vegas 浴室](../images/113-05.jpg)
![Park MGM Las Vegas 外觀](../images/113-06.jpg)

化妝台的鏡子居然還會發光，旁邊還有一個可調式直立放大鏡，真的很適合化妝用
![Park MGM Las Vegas 化妝台](../images/113-07.jpg)

盥洗用具是 MGM 自有的 Kuer
![Park MGM Las Vegas 盥洗用具](../images/113-08.jpg)

## 後記
Park MGM 在 Las Vegas 算是比較新的飯店，不管是外觀或是內裝都蠻新的，最重要的一點是 Park MGM 是整棟樓禁菸的，不管是賭場或是酒店，這點對許多家長來說是一大加分項
![Park MGM Las Vegas 賭場](../images/113-03.jpg)

Las Vegas 的賭場＆酒店是可以室內吸菸的，常常一走進賭場，就吸到濃濃的煙味，很難想像在非疫情不用戴口罩時，菸味會有多濃，但對於吸煙人士來說就是天堂了

也因為全面禁菸這點，下次來 Las Vegas 我仍會選擇 Park MGM