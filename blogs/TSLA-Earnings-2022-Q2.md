---
title: 特斯拉（Tesla）2022 Q2 季報，連 7 季度成長紀錄中止
description: '特斯拉 TSLA 財報|特斯拉 TSLA 2022 Q2 第二季財報|特斯拉 TSLA 生產量 交車量 2022 Q2|TSLA 特斯拉 拆股'
date: 2022-08-21
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 7/20（三）公佈了 2022 第二季財報 </br>
在 2022 Q1 - 2022 Q2 財報的期間（4/20 - 7/20），特斯拉從股價 $977.2 → $742.5，下跌了 24%，同期間 S&P 500 指數則下跌 11.2%，特斯拉的股價在連續三個季度超越大盤後，在本季大幅落後大盤指數

而主要原因，並不是因為財報，雖然財報表現不好，但在公布財報的隔天，股價跳漲了 10%；而在財報發佈後一個月的現在，特斯拉股價上漲了 20%，而 S&P 500 指數僅上漲 7%

這是否表示財報中隱含了哪些訊息，讓投資人對於特斯拉更具信心了呢？


## 特斯拉 2022 Q2 財報
[來源](https://tesla-cdn.thron.com/static/EIUQEC_2022_Q2_Quarterly_Update_Deck_J8VLIK.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22tsla-q2-22-update.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q4</th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
    <th align="center">2022 Q2 vs. 2022 Q1</th>
  </tr>
  <tr>
    <td align="center">電動車營收（百萬）</td>
    <td align="center">15,967</td>
    <td align="center">16,861</td>
    <td align="center">14,602</td>
    <td align="center">5.3%</td>
    <td align="center">-15.47%</td>
  </tr>
  <tr>
    <td align="center">電動車利潤（百萬）</td>
    <td align="center">4,882</td>
    <td align="center">5,539</td>
    <td align="center">4,081</td>
    <td align="center">11.86%</td>
    <td align="center">-35.73%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">30.6%</td>
    <td align="center">32.9%</td>
    <td align="center">27.9%</td>
    <td align="center">2.3%</td>
    <td align="center">-5%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">14.7%</td>
    <td align="center">19.2%</td>
    <td align="center">14.60%</td>
    <td align="center">4.5%</td>
    <td align="center">-4.6%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">2.05</td>
    <td align="center">2.86</td>
    <td align="center">1.95</td>
    <td align="center">28.32%</td>
    <td align="center">-46.67%</td>
  </tr>
</table>

- 電動車營收：受限於主力車款 Model 3/Y：因上海封城＆供應鏈影響，產量減少了約 20%，導致本季營收較 Q1 衰退了 15.5%，這也終止了特斯拉從 2021 Q1 以來，連續 7 季營收成長的紀錄

- 電動車利潤：本季利潤較 Q1 大減 36%，衰退的幅度比營收有過之而無不及，受限於產能問題，近一步拖累了特斯拉的利潤
- 毛利率：利潤的衰退幅度 > 營收，可看出本季毛利率比起 Q1 衰退了不少（較上季衰退 4.10%），若單看電動車的毛利率，則較上季衰退 5%
- 營業利益率：較上季下滑 4.6%，也讓本季的營業利潤大幅下滑了 46%，終止了從 2021 Q2 以來的雙位數成長率
- EPS：因前幾項指標皆走下坡，本季 EPS 較上季衰退了 47%

特斯拉從 2021 開始，每次公佈季報皆在打破以前的紀錄，一季比一季更好，但這個成長趨勢在本季（2022 Q2）因產能問題，徹底被終止

在通膨＆電池短缺的情況下，直接導致各項財務指標走向衰退，所幸馬斯克在本季的股東會上表示，目前特斯拉已過了通膨的高峰，零組件成本均呈現下滑趨勢，因此在下季（Q3），我們就可以看到特斯拉的季報重新恢復成長動能

接著看看特斯拉 2022 Q2 的生產/交付量

## 特斯拉 2022 Q2 生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
    <th align="center">Q2 vs. Q1</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">14,218</td>
    <td align="center">16,411</td>
    <td align="center">46.62%</td>
    <td align="center">8.46%</td>
    <td align="center">15.42%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">291,189</td>
    <td align="center">242,169</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
    <td align="center">-16.83%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">14,724</td>
    <td align="center">16,162</td>
    <td align="center">26.49%</td>
    <td align="center">25.14%</td>
    <td align="center">9.77%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">295,324</td>
    <td align="center">238,533</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
    <td align="center">-19.23%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">305,407</td>
    <td align="center">258,580</td>
    <td align="center">28.6%</td>
    <td align="center">-0.14%</td>
    <td align="center">-15.33%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">310,048</td>
    <td align="center">254,695</td>
    <td align="center">27.84%</td>
    <td align="center">0.45%</td>
    <td align="center">-17.85%</td>
  </tr>
</table>

- Model S/X：身為特斯拉的高價車款， Model S/X 從 2021 Q2 改款後以來一直保持成長的趨勢，未來特斯拉的毛利率也能隨著產量的增加而繼續提升

- Model 3/Y：因 Model Y 使用的新型電池在提升產量上遭遇困難，而若要改用傳統電池，必要工具＆部分零件卻卡在中國港口，使得特斯拉無法展現出應有的產能，而導致 Model 3/Y 的產量減少了約 17%

本季因供應鏈問題＆上海工廠停工，特斯拉主力車款的 Model 3/Y 均面臨產量＆交付量的大幅度下降，而這也是特斯拉在本季財報表現失利的主要因素

所幸在財報中提及了，特斯拉的四個工廠（加州、德州、上海、柏林）均可在這季的短暫失利後，重新恢復原有產量，並進一步增加

## 後記
雖本次財報的表現不盡人意，但最讓大家意外的應該是特斯拉的無人車業務 Robotaxi 在財報中現身了！</br>
目前仍在開發階段，但馬斯克在股東大會上則表示會在 2024 年開始生產，雖先前有不少跳票的紀錄，但至少這項業務已被提上時程，相信未來能成為特斯拉的另一獲利來源

另外特斯拉將在 8/24（三） 完成拆股（1 股 → 3 股），在目前的市場氛圍下，這次拆股無法像上次一樣，在短短的 20 天就讓動股價上漲了 231%，但拆股後更低的股價也會帶給投資者更好的流動性＆吸引更多投資者加入 </br>


而這也是特斯拉在近二年進行的第二次拆股，有關第一次拆股可看：[半年漲了六倍的特斯拉（Tesla），是否該考慮賣出](https://ycjhuo.gitlab.io/blogs/How-Tesla-Soar-600-in-6-month.html)</br>

而拆股後的表現會如何呢？則可參考 [拆股一週後的特斯拉與蘋果，是否仍繼續上漲？](https://ycjhuo.gitlab.io/blogs/Are-TESLA-And-Apple-Still-Going-high-After-Stock-Split.html)


隨著特斯拉未來的發展藍圖更加明確＆市場逐漸走出升息的悲觀情緒，或許這才是特斯拉最近上漲的主要原因

::: warning
點擊觀看更多： </br>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::