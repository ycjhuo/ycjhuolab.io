---
title: Tier Match 會籍匹配，溫德姆鑽石會員（Wyndham Diamond）→ 凱薩鑽石會員（Caesars Diamond）
description: '飯店會籍匹配|飯店 Match|溫德姆 Match|Wyndham Match|Wyndham Caesars|溫德姆 凱薩 Match|溫德姆鑽石 到 凱薩|會籍 Match'
date: 2021-12-07
tags: [Hotel, Wyndham, Caesars, Tier Match]
categories: Travel
---
飯店會籍匹配流程：
::: tip
Wyndham Diamond → [Caesars Diamond]() → MGM Gold → Hyatt Explorist
:::

上篇 [Tier Match 會籍匹配，如何免費取得溫德姆鑽石會員（Wyndham Diamond）](https://ycjhuo.gitlab.io/blogs/Tier-Match-Wyndham-Diamond.html) 提到了如何取得溫德姆鑽石會員 </br>

這篇接著說，如何用溫德姆鑽石會員取得凱薩的鑽石會員

## 凱薩鑽石會員（Caesars Diamond）好處
[凱薩會籍福利表](https://www.caesars.com/myrewards/benefits-overview)

凱薩鑽石會員（Caesars Diamond）享有：免 Resort fee、免停車費以及每年 $100 的餐飲費用折抵（適用於凱薩酒店旗下的餐廳）

其中在每晚房價只須 $50 - $80 的拉斯維加斯，每晚 $40 的 Resort fee 幾乎可以到房價的 50% 了

而每年 $100 的餐飲費用折抵須一次用完，但也夠二個人吃一次 Buffet 了


::: tip
可用於餐飲費用折抵的[餐廳](https://www.caesars.com/myrewards/earn-and-redeem/on-location)
:::

## 如何匹配凱薩鑽石會員
登入凱薩會員帳號後點擊 REWARDS MATCH & EXCHANGE，再輸入溫德姆會員號碼即可

![Match Your Status to Caesars Rewards](../images/137-01.png)


## 凱薩的鑽石會員 → 溫德姆鑽石
而若是要用凱薩的鑽石會員 → 溫德姆鑽石會員（Wyndham Diamond）的話，到溫德姆的 [Match Your Status to Wyndham Rewards](https://www.wyndhamhotels.com/wyndham-rewards/caesars-rewards/status-match)

登入溫德姆會員後，輸入凱薩的會員號碼及 email 等相關資訊，二天後即可匹配成功

![Match Your Status to Wyndham Rewards](../images/137-02.png)


## 後記
從溫德姆到凱薩會籍的 Match 僅需線上即可完成，但若要享受凱薩鑽石會員的 $100 餐飲費用折抵時，則需要凱薩的實體卡片 </br>

若想知道怎麼取得凱薩實體卡，及如何用這些福利的話，可參考：</br>
[凱薩鑽石會員（Caesars Diamond）@ 大西洋城（Atlantic City）免費吃喝玩樂](https://ycjhuo.gitlab.io/blogs/Tier-Match-Caesars-Diamond-Atlantic.html)

延伸閱讀：</br>
[Tier Match 會籍匹配，凱薩鑽石會員（Caesars Diamond）→ 米高梅黃金會員（MGM Gold）](https://ycjhuo.gitlab.io/blogs/Tier-Match-Caesars-To-MGM.html)
