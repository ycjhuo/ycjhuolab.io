---
title: 特斯拉公佈（TSLA）2021 Q2 生產及交付量，交車量將實現年成長 50%
description: '美股特斯拉|特斯拉 TSLA|特斯拉 2021 Q2 第二季產量|TSLA 2021 Q2 第二季交車量'
date: 2021-07-07
tags: [Stock, TSLA]
categories: Investment
---

特斯拉從 4/26 公佈 2021 第一季度的財報後，截至上週五（07/02）股價下跌了 8%（738.2 -> 678.9）</br>
同期間，S&P 500 上漲了 3.93% </br>

在這段期間，特斯拉在 5/19，股價來到了最低點（563.49），共跌了 23.67%，但最近二個星期表現開始回升，股價上漲了 8.85%（S&P 500 則是 +2.49%）</br>

---

## 特斯拉 2021 Q2 生產/交付量（ vs. 2021 Q1）
下表是特斯拉（Tesla）在 7/2 公佈了 2021 第二季的生產＆交付量，總生產量及交付量與 2021 第一季相比，分別上升了 14.46% 與 8.86%

::: tip
上一季度（2021 Q1）與 2020 第四季（2020 Q4）相比，總生產量及交付量則是上升 0.32% 與 2.33%
:::

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q1</th>
    <th align="center">差距</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">2,340</td>
    <td align="center">0</td>
    <td align="center">N/A</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">204,081</td>
    <td align="center">180,338</td>
    <td align="center">13.17%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">1,890</td>
    <td align="center">2,030</td>
    <td align="center">-6.90%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">199,360</td>
    <td align="center">182,847</td>
    <td align="center">9.03%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">206,421</td>
    <td align="center">180,338</td>
    <td align="center">14.46%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">201,250</td>
    <td align="center">184,877</td>
    <td align="center">8.86%</td>
  </tr>
</table>

由於 2021 第一季時，Model S/X 正處在改款階段，因此生產量為 0 </br>
但在 6/10，特斯拉公佈了 Model S Plaid；這款號稱史上加速最快的電動車，從 0 到 97km 只要 1.99 秒，售價為 $129,990 </br>

因此在 2021 的第二季，Model S/X 又回歸到生產階段（雖然本季只生產了 2,340 台，交付了 1,890 台）</br>
但 Model 3/Y 生產與交付量，與上季相比，成長了 13.17% 及 9.03%

---

## 特斯拉各車款比較

下表是特斯拉每個車款的比較圖，可看出：
- Model S Plaid 在改款後，交車時間已縮短到一個月，且價格也是目前各車型中最高的

- Model Y 在售價上也高出 Model 3 約 7 - 8%，在柏林＆德州於下半年度開始生產 Model Y 後，相信在較短的交車時間下，可以吸引大部分的人從 Model 3 轉而購買 Model Y


在這二個優勢下，可以預期特斯拉的盈利能力在第三＆第四季的財報中會有著顯著的成長

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">價格</th>
    <th align="center">行駛距離（km）</th>
    <th align="center">最快速度（kmh）</th>
    <th align="center">0 - 97 km 加速度（秒）</th>
    <th align="center">預計可交車時間</th>
  </tr>
  <tr>
    <td align="center">Model S Plaid / 長程版</td>
    <td align="center">$129,990 / $79,990</td>
    <td align="center">628 / 652</td>
    <td align="center">322 / 249</td>
    <td align="center">1.99 / 3.1</td>
    <td align="center">1 個月 / 2 - 3 個月</td>
  </tr>
  <tr>
    <td align="center">Model X Plaid / 長程版</td>
    <td align="center">$114,690 / $84,990</td>
    <td align="center">547 / 579</td>
    <td align="center">262 / 249</td>
    <td align="center">2.5 / 3.8</td>
    <td align="center">6 - 7 個月</td>
  </tr>
  <tr>
    <td align="center">Model Y 長程版 / 高性能版</td>
    <td align="center">$52,990 / $60,990</td>
    <td align="center">525 / 488</td>
    <td align="center">217 / 249</td>
    <td align="center">4.8 / 3.5</td>
    <td align="center">2 - 3 個月</td>
  </tr>
  <tr>
    <td align="center">Model 3 標準/長程/高性能版</td>
    <td align="center">$39,990 / $48,990 / 56,990</td>
    <td align="center">423 / 568 / 507</td>
    <td align="center">225 / 233 / 261</td>
    <td align="center">5.3 / 4.2 / 3.1</td>
    <td align="center">2 - 3 個月</td>
  </tr>
</table>

## 後記

在 2021 年，單單前二季，Model 3/Y 的生產量（38.5 萬）就已經達到 2020 整年度（45.5 萬輛）的 84.5% 了 </br>
而 Model 3/Y 的交付量則是已達到 2020 整年度的 86.4% </br>

管理層在上季說的，將實現交付量年成長 50% 的目標，就現在看來，已不是問題了 </br>

而在還不算在上篇 [特斯拉（Tesla）2021 Q1 季報，缺席了高價車的財報依舊強勢](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q1.html) 提到的下面三點：

::: tip
- S/X 完成改款後，回歸生產階段
- 柏林及德州工廠投產後（這二廠均以生產 Model Y 為主）
- 財報上提到的，Semi 卡車將於今年開始交付
:::

- Model S 在 6/10 才完成改款，在剩下的 20 天內，就生產了 2,340 台
- 而柏林及德州工廠則能將 Model 3/Y 的產量再往上推升一個層次
- 以及年底即將開交付的 Semi 卡車

總總跡象都能看出，特斯拉股價要突破一月時的 880（距離現在股價仍有 32% 的上漲幅度），僅是時間的問題

但在公佈 2021 Q2 的交車數據後（7/2），今天（7/5）股價則是跌了 2.78%

我個人則是在特斯拉下跌的這段期間仍持續買進特斯拉

![94-01](../images/94-01.png)