---
title: Facebook（FB）2021 Q3 季報，社交龍頭是否已開始走下坡
description: '美股投資 Facebook FB|FB 2021 Q3 第三季財報|Facebook 走下坡|Facebook 庫藏股'
date: 2021-10-27
tags: [Stock, FB]
categories: Investment
---

社交龍頭 Facebook（FB）在 10/25 公佈了 2021 第三季的財報，從 2021 Q2 季報公佈（07/28 盤後），到公佈 Q3 季報（10/25 盤後），三個月的時間，股價從 373.28 → 328.69，下跌了 11.95%（同期間，S&P 500 指數上漲了 3.77%）</br>
可以看出，在 Q3 這季， Facebook 的股價表現遠遠落後於大盤，下面來分析 FB 第三季財報看看現在是不是加碼的好機會

---

Facebook 2021 Q2 財報回顧：[Facebook（FB）2021 Q2 季報，EPS 同期成長 100%](https://ycjhuo.gitlab.io/blogs/FB-Earnings-2021-Q2.html) </br>

## Facebook 2021 Q3 季報
[資料來源](https://s21.q4cdn.com/399680738/files/doc_financials/2021/q3/FB-09.30.2021-Exhibit-99.1.pdf)
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">QoQ (2021 Q1)</th>
    <th align="center">QoQ (2021 Q2)</th>
    <th align="center">QoQ (2021 Q3)</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">26,171</td>
    <td align="center">29,077</td>
    <td align="center">29,010</td>
    <td align="center">-6.77%</td>
    <td align="center">11.10%</td>
    <td align="center">-0.23%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">11,503</td>
    <td align="center">12,513</td>
    <td align="center">10,565</td>
    <td align="center">-11.89%</td>
    <td align="center">8.78%</td>
    <td align="center">-15.57%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">80.39%</td>
    <td align="center">81.43%</td>
    <td align="center">80.11%</td>
    <td align="center">-1.05%</td>
    <td align="center">1.04%</td>
    <td align="center">-1.33%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">43.48%</td>
    <td align="center">42.53%</td>
    <td align="center">35.93%</td>
    <td align="center">-2.03%</td>
    <td align="center">-0.94%</td>
    <td align="center">-6.6%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">3.3</td>
    <td align="center">3.61</td>
    <td align="center">3.22</td>
    <td align="center">-14.95%</td>
    <td align="center">9.39%</td>
    <td align="center">-10.8%</td>
  </tr>
</table>

- 營收：FB 的營收一直是以 Q1 最低，Q4 最高的趨勢向上成長，這季是首次 Q3 營收 < Q2 營收，季成長為 -0.23%
- 稅前淨利：與 Q2 相比，下滑了 15.57%；對比營收僅小幅下滑 0.23%，可以想見毛利＆營業利益率的慘況
- 毛利率：本季為 80.11%，與 Q2 的 81.43% 相比，僅小幅下降 1.33%
- 營業利益率：本季為 35.93%，是 2020 Q3 以來的最低點，與 Q2 相比，下降了 6.6%
- EPS：受到淨利下滑的影響，本季 EPS 為3.22，與 Q2 相比，下降了 10.8%

本季營業利益率下滑的主因為行政費用（General & administrative）的增加，與 Q2 相比，增加了 50.6%（約 9.9 億）</br>
猜測是為因應監管機關要求而新增了不少人力（同期，研發＆行銷費用各上升了 3.6% & 9.05%，屬於正常的增加範圍）

![Facebook 2019 - 2021 Q3 營收](../images/129-01.png)<br/>


## 使用者活躍數（DAUs）
Facebook 的使用者活躍數在本季仍持續成長，但成長幅度一年比一年少，與去年的季成長率 2 - 3% 相比，今年均在 1% 左右 </br>
- Facebook 每日活動用戶 DAUs（daily active users）：與 Q2 相比，成長了 1.05%，目前人數為 19.3 億（ Q2 成長率為 1.6%）
- 整個家族（包含 Facebook, Instagram, What's app）的日活動用戶：與 Q2 相比，成長了 1.81%，目前人數為 29.1 億（ Q2 成長率為 1.47%）
- Facebook 月活用戶：相比 Q2 成長了 0.34%，人數為 29.1 億（ Q2 成長率為 1.75%）
- 家族的月活用戶：相比 Q2 成長了 1.99%，人數為 35.8 億（ Q1 成長率為 1.74%）

- Facebook 每日活動用戶 DAUs（daily active users）：為 19.3 億，與 Q3 持平，上季成長率為 1.05%，目前人數為 19.3 億（ Q2 時為 1.6%）
- 整個家族（包含 Facebook, Instagram, What's app）的日活動用戶：為 29.1 億，一樣與上季持平，Q3 成長率為 1.81%，Q2 則是 1.47%
- Facebook 月活用戶：相比 Q2 成長了 0.34%，人數為 29.1 億（ Q2 成長率為 1.75%）
- 家族的月活用戶：相比 Q2 成長了 1.99%，人數為 35.8 億（ Q1 成長率為 1.74%）

![Facebook 2019 - 2021 Q3 DAUs](../images/129-02.png)<br/>

在使用者增速明顯下滑的時候，再來看看每位使用者對於 FB 的貢獻是否仍有成長空間

下圖顯示了：每位使用者平均為 Facebook 帶來 $10 的收入（相比 Q2 下降了 1.19%）

- 北美使用者：平均價值為 $52.34，下降了 1.26%（Q2 為 +10.37%)
- 歐洲使用者：平均價值為 $16.5，下降了 4.24% (Q2 為 +11.23%）
- 亞太使用者：平均價值為 $4.16，成長了 3.37% (Q2 為 +5.58%）
- 其他地區使用者：平均價值為 $3.14，成長了 2.95% (Q2 為 +15.53%）

![Facebook 2020 - 2021 Q3 ARPU](../images/129-03.png)<br/>

人均貢獻度較高的北美＆歐洲市場在 Q3 均呈現下降趨勢 </br>
雖亞太＆其他區域的貢獻度持續成長，但相比 Q2 的成長幅度，仍減緩不少 </br>
也連帶了影響了平均每位使用者價值的下滑 </br>


## 庫藏股
在第三季，FB 已花了 134.6 億在回購股票上，而目前還有 79.7 億可繼續用於回購上 </br>
相比 Q2 投入的 70.8 億，本季回購金額成長了 90% </br>
同時，在 9/30，FB 又授權了一筆 500 億的額度可用於回購，目前公司現金約 580.8 億（截至 9/30）</br>
可惜，本次回購並未讓股價跑贏大盤（FB 股價在這期間表現為 -11.95%；S&P 500 上漲了 3.77%）

## 後記
在財報中，FB 預測 Q4 營收將落在 315 - 340 億之間，對比 Q3 的 290 億，季成長率約在 8.6% - 17% 之間 </br>
若真能達成，且營業利益率不繼續下滑的前提下，對穩定股價是一大利多 </br>

Facebook Q3 財報，除了庫藏股金額大幅增加 90% 這個優點外，其餘數值均處於下滑狀態，找不到其他亮點可繼續買入 FB </br>
雖本益比來到新低的 22.15（疫情爆發時本益比仍有 23），要趁機加碼 FB 仍須注意財報下滑中透露的風險