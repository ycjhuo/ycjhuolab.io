---
title: 紐約尼加拉瀑布（Niagara Falls）三日遊 霧中少女號（Maid of the Mist），風洞（Cave of the Winds），山羊島（Goat Island） ，漩渦公園（Whirlpool State Park）
description: ' '
date: 2021-11-21
tags: [New York, Niagara Falls]
categories: Travel
---

這次的尼加拉瀑布三天二夜，行程規劃是：
Day 1: 山羊島看夜景
Day 2: 尼加拉瀑布州立公園 -> 風洞 -> 搭乘霧中少女號
Day 3: 漩渦公園

## 霧中少女號（Maid of the Mist）


## 山羊島（Goat Island）


## 漩渦公園（Whirlpool State Park）