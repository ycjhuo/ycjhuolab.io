---
title: Meta（FB）2021 Q4 季報，股價崩跌 36%，是否能加碼了
description: 'Meta FB 2021 Q4 第四季財報｜Facebook FB 2021 Q4 第四季財報｜Facebook 改名 Meta|FB 公布財報後大跌｜Facebook 使用者停滯|Facebook 退出歐盟市場'
date: 2022-02-20
tags: [Stock, FB]
categories: Investment
---

社交龍頭 Meta（FB）在 2/2 公佈了 2021 第四季的財報，從 2021 Q3 季報公佈（10/25），到公佈 Q4 季報（2/2），三個月的時間，股價從 328.69 → 323，下跌了 1.73%（同期間，S&P 500 指數也下跌了 0.5%）</br>

雖說在這個區間 FB 股價表現與大盤差不多，但在第四季財報一發布，股價隨即下跌了 26%，截至現在，也就是股價發布後第 16 天，股價已下跌了 36%，本益比也來到了 14.97 倍

下面就來分析 FB 第四季財報，看看在 FB 前景仍不明朗的現在，是不是加碼的好機會

## Meta 2021 Q4 季報
[資料來源](https://s21.q4cdn.com/399680738/files/doc_financials/2021/q3/FB-09.30.2021-Exhibit-99.1.pdf)
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">29,077</td>
    <td align="center">29,010</td>
    <td align="center">33,671</td>
    <td align="center">11.10%</td>
    <td align="center">-0.23%</td>
    <td align="center">16.07%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">12,513</td>
    <td align="center">10,565</td>
    <td align="center">12,702</td>
    <td align="center">8.78%</td>
    <td align="center">-15.57%</td>
    <td align="center">20.23%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">81.43%</td>
    <td align="center">80.11%</td>
    <td align="center">81.15%</td>
    <td align="center">1.04%</td>
    <td align="center">-1.33%</td>
    <td align="center">1.04%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">42.53%</td>
    <td align="center">35.93%</td>
    <td align="center">37.38%</td>
    <td align="center">-0.94%</td>
    <td align="center">-6.6%</td>
    <td align="center">1.45%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">3.61</td>
    <td align="center">3.22</td>
    <td align="center">3.67</td>
    <td align="center">9.39%</td>
    <td align="center">-10.8%</td>
    <td align="center">13.98%</td>
  </tr>
</table>

- 營收：與 Q3 相比，本季營收成長了 16.07%，其中佔了總營收 97% 的廣告收入在本季成長了 15.43%
- 稅前淨利：與 Q3 相比，本季營收成長了 20.23%，由於成長幅度高於營收，可以想見本季的毛利率＆營業利益率是高於 Q3 的
- 毛利率：本季為 81.15%，在 2021 的 4 個季度均保持在 80% - 81%，表示 FB 的產品盈利能力仍未下滑
- 營業利益率：本季為 37.38%，2021 的 4 個季度均保持在 36% - 43%，平均為 40%（2020 年為 38%），顯示了即使在燒錢發展「元宇宙」的情況下，FB 仍保持著一定的獲利能力
- EPS：本季 EPS 為 3.67，與 Q3 相比，上升了近 14%；若以年來看，2021 年的 EPS 為 13.77，比 2020 年成長了 36%

本季開始，FB 在財報中將營收拆成「廣告」＆「Reality Labs」，但因爲 RL 目前佔的比率仍不到總營收的 3%，這邊就不獨立出來說明

## FB 使用者活躍數（DAUs）
- Facebook 日活用戶 DAUs（daily active users）：FB 在本季的 DAU 為 19.3 億，與 Q3 持平（上季成長率為 1.05%，Q2 時為 1.6%）
- Facebook 月活用戶：每月活躍數為 29.1 億，一樣與上季一樣（Q3 成長率為 0.34%，Q2 則是 1.75%）
- 整個家族（包含 Facebook, Instagram, What's app）的日活動用戶：若加上 Instagrame & What's app 後，每日活躍數則是 28.2 億，與 Q3 相比，成長了 0.36%（Q3 成長率為 1.81%）
- 家族的月活用戶：家族的月活用數則是 35.9 億，與上季相比成長了 0.28%（Q3 成長率為 1.99%）

這一季是 Facebook 首次面臨到用戶數的停滯，是在每日或是每月的使用者都一樣，表示往後 FB 僅能依靠 Instagram 與 What's app 這二個平台來帶動成長率，但這二個平台的利潤並沒有 Facebook 來得高

CFO 在本次財報中提到，雖然人們開始傾向使用 Instagram 的短視頻 Reels，但 Reels 的貨幣轉化率不比 Feed & Stories 低，表示 FB 在 Facebook 用戶停滯的同時，也要嚴防未來使用者即將下滑的處境，這也是 FB 在發布財報後，股價大跌 26.25% 的主要原因

下圖為每個地區的 FB 使用者平均貢獻度：
![Facebook 2020 Q4 - 2021 Q4 ARPU](../images/159-1.png)<br/>



### 後記
在本次財報中，CFO 預測 2022 Q1 的總營收會位於 270 - 290 億之間，YoY 是 3 - 11%，這個營收其實與 2021 Q2 & Q3 的營收差不多，但若與 Q4 的營收 337 億比較的話，其實是減少了 14% 左右，（但 FB 的營收趨勢本來就是 Q1 最低而 Q4 最高）

下圖為 FB 2019 Q4 - 2021 Q4 的每季營收，並標出每個市場的貢獻度：
![Facebook 2019 Q4 - 2021 Q4 Revenue](../images/159-2.png)<br/>

CFO 同時也說了，在 2022 年 FB 營收仍會持續受到 iOS 隱私權改版的影響，以及可能會退出歐盟市場

本來想說，若 FB 未來不再繼續成長，不過能保持現在的營收＆利潤的話，其實以現在的價位（本益比 14.97），仍是一個穩定可投資的標的，但若是退出歐盟市場的話，那才是比 iOS 隱私權政策更為嚴重的影響

雖然歐洲使用者的貢獻度不及北美，但仍是亞太＆其他地區使用者的 4 倍，且因為 FB 的毛利很高（81%），在放棄了歐洲業務後，其實也不會為公司省多少錢，整體來說仍是「弊 > 利」的，但若算上未來歐盟可能的天價罰款，可能就不一定了

考慮到 FB 每季皆投入不少金額在回購庫藏股（Q4 回購金額約 200 億），若 FB 僅受到 iOS 隱私權的影響，而不會退出歐盟市場的話，其實以 FB 目前仍在廣告市場的地位＆收入來說，仍是處於一個可以買入的價位，畢竟有著源源不絕的現金流入，就算「元宇宙」的前景不明，仍有足夠的資金＆收入來支撐 FB 這家公司
