---
title: Potato Media 積分加成？能多獲得多少 CFO
description: 'Potato Media 介紹 | Potato Media 積分加成 | Potato Media CFO | Potato Media 推薦 | Potato Media 百大創作者'
date: 2022-01-07
tags: [Blog, CFO, Potato Media, SocialFi, Crypto]
categories: Blog
---

2020/09 時，Potato Media 舉辦了百大創作者的活動，那時 11 - 100 名的作者都可獲得一個月（實際只有 27 天）的每日積分加成 20%（10/04 - 10/31）
這篇來分享積分加成的效果怎麼樣

上篇[真的能在 Potato Media 賺到錢嗎？公開 Potato Media 收益](https://ycjhuo.gitlab.io/blogs/Potato-Media-Earnings.html) 提到

::: tip
我從 2021/08/13 開始使用 Patato Media，到現在約 2 個月的時間，總共獲得約 83 CFO

每月約 41.5 CFO（約 $22.35 美金，換成台幣約 $625）
平均每天，約為 $1.38 CFO（約 $0.74 美金，換成台幣約 $21）
我每天的積分大概都落在 1,300 - 1,800，取得的 CFO 約是 0.8 - 1.42
:::


## Potato Media 積分加成
積分加成 20% 是每日結算，在「積分等級」頁面的「積分紀錄」可看到積分加成的獎勵是額外獨立出來的，每日結算一次

![Potato Media 積分加成](../images/147-01.png)

在這加成的 27 天中，我每天獲得的（未加成）積分大部分位在 1,000 - 1,400 之間，平均為 1285
而 CFO 位在 1.2 - 1.8 之間，平均為 1.5 左右，對比未加成前的 CFO 每天約 1.42

這樣比下來，20% 積分加成，約讓我每天的 CFO 增加 8%，也就是多了 0.08 CFO

## 後記
因為我的文章並不屬於熱門 or 爆文這種，所以 20% 的積分加成其實影響不大

但因為每天得到的 CFO 數量，是看當天的積分總數

因此，對於文章本身就有不錯迴響的創作者而言，在積分加成後，獲得的 CFO 就會更加可觀

而因為每天 CFO 發放的總數是固定的，有人拿得更多，也就表示有人會拿得更少

或許，未來 Potato Medio 可能會朝著貧者越貧，富者越富的現實社會靠攏？


## Potato Media 推薦碼
若覺得這篇文章有幫助你更了解 Potato Media，或是看完後也想加入 Potato Media 的話 </br>

請用我的[推薦碼](https://www.potatomedia.co/signup?invite=pRUdIeRnB)註冊，成為會員，請用我的 推薦碼 註冊，成為會員，你會得到 15 CFO，我會得到 2 CFO （目前 1 個 CFO 約等於 8 - 11 台幣左右）</br>

或是到 [我的 Potato Media 頁面](https://www.potatomedia.co/user/83aed257-a06a-4b3c-beee-e356c2ba1efb) 看我的文章（無須註冊即可觀看）</br>

若對於 Potato Media 有其他問題也可留言，我會就我所知的盡量解答 </br>