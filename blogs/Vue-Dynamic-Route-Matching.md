---
title: 如何在 Vue 中使用動態路由來動態改變網址
description: 'Vue 介紹|Vue 教學|Vue 動態路由|Vue 改網址|Vue 路由 網址|Vue 自訂 網址'
date: 2021-01-31 20:50:26
tags: [Vue.js, Router]
categories: Programming
---

在上一篇 [如何在 Vue-Cli 使用路由來新增頁面](https://ycjhuo.gitlab.io/blogs/Vue-Router-Add-New-Pages.html) 提到了如何新增頁面。但在最後，我們發現網址中都會有一個 #。該如何消除網址中的 # 呢？其實只要在 ``` index.js ``` 中加入 history 就可以了。

## 消除網址中的 # (hash)
```index.js```
```js{29}
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Account from '../views/Account.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/account',
    component: Account
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
```

## 動態路由
若我們想要在既有的網址中加上一個變數，讓網址可隨著變數，跳轉到不同的頁面該如何做呢？</br>
這裡我們一樣用上篇的例子。下面我們要上篇中新增的 Account 頁面，根據不同的登入的使用者在頁面上網址列顯示他們的 User ID。</br>
1. 先在第 5 行，新增對 User 的歡迎訊息， 且利用 computed 來取得目前登入的 User ID

```Account.vue```
```js{4,10-14}
<template>
  <div>
    <h1>This is an account page</h1>
    <p> Hello, {{userId}} </p>
  </div>
</template>

<script>
export default {
    computed: {
        userId() {
            return this.$route.params.userId
        }
    }
}
</script>
```

2. 在路由頁面中，將 account 頁面的從原本的 ```/account``` 改為在後面新增 userId 變數

```index.js```
```js{23}
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Account from '../views/Account.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/account/:userId',
    component: Account
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
```

3. 最後，在主頁面中的 script 區塊，我們設定一個 User ID，並在上方的 router link 中用 v-bind 方式取得 User ID

```App.vue```
```js{6,12-20}
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link> |
      <router-link to="/about">About</router-link> |
      <router-link v-bind:to="'/account/'+userId">Account</router-link>
    </div>
    <router-view/>
  </div>
</template>

<script>
export default {
  data() {
    return {
      userId: 'Leon'
    }
  }
}
</script>
```

![52-01](../images/52-01.png)

## 禁止使用上一頁
另外，若我們在 Account 這個頁面若不允許使用者按上一頁的話，可以直接在 ```router-link``` 這個 tag 中直接加上 replace 就可以實現這個功能了

如 ```<router-link :to="'/account/'+userId" replace>Account</router-link>```