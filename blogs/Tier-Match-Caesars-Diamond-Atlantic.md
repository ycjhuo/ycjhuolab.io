---
title: 凱薩鑽石會員（Caesars Diamond）@ 大西洋城（Atlantic City）免費吃喝玩樂
description: '飯店會籍匹配|飯店 Match|Caesars Match|凱薩 Match|Caesars MGM Match|凱薩 米高梅 Match|凱薩鑽石 到 MGM|會籍 Match|凱薩 大西洋城|凱薩 實體卡|凱薩 Celebration Dinner|Caesar Celebration Dinner'
date: 2021-12-08
tags: [Hotel, Caesars, Tier Match]
categories: Travel
---

飯店會籍匹配流程：
::: tip
Wyndham Diamond → [Caesars Diamond]() → MGM Gold → Hyatt Explorist
:::

上篇 [Tier Match 會籍匹配，溫德姆鑽石會員（Wyndham Diamond）到凱薩鑽石會員（Caesars Diamond）](https://ycjhuo.gitlab.io/blogs/Tier-Match-Wyndham-To-Caesars.html) 已經讓我們成功得到凱薩鑽石會員了

這篇接著說，如何用凱薩鑽石會員怎麼在紐澤西的大西洋城免費吃喝玩樂

## 凱薩實體會員卡
首先到大西洋城的凱薩飯店（Caesars Atlantic City）的 Caesars Rewards 櫃檯，給櫃檯人員看 ID 以及 App 上的凱薩鑽石會籍，即可得到凱薩的實體卡（同時要設定 4 位數字作為卡的 PIN 碼）

![Caesars Rewards Desk](../images/138-01.jpg)

這張卡有二個福利：
1. 兌換 Free Slot Play（$5）
2. 兌換凱薩鑽石會員每年 $100 的餐飲費用折抵（Celebration Dinner）

::: tip
凱薩鑽石會員所提供的 Celebration Dinner 福利，有限期限是當年度的 02/01 - 下一年的 01/31
:::


## 兌換 Free Slot Play

拿到實體卡後就可到櫃台旁的 Rewards Kiosks 插入實體卡，並輸入 PIN 碼後，就可看到裡面有 1,000 的 Reward Credits

![Caesars Rewards Kiosks](../images/138-02.jpg)
![Caesars Rewards Kiosks](../images/138-03.jpg)

而這 1,000 的 Reward Credits 可以直接兌換成 $5 的 Free Slot Play

![Caesars Rewards Credits to Free Slot Play](../images/138-04.jpg)
![Caesars Rewards Credits to Free Slot Play](../images/138-05.jpg)
![Caesars Rewards Credits to Free Slot Play](../images/138-06.jpg)

兌換完成後，這張卡裡面就有 $5 可以讓我們在 Slots 機玩了，這 $5 是不能兌換成現金的，但你用這 $5 贏到的錢就可以提領（Cash Out）出來

這些 Slot 機並不會顯示你贏了多少錢，因此若只是想把送的 $5 用完的話，要自己記住已經花了多少錢，或是玩幾局後就先 Cash Out

這 $5 有明確寫說是 Free Slot Play，因此不能用在牌桌上

::: tip
Slot 機有很多飲料可以點，都是免費的，若是要收費會另外標示
:::

## 會員餐飲費用折抵
凱薩鑽石會員每年 $100 的餐飲費用折抵可以用在凱薩旗下的飯店，我們在前往大西洋城之前就已先在 Open Table 上預約了飯店（Caesars Atlantic City）內的 Gordon Ramsay Pub & Grill

::: tip
可用於餐飲費用折抵的[餐廳](https://www.caesars.com/myrewards/earn-and-redeem/on-location)
:::

因此在兌換到實體卡後，即可馬上到餐廳使用，Gordon Ramsay Pub & Grill 的午/晚餐菜單不一樣，我們這次是預約中午時段

點了 4 個前菜、2 個主餐、2 個甜點，僅花了 $180 左右，結帳時直接把實體卡交給服務人員即可直接折抵
我們給了二張，因此僅須付小費（現金）

實體卡的 $100 折抵，只限單次使用，但可一次使用多張，不找零，也不能保留下次使用

![Gordon Ramsay Pub & Grill 午餐](../images/138-07.jpg)
![Gordon Ramsay Pub & Grill 甜點](../images/138-08.jpg)

## 後記
雖說凱薩的大本營在拉斯維加斯，但對住在美東的而言，大西洋城算是最容易使用到凱薩福利的地方了，且到了大西洋城拿了實體卡＆享受完凱薩福利後，還可順便到附近的波哥大酒店（Borgata），將凱薩鑽石匹配成 MGM 金卡會員（二間飯店開車約 10 分鐘）

延伸閱讀：</br>
[Tier Match 會籍匹配，凱薩鑽石會員（Caesars Diamond）→ 米高梅黃金會員（MGM Gold）](https://ycjhuo.gitlab.io/blogs/Tier-Match-Caesars-To-MGM.html)
