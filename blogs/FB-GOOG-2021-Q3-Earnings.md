---
title: 2021 Q3 財報，Facebook vs. Google，都是線上廣告，表現卻不同
description: '美股投資 Facebook Google 比較|Facebook Google 2021 Q3 第三季財報|FB GOOG 哪個好|Facebook vs. Google 線上廣告'
date: 2021-11-05
tags: [Stock, FB, GOOG]
categories: Investment
---

在 Facebook & Google 公佈 2021 Q3 財報後，股價表現在這段期間卻截然不同 </br>
FB 股價在這季表現為 -11.95%，Google 則是 +2.1%，而大盤指數 S&P 500 則是 3.94% </br>
可以看出這季 FB & Google 的表現均落後大盤，而這二間數位廣告巨頭是否仍是適合的投資標的？下面來看看他們的財報分析：

## 獲利能力 FB vs. Google

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">FB Q3 </th>
    <th align="center">GOOG Q3</th>
    <th align="center">FB QoQ（Q2）</th>
    <th align="center">GOOG QoQ（Q2）</th>
    <th align="center">FB QoQ（Q3）</th>
    <th align="center">GOOG QoQ（Q3）</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">29,010</td>
    <td align="center">65,118</td>
    <td align="center">11.1%</td>
    <td align="center">11.87%</td>
    <td align="center">-0.23%</td>
    <td align="center">5.23%</td>
  </tr>
  <tr>
    <td align="center">廣告業務營收（百萬）</td>
    <td align="center">28,276</td>
    <td align="center">45,131</td>
    <td align="center">12.35%</td>
    <td align="center">13.1%</td>
    <td align="center">-1.06%</td>
    <td align="center">5.33%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">10,565</td>
    <td align="center">23,064</td>
    <td align="center">8.78%</td>
    <td align="center">3.3%</td>
    <td align="center">-15.57%</td>
    <td align="center">4.91%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">80.11%</td>
    <td align="center">67.7%</td>
    <td align="center">12.96%</td>
    <td align="center">9.37%</td>
    <td align="center">-1.32%</td>
    <td align="center">-1.01%</td>
  </tr>
  <tr>
    <td align="center">營業利潤率</td>
    <td align="center">35.93%</td>
    <td align="center">32.3%</td>
    <td align="center">-0.94%</td>
    <td align="center">1.57%</td>
    <td align="center">-6.6%</td>
    <td align="center">1.01%</td>
  </tr>
  <tr>
    <td align="center">淨利率</td>
    <td align="center">31.69%</td>
    <td align="center">29.08%</td>
    <td align="center">-0.54%</td>
    <td align="center">-2.48%</td>
    <td align="center">-4.06%</td>
    <td align="center">-0.86%</td>
  </tr>
  <tr>
    <td align="center">EPS（Dilited）</td>
    <td align="center">3.22</td>
    <td align="center">36.38</td>
    <td align="center">9.39%</td>
    <td align="center">-6.93%</td>
    <td align="center">-10.8%</td>
    <td align="center">33.46%</td>
  </tr>
</table>

- 總營收：Google 總營收為 FB 的 240%（ Q2 時為 212.81% ），可看出 Google 在總營收成長率這方面優於 FB
- 廣告業務營收：僅看廣告業務這部分的營收的話，Google 約為 FB 的 160%（ Q2 時為 149.92% ），單看線上廣告這塊的話，成長率仍然是 Google > FB
- 稅前淨利：Google 為 FB 的 218%（ Q2 時為 175.7% ），總營收為 FB 2.4 倍的 Google，在稅前淨利這項差距略為縮小，顯示毛利率方面 FB 應該是 > Google 的
- 毛利率：因 Google 的業務多元化特性，毛利率 < FB 約 12.41%（Q2 時相差 12.72%），變化不大 </br>
Google & FB 在 Q3 的毛利率分別為：80.11% & 67.7%，二者的毛利率在本季都小幅下降 1%
- 營利率：FB > Google 約 3.63%（ Q2 相差了 11.24% ），顯示了 FB 的營利率在 Q3 有著大幅的下降
- 淨利率：FB > Google 約 2.61（ Q2 相差了 5.81%），若這個幅度不變的話，Q4 時 Google 的淨利率將會超過 FB
- EPS：不同於 FB 在 Q3 EPS 季成長衰退了 10.8%，Google 在本季 EPS 季成長了 33.46％

總歸上面七點，FB 除了在毛利率有較明顯的優勢外，營業利潤率＆淨利率都與 Google 差別不大，這也就代表著若 FB 無法在營收上拉近與 Google 的距離，則最後的獲利也不會減少與 Google 的差距 </br>
且就 FB 的 Q3 的季成長（QoQ）來看，各項指標均處於衰退/下降的趨勢，完全沒有因為市值小而成長速度較快的優勢（Google 市值約為 FB 的 2.1 倍）</br>


## 庫藏股 Facebook vs. Google

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">FB Q2</th>
    <th align="center">GOOG Q2</th>
    <th align="center">FB Q3</th>
    <th align="center">GOOG Q3</th>
    <th align="center">FB QoQ（Q2）</th>
    <th align="center">GOOG QoQ（Q2）</th>
    <th align="center">FB QoQ（Q3）</th>
    <th align="center">GOOG QoQ（Q3）</th>
  </tr>
  <tr>
    <td align="center">回購金額（百萬）</td>
    <td align="center">7,079</td>
    <td align="center">12,796</td>
    <td align="center">13,457</td>
    <td align="center">12,610</td>
    <td align="center">11.1%</td>
    <td align="center">11.87%</td>
    <td align="center">90.10%</td>
    <td align="center">-1.45%</td>
  </tr>
  <tr>
    <td align="center">股價變化（%）</td>
    <td align="center">21.60%</td>
    <td align="center">18.59%</td>
    <td align="center">-11.95%</td>
    <td align="center">2.10%</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

從庫藏股方面來看：</br>
- FB 在 Q2 時回購了 70.8 億的股票，推升股價 +21.6%；而 Q3 更是將回購金額增加 1.9 倍，來到了 135 億，但股價表現卻是 -11.95%
- GOOG 在 Q2 時回購金額為 128 億，股價 + 18.59%；Q3 時回購了 126 億，股價 + 2.1%

Google 在每季的回購金額較為平均，股價也都維持著正成長；相反的，FB 即使加碼了回購金額，卻仍無法逃過股價下跌 </br>

但因為財報是在 10/25（FB） & 10/26（GOOG） 公佈，而股價變化的區間，我是取 Q2 - Q3 公佈財報的這段期間（GOOG：07/27 - 10/26）&（FB：07/28 - 10/25）</br>

因此，並無法在財報公佈前就知道這些財務指標，所以 FB 在這段期間的下跌，即是投資人已先預見/猜測 Q3 財報的結果，進而賣出 FB 股票 </br>

## 後記
雖然 FB 在 Q3 財報公佈後，即宣布將公司名稱改為 Meta，宣示發展「元宇宙」（Megaverse）的決心 </br>

但 FB 在營收比例上，線上廣告就佔了97.5%，表示 VR & 其他業務僅佔了 2.5% 的營收，且仍處於虧損狀態 </br>

這顯示了 FB 在 2014 年用 20 億收購的 Oculus 在經過 7 年後，仍無法幫助 FB 走出一條新的道路 </br>

而「元宇宙」雖然帶起了一波風潮，但在欠缺殺手級應用的現在，在短期內無法帶給 FB 太多幫助 </br>

目前 FB 本益比為 24.5，在 Q3 財報公佈時有到達 23.5 的價位（去年疫情最嚴重時是 23.04），而 Google 目前本益比為 28.87，相比之下，FB 算是在一個便宜的價位，有著 Facebook/Instagram 這二個金雞母，就算轉型「元宇宙」之路漫長，但線上廣告仍能為 FB 帶來源源不絕的營收，而我也在這段期間加碼了一些 </br>

![Facebook Q3 買入紀錄](../images/132-01.png)<br/>


延伸閱讀： </br>
[Facebook（FB）2021 Q3 季報，社交龍頭是否已開始走下坡](https://ycjhuo.gitlab.io/blogs/FB-Earnings-2021-Q3.html) </br>
[Google（GOOG）2021 Q3 季報，成長動能依然不減](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q3.html) </br>
