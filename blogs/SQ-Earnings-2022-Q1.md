---
title: Block（SQ）公佈 2022 Q1 財報，收購 Afterpay 後的第一次財報
description: 'Block SQ 2022 Q1 第一季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報|Block 收購 Afterpay|Block 收購先買後付平台|Block buy now pay later” (BNPL)'
date: 2022-05-19
tags: [Stock, SQ]
categories: Investment
---

Block（股票代號：SQ），從 2021 Q4 財報發布（02/24），到 2022 Q1 財報發布（05/03）期間，股價僅小漲 0.6%（$94.99 → $95.55），同期間那斯達克指數（Nasdaq Index）卻跌了 8%</br>

這是否代表著 SQ 股價已經止跌回升了嗎？畢竟 SQ 的股價從今年以來已經跌了 42%，距離腰斬也不遠了，而今年那斯達克指數跌了 19%，可看出 SQ 的跌幅足足是那斯達克的 2 倍

在目前的市場氛圍下，SQ 極有可能再往下跌，而這次財報也並未給投資人帶來信心，反而揭露了 Q2 的虧損仍會繼續擴大，下面來看看在收購了 Afterpay 後，對於 SQ 的財報有哪些影響

複習 Block 2021 Q4 財報：</br>
[Block（SQ）公佈 2021 Q4 財報，股價大漲 26%](https://ycjhuo.gitlab.io/blogs/SQ-Earnings-2021-Q4.html)

## Block 財務指標
[來源](https://investors.block.xyz/overview/default.aspx)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4</th>
    <th align="center">2022 Q1</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
  </tr>
  <tr>
    <td align="center">營收（億）</td>
    <td align="center">38.45 億</td>
    <td align="center">40.79 億</td>
    <td align="center">39.61 億</td>
    <td align="center">6.08%</td>
    <td align="center">-2.89%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">29.47%</td>
    <td align="center">28.98%</td>
    <td align="center">32.70%</td>
    <td align="center">-0.49%</td>
    <td align="center">3.71%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">0.6%</td>
    <td align="center">-1.34%</td>
    <td align="center">-5.73%</td>
    <td align="center">-1.94%</td>
    <td align="center">-4.39%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">N/A</td>
    <td align="center">-0.17</td>
    <td align="center">-0.38</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：上季才說到 Block 總算擺脫連續二季的負成長，營收回到 40 億以上，結果本季再度下跌到 39.61 億，與上季相比，營收減少了約 3%
- 毛利率：由於利潤最低的 Bitcoin 業務，在本季營收衰退了 12%，加上利潤最高的訂閱收入成長 24%，將 Block 的毛利率拉升到 32.7%，較上季成長 3.71%
- 營業利潤率：本季行政費用＆交易貸款損失分別上漲了 49%、60%，讓本季營利率來到 -5.73%，是近 8 個季度以來的最低值
- EPS：本季 EPS 也來到 -0.38，較上季 -0.17 虧損多一倍以上

上面可看出 Block 財報的各項數值，除了毛利率成長外，其它均呈現衰退趨勢，尤其是在營業費用大幅增加的情況下，營收也不升反降，雙重打擊下，讓本季的虧損相比上季又擴大一倍，也難怪目前股價仍呈現緩慢下跌趨勢

接著來看目前 Block 的業務近況

::: tip
AfterPay 所帶來的營收被歸類在訂閱＆服務收入類別，Square & Cash App 各佔一半
:::

## Block 業務發展
Block 目前由 Square 與 Cash App 這二大業務組成，這二大塊在利潤佔比上各約 50%，若以營收來看則約 40% vs. 60%，由此可看出 Square 的毛利高出 Cash App 許多，能以較少的營收創造出相同的利潤

可惜的是，從財報資料顯示，這二個業務成長性都不夠高，二者的每季的利潤平均成長率僅約 4% - 5%，身為成長股，但這點成長幅度卻不夠達到投資人的預期

財報中給出的業務指引表示了第二季的虧損仍會較本季多，官方給出的說法是：目前以加強投資＆鞏固現有的產品線為主，盈利暫不是公司的首要事項，令人不禁擔心起，是否今年的四個季度均無法實現獲利

而 Block 在今年的 1/31 完成了對先買後付平台 Afterpay 的收購，Afterpay 在本季貢獻了 1.3 億的營收＆0.92 億的毛利，約佔總營收的 3％、總利潤的 7.1%

這也驗證了為什麼 Block 在本季毛利會上升 3.7% 的原因，但同時 Afterpay 也造成 Block 本季的行政費用＆交易貸款大幅上漲，讓本季的營業利潤虧損擴大 4 倍，從原本的虧損 0.55 億，變成虧損 2.27 億

更為致命的是，據 SEC Form 4 的資料來看，今年以來共有 4 名高階主管紛紛拋售股票，其中 Block 的財務長（CFO）更是在財報發佈的前一天減持約 3,800 股，今年共拋售了 12 萬股，平均價格約 $127，雖說今年賣出的股票僅佔她個人的 0.6%，但身為最了解公司狀況的財務長都在減持股票，難免讓投資者對於 Block 的未來感到擔憂

## 後記
自 2020 買進 SQ 的這二年來，看著它從疫情開始時的最低點 $38，一路漲到去年中的 $275，漲幅高達 723%，而最高點那時，同時也是 Block 宣布收購 Afterpay 的時間點，隨後股價一路下跌，在九個月後的現在，股價已跌到 $82，跌幅高達 70%，距離之前的高點還有 235% 的上漲距離

尤其目前處在加息的情況下，大部分公司的股價都已受到打壓，而這點對於仍在虧損的企業上更為明顯

但值得慶幸的是 SQ 目前的自由現金流仍是正的，帳上現金也仍充足，並沒有倒閉的風險，且在付清與收購 Afterpay 相關的一次性費用後，依靠 Afterpay 帶來的營收＆較高的利潤幫助下，SQ 即能創下歷年來都達不到的 EPS 目標，屆時股價也就能再起了

只可惜這時間並不會馬上發生，最快也得等到 2022 Q3 才有可能看到 SQ 實現小幅盈利、或是損益二平


::: warning
點擊觀看更多： </br>
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::