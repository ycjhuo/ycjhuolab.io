---
title: 公佈第四季及 2020 全年財報，遭到大股東拋售的騰訊表現如何？
description: '騰訊 TCEHY 2020 財報分析|騰訊美股 ADR|大股東減持騰訊|美股買騰訊'
date: 2021-04-10 22:21:45
tags: [Stock, TCEHY]
categories: Investment
---
中國社交巨頭騰訊（Tencent，美股代號：TCEHY）在上週四（04/08）遭到大股東 Naspers 拋售股票，導致騰訊在當日一開盤就爆跌 9.65% </br>

這是否代表著騰訊走下坡了呢？而在近一年中，騰訊的表現皆略落後納斯達克指數</br>
來看看騰訊在 03/24 發佈的 2020 Q4 及全年財報，決定騰訊是否仍是個不錯的投資標的

## 騰訊（TCEHY） Q4 季報 及全年財報
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
  </tr>
  <tr>
    <td align="center">營收（RMB in million）</td>
    <td align="center">133,669 vs. 125,447</td>
    <td align="center">482,064 vs. 377,289</td>
  </tr>
  <tr>
    <td align="center">營收成長率</td>
    <td align="center">6.55%</td>
    <td align="center">27.77%</td>
  </tr>
  <tr>
    <td align="center">其他收益（RMB in million）</td>
    <td align="center">32,936 vs. 11,551</td>
    <td align="center">57,131 vs. 19,689</td>
  </tr>
  <tr>
    <td align="center">其他收益成長率</td>
    <td align="center">185.14%</td>
    <td align="center">190.17%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">44.05% vs. 45.16%</td>
    <td align="center">45.95% vs. 44.40%</td>
  </tr>
  <tr>
    <td align="center">毛利率增長</td>
    <td align="center">-1.11%</td>
    <td align="center">1.55%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">47.66% vs. 35.04%</td>
    <td align="center">38.22% vs. 31.46%</td>
  </tr>
  <tr>
    <td align="center">營業利益率增長</td>
    <td align="center">12.63%</td>
    <td align="center">6.76%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">6.24 vs. 4.06</td>
    <td align="center">16.84 vs. 9.86</td>
  </tr>
</table>

- 營收方面，Q4 營收相比 Q3，成長了 6.55%，年營收成長了 27.8%，毛利率變化不大；皆位於 44 - 45% 左右 </br>
- 營業利益率：受益於其他收益（Other gains）的影響，Q4 的營業利益率（47.66%）甚至高於 Q4 的毛利率（44.05%） </br>
- EPS：也是因為其他收益，相較 Q3 及 2019 年，分別提高了 13.41% 及 70.9% </br>

--- 

可以看出，在這次財報中成長幅度最大的無疑是其他收益（Other gains）：</br>
- Q4 相比 Q3，成長了 185.14%；在 2020 全年財報中也比 2019 成長了 190.17%
- 且其他收益在 Q4 中，約等於總營收的 25%，毛利的 56%；在 2020 全年財報中，約是營收的 12%，毛利的 26%

但因為其他收益並不是能夠持續增長的項目，也無法保證在未來是否能維持這麼高的其他收益</br>
因此若我們將其他收益去掉的話，各項數值會有什麼變化呢？</br>

## 財務指標（扣除其他收益）

下表直接將其他收益視為 0，在不考慮稅率的情況下，直接將稅後淨利（Profit for the period）減去其他收益，而得出新的 EPS


<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">23.02% vs. 25.83%</td>
    <td align="center">26.37% vs.26.24%</td>
  </tr>
  <tr>
    <td align="center">營業利益率（無其他收益） vs. 原營業利益率</td>
    <td align="center">-24.64% vs. -9.21%</td>
    <td align="center">-11.85% vs. -5.22%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">2.78 vs. 2.85</td>
    <td align="center">10.83 vs. 7.83</td>
  </tr>
  <tr>
    <td align="center">EPS（無其他收益） vs. 原 EPS</td>
    <td align="center">-55.48% vs. -29.69%</td>
    <td align="center">-35.68% vs. -20.53%</td>
  </tr>
</table>

可以看到，在排除了其他收益的情況下，營業利益率減少了 5% - 24.64% 不等，EPS 也減少了 20% - 55% 左右 </br>

## 使用者月活數（Monthly active users）
單位百萬（In millions）:
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">用戶增減（％）</th>
  </tr>
  <tr>
    <td align="center">微信 + WeChat 月活數（MAU）</td>
    <td align="center">1,225 vs. 1,212.8</td>
    <td align="center">1.01%</td>
  </tr>
  <tr>
    <td align="center">QQ 智能終端月活數（Smart device MAU of QQ）</td>
    <td align="center">594.9 vs. 617.4</td>
    <td align="center">-3.64%</td>
  </tr>
  <tr>
    <td align="center">收費服務訂閱數（Fee-based VAS registered subscriptions）</td>
    <td align="center">219.5 vs. 213.4</td>
    <td align="center">2.86%</td>
  </tr>
</table>

---

在使用者每月活躍數方面：目前中國人口約 14 億，而微信使用數則達到了 12.25 億（雖然其中包含了 WeChat 這個海外版微信）</br>
但若將 WeChat 的使用者也視為中國人口的一部分（旅居或求學的華人），微信的滲透率達到了中國人口的 87.5%

::: tip
全球社交龍頭 Facebook 月活數為 28 億，Facebook Family（Facebook + Instagram + What's App）月活數為 33 億
:::

資料來源：
[公佈第四季及 2020 全年財報，在企業縮減開支的一年下，Facebook 表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-FB.html)

## 後記
身為中國的社交及遊戲產業的龍頭，騰訊仍能在營收成長的情況下，維持自身的毛利率 </br>
這點顯示了騰訊在市場具有主導性地位，並不需要透過降低毛利率的情況來提升營收 </br>
而在排除其他收益的情況下，營業利益率也能維持在 25% 左右（加上其他收益，則可到 35% 左右）

::: tip
Facebook 的營業利益率為 37%，但因 Facebook 業務較為集中，且不像騰訊有這麼多的轉投資
:::

騰訊在本業上仍有成長的動能，在轉投資方面，成功案例也不少（因此若直接排除了其他收益，其實對騰訊有點苛刻）</br>
目前騰訊較值得注意的投資有：
- 持有 拼多多（PDD） 股權 17%
- 持有 滴滴出行（預計七月在美國 IPO） 股權約 10%
- Grab（東南亞版 Uber），預計四月在美國 IPO；滴滴出行 為它的前三大股東，而騰訊又持有滴滴的股權

在目前騰訊本益比為 32.96 的情況下，其實並不算太貴（2020/03 疫情期間，本益比為 31.81）</br>
而目前 Facebook 的本益比為 30.96，顯示了投資人對於騰訊有著更高的期待（另一個中國巨頭 阿里巴巴 BABA 目前本益比為 26.34）

綜上所述，在目前騰訊股價表現低迷的時候，其實也是入口騰訊的好機會</br>
但因 TCEHY 是騰訊在美國市場的 ADR，因此每年都會被券商自動收取 ADR 管理費（ADR management fee）</br>

ADR 管理費會在每次發放股利（Qualified Dividend）時收取，約為股利的 12%

下表是我持有騰訊約四年被收取的紀錄：
<table style="width:100%">
  <tr>
    <th align="center">日期（Date）</th>
    <th align="center">股利（Qualified Dividend）</th>
    <th align="center">ADR 管理費（ADR management fee）</th>
    <th align="center">收取比例（%）</th>
  </tr>
  <tr>
    <td align="center">2018/06/19</td>
    <td align="center">$2.81</td>
    <td align="center">$0.34</td>
    <td align="center">12.10%</td>
  </tr>
  <tr>
    <td align="center">2019/03/08</td>
    <td align="center">$0.08</td>
    <td align="center">$0.01</td>
    <td align="center">12.50%</td>
  </tr>
  <tr>
    <td align="center">2019/06/18</td>
    <td align="center">$3.20</td>
    <td align="center">$0.38</td>
    <td align="center">11.88%</td>
  </tr>
</table>

參考來源：</br>
[公佈第四季及 2020 全年財報，在企業縮減開支的一年下，Facebook 表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-FB.html)</br>

[騰訊投資者專區](https://www.tencent.com/en-us/investors/financial-news.html) </br>

[TENCENT 2020 Q3 Earnings](https://static.www.tencent.com/uploads/2020/11/12/4c2090d5f6f00fd90ddc9bbd9a1415d1.pdf) </br>

[TENCENT 2020 Q4 Earnings](https://static.www.tencent.com/uploads/2021/03/24/b83f5784d0579b51cf8515cf560b4256.pdf)</br>

