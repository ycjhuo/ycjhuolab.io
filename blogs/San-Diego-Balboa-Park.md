---
title: 聖地牙哥全攻略｜巴爾波公園 Balboa Park｜聖地牙哥動物園 San Diego Zoo
description: '聖地牙哥 San Diego 必去 必吃 景點 介紹 推薦 攻略|聖地牙哥 San Diego 巴爾波公園 Balboa Park 景點 介紹 推薦 攻略|聖地牙哥 動物園 San Diego Zoo 景點 介紹 推薦 攻略'
date: 2024-04-08
tags: [San Diego]
categories: Travel
---

接續[上篇](https://ycjhuo.gitlab.io/blogs/San-Diego-OldTown-Downtown.html)活動範圍集中在市區後，這天將主力放在「巴爾波公園」（Balboa Park），這個公園就像是紐約的「中央公園」，是聖地牙哥最大的公園，大部分市區景點都在這，包涵了：
- 「聖地牙哥動物園」（San Diego Zoo）：票價 $72
- 「航太博物館」（San Diego Air & Space Museum），票價 $28
- 「美術館」（The San Diego Museum of Art）：票價 $20
- 「自然歷史博物館」（San Diego Natural History Museum），票價 $14
- 「日本友誼花園」（Japanese Friendship Garden and Museum），票價 $14

但我個人對於博物館，美術館興趣不大，所以上面這些只去了「聖地牙哥動物園」，但動物園可以待上一天，因此我是第三天才去，今天就僅在公園裡閒晃，拍照為主，若你也對博物館興趣不大的，可參考我的公園景點，推薦程度依排序

若看過照片，覺得興趣不大的，可以直接到動物園

### 荷花池（Lily Pond）
熱門婚紗拍攝景點，同時也是 Instagram 的網紅景點，如它的名稱，是一個背靠「溫室植物園」的長方形荷花池

![聖地牙哥 景點推薦 巴爾波公園 Balboa Park 荷花池 Lily Pond](../images/240-01.jpg)

後方橘色建築就是目前正在整修的「溫室植物園」，目前不開放進入

![聖地牙哥 景點推薦 巴爾波公園 Balboa Park 荷花池 Lily Pond](../images/240-02.jpg)

### 西班牙藝術村 Spanish Village Art Center
西班牙風格的藝術村，裡面多以雕塑，畫作的工作室為主
![聖地牙哥 景點推薦 巴爾波公園 Balboa Park 荷花池 Lily Pond](../images/240-05.jpg)
![聖地牙哥 景點推薦 巴爾波公園 Balboa Park 荷花池 Lily Pond](../images/240-06.jpg)

### 「美術館」（The San Diego Museum of Art）
僅在門口拍照，並未進入
![聖地牙哥 景點推薦 巴爾波公園 Balboa Park 荷花池 Lily Pond](../images/240-03.jpg)


### 風琴廣場 Spreckels Organ Pavilion
露天的風琴廣場，每個星期日的下午二點會有演奏
![聖地牙哥 景點推薦 巴爾波公園 Balboa Park 荷花池 Lily Pond](../images/240-04.jpg)

## 聖地牙哥動物園（San Diego Zoo）

### 門票
分為二種：
- Any Day：顧名思義，買了後任何一天都能去的票，票價為 12 歲以上 $72， 3-11 歲 $62
- Value Days：票價為 12 歲以上 $68， 3-11 歲 $58，只有特定日期可使用，適用大部分的平日，詳細日期可點 [聖地牙哥動物園 Value Day](https://sandiegozoowildlifealliance.org/sites/default/files/2024-01/Value-Days-2024_compressed.pdf?_gl=1%2Arvco1i%2A_ga%2AMTE1NjEzNjkxNC4xNzEyMTE1MjE5%2A_ga_3BDB3H618R%2AMTcxMjcxNzc5NS4xNS4wLjE3MTI3MTc3OTUuNjAuMC4w)


上面各種票，若再加 $7，則可升級成可以看園區內的 4D 電影票，電影內容似乎是全美同步的，跟我在紐約動物園看到的 4D 電影是同一部 ，附上 [聖地牙哥動物園 購票連結](https://zoo.sandiegozoo.org/tickets)

## 聖地牙哥動物園攻略
園區內很大，全部逛完的話，大約需要 5 - 6 小時，為了快速熟悉園區，推薦在入園時，可先去搭遊園巴士（免費的，但其實已包含在票價內）<br />

![聖地牙哥 景點推薦 聖地牙哥動物園 San Diego Zoo](../images/240-07.jpg)

巴士有上下二層，排隊時就會依照上下層區分，多數人都會選擇上層，除非是下午時間太熱則會選擇下層，但不管坐哪一層，都推薦坐在左邊，可以看到比較多動物

![聖地牙哥 景點推薦 聖地牙哥動物園 必搭 遊園巴士](../images/240-08.jpg)
![聖地牙哥 景點推薦 聖地牙哥動物園 必搭 遊園巴士 Guided Bus Tour](../images/240-10.jpg)

從這排隊人潮來看，可以明顯地看出，哪邊是上層，哪邊是下層
![聖地牙哥 景點推薦 聖地牙哥動物園 Guided Bus Tour](../images/240-09.jpg)

除了遊園巴士，動物園內還有纜車可以搭，但從上面看不到動物，只是在園區內快速移動用

![聖地牙哥 景點推薦 聖地牙哥動物園 纜車 Skyfari Aerial Tram](../images/240-11.jpg)

動物園一定會有的長頸鹿
![聖地牙哥 景點推薦 聖地牙哥動物園 纜車 Skyfari Aerial Tram](../images/240-12.jpg)

## 交通
若跟我一樣住在煤氣燈區（或是市區），可搭乘公車 7 號 或是 215 號直達動物園，住在海港村的一樣可搭 215 號公車直達動物園

## 後記
以上就是聖地牙哥「巴爾波公園」（Balboa Park）的介紹，若沒有規劃進去博物館參觀，這邊就是一個很大的公園，以及各種歷史古蹟，適合待 2 - 3 小時 

園區內吃的東西選擇不多，且都是在各博物館的咖啡店，或是餐廳，賣的多以熱狗，漢堡，三明治等，在動物園內也差不多，可自己帶想吃食物進來

聖地牙哥動物園，每年約有 400 萬的訪客，且在 2018 & 2019 被評選為全美最多人造訪的動物園（不排除是位於加州的關係）

如果對聖地牙哥動物園有興趣的，除了開頭提到的 Value Days 可以在門票上享有折扣外，也可考慮 [CityPass](https://www.citypass.com/san-diego) 這個包含多個聖地牙哥景點的套票

::: warning
點擊觀看更多： <br />
- [聖地牙哥景點介紹](https://ycjhuo.gitlab.io/tag/San%20Diego/)
:::
