---
title: 賺取 Anchor 的 20% 年利率，手續費大公開，至少要存入多少錢才划算
description: '|Terra Anchor Protocol 介紹|Terra Anchor Protocol 利率|Anchor 利率調降|MetaMask 手續費|MetaMask Gas Fee|PancakeSwap 手續費|PancakeSwap Gas Fee|Anchor 存入 UST 划算|Anchor 至少存入 UST|Terra Anchor Protocol 手續費|Terra Anchor Protocol Gas Fee'
date: 2022-05-08
tags: [Passive Income, Honeygain, Anchor]
categories: Investment
---

- [Honeygain 掛網被動收入還不夠？教你如何用 JMPT 轉到 Anchor 賺取 20% 年利率（上）](https://ycjhuo.gitlab.io/blogs/Honeygain-Payout-JMPT.html)
- [Honeygain 掛網被動收入還不夠？教你如何用 JMPT 轉到 Anchor 賺取 20% 年利率（下）](https://ycjhuo.gitlab.io/blogs/Anchor-Protocol-Steps.html)

前二篇介紹了：如何在 Metamask 上的幣安智能鏈 (Binance Smart Chain, BSC) 將加密貨幣轉為 UST </br>
接著再透過 Terra Bridge 將 UST 從 BSC 轉進 Terra 鏈，實現將 UST 存入 Anchor，賺取 20% 的年利率，流程如下：

::: warning
MetaMask → Terra Station → Anchor
:::

整個過程非一步到位，途中還會被收取 3 次的手續費，這篇就來算算總共的手續費會是多少？以及一次最少要轉換多少 UST 才會划算呢？

## MetaMask 手續費
在 MetaMask 的 BSC 鏈上，要將加密貨幣轉為另一種加密貨幣，約會收取 0.00177 BNB，以目前 BNB 價格 $375.5 來看，約 $0.665 </br>
但若是在 PancakeSwap 這個基於 BSC 的去中心化交易所的話，手續費則約 0.000248 BNB，換算下來是 $0.093 </br>

二者差了 7 倍，因此若是要在 MetaMask 的 BSC 鏈進行幣種轉換的話，更推薦在 PancakeSwap 來轉換幣種

將幣種轉換成 UST 後，要再透過 Terra Bridge 來將 UST 從 BSC 鏈轉到 Terra 鏈，這部分的手續費為 0.00019 BNB (約 $0.072)

因此，在將 UST 成功轉換到可以存入 Anchor 之前須付出：
- 在 MetaMask 上作幣種交換：成本為 $0.665 + $0.072 = $0.737
- 在 PancakeSwap 作幣種交換：成本為 $0.093 + $0.072 = $0.165

## Anchor 手續費
Anchor Protocol 的 20% 利率在最近已改為動態利率，利率範圍為 15% - 20%，每月會依據現金儲備的狀況來調整 </br>
目前 (2022/05/05) 的利率為 18.1%

在將 UST 存入/取出 Anchor 時，皆會被收取 0.25 UST 的手續費，加起來為 $0.5 UST

## 一次存入多少才划算
最後，綜上所訴，存入 Anchor 的總成本為：
- 在 MetaMask 上作幣種交換，總成本為 $0.737 + $0.5 = $1.237
- 在 PancakeSwap 作幣種交換的話，總成本為 $0.165 + $0.5 = $0.665

若想在一年存入一年後打平手續費的話，以 Anchor 最低的利率 15% 來算，須存入 $8.25 (MetaMask) / $4.44 (PancakeSwap) </br>
而若是以 20% 利率來算的話，則須存入 $6.19  (MetaMask) / $3.33 (PancakeSwap) </br>

而若是只想短期存放一個月就想取出的話，以 15% 的利率來算，須存入 $99 (MetaMask) / $53.28 (PancakeSwap) </br>
20% 的利率則為：$74.28  (MetaMask) / $39.96 (PancakeSwap) </br>

希望以上資訊對大家有所幫助

::: warning
點擊觀看更多： </br>
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::