---
title: PLTR ，用大數據協助美國政府擊殺賓拉登的公司，第四季及 2020 全年財報
description: '美股投資 Palantir PLTR 2020 財報分析|AI 大數據分析的顧問公司|顧客包含美國政府|協助找到賓拉登'
date: 2021-03-13 18:10:34
tags: [Stock, PLTR]
categories: Investment
---
PLTR (Palantir Technologies)，是一家以大數據幫企業其政府找出解決方案的顧問型公司。其共同創辦人 Peter Thiel 先前也是 PayPal 的共同創辦人。

於 2020/09/30 上市，這五個月的時間，股價上漲了 183%。上週表現為 +13.25%（納斯達克為 +2.35%）。

## 2020 Q4 及全年財報

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
  </tr>
  <tr>
    <td align="center">營收成長率</td>
    <td align="center">+11.31%</td>
    <td align="center">+47.15%</td>
  </tr>
    <tr>
    <td align="center">毛利率增長率（現在毛利率）</td>
    <td align="center">+79.67%（78.11%）</td>
    <td align="center">+47.97%（67.74%）</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">-48.61% vs. -44.78%</td>
    <td align="center">-107.41% vs. -77.63%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">-0.08 vs. -0.94</td>
    <td align="center">-1.19 vs. -1.02</td>
  </tr>
</table>

上表可看出，與全年度財報相比，營收成長率下降了 36% 左右。但毛利率則能繼續成長。</br>
表現出顧問型的公司，因執行專案都是一段長期的合作關係。隨著期間拉長，前期的投入成本慢慢回收後，將可有效提升毛利率。</br>

而費用方面，雖然公司成立到現在已經 17 年了，但為了迅速打開市場，行銷及行政費用仍飛速成長。</br>
目前行銷及行政費用佔總費用的 77%（2020 Q3 為 72%）

::: tip
跟 Q3 相比：
- 營收上漲了 11.31%，總獲利上升 79.76%
- 總費用成長了 51.39%：行銷費用（+23.36%），研發費用（+24.05%），行政費用（+124.69%）
:::

## 後記
PLTR 這支股票，相信不少人都是因為共同創辦人 Peter Thiel，以及這家企業曾利用大數據分析幫助美國政府擊殺賓拉登而投資的。</br>
對於這種顧問型的專案公司，面向的客戶都是政府及大型企業（畢竟小型企業也負擔不起高額的專案費用）。要吸引到一個客戶都是需要長期的交涉及流程，在營收成長的幅度上很難跟一般的 B to C 企業相比。</br>

且因為相關政府部門也是 PLTR 的客戶之一，是否會（正向）影響到其他企業客戶的意願也還看不出來，也可能影響到與其他國家政府單位合作的意願（忌憚於資訊可能被 PLTR 外流給美國政府？）。

若專案執行後，無法明顯的看出執行效益的話，要在現有的客戶上再進行其他專案也有不小的難度。</br>


但往好處想，有著與美國政府部門合作的實績，是否可以較為容易打動其他（親美）國家的政府部門，以及同樣注重資訊安全的金融機構等。若能成功展現出專案成果。未來則可與 IBM 及微軟一樣（服務幾乎已滲透了所有的金融機構），每年都可以從這些金融機構收取不低的顧問費。

且憑藉著 PLTR 高毛利率的特性（現在為 78%），再降低費用這塊的支出的話，EPS 即可順利轉正。</br>
但因產業別的特性，要在短期降低營業費用其實不易。雖然 Q4 的 EPS 已經上升到 -0.08，但要達成全年 EPS 正報酬的目標仍很困難。

資料取自：</br>
[PLTR 2020 Q4 季報 及全年財報](https://investors.palantir.com/news-details/2021/Palantir-Reports-Revenue-Growth-of-47-for-Full-Year-2020-Expects-Q1-2021-Revenue-Growth-of-45)</br>

[PLTR 2020 Q3 季報](https://investors.palantir.com/news-details/2020/Palantir-Reports-Revenue-Growth-of-52-in-the-Third-Quarter-Raises-Full-Year-2020-Guidance)</br>