---
title: 在 Chromebook 安裝 Android 應用程式（以 Honeygain 為例）
description: 'Chromebook 安裝 Android 應用程式教學|Chromebook 如何安裝 Google Play 沒有的程式|Chromebook 安裝 APK 教學|Chromebook 安裝 非 Google Play|Chromebook 開發人員'
date: 2022-02-02
tags: [Passive Income, Honeygain]
categories: System
---

要在 Chromebook 上安裝 Google 商店 (Google Play) 上沒有的應用程式的話，需要的步驟有二個：</br>
1. 在 Chromebook 上開啟 Linux 的環境
2. 安裝我們想要的應用程式 (雖然環境是 Linux，但應用程式還是要選擇 Android) 

完成了這二個步驟，我們就可以在 Chromebook 上運行 Android 的程式了，這篇我用 [Honeygain](https://ycjhuo.gitlab.io/blogs/Honeygain.html) 這個應用程式來作範例

## 開啟 Linux 環境
1. 打開設定 → 開發人員 (Developers) → 打開 Linux development environment → 啟用 ADB debugging
![在 Chromebook 安裝 Android 應用程式](../images/155-01.png)

::: tip
若過程中出現要設定 Linux 開發環境，輸入一個喜歡的 username 就好，不影響之後的步驟
:::

![在 Chromebook 安裝 Android 應用程式](../images/155-02.png)

2. 打開 Terminal，輸入 ```sudo apt install android-tools-adb -y``` 來安裝 Android 工具


3. 安裝完畢後，輸入 ```adb connect 100.115.92.2:5555``` ，來確認是否能成功連接上 adb
![在 Chromebook 安裝 Android 應用程式](../images/155-03.png)

::: tip
若過程中出現詢問 Allow USB debugging? 的視窗，勾選 Always allow from this computer，並點擊 OK
:::

## 安裝應用程式
1. 之後把要安裝的 apk 檔案搬到 Linux files 資料夾
![在 Chromebook 安裝 Android 應用程式](../images/155-04.png)

2. 在 Terminal 中輸入 ```adb install honeygain_app.apk``` 來安裝我們要的程式 （把 honeygain_app 替換為 apk 的檔名）

::: tip
若出現 ```error: more than one device/emulator``` 這個錯誤訊息 </br>
輸入 ```adb kill-server``` 來刪除多餘的裝置
:::

![在 Chromebook 安裝 Android 應用程式](../images/155-05.png)

3. 安裝完成後，可在 Chromebook 中看到我們剛剛安裝的應用程式
![在 Chromebook 安裝 Android 應用程式](../images/155-06.png)

## 衍伸閱讀
若對於 Honeygain 有興趣的話，可參考我的這二篇文：</br>
1. [有網路就能賺錢？Honeygain 介紹](https://ycjhuo.gitlab.io/blogs/Honeygain.html)
2. [掛網月賺 $20 美金? Honeygain 常見問題集 Q&A](https://ycjhuo.gitlab.io/blogs/Honeygain-QA.html)
