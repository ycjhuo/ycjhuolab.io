---
title: 一個月跌 30%，特斯拉股價是否還會繼續下探
description: '美股特斯拉|特斯拉 TSLA|TSLA ARK|特斯拉 方舟|ARK 報酬率|方舟基金 報酬率'
date: 2021-03-06 20:30:01
tags: [Stock, Index, ETF, ARK, TSLA]
categories: Investment
---

## 特斯拉近一個月表現（02/08 - 03/05）
這個月對特斯拉的投資者來說，是個考驗心理素質的一週。看著特斯拉的股價從 863 一路跌到了 598（股價最低時為 546，跌幅最高為 37%）。從最初的一路加碼，轉變為是否應趕快賣出。

30% 的跌幅對於小市值的公司來說，可能早已習以為常。但對於特斯拉這種在標普 500 指數中，權重排名第 7 的公司而言（現在跌到第 8 了）無疑是罕見的。

下圖是特斯拉近一個月的表現（02/08 - 03/05），以及對比其他在科技股中市值最大的三家企業（蘋果，微軟，亞馬遜，以及那斯達克指數）。</br>
可以看出，特斯拉的跌幅相較其他企業，幾乎有著 3 - 7 倍的跌幅。而特斯拉若想要再回到一個月前的股價，則須上漲 45%。
![61-1](../images/61-1.png)

若因為這次的下跌而產生恐慌，可複習一下[特斯拉的 Q4 財報](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-Q4-2020.html) 來增強自己對特斯拉的信心。

## 方舟投資近一個月表現（02/08 - 03/05）
接著來看看最為力挺特斯拉的方舟投資表現。</br>
同樣的，近一個月來，方舟投資（ARK Invest）也遭遇了去年熔斷以來的最大跌幅（也是最長）。
旗下的五支 ETF 均面臨了約 15-25% 左右的跌幅。而這次跟去年熔斷不同的是，這一次下跌的只有 ARK。對於其他科技股或其他產業來說，跌幅並不明顯。

下圖是 ARK 五支 ETF 在近一個月的表現：
![61-2](../images/61-2.png)

<table style="width:100%">
  <tr>
    <th align="center">ETF</th>
    <th align="center">1 Months</th>
  </tr>
  <tr>
    <td align="center">ARKK（著重於破壞式創新企業）</td>
    <td align="center">-23.64%</td>
  </tr>
  <tr>
    <td align="center">ARKQ（著重於自動化科技及機器人）</td>
    <td align="center">-17.97%</td>
  </tr>
  <tr>
    <td align="center">ARKW（著重於下一代網路科技）</td>
    <td align="center">-18.67%</td>
  </tr>
  <tr>
    <td align="center">ARKG（著重於基因改良及醫療）</td>
    <td align="center">-24.14%</td>
  </tr>
  <tr>
    <td align="center">ARKF（著重於金融創新科技）</td>
    <td align="center">-13.55%</td>
  </tr>
</table>

若再將 ARK 的 5 支 ETF 中，持股超過 6% 的持股（第1, 2 大持股）抓出來比較。</br>
可看出 TSLA, SQ, TDOC 這三家公司，在這一個月的表現也不好，尤其是 TSLA 及 SQ，直接影響了 ARK 的 3 支 ETF 表現。

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">1 Months</th>
  </tr>
  <tr>
    <td align="center">TSLA（ARKK, ARKQ, ARKW）</td>
    <td align="center">-30.75%</td>
  </tr>
  <tr>
    <td align="center">SQ（ARKK, ARKW, ARKF）</td>
    <td align="center">-16.74%</td>
  </tr>
  <tr>
    <td align="center">ROKU（ARKK）</td>
    <td align="center">-18.66%</td>
  </tr>
  <tr>
    <td align="center">TDOC（ARKK, ARKG）</td>
    <td align="center">-35.60%</td>
  </tr>
</table>

## 方舟投資仍持續買進特斯拉
這幾支 ARK 的重倉股，在最近一年都有著 250 ~ 600% 的漲幅（TDOC 只有 100% 左右），這個月的下跌也只是回歸到一月的股價而已（等於今年的漲幅全沒了）。</br>
相比蘋果，微軟，亞馬遜這種有著穩定獲利能力的企業來說，ARK 重倉的這 4 支股票看重的是未來 5 年的成長性（現階段的獲利能力反而是其次）。

下圖也可看出，雖然特斯拉在這個月的股價跌了 30%，但在 ARK 的 3 支 ETF 中的權重仍都穩定保持在 10% 左右，表示 ARK 在這段時間仍持續買入特斯拉。並沒有因為股價下跌而減少持股比例。
![61-3](../images/61-3.png)

推薦這個網站：[ARK Tracker](https://www.arktrack.com/)，可以觀察 ARK 每天及一段時間的交易紀錄。

## 後記
在投資上，我並沒有配置太多的現金比例，以免錯過股市的上漲。對我來說，將現金投資在 [AOA](https://ycjhuo.gitlab.io/blogs/ETF-AOA.html)，就代表著現金。</br>
AOA 低波動的特性讓我不擔心在下跌的股市中，會遭遇到極大的損失。</br>

而在目前這種狀況，雖然 AOA 也是在下跌，但我仍為了買進更多的 TSLA 而賣出 AOA。</br>
若後續 TSLA 跌幅增大的話，我仍會賣出其它的 ETF（甚至是 BRK），來換取加碼 TSLA 的資金。
![61-4](../images/61-4.png)

延伸閱讀：</br>
[懶人投資，AOA，自動再平衡的 ETF](https://ycjhuo.gitlab.io/blogs/ETF-AOA.html)</br>
[方舟投資（ARK Invest），著重於破壞式創新的主動型 ETF](https://ycjhuo.gitlab.io/blogs/Ark-Invest.html)</br>
[特斯拉（Tesla）公布 Q4 季報，成長速度是否減弱](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-Q4-2020.html)</br>
