---
title: 免費國際匯款｜富達投資（Fidelity）實現零手續費匯款的新選擇，美國匯款回台灣的新捷徑，申請篇
description: '美國匯錢 轉帳 回台灣|最便宜 免手續費跨國匯款轉帳|跨國匯款轉帳推薦|富達投資 Fidelity 免費國際匯款 0 手續費匯款|美金匯回台灣最省錢方法|美 國轉錢 匯錢 回台灣|美金 轉 換 台幣|台幣 美金 互換|最便宜 Bank Wire 國際匯款 轉帳 方法 推薦 評價'
date: 2024-02-08
tags: [Global Transfers]
categories: Investment
---

### Fidelity 富達投資零手續費匯款
在花旗銀行於 2023 退出台灣後，零成本從美國匯錢回台灣的方法就只剩 [中國信託 CTBC 全球速匯](https://ycjhuo.gitlab.io/blogs/CTBC-Global-Transfers-US-To-TW.html)  <br />
但 CTBC 在美國的據點真的不多，僅在東岸的紐約，紐澤西，以及西岸的加州有分行，若非住在這些城市的人，要特別跑一趟開戶真的不容易，且網路開戶也容易被拒絕，那是否有其它方法呢？

這時身為美國第二大券商，且幾乎每個州都有據點的 的 Fidelity（富達投資）就是一個好選擇，Fidelity 提供了 0 手續費且 24 小時內到帳的 Wire Transfer 服務，可以幫助我們將帳戶內的錢轉到世界各個銀行，下面就來看看要如何操作

### 網路申請
1. 登入帳戶後，點擊左上角的綠色選單 Accounts & Trade，選擇 Transfers

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-01.png)

2. 到了 Transfers 頁面後，點擊 Guide to choosing EFTs or bank wire

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-02.png)

3. 點擊 Bank wire 後，會要求電話／簡訊驗證身份，驗證完後，會跳到 Link a bank 頁面

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-03.png)

4. 選擇 Bank wire，案 Continue，接著選擇要轉出錢的帳號，再選擇 International（因為我們要進行國際匯款），再按下 Continue

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-04.png)

5. 因為我們要匯款的幣別是美金，因此這裡下載這個文件 Bank Wire Authorization form (PDF)

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-05.png)

### 分行親簽
在上一部份，我們列印了文件後，再來就要填寫完拿去分行了，下面就來看如何填寫

1. Account Owners 寫上自己的 Fidelity 帳戶名字
2. Fidelity Account(s) 填寫要用來作 wire transfer 的 Fidelity 帳號（我有 3 個 Fidelity 帳號，所以這邊就 3 個都填了）

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-06.png)

3. 接著 Bank Wire Standing Instructions 寫上你要把錢匯到哪個銀行（我是要匯到台灣的兆豐銀行，所以這裡以兆豐銀行為例）
由上到下，依序為：銀行名稱，兆豐銀行的美金帳戶號碼，兆豐的開戶名稱（我是寫英文），分行地址，郵遞區號，國家 <br />
中間的 Correspondent (Intermediary) 則是中轉行，打給兆豐任何一個分行，詢問負責處理外匯的專員即可 <br />

因為我們要做的是國際匯款，所以下面紅框處要打勾 <br />
紅框下的 SWIFT code 也可詢問負責外匯的專員，或是直接 Google XX 銀行 SWIFT code 就能找到

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-07.png)

4. 上面都寫好後，我們就可帶著這份文件到 Fidelity 的分行（Fidelity Investments）了，到了分行後，說出關鍵字「Medallion Signature Guarantee」，或拿出文件給工作人員，他們就會指示你在下方左側簽名，而他們也會在右側簽上他們的名字

::: tip
這步驟是為了要確認是否為本人申辦，所以去的時候要帶著駕照／護照，並在他們面前簽名，不須自己在家先簽好
:::

![富達投資 Fidelity Wire Transfer 國際匯款教學](../images/236-08.png)

5. 他們簽好後，會把文件收走，等個 2 天後就完成囉
::: tip
若想知道處理狀態，可點擊這個連結查詢 [Fidelity Status Tracker](https://digital.fidelity.com/ftgw/digital/universal-tracker/)
:::

### 後記
上面就是整個 Fidelity 0 手續費匯款的申請流程囉，詳細的匯款步驟請移至在下篇

[免費國際匯款｜富達投資（Fidelity）實現零手續費匯款的新選擇，美國匯款回台灣的新捷徑，匯款篇](https://ycjhuo.gitlab.io/blogs/Fidelity-Global-Transfers-US-To-TW-Wire.html)

其它將美國會回台灣的方法可參考：[Global Transfers 國際匯款介紹](https://ycjhuo.gitlab.io/tag/Global%20Transfers/)

::: warning
點擊觀看更多： </br>
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::
