---
title: Meta 2023 Q1 財報｜裁員成果見效，股價已飆漲超過 100%
description: 'Meta FB 2023 Q1 第一季財報｜Facebook FB 2023 Q1 第一季財報｜Apple Vision Pro vs. Meta Quest'
date: 2023-06-07
tags: [Stock, Meta]
categories: Investment
---

社交龍頭 Meta 在 4/26 公佈了 2023 第一季的財報，在 2022 Q4 財報 - 2023 Q1 財報之間（2/1 - 4/26）</br>
股價從 $152.32 → $209.4，上漲了 37.5%（同時期 S&P 500 下跌 1.5%），且今年以來的報酬率已超過 100% </br>

先前裁員的效果已開始顯現在財報上，且仍未結束，看來 CEO 在將 2023 訂為「Year of Efficiency」（效率年）時，也就代表著一連串的組織重組（裁員）要來了（且今年還有三次）

下面就來看看 Meta 這一連串的裁員對於財報的影響

## Meta 2023 Q1 財報指標
[資料來源](https://investor.fb.com/investor-events/event-details/2023/Q1-2023-Earnings/default.aspx)
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q4</th>
    <th align="center">2023 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2023 Q1 vs. 2022 Q4</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">32,165</td>
    <td align="center">28,645</td>
    <td align="center">-4%</td>
    <td align="center">16%</td>
    <td align="center">-11%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">6,149</td>
    <td align="center">7,307</td>
    <td align="center">-32%</td>
    <td align="center">10%</td>
    <td align="center">19%</td>
  </tr>
  <tr>
    <td align="center">廣告營利率</td>
    <td align="center">34%</td>
    <td align="center">40%</td>
    <td align="center">-5%</td>
    <td align="center">--</td>
    <td align="center">6%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">20%</td>
    <td align="center">25%</td>
    <td align="center">-9%</td>
    <td align="center">--%</td>
    <td align="center">5%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">1.76</td>
    <td align="center">2.2</td>
    <td align="center">-34%</td>
    <td align="center">7%</td>
    <td align="center">25%</td>
  </tr>
</table>

- 營收：本季營收為 286.5 億，季減 11%，但因 Meta 營收會受季度影響，若與去年同期比較的話，是年增 3% </br>
將細分營收的話：廣告業務季減 10%、年增 3%，虛擬實境業務（Reality Labs）則是季減 53%，年減 51％
- 稅前淨利：為 73 億，在主要是在各項開支的大幅減少下，達成季增 19% 的成績，但年減 18% 則是當時的支出比現在更低
- 廣告營利率：達到了 40%，季增 6%，是近一年的最高點，原因是營運開支的減少所致
- 營業利潤率：為 25%，季增 5，與 2022 的平均一致，主要是虛擬實境業務在營收減半的情況下（與上季相比），虧損去沒同步縮減所導致
- EPS：為 2.2，季增 25%，是近 3 季的最高點，但年減 19%

Meta 在經歷了二季的衰退後，本季總算又站穩腳跟，在營利率季增 5% 的幫助下，推動 EPS 達到季增 25% 的成績 

而背後原因就是裁員，與上季相比：</br>
研發減少了 4%，行銷減少 33%，行政減少 6%，合計共比上季減少了 12% 的營運支出 </br>
讓營利率從 20% → 25%，大幅提高了 Meta 的獲利能力

但裁員也帶來些後遺症，例如：「虛擬實境業務」在營收比上季少了 53%，但虧損卻僅少了 7%

![Meta 2023 Q1 第一季 營運費用](../images/221-01.png)
![Meta 2022 Q4 第一季 EPS](../images/221-02.png)

## Meta 2023 Q1 財報重點
### 庫藏股
Meta 在本季購買了 94 億的庫藏股，目前仍有 417 億的授權資金可供購買，而 2022 庫藏股實施金額僅為 280 億，今年將實施比去年高出 82.5% 的庫藏金額，展現公司捍衛股價的決心

### 組織重組
目前 Meta 員工約 7.7 萬人（未扣除今年裁員的員工），而今年將再進行三輪的裁員，預計再減少約 1 萬名的員工（約佔總數 13%）

若本季財報排除這些相關費用後，營利率可再提升 4%，達到近一年的最高點（30%），而 EPS 也會升高到 2.64，季增 50%

Meta 在最高峰時（2022 Q3），約有 8.7 萬員工，但 2022 年底已裁了 2 萬人，而加上今年要裁的 1 萬人，Meta 在短短三個季度就將員工人數縮減到僅剩原本的 65%

### 下季預期
預期下季營收約落在 295 - 320 億，季增 3% - 12%，成長率可優於去年同期＆上個季度，加上因裁員而提升的營利率，Q2 的 EPS 可會優於 Q1，達到接近去年同期的水準（2.46）

## 後記
Meta 的股價在今年已飆漲約 118%，本益比也從最低時（2022/11/02）的 8.6 → 34.7，成長了近四倍 </br>
在去年 Q3，Meta 停止了人員招募後仍無法阻止獲利能力＆股價的下滑，隨後即開始進行裁員，而股價也在去年 11 月開始上漲

上漲的原因，除了投資人看好 Meta 可藉著裁員來縮減開支外，還看到 CEO 在財報會議上開始較少提到「元宇宙」，而趕搭上最近的「AI」熱潮，讓 Meta 總算有機會可以擺脫「元宇宙」的燒錢命運

但在昨天的 WWDC 2023 上，蘋果發表了要價 $3,499 的 AR 頭戴裝置「Apple Vision Pro」正式切入頭戴式裝置市場 </br>
相較之下，Meta 的 Quest 系列價格從 $200 - $1,000，與蘋果的目標客群仍有區隔 </br>

未來 Meta 的 VR 業務有望搭上蘋果的順風車，藉著蘋果的影響力＆行銷資源，拓展到更多想嚐鮮卻因「Apple Vision Pro」的價格太高而選擇 Meta Quest 的消費者

雖然 Meta 前景已轉為明朗，但目前約 35 的本益比，相比蘋果的 30.5 以及 Google 的 29，實在讓人難以下手，只能看之後是否會回檔再找機會加碼

::: warning
點擊觀看更多： </br>
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
