---
title: 如何用 Python 寄送 HTML 郵件 ( Email )
description: 'Pyhton 寄信 HTML | Python 寄 email HTML | Python HTML email | Pyhton excel 寄信 HTML | Pyhton 寄信 附件 | Pyhton 寄信 超連結'
date: 2021-01-25 22:01:27
tags: [Python, Pandas, Sendmail]
categories: Programming
---
之前分享了三篇文章：<br/>
- [如何用 Python 寄送郵件](https://ycjhuo.gitlab.io/blogs/Python-How-To-Send-Mails.html)
- [用 Python 寄送客制化郵件](https://ycjhuo.gitlab.io/blogs/Python-Mutiple-And-Customize-Mail-Message.html)
- [如何在 Python 寄送郵件時加上不同副本收件人](https://ycjhuo.gitlab.io/blogs/Python-Send-Mails-With-CC.html) <br/>

這三篇都是屬於寄信單純的文字信件(Plain)。

若要在信件中加入圖片，超連結等功能的話，就須將寄出郵件的格式改為 HTML 才可以，下面就來介紹如何寄出 HTML 的郵件。

## SMTP 相關設定
首先我們來設定 smtp 的郵件伺服器，port，與寄件人，收件人及副本收件人(CC)。
```python
smtp_server = "XXXXX"
port = 25
server = smtplib.SMTP(smtp_server, port)
sender = "sender@XXX.com"
receiver = "receiver@XXX.com"
cc = ["cc@XXX.com"]
```

## MIME 相關設定
這部分就進入到 HTML 的相關連結：
先將要寄送的內容以 HTML 的方式寫出來 ( 包含 html 內的各種 tag )。若有要傳遞到內容中的變數的話，則用雙括號包起來。
最後在內容的字串外面，在用 ``` .format ``` 的形式將變數帶進去。<br/>

之後在 13 行，我們宣告 msg 並指定為 alternative 這個類型。
這裡的 MIMEMultipart 有三種類型，分別為：mixed, alternative, related，若要寄送附件的話，則要用 mixed，而這裡我們用的 alternative 則是可以在寄送 HTML 失敗時，改成寄送純文字。<br/>

再來加上信的主旨，且將寄信者等資訊也加入到 msg 裡面。<br/>
最後記得因為我們的內容都是 HTML 格式，所以最後在寄出時，要將 msg 轉為字串才能成功寄出。
```python
message_text = """\
<html>
<head></head>
<body>

Today is {date}. This mail is sent by {name} <br/> <br/>
Welcome to my <a href="https://ycjhuo.gitlab.io">blog</a>.  <br/><br/>

</body>
</html>

""".format(date = date, name = name)

msg = MIMEMultipart('alternative')
msg = MIMEText(message_text, 'html', 'UTF-8') 

msg['Subject'] = "This is a subject"
msg['Form'] = sender
msg['To'] = receiver
msg['Cc'] = ','.join(cc)

toAddress = [receiver] + cc
server.sendmail(sender, toAddress, msg.as_string())
print("Sent to: " + ','.join(toAddress))
```

## 成果
程式結束後，結果如下圖，<br/>
可以看到寄出的信裡面以成功附上的超連結，且一開始宣告的 date 跟 name 也都有成功被插入到內容中<br/>
![50-01](../images/50-01.png)

若不想將信箱地址直接寫死在程式內的話，可參考這篇：[如何讓 Python 讀取外部 ini 設定檔](https://ycjhuo.gitlab.io/blogs/Python-Read-Ini-File.html) </br>
讓程式透過外部的 ini 檔案讀取相信箱地址

## Source Code
Source code 如下；

```python
import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

### Email Server Settings 
smtp_server = "XXXXX"
port = 25
server = smtplib.SMTP(smtp_server, port)
sender = "sender@XXX.com"
receiver = "receiver@XXX.com"
cc = ["cc@XXX.com"]

date = datetime.date.today()
name = "Leon"

message_text = """\
<html>
<head></head>
<body>

Today is {date}. This mail is sent by {name} <br/> <br/>
Welcome to my <a href="https://ycjhuo.gitlab.io">blog</a>.  <br/><br/>

</body>
</html>

""".format(date = date, name = name)
msg = MIMEMultipart('alternative')
msg = MIMEText(message_text, 'html', 'UTF-8') 

msg['Subject'] = "This is a subject"
msg['Form'] = sender
msg['To'] = receiver
msg['Cc'] = ','.join(cc)

toAddress = [receiver] + cc
server.sendmail(sender, toAddress, msg.as_string())
print("Sent to: " + ','.join(toAddress))
```