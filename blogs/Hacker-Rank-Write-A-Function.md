---
title: HackerRank Python Write a function 解法
description: 'HackerRank 教學|HackerRank 解答|HackerRank Python|HackerRank Python Write a function|Python If else 教學用法'
date: 2021-03-13 11:45:05
tags: [Python, HackerRank, Medium]
categories: HackerRank
---
## Problem
An extra day is added to the calendar almost every four years as February 29, and the day is called a leap day. It corrects the calendar for the fact that our planet takes approximately 365.25 days to orbit the sun. A leap year contains a leap day.</br>

In the Gregorian calendar, three conditions are used to identify leap years: </br>

- The year can be evenly divided by 4, is a leap year, unless:
 - The year can be evenly divided by 100, it is NOT a leap year, unless:
 - The year is also evenly divisible by 400. Then it is a leap year.

This means that in the Gregorian calendar, the years 2000 and 2400 are leap years, while 1800, 1900, 2100, 2200, 2300 and 2500 are NOT leap years. </br>

### Task
Given a year, determine whether it is a leap year. If it is a leap year, return the Boolean ```True```, otherwise return ```False```. </br>
Note that the code stub provided reads from STDIN and passes arguments to the ```is_leap``` function. It is only necessary to complete the ```is_leap``` function.

### Input Format
Read ```year```, the year to test.

### Constraints
1900 <= year <= 10^5

### Output Format
The function must return a Boolean value (True/False). Output is handled by the provided code stub.

## Answer
這題是運用 if-else 來判斷該年份是否為 leap year </br>
可以被 4 整除就是 leap year，若同時能被 100 整除，則不是 leap year </br>
但若該年份又可被 400 整除的話，則又為 leap year </br>

因此，我們可以將條件簡化成：</br>
若可被 4 整除，就是 True，但在這個條件下，若可以被 100 整除，則又改為 False  </br>

但因為可被 400 整除的話，表示一定可以被 100 整除 </br>
所以我們可將判斷是否被 400 整除的條件放到是否可被 4 整除條件的下方 </br>

### Source Code
```python
def is_leap(year):
    leap = False
    
    # Write your logic here
    if (year % 4 == 0):
        leap = True
        if(year % 100 == 0):
            leap = False
        if(year % 400 == 0):
            leap = True
    
    return leap

year = int(input())
```