---
title: Block（SQ）2023 Q2 財報｜二個月暴跌 40%，股價已到三年最低
description: 'Block SQ 2023 Q2 第二季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報|Block 收購 Afterpay|Block 收購先買後付平台|Block buy now pay later (BNPL)'
date: 2023-09-30
tags: [Stock, SQ]
categories: Investment
---

支付科技公司 Block（股票代號：SQ），於 8/3 發布了 2023 Q2 財報

在 Q1 - Q2 財報期間（5/4 - 8/3），Block 股價上漲了 22%（$60.43 → $73.55），同期的 S&P 500 指數上漲 10%，Block 漲幅為大盤的二倍

若將時間拉長到今年 - Q2 財報公佈前，Block 上漲 14%，S&P 500 上漲 18%，二者相差無幾

但 Block 在公布 Q2 財報後，股價當天就下跌了 14%，直到今天（9/30），股價更是下跌了 40%（$73.55 → $44.26），同時 S&P 500 僅下跌 5%

究竟 Block 的第二季財報發生了什麼事，造成股價連續下跌，目前更是跌到了三年來的最低點

下面就來看看 Block（SQ）在 2023 Q2 的財報，看看是否為加碼的好機會

## Block 財務指標
[來源](hthttps://s29.q4cdn.com/628966176/files/doc_financials/2023/q2/2Q23_Block_Shareholder-Letter.pdf)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q1</th>
    <th align="center">2023 Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2023 Q1 vs. 2022 Q4</th>
    <th align="center">2023 Q2 vs. 2023 Q1</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">49.9 億</td>
    <td align="center">55.3 億</td>
    <td align="center">3%</td>
    <td align="center">7.3%</td>
    <td align="center">10.9%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">34.4%</td>
    <td align="center">33.7%</td>
    <td align="center">1%</td>
    <td align="center">-1.3%</td>
    <td align="center">-0.6%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-0.1%</td>
    <td align="center">-2.4%</td>
    <td align="center">-1.8%</td>
    <td align="center">2.8%</td>
    <td align="center">-2.3%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">-0.03</td>
    <td align="center">-0.2</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：連續 5 季成長，本季營收為 55.3 億（季增 11%），是由「交易」業務所推動（季增 15%），目前佔總營收的 30%
- 毛利率：與上季持平，維持在 34%，從 2022 一直維持至今，變化不大
- 營業利潤率：較上季下滑 2%，為 -2.4%，主要是因研發＆一般行政的支出所致
- EPS：退回到了 2022 Q4 的水準，為 -0.2，因各項營業費用的增加，導致本季無法實現獲利

本季營收比上季成長了 11%，原本在營收的加持下，Block 在本季有望達成獲利的目標，可惜營業費用比上季增加了 16%，讓本季虧損比上季嚴重


## Block 各項表現
本季的毛利為 18.7 億，季成長從 3% → 9%，年成長則是 32% → 27% 

以毛利的來源來看：商家服務 Square 貢獻了 47%（年成長 18%），消費者服務 Cash App 則佔了 53%（年成長 37%），佔了較高毛利的 Cash App 展現了更高的成長率，表示 Block 的毛利仍能維持增長動能


### Square
在 Square 的毛利組成中，最亮眼的為銀行服務（banking product），佔了 Square 毛利的19%（年成長 24%），在 Square 的客群組成中，中大型企業使用了更多的 Square 服務，也帶來更多營收，讓 Square 決定積極推展大型企業市場

::: tip
目前 Square 的中型企業客群，所貢獻的毛利，年成長為 20%
:::

### Cash App
Cash App 的 P2P 交易量在本季達到 530 億，年成長 18%，就帶來了毛利年成長 37% 的成績，更別提目前 Cash App 的變現換（Monetization rate）率僅為 1.44%（年成長 0.16%，季成長 0.03%），顯示 Cash App 的盈利空間仍相當巨大

### 營業費用
總營業費用在本季為 20 億，年增 19%，下面佔比最大的三項：
- 研發佔了 35%，年增 32%，主要是工程團隊的人數增加所致
- 銷售佔了 27%，年增僅 1%，以 Cash App 的 P2P 交易處理＆損失的費用為主
- 一般/行政佔了 28%，年增 39%，是因客服＆法遵團隊的人數增加

研發團隊是公司競爭力的來源，但目前公司已要求各部門的主管在招募上多加考慮，以求有效控制成本 </br>
而銷售的增長率與去年持平，表示這部分的支出已得到控制 </br>
最後是增長最大的一般/行政業務，因 Block 仍處在擴張期，為保持良好的客戶體驗，在後勤上的投資始終難以降低，但相信在作業程序＆體系建立起來後，這部分的費用會大幅下降 </br>


## 後記
Block 在本季由於「營業費用」的成長率 > 「營收」增長率，使整體虧損又近一步的擴大，但在營收成長的同時，毛利率並沒有下降，表示 Block 並不須依靠降價促銷來保持營收的成長

在營收增長仍維持 2 位數成長的情況下，股價在這二個月卻跌了 40% 真的讓人意外 </br>
但要說 Block 的缺點，應該只有營業費用太高，始終無法降低這點了吧，但隨著管理層對於人員招募的控制下，相信未來幾個季度將能看見成效

單從股價來看，可能會覺得 Block 正在走下坡，畢竟股價都到了近三年的最低點了，但若將 Block 跟同為支付產業的 PayPal 比較：

- 近半年股價表現：PayPal 是 -22%，Block 為 -35%，S&P 500 上漲 4%
- 今年以來股價表現：PayPal 是 -22%，Block 為 -32%，S&P 500 上漲 12%

就算是 EPS 年成長 25% 的 PayPal，在股價也來到五年內最低點，本益比更只有 15.5，為三年內最低

因此，我認為 Block 目前股價只是低估，公司的成長速度仍然強勁

::: tip
目前我的 Blcok 投資部位，累積虧損金額已突破 $38,000，這二個星期又加碼了 60 股（$44 - $54），雖虧損金額不少，但我仍希望 Block 股價能繼續下跌
:::
下圖為主帳號的截圖： </br>
![SQ Block 2023 Q2 Earnings](../images/228-01.png)

::: warning
點擊觀看更多： </br>
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
