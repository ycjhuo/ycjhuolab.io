---
title: Google（GOOG）2021 Q3 季報，成長動能依然不減
description: '美股投資 Google GOOG|Google GOOG 介紹|Google GOOG 2021 Q3 第三季財報分析'
date: 2021-10-30
tags: [Stock, GOOG]
categories: Investment
---

搜尋龍頭 Google（GOOG）在 10/26 公佈了 2021 Q3 的財報，從 2021 Q2 季報公佈後（07/27 盤後），到公佈 Q3 季報（10/26 盤後），三個月的時間，股價從 2,735.93 → 2,703.44，上漲了 2.1%，同期間，S&P 500 指數上漲了 3.94%

---

Google 2021 Q2 財報回顧：[Google（GOOG）2021 Q2 季報，近一年來表現最好的科技巨頭](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q2.html) </br>


## Google 2021 Q3 財報
[來源](https://abc.xyz/investor/static/pdf/2021Q3_alphabet_earnings_release.pdf?cache=f1ba3f6)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">QoQ (2021 Q1)</th>
    <th align="center">QoQ (2021 Q2)</th>
    <th align="center">QoQ (2021 Q3)</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">55,314</td>
    <td align="center">61,880</td>
    <td align="center">65,118</td>
    <td align="center">-2.78%</td>
    <td align="center">11.87%</td>
    <td align="center">5.23%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">21,283</td>
    <td align="center">21,985</td>
    <td align="center">23,064</td>
    <td align="center">13.88%</td>
    <td align="center">3.3%</td>
    <td align="center">4.91%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">70.28%</td>
    <td align="center">68.71%</td>
    <td align="center">67.7%</td>
    <td align="center">-2.21%</td>
    <td align="center">-1.57%</td>
    <td align="center">-1.01%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">29.72%</td>
    <td align="center">31.29%</td>
    <td align="center">32.2%</td>
    <td align="center">2.21%</td>
    <td align="center">1.57%</td>
    <td align="center">1.01%</td>
  </tr>
    <tr>
    <td align="center">Diluted EPS</td>
    <td align="center">29.29</td>
    <td align="center">27.26</td>
    <td align="center">36.38</td>
    <td align="center">31.35%</td>
    <td align="center">-6.93%</td>
    <td align="center">33.46%</td>
  </tr>
</table>

- 總營收：在 Q2 季成長的優越表現下（11.87%），Q3 仍舊繼續成長（5.23%），若與 Q1 相比，營收在本季已成長了 17.72%
- 稅前淨利：季成長幅度與營收差不多，為 4.91%，顯示在營收成長的同時，仍有維持住毛利率＆營業利益率
- 毛利＆營業利益率：Q3 毛利率降低了 1.01%，而營業利益率則是上升了 1.01%
- EPS：Q3 EPS 季成長超越了 Q1 的 31.35%，來到了 33.46%


## 營收來源
Google 本季總營收來到了 651 億（Q2 為 619 億），季成長為 5.23% </br>
而本季推動營收成長的功臣仍是 Google 搜尋業務，相比 Q2 成長了 5.81%，佔了總營收的 58%（佔比與上季持平）</br>

相比之下，另一個線上廣告龍頭 Facebook 本季總營收為 290 億，僅是 Google 搜尋業務營收的 80%，總營收的 44.5%</br>
下面將營收細分為三類： </br>

- Google Service：包含線上廣告，Google Search，Android，YouTube 等我們常用的 Google 服務，季成長 4.94%
- Google Cloud：以 Google 雲端平台，以及提供給企業用的相關服務，季成長 4.94%（上季成長率為 14.36%）
- Other Bets：其他收入來源，僅占總營收的 0.3%，占比太小，可忽略不看

上季成長速度最快的 YouTube ads（16.6%）& Google Gloud（14.36%），本季成長率已分別降為 2.9% & 7.82%


## 庫藏股
在本季度，花費了 126 億回購了 Class A（代號：GOOGL） & Class C（代號：GOOG）

本季回購的金額與 Q2 相差無幾，以下為這期間的股價表現： </br>

- 2021 Q1：Google 股價上漲 19.69%，S&P 500 上漲 9.42%，回購金額 114 億
- 2021 Q2：Google 股價上漲 18.56%，S&P 500 上漲 5.13%，回購金額 128 億
- 2021 Q3：Google 股價上漲 2.1%，S&P 500 上漲 3.94%，回購金額 126 億


## 後記
就這個季度的表現來看，Google 的股價雖然僅上漲了 2.1%，卻已遠遠領先同為以線上廣告業務為主的 Facebook（FB 在這個季度，股價下跌了約 12%）</br>
且不只是 Facebook，Apple 與 Amazon 的股價表現也皆不如 Google <br/>

![FAANG 近半年走勢圖](../images/131-01.png) <br/>

加上本季隨著 Google 自行研發晶片的 Pixel 6 & 6 Pro 推出，可看出 Google 已準備好攻佔行動裝置的市場 </br>

憑藉著效能及拍照技術的加持，我認為 Google 的 Pixel 系列很有可能成為與三星爭奪 Android 高階手機陣營的一個強力挑戰者 </br>

隨著 Google 業務的多元化，以及營收的多樣性，以 Google 目前的本益比（28.56），相比比蘋果＆亞馬遜，我認為 Google 是更好的選擇 </br>

延伸閱讀： </br>
[Facebook（FB）2021 Q3 季報，社交龍頭是否已開始走下坡](https://ycjhuo.gitlab.io/blogs/FB-Earnings-2021-Q3.html) </br>
