---
title: 特斯拉（TSLA）2022 Q3 季報，成長速度重回軌道
description: '特斯拉 TSLA 財報|特斯拉 TSLA 2022 Q3 第三季財報|特斯拉 TSLA 生產量 交車量 2022 Q3|TSLA 特斯拉 拆股'
date: 2022-10-30
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 10/02（三）公佈了 2022 第三季財報 </br>
在 2022 Q2 - 2022 Q3 財報的期間（7/20 - 10/19），股價從 $247.6 → $222.04，下跌了 10.32%（同期間 S&P 500 為 -6.69%）</br>
在升息的市場環境下，特斯拉股價已連續三季下跌，而在公布財報的隔天，股價又下跌了 6.6% </br>

在已從歷史高點跌去 45% 的現在，特斯拉是否仍值得投資呢？ 下面來看看特斯拉第三季財報：

## 特斯拉 2022 Q3 財報
[來源](https://tesla-cdn.thron.com/static/WTULXQ_TSLA_Q3_2022_Update_KPK2Y7.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22tsla-q3-2022-update.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">2022 Q2 vs. 2022 Q1</th>
    <th align="center">2022 Q3 vs. 2022 Q2</th>
  </tr>
  <tr>
    <td align="center">電動車營收（百萬）</td>
    <td align="center">16,861</td>
    <td align="center">14,602</td>
    <td align="center">18,692</td>
    <td align="center">-15.47%</td>
    <td align="center">21.88%</td>
  </tr>
  <tr>
    <td align="center">電動車利潤（百萬）</td>
    <td align="center">5,539</td>
    <td align="center">4,081</td>
    <td align="center">5,212</td>
    <td align="center">-35.73%</td>
    <td align="center">21.7%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">32.9%</td>
    <td align="center">27.9%</td>
    <td align="center">27.9%</td>
    <td align="center">-5%</td>
    <td align="center">-</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">19.2%</td>
    <td align="center">14.6%</td>
    <td align="center">17.2%</td>
    <td align="center">-4.6%</td>
    <td align="center">2.6%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted, 拆股後）</td>
    <td align="center">0.95</td>
    <td align="center">0.65</td>
    <td align="center">0.95</td>
    <td align="center">-46.67%</td>
    <td align="center">31.58%</td>
  </tr>
</table>

- 電動車營收：因上季受困港口的關鍵零組件持續到貨，讓特斯拉生產量比上季多了 41.5%，也就帶動營收成長了 22%，達到約 187 億，佔了總利潤的 87%
- 電動車利潤：利潤比上季成長了約 21%，達到約 54 億，與 Q1 相差無幾；但相比於 2021 時每季均成長 20% 左右，遜色不少
- 毛利率：Q3 毛利率為 27.9%，與 Q2 持平，但比 Q1 少了 4%，但總生產量比 Q1 多了 20%

::: tip
今年總生產量從 Q1 → Q3，依序為：0.5 萬 → 25.9 萬 → 36.6 萬
:::
- 營業利益率：Q2 時因為多了一筆 Restructuring 的費用，導致營業利益率降至 14.6%，本季在消除後這個影響後，營業利益率又重回 17.2%
- EPS：拆股後，EPS 與 Q1 均為 0.95，較 Q2 時上升了 31.6%

本季財報與 Q2 相比，有著顯著的成長，但若與 Q1 相比，則僅有營收成長，其它指標均持平/遜於 Q1 </br>
而往常，在一季優於一季的成長速度下，Q3 通常都會比 Q1 有著 30% - 50% 的成長幅度，而今年 Q2 因供應鏈產生的亂流則打亂了這個趨勢

## 財報重點
財報顯示，特斯拉目前的四大廠（加州、上海、柏林、德州）的總年產量為：
- Model S/X：10 萬輛（今年已生產 50,564）
- Model 3/Y：180 萬輛，其中上海就佔了 75 萬輛（今年已生產 87.9 萬輛）

可以看出，特斯拉對自身的產量是非常樂觀的，但這個數字則須在 2023 的完整年度才有機會達成 </br>
同時也重申了要實現「交付量 50% 年成長」的目標，目前到 Q3 為止，已達成了 65% </br>
若是最後沒達成的話，也算是非常接近這個目標了，以現在約 7,000 億的市值來看，足夠算是成長速度飛快了 </br>

除此之外，財報也揭露了 Semi 卡車將在年底開始交付的消息 </br>
軟體方面，目前 FSD 自動駕駛在北美已累積了 16 萬的使用者，而這星期，自動駕駛巨頭 Argo AI 則面臨了解散的命運 </br>

## 後記
這次股價的走跌，大多數原因是未達到原先的預期 & 成長速度放緩所導致 </br> 
但我認為雖成長速度不如預期，但毛利率＆營益率仍持續成長，表示特斯拉的獲利能力仍在成長，且本季的自由現金流（FCF）與 Q1 相比，成長了 48％，為歷年最佳 </br> 
在其餘科技巨頭的成長性皆呈現放緩＆衰退的情況下，特斯拉仍能保持雙位數的成長率，實屬不易

下圖也可看到特斯拉的營收增長率＆營業利潤率皆處在超越大盤＆同產業的階段，且在最近科技巨頭的股價皆表現低迷的情況下，，特斯拉股價在公佈財報後的這星期也上漲了 11% </br> 
在美元強勢＆加息的壓力下，我仍認為特斯拉在從歷史高點已下跌了 44% 的現在，仍能創造出超越大盤的成績 </br> 
![特斯拉 TSLA 營收增長率 vs. 同產業 vs. SP500](../images/195-01.png)

延伸閱讀：</br>
特斯拉本季生產＆交車輛：[特斯拉（TSLA）發布 2022 Q3 生產及交付量，股價已被腰斬](https://ycjhuo.gitlab.io/blogs/TSLA-P&D-2022-Q3.html) </br>

::: warning
點擊觀看更多： </br>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
