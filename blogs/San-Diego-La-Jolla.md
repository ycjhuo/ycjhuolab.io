---
title: 聖地牙哥全攻略｜拉霍亞 La Jolla｜拉霍亞海灣 La Jolla Cove｜聖地牙哥加利福尼亞大學 University of California San Diego
description: '聖地牙哥 San Diego 必去 必吃 景點 介紹 推薦 攻略|聖地牙哥 San Diego 巴爾波公園 Balboa Park 景點 介紹 推薦 攻略|聖地牙哥 動物園 San Diego Zoo 景點 介紹 推薦 攻略'
date: 2024-04-18
tags: [San Diego]
categories: Travel
---

到了聖地牙哥，怎能不看看海呢？這時候就要來到拉霍亞（La Jolla）這個距離市區車程為 25 分的濱海聖地了，也是在聖地牙哥唯一搭乘 Uber/Lyft 的一次，其他時候都是搭乘輕軌／公車

![聖地牙哥 拉霍亞 La Jolla 加州大學 UC San Diego 必去 景點 介紹 推薦](../images/242-01.png)

## 加州大學的聖地牙哥分校（University of California San Diego）

上圖可看到，拉霍亞距離加州大學的聖地牙哥分校（University of California San Diego）蠻近的，也就 15 分鐘的車程，因此我們就先繞到這邊逛逛校園，並打卡熱門地標「蓋澤爾圖書館」（Geisel Library），這也是電影「全面啟動」的場景之一

從市區到加州大學的聖地牙哥分校，搭乘 Lyft 為 $33

![加州大學聖地牙哥分校 UC San Diego Geisel Library 必去 景點 介紹 推薦](../images/242-02.jpg)

逛完校園後，就前往拉霍亞（La Jolla）囉

## 拉霍亞（La Jolla）
這邊有名的點為沙灘，海景，野生海獅／海豹，另外有個「洞穴」可以深入內部，從加州大學搭 Lyft 到這邊為 $16

## The Cave Store
洞穴位於「The Cave Store」這家店的裡面，沒錯，店裡面居然有洞穴，進去的門票為 $10（可刷卡），在櫃台結帳後，就可經由左邊的樓梯進入，整段路程為 15 分鐘，一路都是木製樓梯，不會危險，也適合小朋友

![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-03.jpg)
![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-04.jpg)

一路沿著樓梯往下走，盡頭就如下圖

![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-05.jpg)
![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-06.jpg)

看完後，出來店家外面，沿著下坡路段走，會到拉荷亞海灣（La Jolla Cove），這邊可在沙灘玩沙，旁邊也有化妝室跟沖水的可以整理 </br>
這整片區域都是公園的一部分，沿著海岸旁的木製圍欄走，可以欣賞海岸線

![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-07.jpg)

在 La Jolla Cove 到 Point La Jolla 之間，可以看到很多，超多，爆多海獅／海豹，牠們完全不怕人，還會直接趴在沙灘上曬日光浴

![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-08.jpg)
![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-09.jpg)
![拉霍亞 La Jolla The Cave Store 必去 景點 介紹 推薦](../images/242-10.jpg)


逛完後，可以回到 拉霍亞（La Jolla）市區吃午餐，或是在探險完 The Cave Store 的洞穴後，直接走 5 分鐘到拉霍亞市區

### La Jolla 必吃 Duke's La Jolla
我吃的是 Duke's La Jolla 這家夏威夷餐廳，在 Google 上有 4,500 個評論的 4.5 顆星餐廳，店內位置很大，但我在平日下午二點到，也等了半小時才入座，就知道生意有多火爆

![拉霍亞 La Jolla Duke's La Jolla 必吃 餐廳 介紹 推薦](../images/242-11.jpg)
![拉霍亞 La Jolla Duke's La Jolla 必吃 餐廳 介紹 推薦](../images/242-12.jpg)
![拉霍亞 La Jolla Duke's La Jolla 必吃 餐廳 介紹 推薦](../images/242-13.jpg)

點了芒果豬肋排（$17.5），椰子炸蝦（$18.5），炸花枝（$16.5），這樣不算酒的話，含稅＆小費，一人約 $33

![拉霍亞 La Jolla Duke's La Jolla 必吃 餐廳 介紹 推薦](../images/242-14.jpg)


### La Jolla 必吃 Bobboi Natural Gelato
天氣熱想吃冰的話，附近也有間 Bobboi Natural Gelato 義式冰淇淋，但其實這家冰淇淋在海洋世界＆市區的小義大利區也有分店
不知道點什麼口味的話，推薦 MOKA，這個由咖啡＋黑巧克力組成的口味

![拉霍亞 La Jolla Bobboi Natural Gelato 必吃 餐廳 介紹 推薦](../images/242-15.jpg)
![拉霍亞 La Jolla Bobboi Natural Gelato 必吃 餐廳 介紹 推薦](../images/242-16.jpg)


## 後記
拉霍亞 La Jolla 這個景點可安排半天左右（不玩水＋去洞穴），如果玩水的話，時間可拉得更長 </br>

拉霍亞市區的話，可能是平日的關係，蠻冷清，可逛的店也不多，The Cave Store 的洞穴，可去可不去，裡面就跟照片一模一樣

當初想在這個區域待上一天，所以在前面加上「加州大學－聖地牙哥分校」的行程，但除了圖書館很新奇，其它的記憶點不多

從拉霍亞搭 Lyft 回聖地牙哥市區為 $35，一整天的交通花費為 $33 + $16 + $35 = $84

若只是想看海的話，第一張圖的「日落懸崖公園」（Sunset Cliffs Natural Park）也可考慮，但就沒有海豹可看（我沒去，網路上查的），以上就是 聖地牙哥 拉霍亞 La Jolla 的景點介紹

::: warning
點擊觀看更多： <br />
- [聖地牙哥景點介紹](https://ycjhuo.gitlab.io/tag/San%20Diego/)
:::
