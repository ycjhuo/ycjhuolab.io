---
title: 聖地牙哥全攻略｜機場如何到市區|舊城區 Old Town｜海港村 Seeport Village｜煤氣燈區 Gaslamp Quarter
description: '聖地牙哥 San Diego 必去 必吃 景點 介紹 推薦 攻略|聖地牙哥 San Diego 大眾交通工具 公車 輕軌|聖地牙哥 San Diego 機場交通 機場到市區|聖地牙哥 San Diego 舊城區 Old Town 必吃|聖地牙哥 San Diego 海港村 Seeport Village 看海 景點|聖地牙哥 San Diego 海港村 Seeport Village 看海 景點|聖地牙哥 San Diego 市區 煤氣燈區 Gaslamp Quarter 必吃'
date: 2024-04-03
tags: [San Diego]
categories: Travel
---

聖地牙哥（San Diego）是加州的第二大城市，僅輸給洛杉磯，從洛杉磯開車只要 2 小時即可到達，若是從迪士尼樂園的安那翰（Anaheim）出發，更只要 1.5 小時

聖地牙哥相比洛杉磯，治安好了很多，雖然還是會看到路上有流浪漢，但數量比洛杉磯少了很多，且大眾運輸方便，也不怕車上有怪人逗留

下圖為聖地牙哥市區的主要景點，各景點之間的車程都只要 10-20 分鐘即可抵達

![聖地牙哥市區景點介紹](../images/239-01.png)

## 聖地牙哥大眾運輸
聖地牙哥的大眾運輸對於沒開車的旅客來說非常方便，分為「公車、和「輕軌」二種，搭一次不限距離只要 $2.5，且二種交通工具可以互相轉乘，在第一次刷卡後，App 上會顯示免費轉乘的時間

![聖地牙哥 大眾運輸介紹 公車](../images/239-09.jpg)
![聖地牙哥 大眾運輸介紹 輕軌](../images/239-10.jpg)

公車在上車時要刷卡，輕軌則是上車前要在車站附近掃描機器，我自己搭了四天都沒遇到人工查票，下車時不須刷卡 <br />

![聖地牙哥 大眾運輸介紹 輕軌 刷票機](../images/239-11.jpg)

聖地牙哥的 App 為「Pronto - San Diego」，下載後註冊一個帳號就會得到一個 QR code（也就是虛擬卡），再用信用卡事先儲值（如同悠遊卡）

## 聖地牙哥景點介紹

### 機場 → 煤氣燈區飯店（Gaslamp Quarter）
這次搭乘 United 的班機，從紐約 EWR 機場 → 聖地牙哥 SAN 機場（06:31 - 09:50），購買可以帶手提行李的經濟艙，一人只須 $179.22，飛了 6 小時後就抵達聖地牙哥囉 <br />

下飛機後，在出口就能看到直達市區的 992 公車，每 15 分鐘一班，半小時即可到我在煤氣燈區的飯店（花費 $2.5），若是搭 Uber 則 20 分鐘就能到，但車費要 $23

![聖地牙哥機場公車交通](../images/239-02.jpg)

公車在市區會沿著 Broadway 開，若跟我一樣住在煤氣燈區的，則在 Broadway 跟 九大道（Ninth Ave.） 交叉口下車，再走 10 分鍾就能到達飯店

::: tip
強烈建議住在 Broadway 附近的人，到 九大道（Ninth Ave.）就下車，因為再過去的十大道（Tenth Ave）跟 十一大道（Eleventh Ave.）有很多流浪漢，不建議走路經過，若是開車就沒關係
:::

### 舊城區（Old Town）
在飯店 Check-in 完後，附近有輕軌，搭到「Old Town Transit Center」站就到舊城區囉，裡面有很多古蹟建築以及墨西哥餐廳 <br />

![聖地牙哥 Old Town](../images/239-03.jpg)

來到這裡最重要的就是吃「Cafe Coyote」這間有破萬 Google 評價，且 4.3 顆星的墨西哥餐廳，因為墨西哥菜有很多我不喜歡的豆豆泥配菜，所以就單點了三個 Taco，這樣只要 $38.2 含小費（玉米片是送的），這家的 Taco 尺寸是平常的 2-3 倍大，一個人吃 2 個就差不多了，個人推薦牛肉跟炸魚的最好吃

![聖地牙哥 舊城區 餐廳推薦 必吃 Casa Guadalajara](../images/239-04.jpg)


### 海港村（Seeport Village）
聖地牙哥看夕陽的熱門景點，裡面有餐廳，攤販等，也有公園可以放風箏，算是離市區最近看海景點

![聖地牙哥 景點推薦 海港村 Seeport Village](../images/239-05.jpg)


### 煤氣燈區（Gaslamp Quarter）
餐廳與酒吧林立的地區，偶爾也會封街來讓攤販擺攤（可想成白天版的夜市），在市區是屬於治安較好的區域，若是住在市區的話，推薦住在這邊或是靠近海港村，只要不靠近 Boardway 就不太會看到流浪漢，晚上也很安全，代表性的拱門，晚上還會發光

![聖地牙哥 景點推薦 煤氣燈區 Gaslamp Quarter](../images/239-06.jpg)

這邊推薦「Tacos El Gordo」這家 Taco 店，Google 評價 4.6 顆星，這家跟舊城區「Casa Guadalajara」，它的 Taco 尺寸就是紐約平常吃到的尺寸，一個 Taco 不到 $4，可當作小吃

![聖地牙哥 餐廳推薦 煤氣燈區 必吃 Tacos El Gordo](../images/239-12.jpg)
![聖地牙哥 餐廳推薦 煤氣燈區 必吃 Tacos El Gordo](../images/239-13.jpg)

## 後記
到這邊，就結束第一天了，第二天開始，有完整的一天可以安排行程，可以參考這篇 [CityPass](https://ycjhuo.gitlab.io/blogs/San-Diego-CityPass.html) ，CityPass 是一個套票，裡面包含了大部分聖地牙哥的付費景點，很適合會在聖地牙哥待 4 天以上的遊客

::: warning
點擊觀看更多： <br />
- [聖地牙哥景點介紹](https://ycjhuo.gitlab.io/tag/San%20Diego/)
:::
