---
title: Facebook（FB）2021 Q2 季報，EPS 同期成長 100%
description: '美股投資 Facebook FB|FB 2021 Q2 第二季財報|Facebook 疫情|Facebook 庫藏股'
date: 2021-08-04
tags: [Stock, FB]
categories: Investment
---

社交龍頭 Facebook（FB）在 07/28 公佈了 2021 第二季的財報，從 2021 Q1 季報公佈後（04/28 盤後），到公佈 Q2 季報（07/28 盤後），三個月的時間，股價從 307.1 → 373.28，上漲了 21.6% </br>
同期間，納斯達克指數（Nasdaq）上漲了 5.1%

---

Facebook 2021 Q1 財報回顧：[Facebook（FB）公布 2021 Q1 季報，交出季成長翻倍的成績單](https://ycjhuo.gitlab.io/blogs/FB-Earnings-2021-Q1.html) </br>

## Facebook 2021 Q2 季報
[資料來源](https://s21.q4cdn.com/399680738/files/doc_financials/2021/q2/FB-06.30.2021-Exhibit-99.1_final.pdf)
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q1 vs. 2020 Q4</th>
    <th align="center">2021 Q2 vs. 2021 Q1</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">26,171</td>
    <td align="center">29,077</td>
    <td align="center">-6.77%</td>
    <td align="center">11.10%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">11,503</td>
    <td align="center">12,513</td>
    <td align="center">-11.89%</td>
    <td align="center">8.78%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">80.39%</td>
    <td align="center">81.43%</td>
    <td align="center">-1.05%</td>
    <td align="center">1.04%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">43.48%</td>
    <td align="center">42.53%</td>
    <td align="center">-2.03%</td>
    <td align="center">-0.94%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">3.3</td>
    <td align="center">3.61</td>
    <td align="center">-14.95%</td>
    <td align="center">9.39%</td>
  </tr>
</table>

- 營收方面：Facebook 的營收季節變化明顯，Q4 最高，Q1 為最低，因此 Q2 與 Q1 相比，理所當然地會呈現增加的趨勢；但若與去年同期（2020 Q2）相比，成長了 56%
- 稅前淨利：與 Q1 相比，成長了 8.78%；但若與去年同期（2020 Q2）相比，成長了 104%（ Q1與去年同期相比，則是成長 96.28% ）
- 毛利率＆營業利益率：與 Q1 差不多，維持在 81% ＆ 42%（2020 年營業利益率平均為 37% ）
- EPS：與 Q1 相比，成長了近 10%；與去年同期（ 2020 Q2 ）相比，成長了 100%；光 2021 年的前二季 EPS 就已達成 2020 全年 EPS 的近 70%

![Facebook 近二年營收](../images/FB-2021-Q2-Revenue.png)<br/>

由於毛利＆營益利益率均變化不大，接著從使用者活躍數來觀察這是不是推升淨利的關鍵

## 使用者活躍數（DAUs）
Facebook 在本季雖然各項數值相比 Q1 均為成長，但成長率相比 2020 Q4 ~ 2021 Q1 降低不少 </br>

除了歐洲的使用人數稍微下滑外，亞洲/太平洋地區成長的幅度最高（ 3.7%）

- Facebook 每日活動用戶 DAUs（daily active users）：與 Q1 相比，成長了 1.6%，目前人數為 19.1 億（ Q1 成長率為 2.17%） </br>
- 整個家族（包含 Facebook, Instagram, What's app）的日活動用戶：與 Q1 相比，成長了 1.47%，目前人數為 27.6 億（ Q1 成長率為 4.62%）</br>
- Facebook 月活用戶：相比 Q1 成長了 1.75%，人數為 29 億（ Q1 成長率為 1.79%）</br>
- 家族的月活用戶：相比 Q1 成長了 1.74%，人數為 35.1 億（ Q1 成長率為 4.55%）</br>

![Facebook 每日使用者活躍數 2021 Q2](../images/FB-DAU-2021Q2.png)<br/>

上圖可看到，Facebook 使用者的成長速度並不明顯，那就可以推測營收的成長的因素是因為每位使用者所帶來的收入增加了 </br>
下圖顯示了：每位使用者平均為 Facebook 帶來 $10.12 的收入（相比上季成長了 9.17%，與營收成長 11.1% 差不多）
- 北美使用者：平均價值為 $53.01，成長了 10.37%
- 歐洲使用者：平均價值為 $17.23，成長了 11.23%
- 亞太使用者：平均價值為 $4.16，成長了 5.58%
- 其他地區使用者：平均價值為 $3.05，成長了 15.53%

![Facebook 使用者平均價值 2021 Q2](../images/FB-ARPU-2021Q2.png)<br/>

原本在上篇[Facebook（FB）公布 2021 Q1 季報，交出季成長翻倍的成績單](https://ycjhuo.gitlab.io/blogs/FB-Earnings-2021-Q1.html) 提到：

::: tip
在目前北美及歐洲的使用者增長幾乎已停滯的情況下，未來 Facebook 要提升營收只能寄望在亞太區及其他市場 </br>
但在滲透率已如此高的現在，想辦法提高每位使用者的貢獻度更能有效地讓營收再上一層樓，但這也不是件容易的事 </br>
:::

從這季財報看來，Facebook 仍能繼續提升每位使用者的貢獻度，除亞太市場外，其他市場的使用者平均價值都提升了二位數 </br>

## Facebook 庫藏股

Facebook 在本季仍繼續實施庫藏股政策，約投入了 70.8 億來回購股票（股價在這段時間上漲 21.6%；S&P 500 上漲 5.2%）

::: tip
- 2021 Q1 投入 39 億來回購（股價在這段時間上漲 9.5%；S&P 500 上漲 7.36%）
- 2020 Q4 投入 19 億來回購股票（股價在這段時間上漲 2.45%；S&P 500 上漲 11.1%）
:::

## 後記
Facebook 在同是社交領域的 SNAP 於 7/22 公佈 2021 Q2 財報後，受到 SNAP 亮麗財報的激勵（SNAP 股價於隔日上漲了 23.8%），隔天（7/23）Facebook 股價也隨之上漲了 5.3%（351.19 → 369.79）</br>

截至目前（8/3），Facebook 財報發佈後的第四天，股價又回到財報發佈前的 $351.24 </br>

但這並不代表 Facebook 財報不好，而可能是因管理層在財報中表示後續的二個季度，由於疫情逐漸和緩，Facebook 無法繼續保持 Q1 & Q2 的高成長率，而造成上漲的動力無法持續 </br>

截至現在，Facebook 今年的表現仍在科技五巨頭中排行第二（上漲了 32.83%），第一名為 Google 的 57.18%，顯示了線上廣告市場在今年的強勁復甦 </br>

Facebook 目前的本益比受惠於本季 EPS 的成長，降到 26.07；對比先前本益比均在 3x 左右，我認為目前是個適合逢低買進的價位 </br>

![Facebook 2021 Q2 上漲幅度](../images/FB-YTD-2021Q2.png)

更多 Facebook 本益比的歷史資料，可參考：</br>
[Facebook & Google 公佈 2020 Q3 財報，線上廣告的市場是否能持續成長？](https://ycjhuo.gitlab.io/blogs/2020-Q3-Earnings-FB-GOOG.html) </br>

看更多圖表：</br>
[Facebook 2021 Q2 圖表](https://s21.q4cdn.com/399680738/files/doc_financials/2021/q2/Q2-2021_Earnings-Presentation.pdf) </br>


延伸閱讀： </br>
[Google（GOOG）2021 Q2 季報，近一年來表現最好的科技巨頭](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q2.html) </br>