---
title: 如何解決 VuePress 在上傳 Gitlab 後找不到 plugin
description: 'VuePress Gitlab|VuePress plugin|warning An error was encountered in plugin "container|VuePress 找不到 Plugin|VuePress 找不到 container|VuePress container'
date: 2022-01-21
tags: [VuePress, Git]
categories: Blog
---

最近用 VuePress 在上傳新文章到 Gitlab 時，發現在 build 的階段，會出現這個錯誤訊息： </br> ```warning An error was encountered in plugin "container"``` 

字面意思是：在渲染的過程中，VuePress 找不到 container 這個插件，雖可以組建成功，但組建完後會因缺少 ```container``` 這個 plugin，而無法實現 ```container``` 原有的功能


## VuePress Container
```container```  這個插件，是用在渲染 Markdown 的自定義容器，如：</br>

::: tip
tip 提示框
:::

::: warning
warning 警告框
:::


缺少 ```container``` 後，原有的渲染效果消失，就會變成：

:::
tip 提示框
:::

## VuePress 找不到 Plugin
而這次遇到的這個錯誤訊息 ```warning An error was encountered in plugin "container"```，是僅出現在 push 檔案到 Gitlab 上才會發生的，在本機 (localhost) 的環境 build 會是正常的

```powershell{7-14}
$ yarn build
yarn run v1.22.4
$ vuepress build .
wait Extracting site metadata...
tip Apply local theme at /builds/ycjhuo/ycjhuo.gitlab.io/.vuepress/theme...
tip Apply theme local ...
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "container"
warning An error was encountered in plugin "vuepress-plugin-container"
tip Apply plugin @vuepress/last-updated (i.e. "@vuepress/plugin-last-updated") ...
tip Apply plugin @vuepress/register-components (i.e. "@vuepress/plugin-register-components") ...
```


## 解決方法
1. 更新 ```.gitlab-ci.yml ``` 裡面的 node 版本，我自己本來是從 ```node:10.22.0``` → ```node:15.7.0 ``` </br>
可直接參照我的  ```.gitlab-ci.yml ```

```yml
image: node:15.7.0
pages:
  cache:
    paths:
    - node_modules/

  script:
  - yarn install # npm install
  - yarn build # npm run docs:build
  artifacts:
    paths:
    - public
  only:
  - master
```

2. 將 ```yarn.lock ``` 加入 ```.gitignore ``` 裡面，如：</br>

```powershell 
node_modules/
yarn.error
.DS_Store
```


參考上面二個步驟，即可解決 VuePress 在 Gitlab 上找不到 Plugin 的問題 </br>
```warning An error was encountered in plugin "container"```