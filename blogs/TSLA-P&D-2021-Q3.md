---
title: 特斯拉（TSLA）2021 Q3 生產及交付量，將帶來史上最好的財報
description: '美股特斯拉|特斯拉 TSLA|特斯拉 2021 Q3 第三季產量|TSLA 2021 Q3 第三季交車量'
date: 2021-10-06
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 10/02（六）公佈了第三季的生產 & 交車數量，在 Q2 財報發布（7/2）到 公佈 Q3（10/2）交車數據的這段期間內，特斯拉股價上漲了 14.19%，同期間內 S&P 500 指數下跌了 0.1%，可看出特斯拉的表現是較為強勁的 </br>

## 特斯拉 2021 Q3 生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">差距（QoQ）</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">2,340</td>
    <td align="center">8,941</td>
    <td align="center">282.09%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">204,081</td>
    <td align="center">228,882</td>
    <td align="center">12.15%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">1,895</td>
    <td align="center">9,275</td>
    <td align="center">389.45%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">199,409</td>
    <td align="center">232,025</td>
    <td align="center">16.36%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">206,421</td>
    <td align="center">237,823</td>
    <td align="center">15.21%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">241,300</td>
    <td align="center">184,877</td>
    <td align="center">19.87%</td>
  </tr>
</table>

隨著 Model S Plaid 完成改款升級並回歸生產線後，Model S/X 的生產 & 交付量在 Q3 均有著三位數的成長（282% & 389%） </br>

而在 Model 3/Y 的部分：
- 生產量來說：與相比上季，成長了 12.15%，且近四次的生產量都保持著 10% 以上的增長速度，今年累計生產量已達到 61.3 萬台（已是 2020 總生產量的 134.8%）
- 交付量的話：相比上季，成長了 16.36%，除了上季交付量成長幅度為 9.06%，其餘季度也維持著 10% 以上的增長，今年累計交付量已達到 61.4 萬台（已是 2020 總交付量的 139%）

## 特斯拉各車款交車速度

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">價格</th>
    <th align="center">行駛距離（km）</th>
    <th align="center">最快速度（kmh）</th>
    <th align="center">0 - 97 km 加速度（秒）</th>
    <th align="center">預計可交車時間</th>
  </tr>
  <tr>
    <td align="center">Model S Plaid / 長程版</td>
    <td align="center">$129,990 / $89,990</td>
    <td align="center">628 / 652</td>
    <td align="center">322 / 249</td>
    <td align="center">1.99 / 3.1</td>
    <td align="center">4 個月 / 6 個月</td>
  </tr>
  <tr>
    <td align="center">Model X Plaid / 長程版</td>
    <td align="center">$114,690 / $99,990</td>
    <td align="center">547 / 579</td>
    <td align="center">262 / 249</td>
    <td align="center">2.5 / 3.8</td>
    <td align="center">7 - 8 個月</td>
  </tr>
  <tr>
    <td align="center">Model Y 長程版 / 高性能版</td>
    <td align="center">$54,990 / $61,990</td>
    <td align="center">525 / 488</td>
    <td align="center">217 / 249</td>
    <td align="center">4.8 / 3.5</td>
    <td align="center">6 / 2 個月</td>
  </tr>
  <tr>
    <td align="center">Model 3 標準/長程/高性能版</td>
    <td align="center">$41,990 / $49,990 / 57,990</td>
    <td align="center">423 / 568 / 507</td>
    <td align="center">225 / 233 / 261</td>
    <td align="center">5.3 / 4.2 / 3.1</td>
    <td align="center">6 / 2 / 1 個月</td>
  </tr>
</table>

與上季比較，特斯拉各車款幾乎將售價都上調了 1 - 2,000 美金，拿售價最低的 Model 3 標準版來說，晚了 3 個月訂，售價就上漲了 5% </br>
在交車速度方面，除了 Model 3 的長程版＆高性能版提前到下訂二個月內即可拿車，其餘車款皆延長了 3 - 6 個月不等 </br>

詳細變化如下：
- Model S：長程版上調 1,000，Plaid 是唯一售價不變的車種，交車時間均延長了 3 個月
- Model X：長程版上調 1,500，Plaid 上調 5,300，交車時間均延長了 1 個月，來到了 8 - 9 月
- Model Y：各版本售價上調了 1 - 2,000 美金，長程版交車時間延長了 4 個月，下訂後須等待 6 個月才可拿到車；高性能版則維持 2 個月
- Model 3：各版本售價均上調 1 - 2,000 美金，交車速度除了標準版延長至 6 個月外，其餘二個版本均提前到下訂 2 個月內即可拿到車

## 後記
特斯拉在 Q3 的生產＆交付量又創新高（且還是在目前全球晶片短缺的情況下），如此快速的增長速度（季成長 10% 以上），大部分的交車速度仍是被延長的，從這點看來，特斯拉買氣仍在持續上升中 </br>

且從特斯拉調漲售價來看，顯示出它並不怕消費者會因漲價而轉而購買其它廠牌的車；消費者對於特斯拉品牌的認同及喜愛，也成為了特斯拉的護城河 </br>

這季因為高價車款（Model S/X）生產＆交付量的急速增長（282% & 389%），可預見特斯拉在 Q3 財報上，毛利率將能突破 30%（Q2 為 28.4%），並再次推高特斯拉的獲利能力 </br>

有了上面的優點，也難怪在這 1 個月，科技股股價皆下跌的趨勢中，特斯拉仍能逆勢成長（代表科技股的那斯達克指數 QQQ 下跌 6.86% vs. 特斯拉上漲 3.6%），目前距離 1 月底的歷史高點（$883），僅剩 13% 的距離，而相信特斯拉將能用強勁的基本面毫無阻礙的將股價再創新高