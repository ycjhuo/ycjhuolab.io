---
title: 美國運通（American Express）信用卡購買保護（Purchase Protection）申請教學
description: '美國運通 American Express AE 大白卡|Platinum 白金卡|信用卡購買保護|Purchase Protection|如何申請保險教學'
date: 2021-07-02
tags: [Credit Cards, AMEX]
categories: Investment
---

最近因為不小心摔壞了 Dyson 吸塵器，正好可以利用這個機會來分享該怎麼申請美國運通的購買保護 </br>
[美國運通購買保護原文](https://www.americanexpress.com/en-us/credit-cards/credit-intel/purchase-protection) </br>

下表是購買保護的賠償金額，如果是高價的商品，推薦大家可以用美國運通白金卡來購買

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">每次 Claim 金額上限</th>
    <th align="center">每年 Claim 金額上限</th>
  </tr>
  <tr>
    <td align="center">信用卡年費 < $250 </td>
    <td align="center">$1,000</td>
    <td align="center">$50,000</td>
  </tr>
  <tr>
    <td align="center">信用卡年費 > $250 </td>
    <td align="center">$10,000</td>
    <td align="center">$50,000</td>

  </tr>
</table>

::: tip
要申請美國運通的購買保護須符合以下二點：
1. 要使用美國運通的信用卡購買
2. 商品須在購買的 90 天以內發生損壞或遺失
:::

## 申請購買保護
首先到美國運通的 [申請頁面（Claims Center）](https://claims-center.americanexpress.com) 點擊 File a Claim </br>

![AMEX Claims Center](../images/92-01.png)

點擊後可以看到共有 7 個步驟

### 選擇購買該商品的信用卡
選擇好信用卡，填上商品購買日期後，按下一步 （Continue）
![Choose Card](../images/92-02.png)

### 選擇購買保護
美國運通信用卡共有三種福利：延長保固（Extended Warranty）、購買保護（Purchase Protection）、退貨保護（Return Protection） </br>
這邊我們選擇 購買保護（Purchase Protection）
![Claim Type](../images/92-03.png)

### 填寫損壞資訊
輸入商品損壞的日期（需要購買的 90 日內才符合購買保護的條件），發生原因（選擇損壞），以及對於商品損壞的描述 </br>
下方填上商品損壞時的國家、城市、州名後，按下一步 （Continue）
![Loss Information](../images/92-04.png)

### 輸入商品資訊
按下 Add Item 來到填寫損壞東西的資訊頁
![Add Item](../images/92-05.png)

這邊要寫上購買商品的名字、日期、類型、購買的地方（Merchant） 以及商品的製造商 （Manufacturer） </br>
填好後，按下儲存 （Save） </br>

![Item Detailed](../images/92-06.png)

就會回到列表頁（可以填寫多種以上的商品），若只有一種的話則按下一步 （Continue）</br>

![Item List](../images/92-07.png)

### 填寫個人資訊
![Personal Infomation](../images/92-08.png)

### 送出 Claim
![Claim Submit](../images/92-09.png)

### Claim Confirmation
![Claim Confirm](../images/92-10.png)

申請完後，過幾天會收到美國運通寄來的確認信 <br />

有關後續須附上的文件可參考這篇：<br />
[美國運通（American Express）信用卡購買保護（Purchase Protection）提交文件](https://ycjhuo.gitlab.io/blogs/AMEX-Purchase-Protection-Submit.html)<br />

對手機保護有興趣的可參考這篇： <br />
[美國運通（American Express）信用卡手機保護（Cell Phone Protection）申請教學](https://ycjhuo.gitlab.io/blogs/AMEX-Phone-Protection.html)<br />


::: warning
點擊觀看更多： </br>
- [美國運通福利文](https://ycjhuo.gitlab.io/tag/Credit%20Cards/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::