---
title: 台灣美國之間匯款利器，花旗全球速匯（美國匯款到台灣篇）
description: '美國匯錢回台灣|免手續費跨國匯款|跨國匯款推薦|花旗銀行全球速匯介紹|Citibank Global Transfers|美金匯回台灣最省錢方法|美國轉錢回台灣|美金轉台幣|台幣美金互換'
date: 2021-01-18 18:05:43
tags: [Global Transfers]
categories: Investment
---

::: danger
台灣花旗已於 2023/07/23 停止「全球速匯」服務 </br>
可參考替代方案： [中國信託 CTBC 全球速匯介紹](https://ycjhuo.gitlab.io/blogs/CTBC-Global-Transfers-US-To-TW.html)
:::

這篇是介紹跨國轉帳：美國 ➔ 台灣；若對台灣 ➔ 美國有興趣的，可參考這篇 </br> 
[台灣美國之間匯款利器，花旗全球速匯（台灣匯款到美國篇）](https://ycjhuo.gitlab.io/blogs/Citi-Global-Transfers-TW-To-US.html)

最近股市一片欣欣向榮，從三月中疫情之後，大盤就一路向上，完全沒有趨緩的跡象。S&P500 指數從最低點的 2,304.92（2020/03/20）到今天（2021/01/18）的 3,768.25點，約十個月的時間，漲幅為 63.5%。這個驚人的報酬率，讓很多人躍躍欲試的想投入到美股市場。

而現在開美國證券戶相當的容易，只須找到喜歡的券商，直接線上開戶即可，且交易股票都是免手續費。但唯一的成本就是該如何從台灣匯款到美國券商的戶頭呢？

以我先前的經驗，永豐的跨國匯款費用是 $600，兆豐則為 $840（美金匯率都以 30 來算）。而若要從美國券商（Schwab）將錢匯回台灣的話，費用為 ＄25 美金（約台幣 $750），這樣一來一回，匯款手續費就差不多 $1500了。

對於在台灣的美股投資人，或是在美國工作的台灣人，若有定期將錢匯回台灣的需求，會是一筆不小的負擔，那有沒有方法能夠免除掉這個跨國匯款費用呢？

這裡就要來介紹花旗銀行的全球速匯（Citibank Global Transfers），可以讓我們 0 成本的實現台美之間的跨國轉帳。

如果要看如何從台灣匯款到美國的，可以參考這一篇[台灣美國之間匯款利器，花旗全球速匯（台灣匯款到美國篇）](https://ycjhuo.gitlab.io/blogs/Citi-Global-Transfers-TW-To-US.html)

## 全球速匯（Global Transfers） 開戶
全球速匯是花旗銀行提供給在 台灣 及 美國 都有 花旗帳戶 的客戶的一項免手續費的跨國轉帳服務。因此，要使用這個服務，需要在台灣及美國都有帳戶才可以。美國的花旗銀行不支援線上開戶，因此一定要親自跑到分行開戶才可。

### 美國花旗帳戶選擇
美國花旗帳戶在開戶時，會有多種等級可以選擇，越高的等級，享受的福利越多，但每個月的帳戶管理費也越高。美國的銀行帳戶每月都會收取帳戶管理費（但都有方法可以免除，帳戶的等級越高，免除的條件也越高）。

這裡我推薦 Citi Basic Account Package，這是最初階的花旗帳戶，每月帳管費為 $25，但只要帳戶餘額保持在 $1500+ 就可免除帳管費（或是每個月有一次薪轉紀錄，這對沒在美國工作的人比較難達成）
::: tip
美國花旗在開戶時，須提供電話號碼及住址，系統會偵測這個住址是不是屬於商業區等，因此不確定若填寫飯店的住址能否成功通過。
:::

### 台灣花旗帳戶選擇
台灣的話我是選擇 Smart 活存，沒有帳款費，若帳戶餘額 > 5 萬時，則提供國內 5 次跨行提款免手續費。<br/>
開戶時，也要記得跟行員說要一起開啟外幣帳戶。
::: tip
2021/01/18 確認台灣花旗可線上開戶，但若具有美國稅務身份（也就是有填寫過 W-9，而不是 W-8BEN 的人），則須本人到分行才能開戶
:::

## 美國匯款到台灣 設定
美國匯款到台灣這一段可以直接透過美國花旗的網銀操作，到 Payments & Transfer 這頁的 Any External Account。
接著點選 Manage Payees 的 PEOPLE 分頁就可以開始設定收款人的資訊。

![47-01](../images/47-01.png)
![47-02](../images/47-02.png)

設定好後，我們就可以在 Payments & Transfer 這頁中的 Any External Account， Transfer to Others 這個區塊看到剛剛設定的收款人資訊了。

## 美國匯款到台灣 匯款
在設定好台灣帳戶的資訊後，就可以開始來匯款了

1. 首先輸入要匯款的金額，從哪個帳號匯款，以及匯款目的，之後按下 Next 進入到匯款明細的頁面。
![47-03](../images/47-03.png)
::: tip
美國的銀行帳號分為支票帳戶（Checking）與儲蓄帳戶（Saving）二種，開戶時兩種都會一起開。<br/>
支票帳戶沒利息，如果有寫支票的話，都是從這個帳號去扣款<br/>
儲蓄帳戶有利息，但也很少，現在大概是 0.03%（類似於台灣的活存）。每月限制轉出的次數為 6 次，超過就可能會收手續費。
:::
2. 到了匯款明細的頁面。可看到我們這次的匯款費用是 $0，在確認沒問題後，按下 Send 即可完成我們這次的匯款。
![47-04](../images/47-04.png)

3. 完成後就會跳轉到結果頁，可以看到我們這次匯款與收款人的資訊，以及大約需要 4 天才會到帳的訊息。
![47-05](../images/47-05.png)

4. 經過實測後，不到 12 小時就可以在台灣花旗帳戶中的外幣帳戶交易明細看到那筆交易囉。
![47-06](../images/47-06.png)


## 後記
花旗銀行於 2021/04/15 宣布即將退出台灣市場後，從美國（免費）匯款到台灣的方法可能又會少一條 </br>
若真的因為花旗銀行因為退出台灣市場而讓我們無法再使用 ```全球速匯``` 這個免費台灣 ⇄ 美國匯款服務的話 </br> 

也可透過開設 ```嘉信理財（Charles Schwab）``` 帳號來達到，將美國的錢，轉回台灣來使用 </br> 

若對這個方法有興趣的話，可參考我的這篇文章：</br> 
[投資美股，最推薦的美股券商嘉信理財（Charles Schwab）](https://ycjhuo.gitlab.io/blogs/Brokerage-Account-Charles-Schwab.html)

手機版的全球速匯教學可參考：</br>
[如何從台灣匯錢到美國，而且免手續費？花旗全球速匯手機版（台灣 → 美國）](https://ycjhuo.gitlab.io/blogs/Citi-Global-Transfers-TW-To-US-Mobile.html)

::: warning
2023/03/11 更新另一個 美國 ➔ 台灣 匯款新方法 [(花旗全球速匯走入歷史? 中國信託全球速匯介紹（美國匯款到台灣篇）](https://ycjhuo.gitlab.io/blogs/CTBC-Global-Transfers-US-To-TW.html)
:::

::: warning
點擊觀看更多： </br>
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::