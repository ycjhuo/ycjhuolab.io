---
title: 特斯拉（TSLA）2023 Q4 生產及交付量｜總產量年成長35%，但明年恐無法維持
description: '美股特斯拉|特斯拉 TSLA|特斯拉生產交車量 2023 Q4|特斯拉 Model 3 Y Cybertrunk生產交車量 2023 Q4 第四季'
date: 2024-01-06
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 01/02（二）公佈了 2023 第四季的生產 & 交車數量

在上次財報發布（ Q3 ）→ 公佈最新交車數量的期間（10/18 → 01/02），特斯拉的股價小幅上漲 2%，而 S&P 500 上漲 10% <br />

表示市場對於上次財報不太滿意，而上個季度的確因為產線停工升級的關係，造成生產＆交付量均下滑，連帶導致財報成績不理想 <br />

那麼，在產線停工升級後，大家對於特斯拉是否重拾信心了呢？ 這點從生產＆交付量公布後，到目前（01/05），股價跌了 5％ 可看的出來

::: tip
本季因為 Cybertrunk 的加入，原本 Model S/X production 分類改成 Other Models，Model 3/Y 則保持不變
:::

## 特斯拉 2023 Q4 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q3</th>
    <th align="center">2023 Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">Other Models 生產量</td>
    <td align="center">13,688</td>
    <td align="center">18,212</td>
    <td align="center">--</td>
    <td align="center">-30%</td>
    <td align="center">33%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">416,800</td>
    <td align="center">476,777</td>
    <td align="center">9%</td>
    <td align="center">-9%</td>
    <td align="center">14%</td>
  </tr>
  <tr>
    <td align="center">Other Models 交付量</td>
    <td align="center">15,985</td>
    <td align="center">22,969</td>
    <td align="center">80%</td>
    <td align="center">-17%</td>
    <td align="center">44%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">419,074</td>
    <td align="center">461,538</td>
    <td align="center">8%</td>
    <td align="center">-6%</td>
    <td align="center">10%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">430,488</td>
    <td align="center">494,989</td>
    <td align="center">9%</td>
    <td align="center">-10</td>
    <td align="center">15%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">435,059</td>
    <td align="center">484,507</td>
    <td align="center">10%</td>
    <td align="center">-7%</td>
    <td align="center">11%</td>
  </tr>
</table>

2023 Q4 生產/交付量：
- Other Models：本季產量為 1.8 萬，雖然加入了 Cybertrunk，但產量稀少。因為上季產線停工的關係，季成長為 33%，但產量仍比停工前少了 7%
- Other Models：本季交付 2.3 萬，是 2023 的最佳季度，季成長 44%
- Model 3/Y：產量為 47 萬輛，季成長為 14%，成長速度重回雙位數，且成長速度也是 2023 年最快的一季
- Model 3/Y：交付 48.5 萬輛，季成長 11%，季成長也是 2023 年最快

2023 年總共生產了 184.6 萬輛車，年成長 35%（2022 年成長為 47%），其中：
- Model 3/Y：生產了 177.5 萬輛，年成長 37%（2022 年成長為 43%）
- Other Models：生產了 7.1 萬輛，與 2022 持平

總交付量 180.9 萬輛，Model 3/Y 佔了 174 萬，年成長 40%（2022 年成長為 37%），Other Models 則交付了 6.9 萬輛，與去年一致

不管是季成長或是年成長，特斯拉這次的表現都很不錯，在產量已越來越高的情況下，成長速度還能保持在一定水準，並沒有出現大幅趨緩的現象

## 特斯拉 2024 產能與成長性
根據特斯拉 Q3 的財報，目前 Model 3/Y 的年產量為 212.5 萬，若再加上其他車款，2024 的總產量將可到達 220 萬的水準，到時，年成長會是 20% <br />
與今年的 35% 相比，年成長會衰退近一半，且 Cybertruck 須再 2-3 年才能實現年產量 25 萬，要達到 Model 3/Y 年產能 170 萬的數量級，在五年內都很困難 <br />
因此，特斯拉在未來二年，產能上難有爆發性突破，僅能維持穩定增長，並將焦點放在自動駕駛 FSD 與超級電腦 Dojo 等「軟」實力上

## 後記
最近媒體一直在說比亞迪 Q4 銷量以 52.6 萬輛超越了特斯拉的 48.5 萬輛，讓特斯拉痛失了銷售龍頭的寶座，是否光環不在 <br />

首先，特斯拉的車款價格貴了比亞迪 60% 左右，表示二者的目標客群與價格區間有段落差，且依據 Q3 財報，比亞迪營收為 87.5 億跟特斯拉的 233 億，差距更為明顯

依比亞迪 Q3 財報，毛利率約為 20%，與特斯拉的 18% 相差無幾，在這個條件下，特斯拉賣了 1 台車，比亞迪須賣出 1.6 台才能得到一樣的利潤，這還是考慮了比亞迪的營運成本與特斯拉一致的情況，但特斯拉的營業利潤率一直都是穩居同業冠軍，所以二者的差距只會更大，而不會縮小 <br />

熟悉特斯拉的投資人，都能輕易看出二者的差距，也相信這種比亞迪超越特斯拉的報導會在今年一直出現，這時，特斯拉的支持者，是否會不為所動，繼續堅持，或是覺得大勢已去，趕緊出清持股呢？

::: warning
點擊觀看更多： </br>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::