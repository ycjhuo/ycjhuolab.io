---
title: 美股券商帳戶轉出教學 (從德美利證券 TD Ameritrade 轉到嘉信理財 Charles Schwab)
description: '美國券商推薦|美國券商介紹|美國券商哪個好|TD 轉 嘉信|TD 轉 Schwab|美股券商轉出|美股帳號轉出|美股券商更改|嘉信提款卡|美金匯台灣|ATM 0手續費提款|花旗撤出台灣'
date: 2021-05-12
tags: [Brokerage account, Global Transfers]
categories: Investment
---

嘉信理財 Charles Schwab 與 德美利證券 TD Ameritrade 於 2019/11/25 宣布合併，而我剛好這二個券商都有帳號

雖然不確定他們之後會不會將這二個平台合併，但為了方便管理（以及嘉信帳戶總額達到 25 萬之後，美國運通白金卡年費就可以 -$100），我決定將 TD Ameritrade 帳號內的所有股票及資金都轉到 嘉信理財 Schwab

::: tip
Receive a $100 Card statement credit if your qualifying Schwab Holdings are <= $250,000 </br>
or receive a $200 Card statement credit if your qualifying Schwab Holdings are <= $1,000,000
:::

對於美國運通白金卡（嘉信理財聯名卡版本）有興趣的可以參考這篇：</br>
[如何透過嘉信理財聯名卡將美國運通點數（AMEX MR）兌換成現金](https://ycjhuo.gitlab.io/blogs/AMEX-Cash-Out-Points.html)

---

::: warning
轉移帳號前，須在二個平台都有相同類型的帳號，且在轉移後，原本 TD Ameritrade 的帳號就無法再登入 </br>

但可致電 TD Ameritrade 客服，請他們手動開啟原本的 TD Ameritrade 帳號 </br>
:::

轉移帳號有二種：一種是轉移所有股票及資金，另一種則是轉移部份股票及資金 </br>

- 轉移全部的話，會被收取 $75 的帳戶轉移費 (TD Ameritrade 跟 Robinhood 都是)
- 轉移部份的話則是不收費

但因為 TD Ameritrade 跟嘉信實際上已合併，因此不會被收取帳戶轉移費；若是轉移進來的股票 + 資金 > 50,000 的話，嘉信會補貼帳戶轉移費

---

## 登入嘉信理財來轉移帳號

1. 登入 嘉信理財 Charles Schwab 頁面 </br>
點擊左上角的 Account ，選擇 Transfer & Payment 項下的 Transfer Account 
![嘉信理財帳號轉移](../images/86-01.png)

2. 之後在下方選擇 TD AMERITRADE CLEARING 或是直接搜尋 TD AMERITRADE
![嘉信理財選擇要轉入的券商](../images/86-02.png)

3. 接著在帳戶類型中選擇 Individual (個人帳戶)；若你要轉移的是 IRA 或是 Roth IRA 則分別選擇相對應的帳戶 </br>
再來填上 TD Ameritrade 的帳號號碼 & 要轉移到的嘉信帳戶 (若不知道怎麼看帳戶號碼的，可參考下個步驟)

::: tip
若要轉移 IRA 或是 Roth IRA，則在嘉信理財中也須先開好相對應的帳號（IRA or Roth IRA）
:::

![嘉信理財填寫轉移帳號資訊](../images/86-03.png)

4. 登入 TD Ameritrade 後，點擊右上角的 My Profile，選擇 Personal Information，即可再下方看到 Account Number
![德美利證券查看帳戶號碼](../images/86-04.png)

5. 完成上述步驟後，就會收到案件已在處理中的信了 (Your transfer request is in review. ) </br>
主旨為：We've received your transfer request </br>
在嘉信理財帳戶內的 Message 也會有通知
![嘉信理財帳戶轉移處理中](../images/86-05.png)

---

## 從 TD 轉入嘉信完成
1. 我是在 5/7 (五) 申請帳號轉移，經過四個工作日，在 5/12 (三) 收到帳號轉入完成的通知 </br>
嘉信會寄信通知（登入帳號的 Message 中也可看到） </br>
![嘉信理財帳戶轉入完成](../images/86-06.png)

2. 但因為帳號轉移其實是分批轉的，因此收到通知後，其實轉移還沒完全結束 </br>
買入成本及總報酬的資料會顯示 Incomplete 及 N/A </br>
![嘉信理財帳戶轉入完成](../images/86-07.png)

3. 在 History 可看到 TD AMERITRADE 的股票都已被轉移過來
![TD AMERITRADE 股票轉入](../images/86-08.png)

4. 5/14（五）也就是申請帳號轉移後的第五個工作日，登入嘉信後，發現買入成本及總報酬均已正常顯示（但系統並不會另外再發信通知）
![TD 股票轉入完成](../images/86-09.png)


::: warning
點擊觀看更多： </br>
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::