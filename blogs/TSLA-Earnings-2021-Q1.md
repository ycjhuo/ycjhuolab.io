---
title: 特斯拉（Tesla）2021 Q1 季報，缺席了高價車的財報依舊強勢
description: '特斯拉財報|TSLA 財報|特斯拉 2021 Q1 第一季財報|TSLA 2021 Q1 第一季財報'
date: 2021-04-27
tags: [Stock, TSLA]
categories: Investment
---

特斯拉從 01/27 盤後公佈 2020 Q4 財報後，截止今天盤後（04/26），三個月的時間，股價跌了 11.64%（最低的時候跌了 32.61%）

先前說到特斯拉如果跌破 $800 就加碼，沒想到之後一路跌到 $563（目前股價為 $738）

而原因並不是財報太差，而是科技股集體回調（同期間，S&P 500 漲了 10.57%，那斯達克為 +6%），

下面來看看特斯拉的 2021 Q1 財報，是否能讓特斯拉重回 $800

## 特斯拉 2021 Q1 財報（ vs. 2020 Q4）
[來源](https://tesla-cdn.thron.com/static/R3GJMT_TSLA_Q1_2021_Update_5KJWZA.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22TSLA-Q1-2021-Update.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q4</th>
    <th align="center">差距</th>
  </tr>
  <tr>
    <td align="center">電動車總營收（百萬）</td>
    <td align="center">9,002</td>
    <td align="center">9,314</td>
    <td align="center">-3.35%</td>
  </tr>
  <tr>
    <td align="center">電動車總利潤（百萬）</td>
    <td align="center">2,385</td>
    <td align="center">2,244</td>
    <td align="center">6.28%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">26.5%</td>
    <td align="center">24.10%</td>
    <td align="center">2.4%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">5.7%</td>
    <td align="center">5.4%</td>
    <td align="center">0.3%</td>
  </tr>
    <tr>
    <td align="center">EPS</td>
    <td align="center">0.39</td>
    <td align="center">0.24</td>
    <td align="center">62.5%</td>
  </tr>
</table>

- 營收：與上季相比，減少了 3.35%；原因在於高單價的 Model S/X 正進行更新，在產量為 0 的情況下，拉低了營收
- 利潤：與上季相比，上升了 6.28%；在僅依靠 Model 3/Y 的情況下，利潤仍能成長，顯示了特斯拉在成本控管上，進步了不少
    - 電動車銷售成本相比上季，降低了 6.7%
- 毛利率，營利率，相比上季均小幅成長，EPS 則是大幅成長了 62.5%

## 特斯拉 2021 Q1 生產/交付量（ vs. 2020 Q4）
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q4</th>
    <th align="center">差距</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">0</td>
    <td align="center">16,097</td>
    <td align="center">-100%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">180,338</td>
    <td align="center">163,660</td>
    <td align="center">10.19%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">2,030</td>
    <td align="center">18,966</td>
    <td align="center">-89.3%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">182,847</td>
    <td align="center">161,701</td>
    <td align="center">13.08%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">180,338</td>
    <td align="center">179,757</td>
    <td align="center">0.32%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">184,877</td>
    <td align="center">180,997</td>
    <td align="center">2.33%</td>
  </tr>
</table>

在少了 Model S/X 產線後，特斯拉的總生產量與上季相比，仍小幅上升 0.32%；若假設這季的 S/X 生產量與上季一樣，則總生產量則會上升 9.28%

管理層在上季（2020 Q4）曾說：下一季度生產/交付量將會再成長 50%（本次財報也重申了將會實現交付量年成長 50% 的目標）

若本季要達到生產量 +50% 這個目標的話，這季的 S/X 生產量須達到 89,298，約是上季的 5.55 倍（上季 S/X 的產量為 16,097）</br>
而這是否代表管理層的預估過於樂觀呢？

目前柏林及德州工廠均預計下半年進入生產階段，而 Semi 卡車也會在 2021 年內開始交付，因此交付量年成長 50% 仍很有機會達成


## 後記
本次財報的亮點在於：特斯拉僅依靠 Model 3/Y 這二款平價車型即可讓毛利率達到 26.5% </br>

而接下來的三個季度，仍有三個因素能再推升毛利率：
1. S/X 完成改款後，回歸生產階段
2. 柏林及德州工廠投產後（這二廠均以生產 Model Y 為主）
3. 財報上提到的，Semi 卡車將於今年開始交付

這次主要討論了特斯拉的獲利能力，並沒延伸到特斯拉先前投入 15 億來購買比特幣

我認為特斯拉僅將比特幣作為資產配置中的一環，並不會考慮以買賣比特幣作為獲利手段（且這個方法也不是可持續的）</br>

在目前自由現金流為正的情況下，特斯拉並不需要擔心現金不足的問題 </br>

這個情況下，將一部分的現金轉換為比特幣，不僅在目前美元氾濫的情況下有著保值的效果，以長期持有來說，也不須擔心比特幣波動性太大的問題

::: tip
特斯拉目前帳上有 18.9 億的現金，若要維持現金與比特幣的比例，則後續仍須繼續買入比特幣
:::

延伸閱讀：</br>
[一個月跌 30%，特斯拉股價是否還會繼續下探](https://ycjhuo.gitlab.io/blogs/TSLA-Tumble-30-In-A-Month.html) </br>
[特斯拉（Tesla）公布 Q4 季報，成長速度是否減弱](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-Q4-2020.html) </br>
