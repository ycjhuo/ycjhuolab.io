---
title: Tier Match 會籍匹配，凱薩鑽石會員（Caesars Diamond）→ 米高梅黃金會員（MGM Gold）
description: '飯店會籍匹配|飯店 Match|Caesars Match|凱薩 Match|Caesars MGM Match|凱薩 米高梅 Match|凱薩鑽石 到 MGM|會籍 Match'
date: 2021-12-09
tags: [Hotel, Caesars, MGM, Tier Match]
categories: Travel
---
飯店會籍匹配流程：
::: tip
Wyndham Diamond → Caesars Diamond → [MGM Gold]() → Hyatt Explorist
:::

上篇 [Tier Match 會籍匹配，溫德姆鑽石會員（Wyndham Diamond）→ 凱薩鑽石會員（Caesars Diamond）](https://ycjhuo.gitlab.io/blogs/Tier-Match-Wyndham-To-Caesars.html) 提到了如何取得溫德姆鑽石會員 </br>

這篇接著說，如何用凱薩的鑽石會員取得 MGM 的黃金會員

## 米高梅黃金會員（MGM Gold）好處
MGM 會員共分為五個等級：</br>
```藍寶石（Sapphire）→ 珍珠（Pearl）→ 黃金（Gold）→ 白金（Platinum）→ 黑卡（NOIR，僅限邀請制）```

去掉黑卡不看的話，黃金會員在 MGM 中也算是中高階了 </br>
但由於 MGM 沒有免費早餐的福利，因此黃金會員相比初階會員來說，多了免費停車、房間升級（不保證）以及 VIP Line 的福利

這個 VIP Line 可以讓我們在 Check In/Out、餐廳、夜店排隊時，都可走優先通道

## 如何匹配 MGM 黃金會員
在得到凱薩鑽石會員後，不像其他飯店的會籍匹配可以直接線上完成，而是須要親自到波哥大飯店（Borgata），位於紐澤西的大西洋城（Atlantic City）</br>

到了波哥大飯店後，找到 M Life Rewards Desk （面對 Check-In 對台的左邊），出示 ID / 護照以及凱薩鑽石會員的證明（App 畫面也可）即可匹配完成

在匹配到 MGM 黃金會員後，僅會得到二張實體卡，而不像其他飯店一樣有送 Free Slot Play，因此若要跑一趟大西洋城卻不想空手而歸的話，可以先用手機下載 myVegas 這個 App，玩一下裡面的遊戲，蒐集一定的金幣即可兌換下圖的獎勵：

![myVegas Rewards Borgata](../images/139-01.png)

因為 $25 Dining Credit & 免費房晚（限一到四）很容易被換完，若真的想要換到這二個的話，須要常常檢查 myVegas 是否有放出更多獎勵，若金幣夠的話也可以四個都換

## 後記
這篇介紹了如何用凱薩的鑽石會員來取得 MGM 的金卡會員，但其實這個會員匹配是雙向的 </br>

因此，若你本來就有 MGM 的金卡會員，也可以直接在大西洋城的凱薩飯店（Caesars Atlantic City）匹配成凱薩的鑽石會員

若對凱撒鑽石會員有哪些福利的話，可參考：</br>
[凱薩鑽石會員（Caesars Diamond）@ 大西洋城（Atlantic City）免費吃喝玩樂](https://ycjhuo.gitlab.io/blogs/Tier-Match-Caesars-Diamond-Atlantic.html)


延伸閱讀：</br>
[Tier Match 會籍匹配，米高梅黃金會員（MGM Gold）→ 凱悅冒險家（Hyatt Explorist）](https://ycjhuo.gitlab.io/blogs/Tier-Match-MGM-To-Hyatt.html)
