---
title: Global Entry 如何使用？辦好 Global Entry 後，要做哪些事
description: 'Global Entry 申請 流程 步驟|Global Entry 教學|Global Entry 申辦流程|TSA PreCheck|美國機場快速通關'
date: 2022-09-24
tags: [Global Entry, TSA PreCheck]
categories: Travel
---

上篇 [Global Entry 申請教學、面試流程全攻略](https://ycjhuo.gitlab.io/blogs/Global-Entry.html) 介紹了什麼是 Global Entry，以及如何申請＆面試的過程 </br>
這篇來分享，在辦到 Global Entry 後要做哪些事，以及如何使用呢？

## 更新航空會員資料
1. 首先登入自己常搭的航空公司的網頁，進到 Profile 頁面
2. 找到 ```Known Traveler Number or PASS ID``` 或是 ```Secure Flight Info``` 的區塊，填上自己 Global Entry 的 9 個號碼，即可讓航空公司知道我們已具有 Global Entry / TSA 的資格了

以下分別是聯合航空（United Airlines）＆達美航空（Delta Airlines）所填寫的地方（紅框處）

![Global Entry](../images/192-01.png)
![Global Entry](../images/192-02.png)

## 檢查登機證
更新完會員資料後，往後我們在搭機前，列印出登機證或是在電子登機證上，均可看到登機證上有 TSA 的標誌，這也代表著我們等等就可以在或海關時，走 TSA（快速通關）通道

![Global Entry](../images/192-03.png)

## 過安檢
沒有 Global Entry / TSA 前，我們在過安檢前，都須將電腦/平板從行李箱/背包中拿出來，以及將鞋子脫掉 </br>
有了 Global Entry / TSA 後，以上這些全都不需要了 </br>

因此，若是跟家人出遊時，也可先將他們的電腦/平板都放在我們這邊，省去過安檢還要拿出來的步驟


## 後記
以上就是 Global Entry / TSA 所帶來的好處，在人潮少的時候，或許感受不太出來，但若是在連假＆大機場的話，快速通關則可幫我們省下至少 30 分鐘的排隊時間