---
title: 如何用 Python 檢查遠端電腦的服務是否運作 (WMI 教學)
description: 'Python Service | Python Service 狀態 | Python 讀取 Service | Python 遠端 Service | Python Service 教學'
date: 2021-05-15
tags: [Python, Pandas, Remote]
categories: Programming
---

上篇提到了如何遠端直接重開多台電腦 [如何用 Python 重啟遠端電腦 (WMI 教學)](https://ycjhuo.gitlab.io/blogs/Python-Restart-Remote-Servers.html)，這篇我們一樣用 VMI 來確認這些電腦中的服務是否有正常運作 </br>

## 用 Pandas 取得 Excel 中的 IP
一開始先用 Pandas 取得 RPQS.xlsx 檔案裡面的 IP（為了避免取到空的儲存格，我們用 ```dropna``` 去掉空值）</br>
action 是最後寄信時會用到的參數

```python
desktopPath = winshell.desktop() # get desktop path
filePath = desktopPath + r'\\RPQS\RPQS.xlsx'

# parameters(file path, sheet name)
checkServiceReports = pd.read_excel(filePath)


ipList = checkServiceReports['IP'].dropna().tolist()
action = 'checked status {}'.format(serviceName)
print('Going to check services .....')

for ip in ipList:
    checkServices(ip, finishedIp)

sendMail(action, finishedIpStr)
```

---

## 進行遠端
從 Excel 中取得 IP 後，就可以開始進行確認 Service 的環節了 </br>
一進入 ```checkServices()``` 時，我們會進行遠端 ```remote(ip)```，並取得遠端電腦的資訊 ```conn```</br>

```python
### Remote PCs
def remote(ip):
    print('')
    print('Logging to ', ip)
    try:
        conn = wmi.WMI(ip, user=username, password=password)
        return conn
    except Exception as e:
        print('Failed; Excetion: {}'.format(e))
```
取得  ```conn``` 後，就可以用 ```conn.Win32_Service(Name = serviceName)``` 來取得想要查詢的 service </br>
這裡我們要檢查的是 WM3Agent 這個服務 </br>
因為只要知道 WM3Agent 有沒有在運作，所以可以用 ```getattr(service, 'State')``` 來取得狀態 </br>

最後將 IP 跟 狀態存到 ```ipStatus``` 這個 list，再將這個 list 存到 ```finishedIp``` (寄通知信時可以用到)

```python
serviceName = 'WM3Agent'

def checkServices(ip, finishedIp):
    print('')
    print('Checking services from IP:', ip)
    
    conn = remote(ip)
    print('### conn = ', conn)

    # remote to the server successfully
    if conn != None:
        print('Remote successfully')
        # Get the service by name
        for service in conn.Win32_Service(Name = serviceName):
            print(ip, getattr(service, 'State'))

            ipStatus = [ip, getattr(service, 'State')]
            finishedIp.append(ipStatus)
            return finishedIp
    else:
        print('Remote failed')
        ipStatus = [ip, 'Remote failed']
        finishedIp.append(ipStatus)
        return finishedIp
```

---

## 寄送通知信
我們在前面取得的 ```finishedIp``` 是一個 list，如 [ [1.2.3.5 : Remote failed ], [1.12.13.14 : Running] ] </br>
如果直接將這個內容顯示在信上的話，會不方便看，所以這裡我們將 ```finishedIp``` 從 list 轉成 str 形式，並加上換行符號

```python
# arrange finished IP from list to string for sending mail
finishedIpStr = ''

for i in finishedIp:
    finishedIpStr += ' : '.join(i)
    finishedIpStr += '\n <br/>'

### Remote Account 
username = r'UserNameXXX'
password = 'PasswordXXX'


### Email Server Settings 
smtp_server = "mail.mailserver.com"
port = 25  # For starttls
server = smtplib.SMTP(smtp_server, port)
sender = "sender@mail.com"
receiver = "receiver@mail.com"
cc = ["cc@mail.com"]
receiverList = []


### Send notification Mail 
def sendMail(action, finishedIpStr):
    message = """\
        <html>
        <head></head>
        <body>

        The following PCs have been {action}:  <br/><br/>
        IP <br/><br/>
        {finishedIp} <br/><br/>
        </body>
        </html>
        """.format(action = action, finishedIp = finishedIpStr)
        
    msg = MIMEMultipart('alternative')
    msg = MIMEText(message, 'html')
    msg['Subject'] = "Remote Jobs list"
    msg['Form'] = sender
    msg['To'] = receiver
    msg['Cc'] = ','.join(cc)

    toAddress = [receiver] + cc
    server.sendmail(sender, toAddress, msg.as_string())
    print("Mail was sent")
    server.quit()
```

---

最後通知信的內容如下：</br>
![87-01](../images/87-01.png)

若要了解怎麼用 Python 寄信，可參考這篇 [如何用 Python 寄送 HTML 郵件 ( Email )](https://ycjhuo.gitlab.io/blogs/Python-How-To-Send-HTML-Mails.html)

## Source Code

```python
import winshell
from os import system
import wmi
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import pandas as pd
import numpy as np


# Storing restarted IPs for sending mail
finishedIp = []

serviceName = 'WM3Agent'

### Read files
desktopPath = winshell.desktop() # get desktop path
filePath = desktopPath + r'\\RPQS\RPQS.xlsx'

# parameters(file path, sheet name)
checkServiceReports = pd.read_excel(filePath)


### Remote Account 
username = 'UserNameXXX'
password = 'PasswordXXX'


### Email Server Settings 
smtp_server = "mail.mailserver.com"
port = 25  # For starttls
server = smtplib.SMTP(smtp_server, port)
sender = "sender@mail.com"
receiver = "receiver@mail.com"
cc = ["cc@mail.com"]
receiverList = []


### Send notification Mail 
def sendMail(action, finishedIpStr):
    message = """\
        <html>
        <head></head>
        <body>

        The following PCs have been {action}:  <br/><br/>
        IP <br/><br/>
        {finishedIp} <br/><br/>
        </body>
        </html>
        """.format(action = action, finishedIp = finishedIpStr)
        
    msg = MIMEMultipart('alternative')
    msg = MIMEText(message, 'html')
    msg['Subject'] = "Remote Jobs list"
    msg['Form'] = sender
    msg['To'] = receiver
    msg['Cc'] = ','.join(cc)

    toAddress = [receiver] + cc
    server.sendmail(sender, toAddress, msg.as_string())
    print("Mail was sent")
    server.quit()


### Remote PCs
def remote(ip):
    print('')
    print('Logging to ', ip)
    try:
        conn = wmi.WMI(ip, user=username, password=password)
        return conn
    except Exception as e:
        print('Failed; Excetion: {}'.format(e))

def checkServices(ip, finishedIp):
    print('')
    print('Checking services from IP:', ip)
    
    conn = remote(ip)
    print('### conn = ', conn)

    # remote to the server successfully
    if conn != None:
        print('Remote successfully')
        # Get the service by name
        for service in conn.Win32_Service(Name = serviceName):
            print(ip, getattr(service, 'State'))

            ipStatus = [ip, getattr(service, 'State')]
            finishedIp.append(ipStatus)
            return finishedIp
    else:
        print('Remote failed')
        ipStatus = [ip, 'Remote failed']
        finishedIp.append(ipStatus)
        return finishedIp
    

# Get IP from the report, remove nan in the Series & convert it from series to list
ipList = checkServiceReports['IP'].dropna().tolist()
action = 'checked status {}'.format(serviceName)
print('Going to check services .....')

for ip in ipList:
    checkServices(ip, finishedIp)


print('Finished IP = ', finishedIp)

# arrange finished IP from list to string for sending mail
finishedIpStr = ''

for i in finishedIp:
    finishedIpStr += ' : '.join(i)
    finishedIpStr += '\n <br/>'

sendMail(action, finishedIpStr)
```

