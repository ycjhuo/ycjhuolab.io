---
title: 特斯拉（TSLA）2024 Q1 季報｜股東會猛畫大餅，真讓股價逆勢上漲
description: '美股 特斯拉 Tesla TSLA 財報|特斯拉 TSLA 2024 Q1 第一季財報 分析 重點 介紹|特斯拉 Tesla TSLA 2024 股東會'
date: 2024-06-30
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 04/24（二）公佈了 2024 的第一季財報 <br />

在 2023 Q4 - 2024 Q1 財報期間（1/24 - 4/24），特斯拉的股價從 $207.83 → $162.13，下跌 22%（同時 S&P 500 上漲 4%）

財報發佈後，特斯拉的股價就未低於 $162，且在近五天上漲了 7%，將股價推升到 $198，顯示市場又開始關注特斯拉

今年上半年，市場資金專注於 AI 類股，但獨獨忘了已在 AI 領域耕耘許久的特斯拉（FSD 自動駕駛，RoboTaxi 無人駕駛，Dojo 運算，Optimus 機器人等多項應用），使特斯拉股價在上半年的表現為悲慘的 -20%，同期大盤表現為 +15%，更別提最紅的輝達（NVDA），上半年股價為驚人的 156％

好在股東會後，特斯拉的股價開始逆勢上漲，下面來複習特斯拉 2024 Q1 財報，以及馬斯克在 6/13 的股東會上對於未來的發展藍圖

## 特斯拉 2024 Q1 財報
[來源](https://digitalassets.tesla.com/tesla-contents/image/upload/IR/TSLA-Q4-2023-Update.pdf)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q3</th>
    <th align="center">2023 Q4</th>
    <th align="center">2024 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2024 Q1 vs. 2023 Q4</th>
  </tr>
  <tr>
    <td align="center">電動車營收</td>
    <td align="center">196.3 億</td>
    <td align="center">215.6 億</td>
    <td align="center">173.8 億</td>
    <td align="center">-8%</td>
    <td align="center">9%</td>
    <td align="center">-24%</td>
  </tr>
  <tr>
    <td align="center">總營收</td>
    <td align="center">233.5 億</td>
    <td align="center">251.7 億</td>
    <td align="center">213 億</td>
    <td align="center">-7%</td>
    <td align="center">7%</td>
    <td align="center">-18%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">17.9%</td>
    <td align="center">17.6%</td>
    <td align="center">17.4%</td>
    <td align="center">-0.3%</td>
    <td align="center">-0.3%</td>
    <td align="center">-0.3%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">7.6%</td>
    <td align="center">8.2%</td>
    <td align="center">5.5%</td>
    <td align="center">-2.1%</td>
    <td align="center">0.7%</td>
    <td align="center">-2.7%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.66</td>
    <td align="center">0.71</td>
    <td align="center">0.45</td>
    <td align="center">-38%</td>
    <td align="center">7%</td>
    <td align="center">-58%</td>
  </tr>
</table>

- 電動車營收：本季電動車營收為 174 億，較上季衰退 24%，比去年衰退 13%，是因降價促銷所導致
- 總營收：Q4 總營收為 213 億，較上季衰退 18%，比去年衰退 9%
- 毛利率：連續三季維持在 17%，細看的話，每季下滑 0.3%，與去年同期相比，下滑 2%
- 營業利益率：本季營利率為 5.5%，比上季少了 2.7%，而與去年的 11.4% 相比，下滑 5.9%，等於衰退了一半以上
- EPS：本季 EPS 為 0.45，季衰退 58%，年減 47%

本季特斯拉財報，除了毛利率維持水準外，其他指標皆嚴重衰退，電動車營收季減 24%，導致總營收季減 18%，總利潤季減 20% <br />
與上季相比，營益率也因營業費用上漲了 6%，而在本季降低到 5.5%，為 2021 以來的最低水準 <br />

可喜的是，特斯拉車款在經歷多次降價後，毛利率仍能維持在 17%，這是其他同業所無法比擬的，也表示著在殺價競爭的市場下，特斯拉仍保有先天優勢，雖然近期因連續降價導致營收不如預期，但目前特斯拉每賣一台車的獲利（$7,000 美金）仍是同業第一，甚至是比亞迪（$1,300）的 5.5 倍，這也證明了，雖然上個季度媒體大幅報導比亞迪銷量已超越特斯拉，但從獲利面來看，特斯拉仍是遙遙領先各大車廠

## 特斯拉 2024 Q1 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q3</th>
    <th align="center">2023 Q4</th>
    <th align="center">2024 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2024 Q1 vs. Q4</th>
  </tr>
  <tr>
    <td align="center">Other Models 生產量</td>
    <td align="center">13,688</td>
    <td align="center">18,212</td>
    <td align="center">20,995</td>
    <td align="center">-30%</td>
    <td align="center">33%</td>
    <td align="center">15%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">416,800</td>
    <td align="center">476,777</td>
    <td align="center">412,376</td>
    <td align="center">-9%</td>
    <td align="center">14%</td>
    <td align="center">-14%</td>
  </tr>
  <tr>
    <td align="center">Other Models 交付量</td>
    <td align="center">15,985</td>
    <td align="center">22,969</td>
    <td align="center">17,027</td>
    <td align="center">-17%</td>
    <td align="center">44%</td>
    <td align="center">-26%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">419,074</td>
    <td align="center">461,538</td>
    <td align="center">369,783</td>
    <td align="center">-6%</td>
    <td align="center">10%</td>
    <td align="center">-20%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">430,488</td>
    <td align="center">494,989</td>
    <td align="center">433,371</td>
    <td align="center">-10</td>
    <td align="center">15%</td>
    <td align="center">-13%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">435,059</td>
    <td align="center">484,507</td>
    <td align="center">386,810</td>
    <td align="center">-7%</td>
    <td align="center">11%</td>
    <td align="center">-20%</td>
  </tr>
</table>

生產＆交付量部分，主力車款的 Model 3/Y 在本季的表現為 41.2 萬 & 37萬，分別比上季衰退了 14% & 20%  <br />
其餘車款則是 2.1 萬 & 1.7 萬，與上季相比為 15% & -20%  <br />
本季總生產量為 43.3 萬輛，且 Semi 也將開始量產，據馬斯克在六月的股東大會上所說，到年底，全車款年產能可達 700 萬輛


## 財報重點
在產能方面，本季下滑是因「紅海衝突」＆「柏林廠縱火事件」所導致，且 Model 3 剛完成改款，產線仍在調整中，未達到產能全開的階段  <br />

目前最受矚目的 Cybertruck 生產速度已超過一週 1,300 台（根據 6/13 股東會所說），財報中預計年產能可達到 12.5 萬台，若依每台平均價格 7.6 萬來算，可貢獻 95.5 億的營收，等於是現在 45% 總營收，而這個目標在 2025 年底即有機會實現

其他業務方面，能源業務營收的季成長為 12%（年成長 7%），是目前是利潤率最高的業務（24.6%），而服務營收方面，季成長 5%（年成長 25%）

今年電動車業務的成長率將會低於去年（15%），但能源＆服務的成長率將會超過電動車業務

而新車款將會在 2025 年中推出，且因採用了相同的生產線，所以在成本方面，也會比原本預期更低

在股東會上，馬斯克也明確表示，明年將開始生產 Optimus 機器人，但以他一慣的個性來說，最快可能是 2025 年底才會開始生產

## 後記
特斯拉在本季的財報表現上，比去年更差，可說是已連續二季呈現衰退，而在接下來馬上要公布的 2024 Q2 財報，也很難有好表現

但在股價方面，從 2024 Q4 財報公布後，從 $208 → $183，接著到這次財報公布後，又跌到 $147，為何現在又回升到 $198 了呢？

關鍵在於 6/13 的股東會，多數股東支持了馬斯克的薪酬方案，消除了投資人對於馬斯克離開特斯拉的疑慮，且馬斯克也闡述了特斯拉在 Robotaxi 無人計程車上的進展，加上同業 Waymo 的無人叫車服務，也在 6 月底時在舊金山全面開放，所有人可直接透過 App 叫車，帶動了投資人對於特斯拉 Robotaxi 業務的信心

再加上 Optimus 機器人的發展，馬斯克表示 Optimus 有著比電動車更高的獲利能力，在明年量產後，更可成為推動特斯拉的另一引擎，他樂觀表示，以特斯拉目前市值 0.6 萬億來看，自動駕駛能幫助特斯拉達到 5 萬億市值，而再加上 Optimus，特斯拉將會達到 25 萬億市值

白話來說，自動駕駛可讓特斯拉股價上漲 8 倍，Optimus 可讓股價再上漲 41 倍，

馬斯克表示他是個樂觀的人，但也不是沒有自知之明，如果他不樂觀的話，特斯拉不會從瀕臨破產的時期走到現在，雖然常開支票，但最終總會實現自己的承諾

不得不說，這些話的確給了投資人信心，也難怪股價在最近五天就漲了 7%，但七月中公布的財報將會給投資人澆一盆冷水，認清馬斯克所描繪的前景都不會馬上實現，但我仍保持相同觀點，目前正是趁著低檔，加碼股票的最好時機，到了 2025 年，有著 Cybertruck 量產 ＆ 新車款推出等話題的特斯拉，將會再迎來爆發式的成長


::: warning
點擊觀看更多： <br />
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
