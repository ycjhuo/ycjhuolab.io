---
title: 聖地牙哥全攻略｜聖地牙哥 CityPass 介紹
description: '聖地牙哥 San Diego CityPass 介紹|聖地牙哥 San Diego CityPass 推薦|聖地牙哥 San Diego CityPass 攻略|聖地牙哥 San Diego CityPass 必去'
date: 2024-04-13
tags: [San Diego]
categories: Travel
---

CityPass 是一個賣旅遊套票的網站，有針對美國的大城市推出觀光景點的套票，若還沒規劃要去哪些景點的話，通常都是直接參考 CityPass 上的景點

而聖地牙哥身為加州的第二大城市，當然也能在 CityPass 上找到旅遊套票，下面就來介紹 CityPass 上有什麼行程，以及購買這個套票的話，可以幫我們省下多少錢


## 聖地牙哥 CityPass 三選一景點介紹
聖地牙哥的 CityPass 分為下面三種：
1. SeaWorld San Diego（票價 $72）+ 3 個自選景點，票價 $177（13 歲以上）/ $152（3-12 歲） <br />
聖地牙哥海洋世界：從市區開車只要 15 分鐘，是一個集遊樂園＆水族館於一身的主題樂園，要動物有動物（虎鯨／海獅），要看海洋生物也有，想體驗雲霄飛車也沒問題，是蠻推薦的點，可以待上一整天 <br /> 
我因為待的時間不夠，以及之後還要去加州環球樂園＆迪士尼，才放棄了這個行程 <br />

2. LEGOLAND California（票價 $80）+ 3 個自選景點，票價 $179（13 歲以上）/ $159（3-12 歲）<br />
加州樂高樂園，從市區開車須 40 分鐘，如其名，是以樂高為主題的遊樂園，多數遊樂設施都適合小孩的樂園，能滿足大人刺激感的雲霄飛車（設施）只有 1-2 個，若不是對樂高很有興趣的，可以略過 <br />
我因為去過了紐約的樂高樂園，所以就沒去這邊 <br />

3. SeaWorld San Diego + LEGOLAND California + 3 個自選景點，票價 $244（13 歲以上）/ $222（3-12 歲）<br />
看完上面，「海洋世界」跟「樂高樂園」都想去的，就是選這個了，單獨買「海洋世界」＆「樂高樂園」的門票要 $152，換句話說，只要剩下的自選景點，一個超過 $30，這個套票就回本囉

## 聖地牙哥 CityPass 自選景點
CityPass 的自選景點有這些選擇：
1. 聖地牙哥動物園（San Diego Zoo）或野生動物園（Safari Park），二擇一，官網票價 $72/62 <br />
野生動物園（Safari Park），距離市區車程 40 分鐘，是一個動物保護區，須開車進入，進去園區後，裡面有免費搭乘的車，可遠距離的看到長頸鹿，大象等動物，而若想再更近距離看的話，則有付費搭乘的敞篷車可以選擇，付費版還分為普通版（$92/一人）跟高級版（$115/人），假日的話票價會更貴 <br />

::: tip
因我這次沒有租車，往返不便，且聽同事說，到了那邊，沒有付費搭車的話，自己能看到的動物既少又遠，須帶望遠鏡才看得清楚的那種，因此我就放棄了「野生動物園（Safari Park）」，改去位於市區的「聖地牙哥動物園（San Diego Zoo）」<br />
對於「聖地牙哥動物園」有興趣可參考 [聖地牙哥全攻略｜巴爾波公園 Balboa Park｜聖地牙哥動物園 San Diego Zoo](https://ycjhuo.gitlab.io/blogs/San-Diego-Balboa-Park.html)
:::

2. 中途島號博物館（USS Midway Museum），官網票價 $32/22 <br />
是美國最大的航空母艦博物館，實際參與過韓戰 波斯灣戰爭等戰役的航空母艦，在退役後作為博物館，有興趣可參考 [未完成 聖地牙哥全攻略｜中途島號博物館 USS Midway Museum ]() 」<br />

3. 搭船觀光（City Cruises），官網票價 $40 <br />
90 分鐘的搭船觀光，從海上遙望聖地牙哥市區、「科羅納多橋（Coronado Bridge）」以及「科羅納多飯店著名飯店（Hotel del Coronado）」，因為我容易暈船，且從市區可自行開車／搭公車到 科羅納多（Coronado），所以沒選擇這個行程 <br />

4. 博趣水族館（Birch Aquarium at Scripps），官網票價 $24.95/19.95 <br />
距離市區車程 20 分鐘的水族館，因我選擇了到「拉霍亞（La Jolla）」看野生的海獅，覺得景點重複，所以沒前往
有興趣可參考 [聖地牙哥全攻略｜拉霍亞 La Jolla｜拉霍亞海灣 La Jolla Cove｜聖地牙哥加利福尼亞大學 University of California San Diego](https://ycjhuo.gitlab.io/blogs/San-Diego-La-Jolla.html) 」<br />

5. 舊城區觀光車（Old Town Trolley Tours），官網票價 $60/35 <br />
搭乘觀光車到聖地牙哥的各個景點，24 小時內可隨時上下車，共有 11 個站點，也包含「科羅納多（Coronado）」，類似紐約或各大城市的 Big Bus 等觀光巴士，但因為這些景點坐公車／開車都能到，所以沒選擇這個行程 <br />
關於舊城區的介紹，可參考 [聖地牙哥全攻略｜機場如何到市區|舊城區 Old Town｜海港村 Seeport Village｜煤氣燈區 Gaslamp Quarter](https://ycjhuo.gitlab.io/blogs/San-Diego-OldTown-Downtown.html)

6. 海事博物館（Maritime Museum of San Diego），官網票價 $24/12 <br />
外觀像帆船的博物館，裡面展示了各種歷史性的船隻／遊艇，但我覺得航空母艦更吸引我，所以在這二個景點之間，選擇了「中途島號博物館（USS Midway Museum）」

## 後記
若是以我的喜好選擇了 CityPass 的 #1 套票，海洋世界（$72）＋動物園（$72）＋中途島博物館（$32），這樣單獨買票加起來就 $176 了，還剩下一個自選景點，對比 #1 套票 的 $179，是真的能省下一點錢，但海洋世界跟動物園都是可以單獨玩一整天的景點，而中途島博物館搭配另一個自選景點，也需要一天，這樣就需要完整的三天了，再加上第一天＆最後一天往返其他城市的時間，要五天才比較充裕

我自己因為還有規劃其他免費景點，時間無法配合，所以沒有購買套票，但若打算待五天以上的，且這些景點都有興趣的，使用 CityPass 就很值得


::: warning
點擊觀看更多： <br />
- [聖地牙哥景點介紹](https://ycjhuo.gitlab.io/tag/San%20Diego/)
:::
