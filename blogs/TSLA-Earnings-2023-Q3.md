---
title: 特斯拉（TSLA）2023 Q3 季報｜產線停工升級，特斯拉迎來短暫下滑
description: '美股 特斯拉 Tesla TSLA 財報|特斯拉 TSLA 2023 Q3 第三季財報|'
date: 2023-12-25
tags: [Stock, TSLA]
categories: Investment
---

特斯拉在 10/18（三）公佈了 2023 的第三季財報 <br />
在 Q2 - Q3 的財報期間（7/19 - 10/18），特斯拉的股價從 $291.26 → $242.68，下跌 17%（同期 S&P 500 下跌 5.5%）

截至上週五（12/22），特斯拉今年的股價上漲了 134%，S&P 500 為 24%，領先大盤 5.5 倍

特斯拉從 2022 Q4 開始，毛利率＆營業利益率就隨著殺價競爭而逐漸下滑，雖然上季搭著 AI Dojo 的熱潮推動，讓股價漲了一波，但並未改變特斯拉財報不振的事實

下面來看看特斯拉 2023 Q3 財報是否在財務指標上有所好轉

## 特斯拉 2023 Q3 財報
[來源](https://digitalassets.tesla.com/tesla-contents/image/upload/IR/TSLA-Q3-2023-Update-3.pdf)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q2</th>
    <th align="center">2023 Q3</th>
    <th align="center">2022 Q4 vs. 2023 Q1</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">電動車營收</td>
    <td align="center">212.7 億</td>
    <td align="center">196.3 億</td>
    <td align="center">-7%</td>
    <td align="center">6%</td>
    <td align="center">-8%</td>
  </tr>
  <tr>
    <td align="center">總營收</td>
    <td align="center">249.3 億</td>
    <td align="center">233.5 億</td>
    <td align="center">-4%</td>
    <td align="center">6%</td>
    <td align="center">-7%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">18%</td>
    <td align="center">18%</td>
    <td align="center">-4%</td>
    <td align="center">-1%</td>
    <td align="center">--</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">10%</td>
    <td align="center">8%</td>
    <td align="center">-5%</td>
    <td align="center">-2%</td>
    <td align="center">-2%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.78</td>
    <td align="center">0.53</td>
    <td align="center">-47%</td>
    <td align="center">6%</td>
    <td align="center">-47%</td>
  </tr>
</table>

- 電動車營收：本季電動車營收僅 196 億，約與 Q1 持平，季成長為 -8%
- 總營收：本季總營收為 233.5 億，一樣跟 Q1 差不多，季成長是 -7%（年成長為 9%）
- 毛利率：本季毛利率與上季一次，保持在 18%，目前已連續四季下跌（2022 平均毛利率為 29%）
- 營業利益率：本季營利率為 8%，比上季降低了 2%，一樣是連續四季下跌（2022 平均營利率為 17%）
- EPS：本季 EPS 為 0.53，季成長為 -47%

特斯拉本季財報與 Q1 表現類似，營收，營利率與 EPS 都有著不小幅度的下跌，而能源＆服務的營收成長速度也下滑到 Q1 的 1/4 左右 <br />
這次僅有毛利率維持在與 Q2 一樣的水準，其它財務指標皆呈現衰退 <br />
搭配上特斯拉本季度的生產＆交車輛都遜於上季的成績，財報數字不好看似乎也在預期範圍內 <br />

::: tip
特斯拉本季 Model 3/Y 生產量為 416,800，交付量為 419,074，分別落後上季 9% & 6% <br />
而落後主因是升級了工廠的生產線所致，下個季度將可看到生產量繼續保持增長
目前造車成本每輛為 $37,500，從今年以來，每個季度成本約下降 2% <br />
:::

## 財報重點
本季因 Cybertruck 與 AI 的研發投入，造成研發費用與上季相比。大幅上升了 19%（上季為 18%），可見特斯拉對於研發的投入＆重視，也難怪營利率逐季下滑

接著來看各個造車工廠的現況：
- 德州：目前 Model Y 年產量維持 25 萬，但未來會持續增加，Cybertruck 年產量則為 12.5 萬（不保證），且在 12 月已首次交付（但只有 10 輛）
- 上海：年產量從上季的 75 萬成長到 95 萬，成長了近 27%，目前產量已接近滿載，再提升的數量有限
- 柏林：年產量從上季預期的 35 萬成長到 37.5 萬輛，產量仍會繼續成長

目前 Model 3/Y 的年產量已從年初預計的 200 萬，成長到了 212.5 萬，比 Q1 時多了 6%

而在上季最受矚目的 AI 方面，隨著越來越多的特斯拉正式交車上路，這些車輛源源不絕的提供真實駕駛資料給特斯拉，使特斯拉的 AI 運算能力急速增長，目前運算能力已是上個季度的二倍以上，而這將幫助特斯拉改善＆將運用於自動駕駛 FSD 上

## 後記
特斯拉本季因為進行了超級工廠的產線升級，使得本季的總產量下滑 10%，營收下跌 8% <br />
利潤方面因 Cybertruck & AI 造成營運費用增加 12%，導致營業利潤的季成長下跌了 36% <br />

好在產線升級已完成，下季即可恢復產能，並在造車成本持續下降的情況下，維持住毛利率，特斯拉的困境就僅剩 Cybertruck & AI 的研發費用 <br/>
在 Cybertruck 已邁入生產階段的同時，研發費用再進一步控制，下個季度的財報將會擺脫營收＆利潤下滑的趨勢，恢復成長動能 <br/>
但由於 AI 的持續投入以及短期間難以看到回報的特性，財報將不會出現大幅成長 <br/>

::: warning
點擊觀看更多： <br />
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
