---
title: 如何透過嘉信理財聯名卡將美國運通點數（AMEX MR）兌換成現金
description: '美國運通 American Express AE|嘉信大白卡 Platinum 嘉信白金卡|American Express Platinum Card for Schwab|點數 MR 換現金|American Express Membership Rewards cash out'
date: 2021-02-27 19:09:52
tags: [Brokerage account, Credit Cards, AMEX, Schwab]
categories: Investment
---

本文僅適用於美國版本的美國運通點數。</br>
此方法可用 1000 點兌換 USD $12.5 的方式，將金額轉到嘉信帳戶```（且是立即到帳）```

::: important
2021/09/01 起，兌換比例已由 1,000 點兌換 $12.5 降為 $11
:::

要使用此方法，須符合二項條件：
1. 申辦嘉信理財（Charles Schwab ）投資帳號。
2. 申辦美國運通的嘉信理財白金卡（American Express Platinum Card® for Schwab）
 - 申辦網頁 [點擊](https://www.schwab.com/credit-cards)

## 嘉信理財介紹
在現在各大券商買賣股票都是 0 手續費的情況下，用哪家券商似乎沒太大差別。</br>
但我從 2019 年就開始用嘉信理財，原因是嘉信理財會為投資帳號的會員提供一張全球提款免手續費的簽帳卡（Debit Card）。</br>
這張卡在全球的 ATM 都可直接提領當地現金（當日上限為 USD 1,000），且匯率不錯，相當於台銀的即期賣出。</br>

有了這張卡，我們就可以實現，賣出嘉信股票時，馬上將獲利的金額藉由這張簽帳卡在台灣提領出來，省下了之後電匯回台灣的成本（約為 USD $30）。</br>
雖然有每天有提領上限，但金額不多的話，沒幾天就可以提領完了。

## 美國運通的嘉信理財白金卡
這張聯名卡，福利跟美國運通白金卡一模一樣，唯一特點就是多了： 以 1000 點兌換 USD $12.5 的方式，將點數以不錯的兌換率換為現金。</br>
雖說兌換機票或者升等，會讓點數的價值發揮得更高，但在現在各國嚴格控制旅客進出的情況下，換成現金仍是個不錯的方式。</br>

## 兌換方法
1. 登入美國運通的網頁，在右下方的 Membership Rewards Points 會看到 Invest with Rewards
![58-01](../images/58-01.png)

2. 點擊 Invest with Rewards 後，會跳轉到下圖這個頁面，上面寫著兌換標準最低從 1000 點開始
![58-02](../images/58-02.png)

3. 按下 Get Started 後，會來到填寫信用卡資訊的頁面。依序填完後，按下 Next
![58-03](../images/58-03.png)

4. 就會來到選擇兌換點數的頁面（這裡要記得選擇自己的嘉信帳戶），選擇要兌換的點數後，按下 Next
![58-04](../images/58-04.png)

5. 會跳轉到確認頁，確認沒問題後按下 Redeem Now
![58-05](../images/58-05.png)

6. 最後就會跳到結果頁（金額會立即到達所連結的嘉信帳戶）
![58-06](../images/58-06.png)

7. 之後到 Point Activity 就會看到點數已被扣除
![58-07](../images/58-07.png)

8. 登入嘉信網站，也可看到兌換的金額馬上就轉進來了
![58-08](../images/58-08.png)