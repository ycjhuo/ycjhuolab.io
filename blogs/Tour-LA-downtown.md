---
title: 洛杉磯 Downtown 一日遊 天使鐵路（Angels Flight Railway） 中央市場（Grand Central Market）最後一間書店（The Last Bookstore）
description: 'LA 景點|LA 推薦|洛杉磯 推薦|洛杉磯 景點|天使鐵路 介紹|La La Land 景點|LA La La Land|樂來樂愛你 景點|Angels Flight Railway 介紹'
date: 2021-10-28
tags: [Los Angeles]
categories: Travel
---

這次在 LA 待了五天四夜，其中 Downtown 待了二天，LA Downtown 治安其實不太好，雖然這些景點都是白天去的，但路上還是有很多帳篷＆流浪漢，建議還是結伴行動

![LA Dowtown 一日遊](../images/130-01.png)

這篇要介紹的景點如上圖，分別為：
1. 天使鐵路（Angels Flight Railway）
2. 中央市場（Grand Central Market）
3. 最後一間書店（The Last Bookstore）

圖中藍框是這次所住的旅館：DoubleTree by Hilton Hotel Los Angeles Downtown </br>
有興趣可參考：[開箱！洛杉磯-希爾頓 Downtown DoubleTree 飯店（DoubleTree by Hilton Hotel Los Angeles Downtown）](https://ycjhuo.gitlab.io/blogs/Hotel-LA-Downtown-Hilton-DoubleTree.html)


## 天使鐵路（Angels Flight Railway）
天使鐵路，是世界上最短的鐵路，於 1901 年建造，全長只有 96 公尺（其實就是一個 block 的距離）</br>
票價為 $1 美元（單程），若刷卡的話則要 $1.5；因為在 La La Land（樂來越愛你）中有出現過，而讓更多人知道 </br>

![洛杉磯 LA 天使鐵路](../images/130-02.jpg)


## 中央市場（Grand Central Market）
我個人覺得算是個普通的市場，若不是剛好在天使鐵路旁邊，其實也沒必要特地去 </br>
裡面分二層，一進去這層主要以吃的東西為主，樓下則是偏向衣服＆飾品類 </br>

賣的東西都不貴，吃的部分跟紐約相比便宜不少，一份餐約在 $8 - 12 之間
![洛杉磯 LA 中央市場](../images/130-03.jpg)
![洛杉磯 LA 中央市場](../images/130-04.jpg)
最有名的 eggslut 早午餐，幾乎隨時都有長長的排隊人龍，至少要排 30 分鐘以上 </br>

因為也沒位置可以吃，隔天早上直接用 Ubereats 送到飯店 </br>
下圖是：Farfax 加上 Slut，共 $19.44 </br>
![洛杉磯 LA 中央市場 eggslut](../images/130-05.jpg)

## 最後一間書店（The Last Bookstore）
這是我認為 LA Downtown 最好逛的地方，倒不是我很喜歡看書，而是因為這裡面很多用書做出來的擺設，很適合拍照 </br>
![洛杉磯 LA 最後一間書店](../images/130-06.jpg)

這二個點在我們一開始拍的時候完全沒人，但可能真的拍得太久，漸漸的大家就開始排起隊來了 </br>
![洛杉磯 LA 最後一間書店](../images/130-07.jpg)
![洛杉磯 LA 最後一間書店](../images/130-08.jpg)

## 後記
上面介紹的這三個點可以花一個早上或下午的時間逛，另外半天可以去在天使鐵路附近的「The Board」（布洛德博物館），是個以裝置藝術聞名的博物館 </br>
雖然票價是 $0，但還是要先上網訂票，建議提前一星期訂（我們前二天訂，想去的這天已經額滿了）</br>
訂票連結：[The Broad General Admission GET TICKETS](https://ticketing.thebroad.org/events/84185978-8137-44a7-9774-356133b9997d) </br>

我們花了一個早上逛了上面三個點，下午則搭 Uber 去「The Getty」（盖蒂博物馆），以繪畫，雕塑聞名的博物館，最出名的收藏即為（Gogh）梵谷的「鳶尾花」（Irises）</br>