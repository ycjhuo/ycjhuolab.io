---
title: Snowflake，波克夏也投資的 SaaS 公司，公佈第四季及 2020 全年財報
description: '美股 Snowflake SNOW 2020 財報分析|波克夏也投資的科技股|雲端科技股 SaaS|SnowFlake SNOW IPO'
date: 2021-03-28 15:25:46
tags: [Stock, SNOW]
categories: Investment
---

Snowflake，一家雲端儲存公司，於 2020/09/16 上市，IPO 價格為 $120，當天收盤價為 $253.93，當日上漲了 112%</br>
去年 12 月時最高漲到 $390，隨後開始一路滑落。上週五 (03/26) 收盤價為 $235，今年漲幅為 -15.54%

而現在 Snowflake 經歷了半年，股價又回到了上市第一天的收盤價，現在是否是值得投資 Snowflake 的時機呢？來看看 Snowflake 在 03/03 發佈的 2020 Q4 及全年財報

## 2020 Q4 及全年財報
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
  </tr>
  <tr>
    <td align="center">營收（Thousand）</td>
    <td align="center">190,465 vs. 159,624</td>
    <td align="center">592,049 vs. 264,748</td>
  </tr>
  <tr>
    <td align="center">營收成長率</td>
    <td align="center">19.32%</td>
    <td align="center">123.63%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">56.47% vs. 58.23%</td>
    <td align="center">59.03% vs. 55.97%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">-105.21% vs. -106.16%</td>
    <td align="center">-91.87% vs. -135.26%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">-0.07 vs. -1.01</td>
    <td align="center">-3.81 vs. -7.77</td>
  </tr>
</table>

::: tip
Snowflake Q3 財報區間：08/01 - 10/31；Q4 區間：11/01 - 01/31；全年財報期間：02/01 - 01/31
:::

營收方面：Snowflake Q4 相較於 Q3 有著約 20% 的成長（2020 全年相比去年則是成長 123.63%）</br>
毛利率在 Q4 則是小幅下降了 1.75%，但仍有著約 56.5% 的水準，2020 全年則是 59.03%（相比 2019 全年上升了 3.05%）</br>
營業利益率在 Q4 為 -105.21%，跟 Q3 相比不大，但 2020 全年為 -91.87%（相較 2019 全年的 -135.26%，進步了 43.38%）

但由於 Snowflake 在營運上的花費，目前公司還是處於虧損的狀態中，接著來看看是哪項花費造成公司雖然有著約 60% 的毛利率，卻仍無法盈利

## 各營運費用佔比
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
  </tr>
  <tr>
    <td align="center">Sales & Marketing</td>
    <td align="center">50.02% vs. 51.34%</td>
    <td align="center">53.65% vs. 57.99%</td>
  </tr>
  <tr>
    <td align="center">Research & Development</td>
    <td align="center">30.52% vs. 28.25%</td>
    <td align="center">26.63% vs. 20.77%</td>
  </tr>
  <tr>
    <td align="center">General and administrative</td>
    <td align="center">19.45% vs. 20.40%</td>
    <td align="center">19.72% vs. 21.24%</td>
  </tr>
  <tr>
    <td align="center">總營運費用增減</td>
    <td align="center">17.36%</td>
    <td align="center">76.46%</td>
  </tr>
</table>

業務及行銷費用一直都佔了總營運費用的一半以上，雖說在 2020 有逐步減低這個比重（2019 全年佔比為 57.99%）</br>
行銷費用減少的比重都加在了研發方面（從 2019 的 20.77 逐步上升到 2020 Q4 的 30.53%）</br>

行政費用相比不大，但總體營運費用在 Q4 仍比 Q3 增加了 17.36%（這個時期，營收成長了 19.32%），可以看出，隨著營收的增加，營運費用也跟著以差不多的比例增加（但若以全年財報來看的話，2020 營收成長了 123.63%，營運費用僅成長 76.46%）

## 後記
分析前並不覺得 Snowflake 的毛利率會有這麼高 (60%)，作為一家整合雲端倉庫（AWS, Azure, Google Cloud）的公司，提供客戶一個操作介面，讓客戶可以無縫在雲端平台上操作/轉移，藉此向客戶收取費用。

但因身為中間商的角色，若未來侵佔到 AWS, Azure, Google Cloud 的市場及利潤，難保 Amazon, Microsoft 及 Google 不會切斷自身的雲端平台對於 Snowflake 的支援，或作出反擊。

而以 Snowflake 自身來說，在產品有一部分仰賴於其他企業的支援時，限制了本身毛利率的天花板。那麼要追求更高的獲利，勢必只能從降低營運費用做起。雖說現在因公司仍處在高速發展期，而在營運費用上的開銷仍不手軟。但到了業務成熟期時，營運費用的縮減應該也是有限。

畢竟若沒有繼續在業務及行銷上投入成本的話，僅依靠客戶自己選擇 Snowflake，而不直接選擇 AWS, Azure，那麼在獲取新客戶的速度上會減慢許多。而研發這一塊也是不能省的一個領域，畢竟這個使用平台是客戶選擇 Snowflake，而不是 AWS, Azure 的原因。
且 AWS, Azure 更新後相關功能及設定後，Snowflake 勢必也得跟著調整相關設定，以求完整支援各雲端平台。

綜上所述，有著不少限制條件的 Snowflake，不知道為何波克夏會參與他的 IPO，而巴菲特又是從中看到了什麼因素才批准了這筆投資。


資料取自：</br>
[Snowflake 2020 Q3 財報](https://investors.snowflake.com/news/news-details/2020/Snowflake-Reports-Financial-Results-for-the-Third-Quarter-of-Fiscal-2021/default.aspx)</br>

[Snowflake 2020 Q4 財報](https://investors.snowflake.com/news/news-details/2021/Snowflake-Reports-Financial-Results-for-the-Fourth-Quarter-and-Full-Year-of-Fiscal-2021/)</br>