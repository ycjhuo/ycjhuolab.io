---
title: Google（GOOG）公布 2021 Q1 季報，近半年中表現最好的權值股
description: '美股投資 Google GOOG|Google GOOG 介紹|Google GOOG 2021 Q1 第一季財報分析'
date: 2021-04-28
tags: [Stock, GOOG]
categories: Investment
---

昨天在 S&P500 指數中權重排名第 6 的[特斯拉公布了 2021 Q1 季報](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q1.html)，今天權重第 5 的 Google 也發佈了 2021 Q1 季報

作為 S&P500 前五大的權值股之一（前四大分別為：AAPL, MSFT, AMZN, FB）中，Google 在近半年的表現中，以 52% 的報酬率遙遙領先了其他四支股票（第二名的 MSFT 在近半年上漲了 29% ）

而作為表現最好的權值股，Google 是否能在維持（股價）高成長呢？讓我們從季報中一探究竟

</br>

若想了解美股三大指數的，可參考這篇：
[追蹤美股三大市場的指數型 ETF](https://ycjhuo.gitlab.io/blogs/US-Stock-Markets-Index-ETF.html)</br>

## Google 2021 Q1 季報（ vs. 2020 Q4）
[來源](https://abc.xyz/investor/static/pdf/2021Q1_alphabet_earnings_release.pdf?cache=0cd3d78)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q4</th>
    <th align="center">2021 Q1 vs. 2020 Q4</th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">55,314</td>
    <td align="center">56,898</td>
    <td align="center">-2.78%</td>
    <td align="center">23.23%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">21,283</td>
    <td align="center">18,689</td>
    <td align="center">13.88%</td>
    <td align="center">39.9%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">70.28%</td>
    <td align="center">72.49%</td>
    <td align="center">-2.21%</td>
    <td align="center">-3.22%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">29.72%</td>
    <td align="center">27.51%</td>
    <td align="center">2.21%</td>
    <td align="center">3.22%</td>
  </tr>
    <tr>
    <td align="center">EPS</td>
    <td align="center">26.63</td>
    <td align="center">22.54</td>
    <td align="center">18.15%</td>
    <td align="center">36.19%</td>
  </tr>
</table>

這季的營收，小幅減少了 2.79%，相比 2020 Q4 的高成長（23.23%），營收動能在缺少了疫情的刺激後下降了許多 </br>

但稅前淨利跟上季相比仍舊成長了 13.88%，EPS 也多了 18.15%，在毛利率跟營業利益率並無變化太多的情況下，是什麼讓 Google 的淨利跟 EPS 大幅成長呢？

## 推升淨利及 EPS 的關鍵
Google 這季的其他收入約 48.5 億（佔總利潤的 27%），關於這點 Google 僅說明是目前持有股權的（未）實現收益（上季的其他收入約 30 億，為總利潤的 20%）

而 Google 一直都有在回購股票，在 2020 Q3 及 Q4 時各投入了 79 億用於回購，而在這一季更是投入了 114 億在回購上，幫助了 Google 的 EPS 逐季升高

而董事會在 4/23 時也授權了 500 億回購計畫，也難怪在近半年 GOOG 的表現（+52.12%）約是 S&P500（+27.99%） 及 Nasdaq 指數（+28.04%）的二倍

## 後記
本次季報中，Google 的本業相比上季並無明顯的成長，Google Cloud 的營收也較上季減少了 3.21%（但虧損程度也縮減了 21.64%）

另外從今年度開始（2021），Google 將伺服器其網路設備的使用年限從 3 年延長到 4 年（特定的一些設備則延長 5 年），看得出來 Google 在營收趨緩的情況下，也朝著縮減成本的方向進行


但因這麼做對於改善淨利及 EPS 幫助有限，因此將手中的現金用於回購股票來達到立竿見影的效果就成了 Google 的最佳選擇

目前 Google 的現金約 266 億，若再加上有價證券則是 1350 億，可以推算出四月底批准的 500 億回購計劃並不會在第二季期間就回購完成（畢竟金額是 2020 Q4 的 4.4 倍，2020 Q3 的 6.33 倍）

延伸閱讀：</br>
[Facebook & Google 公佈第四季財報，股價卻不同步？](https://ycjhuo.gitlab.io/blogs/2020-Q4-Earnings-FB-GOOG.html) </br>
[Facebook & Google 公佈第三季財報，線上廣告的市場是否能持續成長？](https://ycjhuo.gitlab.io/blogs/2020-Q3-Earnings-FB-GOOG.html) </br>