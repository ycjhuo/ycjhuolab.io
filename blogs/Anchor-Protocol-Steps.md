---
title: Honeygain 掛網被動收入還不夠？教你如何用 JMPT 轉到 Anchor 賺取 20% 年利率（下）
description: 'Honeygain 介紹|Honeygain 推薦|Honeygain 心得|Honeygain 掛網賺錢|Honeygain 被動收入|被動收入推薦|被動收入 Passive Income|JMPT 出金|JMPT換現金|MetaMask 小狐狸錢包轉 Terra|MetaMask 小狐狸錢包 UST|MetaMask 小狐狸錢包 幣安鏈 BSC BEP|UST Anchor 利率 MetaMask 小狐狸錢包|Terra Anchor Protocol 介紹'
date: 2022-04-18
tags: [Passive Income, Honeygain, Anchor]
categories: Investment
---

在上篇 [Honeygain 掛網被動收入還不夠？教你如何用 JMPT 轉到 Anchor 賺取 20% 年利率（上）](https://ycjhuo.gitlab.io/blogs/Honeygain-Payout-JMPT.html) </br> 
介紹了如何將 Honeygain 的 JMPT 出金到 MetaMask，並且將 JMPT 轉換成 UST 再傳送到 Terra Station 錢包 </br> 
這篇就要來完成最後一個步驟，也就是將 Terra Station 的 UST 存到 Anchor 獲取 20% 的年利率

::: warning
Honeygain → JMPT → MetaMask → [Terra Station → Anchor]()
:::

::: tip
Anchor Protocol 的利息從 2021/03 開始就一直維持在 20% 左右 </br>
下圖可看到目前借錢的人會被收取的利率為 11%，而存錢的人會收到 19.59% 的利息 </br>

而之間的利息差距則是由官方補貼的，雖然 20% 的利率已維持了 13 個月，但無法保證這個利率能一直保持下去 </br>

會隨著存款的人變多／借錢的人變少＆官方補貼用的資金逐漸下降，利息在未來有可能會降低
:::

![Anchor Protocol 利息](../images/172-00.png)

## 存入 UST 到 Anchor
1. 首先進到 [Anchor Protocol](https://app.anchorprotocol.com/) 後，點擊右上方的 Connect Wallet 來連結錢包（選擇我們有的 Terra Station）

![Connect Terra Station Wallet to Anchor](../images/172-01.png)

2. 連接好錢包後，選擇上方的 Earn，再點擊 Deposit 存入我們的 UST </br>
 由於存入和取出都需要一定量的 UST 做為手續費（ 0.25 UST），因此在這邊我們不能選擇直接存入

![Deposit UST to Anchor](../images/172-02.png)

3. 選擇好要存入的 UST 後，按下 Proceed 來確認交易，就可以看到交易已完成了

![Anchor Confirmation](../images/172-03.png)

::: warning
若是用手機 App 掃碼登入 Anchor 的人，則需要回到 App 到點擊確認交易的視窗，否則 Anchor 會出現 Timeout 的錯誤訊息：</br>
Failed to receive responses to post(Tx) for 300000milliseconds
:::
![Anchor Timeout Failed to receive responses to post(Tx) for 300000milliseconds](../images/172-04.png)


## 後記
存入 UST 到 Anchor Protocol 後，就可以在 My Page 看到自己的在 Anchor 中的資產了 </br>
深綠色的是我們存入的 UST，淺綠色的則是 Anchor 所產生給我們的利息 </br>

![Anchor Protocl My Page](../images/172-05.png)

若要將 UST 領出來到錢包的話，只要到 Earn Page 點擊 Withdraw 即可

雖說一開始提到 Anchor 的利率無法保證能一直維持在 20%，且一直有官方補貼即將結束的消息 </br>
但我認為，就算沒了官方補貼，依照目前借錢的利率為 11%，存款人的利率最低也能有 10% </br>

而這利息相比銀行已經高了 10 倍以上了，雖說加密貨幣的穩定性不比現金 </br>
但藉由穩定幣的特性，在這邊配置一部分可承受風險的資產，我認為對整體資產的報酬率仍是有效率的 </br>


延伸閱讀：</br>
[賺取 Anchor 的 20% 年利率，手續費大公開，至少要存入多少錢才划算](https://ycjhuo.gitlab.io/blogs/Anchor-Cost.html)


::: warning
點擊觀看更多： </br>
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::