---
title: UiPath（Path）公佈 2021 Q3 財報，最受 ARK 青睞的成長股
description: '美股 UiPath Path 介紹 | 美股投資 UiPath Path | UiPath Path 2021 Q3 財報分析 | 方舟基金 ARK UiPath Path '
date: 2022-01-05
tags: [Stock, Path]
categories: Investment
---

上篇[方舟投資（ARK）2021 Q3 持股變化，多數持股皆是負報酬](https://ycjhuo.gitlab.io/blogs/ARK-13F-2021-Q3.html)提到，ARK 在 2021 中最看好的一家公司 UiPath（Path），僅僅 2 個季度，ARK 就將 Path 的持股比重提高到 3.01%（佔總投資比重的第 7 名）

這篇就來看看 Path 有什麼魔力讓 2020 年報酬率遠超大盤（S&P 500）9.2 倍的 ARK 如此青睞呢

:::tip
2020 年 ARKK 報酬率為 148.33%；S&P 500 index 為 16.11% </br>
2021 年 ARKK 報酬率為 -25.06%；S&P 500 index 為 94.59%
:::


## UiPath 簡介
UiPath（Path）致力於 RPA（Robotic process automation）機器人流程自動化的公司 </br>
UiPath 的 RPA 聚焦在軟體上的機器人，透過 UiPath 的 StudioX 這個軟體，我們可將日常作業的例行工作自動化（無需程式背景），官方有出一個[短片來介紹 UiPath StudioX](https://www.youtube.com/watch?v=dxCBkMmB5bQ)

同時也有跟麥肯錫（McKinsey & Company）、貝恩（Bain & Company）等顧問公司合作，為其他企業量身訂造適合的解決方案
目前客戶涵蓋了金融、保險、航空、汽車等各產業

於 2021/04/21 上市（上市價為 $40.51），並在上市後一個月上漲了 23%，隨後就一路下跌，目前距離上市價格為 -41%

:::tip
UiPath 的財報季度多其他企業一年，因此 2022 Q1 其實就代表 2021/01/01 - 2021/03/31 以此類推
:::


## 財務指標
[來源](https://ir.uipath.com/financials/quarterly-results)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">Chg（Q2 vs Q1）</th>
    <th align="center">Chg（Q3 vs Q2）</th>
  </tr>
  <tr>
    <td align="center">營收（千）</td>
    <td align="center">186,217</td>
    <td align="center">195,521</td>
    <td align="center">220,816</td>
    <td align="center">5%</td>
    <td align="center">12.94%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">73.68%</td>
    <td align="center">81.8%</td>
    <td align="center">80.48%</td>
    <td align="center">8.11%</td>
    <td align="center">-1.31%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-127.73%</td>
    <td align="center">-50.03%</td>
    <td align="center">-52.64%</td>
    <td align="center">76.70%</td>
    <td align="center">-2.61%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">-1.1</td>
    <td align="center">-0.19</td>
    <td align="center">-0.23</td>
    <td align="center">91%</td>
    <td align="center">-4%</td>
  </tr>
</table>

- 營收：營收處在成長的階段，但成長幅度並不算快速
- 毛利率：保持著 SaaS 的高毛利的特性，近二季均在 80% 左右
- 營業利潤率：近二季營業利潤約在 -50% 左右，且光是業務行銷（Sales & Marketing）的支出就佔了總營收的 80%
- EPS：由於公司本身並未實現盈利，EPS 離轉正的時間仍有一大段路要走

由於產品利潤幾乎被行銷費用吃走，導致 UiPath 仍處在虧損狀態，且營收成長的速度與行銷費用相當，短期無法轉虧為盈，而從這點也可看出產品推廣的難度

雖然 UiPath 的產品一但被客戶導入，就不易被汰換，但也因這個特性，企業在導入前都會考慮再三，確認 UiPath 的產品是否真能達到預期的效益，因此，營收要在短期間內大幅增長的比例不高

:::tip
營業費用佔比為：業務行銷 60%，研發 20%，行政 20%
:::

## 後記
儘管 UiPath 真的能幫助企業在營運中減少例行性事務的人力，但若是 UiPath 的產品/服務訂價太高，反而會讓企業開始思考導入是否有其必要性

畢竟導入新產品/服務都有其風險性，況且導入後還會遇到使用者抗拒或是原有作業流程的改變，而無法達到預期的成效


目前 UiPath 仍處在虧損階段，因為產品本身的特性，要在短期間拉高營收或是降低營業費用均不太可能，我想這可能也是 UiPath 的股價一路從最高點的 $85 到現在的 $40

在現階段很難期待 UiPath 在短期內就轉虧為盈，加上目前成長股均表現不佳的情況下，UiPath 要想回到當初上市的價格（$69），須再上漲 80％ 才可達到

在這麼多不利的條件下，ARK 仍在持續加碼 UiPath，可見 ARK 對於 UiPath 的發展潛力極具信心
