---
title: CryptoTab 介紹，手機挖礦，用電腦就能賺取比特幣
description: 'CryptoTab 介紹 推薦|CryptoTab 攻略|CryptoTab 推薦|CryptoTab 教學||CryptoTab 詐騙||CryptoTab 賺到錢|CryptoTab 比特幣 BTC Bitcoin|免費拿比特幣 BTC|電腦挖礦 手機挖礦 加密貨幣 比特幣|比特幣 BTC 水龍頭 推薦'
date: 2022-05-06
tags: [Passive Income, CryptoTab, Crypto]
categories: Investment
---

::: tip
- 用途：使用電腦效能，賺取比特幣 (BTC)
- 收益：依個人電腦的效能而定 (我的經驗一個月約 0.00004 BTC，依目前 BTC 價格 40,000 算的話，約美金 $1.6)
- 出金方式：到任何支援比特幣 (BTC) 的錢包，出金門檻為 0.00001 BTC
- 適用裝置：Mac、Windows、Android、iOS
- 支援一次掛多個裝置，但只能有一個 Android 裝置
- [我的 CryptoTab 推薦連結](https://cryptotabbrowser.com/33478264) </br>
:::

今天介紹一款不用組礦機、也不用買顯卡，只要用自己的電腦/手機就能挖比特幣的軟體 CryptoTab </br>
因為是使用電腦效能，所以在使用時，會感受到系統變慢 (若使用 Android 則沒這個問題)，我自己是在睡覺跟出門時才會開 </br>
而能挖到的比特幣則是依據電腦效能不同而有所差異，以下是我測試過的裝置：
- Windows, CPU i7 4600 2.1GHz/2.7GHz：CryptoTab 的挖礦效能設定 80%，算力約 1000 H/s
- Macbook Pro 2016, CPU Intel i5 2.1GHz：CryptoTab 的挖礦效能設定 50%，算力約 650 H/s
- iPhone 5s：CryptoTab 的挖礦效能設定 100%，算力約 5 H/s
- Android：不論使用哪隻手機，算力皆約 1000 H/s

算力越高，挖到的比特幣也越多，但官方並沒有給出挖礦效率的數字，所以只能自己測試 </br>
下面來介紹 CryptoTab 的三種版本：電腦版、iOS、Android

## CryptoTab 電腦版 & iOS 介紹
CryptoTab 的電腦版 & iOS 都是下載後，登入自己的帳號即可開始挖礦，差別在於： </br>
電腦版可將視窗縮到最小，iOS 版在挖礦時則須一直在 CryptoTab 的畫面 </br>
下面來介紹如何安裝 CryptoTab：
1. 點擊我的 [推薦連結後](https://cryptotabbrowser.com/33478264)，下載與自身裝置相符的版本 </br>
2. 打開下載的 CryptoTab，點擊右上角的橘色 icon 進入主畫面

![CryptoTab 教學](../images/174-01.jpg)

3. 進到主畫面後，再點擊右上角的設定（上圖紅框），選擇以 Google 帳號登入
4. 下圖為主畫面，左上角的紅框可調整挖礦效率/要使用多少系統效能

![CryptoTab 介紹](../images/174-02.jpg)

## CryptoTab Android 版介紹
Android 則與上面介紹的不同，但卻是我認為最值得的用的裝置 </br>
我們可將 Android 版本想成，跟水龍頭網站一樣，只要每二小時打開 App，點一下 Active 後即可 </br>
不論用哪款 Android 手機，系統都會自動用算力 1000 H/s 挖幫你二個小時  </br>
也因此，雖然一個 CryptoTab 帳號可同時掛多個裝置，但若是 Android 的話則只能掛一個 </br>

![CryptoTab Android 介紹](../images/174-04.jpg)

但試想，為什麼官方要免費送我們 BTC，還不須用到我們的手機效能呢？ </br>
因此 Android 版須另外下載 CryptoTab VPN 這個 App，才可以開始用 CryptoTab 進行雲端挖礦 </br>
CryptoTab VPN 其實是一個強迫我們看廣告的 App，裝了之後，每次只要打開什麼 App，都會跳出 5 秒鐘的廣告 </br>
這也是我們用 CryptoTab Android 版的唯一缺點 </br>

下圖左邊是 CryptoTab Android 版的挖礦 App，右邊則是 CryptoTab VPN

![CryptoTab Android 教學](../images/174-03.jpg)

## CryptoTab 心得
雖然 CryptoTab 的效率不比礦機，且會有耗電＆降低電腦效能的風險 </br>
但我認為，在不用電腦的多餘時間掛著，使用了一個月後，電費並沒有明顯的增加，但相對的，收益也僅有 0.00004 的比特幣（依目前價格約 $1.6 美金） </br>

但由於不須耗費額外成本，且若是用 Android 手機的話，則更是方便 </br>
我並沒有準時每隔 2 小時就點一次，都是有想到才點，而這樣就能讓我每個月都有些許比特幣入帳，也算不無小補，更何況比特幣未來的升值空間更是不可想像 </br>

而對於 CryptoTab 各個裝置版本的話，我推薦程度，Android > 電腦版 > iOS </br>
畢竟 iOS 要一直開著畫面，且考量到手機的耗電程度，恐怕只有睡覺時插著電才有用，其它裝置則是完全不用煩惱

最後若是對 CryptoTab 有興趣的話，請用 [我的 CryptoTab 推薦連結](https://cryptotabbrowser.com/33478264) 註冊

想知道 CryptoTab 要如何出金到自己的比特幣錢包的話，可參考這篇 [CryptoTab 手機挖礦，如何將賺到的 BTC 出金](https://ycjhuo.gitlab.io/blogs/CryptoTab-Payout.html)

::: warning
點擊觀看更多： </br>
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::