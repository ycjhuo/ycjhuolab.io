---
title: 開箱！Google Pixel 5a
description: 'Google Pixel 5a 介紹|Google Pixel 5a 推薦|Google Pixel 5a 開箱|Google Pixel 5a 照相|Google Pixel 5a vs. 4a'
date: 2021-09-11
tags: [Phone]
categories: Travel
---

![](../images/110-01.jpg)<center>左：Pixel 4a，右：Pixel 5a，Photo by iPhone 5s</center></br>

上週（9/2）由於颶風艾達（Hurricane Ida）席捲全紐約，我在下班途中，手機（Pixel 4a）放在口袋中，回到家後，手機已死機，且機身也一直滴水出來，隔天雖馬上送 Google Store 檢測，仍被宣告死亡，且不在保固範圍內（Trade-in 價值也是 $0）</br>

沒有手機用的情況下，只好現場直接買一隻（Pixel 5a），也就有了這篇開箱文 </br>

結帳時忍不住跟店員抱怨一下，我的 4a 才用二個月就壞了，店員知道我的遭遇後，表示雖然這隻 5a 具備 IP67（防水防塵），應該不會再這麼容易進水，但如果進水一樣不在保固內 </br>

## 規格 Pixel 4a vs. 5a
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">Pixel 4a</th>
    <th align="center">Pixel 5a</th>
  </tr>
  <tr>
    <td align="center">價格（美金稅前）</td>
    <td align="center">$349</td>
    <td align="center">$449</td>
  </tr>
  <tr>
    <td align="center">顏色</td>
    <td align="center">淺藍＆黑</td>
    <td align="center">黑（墨綠）</td>
  </tr>
  <tr>
    <td align="center">尺寸（長/寬）</td>
    <td align="center">5.8寸(6.1/2.9)</td>
    <td align="center">6.3寸(5.7/2.2)</td>
  </tr>
  <tr>
    <td align="center">電池容量</td>
    <td align="center">3140 mAh</td>
    <td align="center">4680 mAh</td>
  </tr>
</table>

更多規格比較可參考：[Google Store](https://store.google.com/us/magazine/compare_pixel?toggler0=Pixel+4a&toggler2=Pixel+4a+with+5G) </br>

- 價格方面：5a 比 4a 貴了 $100，但相比 iPhone 12 系列中最便宜的 mini（$699）還是便宜許多
- 尺寸方面：相比原本用的 4a（5.8寸），5a（6.3寸）的尺寸大了許多，但若跟我前一隻 3 XL（6.3寸）比，因為 5a 的寬度窄了一點，所以單手使用還是不成問題（但我還是習慣 4a 的大小）
- 顏色方面：4a 有淺藍（似乎為美國限定）跟黑色二種，5a 只有黑色一種，但 5a 的黑色其實不是純黑，而是（消光）墨綠
- 電池方面：相比 4a 的容量僅有 3140 mAh，5a 電池足足增加了 49%，達到了 4680 mAh

## 拍照 Pixel 4a vs. 5a
以下照片皆未調過濾鏡，直接用原廠相機拍的，左圖是 Pixel 4a，右圖為 Pixel 5a </br>

因為照片都是不同天拍的，前三張的 4a 拍照日為陰天，所以天空較陰，因手機已壞，也無法再重拍，僅能從現有的照片選擇較為接近的景色 </br>

![](../images/110-02.png)<center>紐約中央公園（Belvedere Castle）</center></br>
![](../images/110-03.png)<center>紐約中央公園（Bethesda Terrace）</center></br>
![](../images/110-04.png)<center>紐約中央公園（Bow Bridge）</center></br>
![](../images/110-05.png)<center>紐約 Chelsea Market vs. 布魯克林 Dumbo</center></br>
![](../images/110-06.png)<center>曼哈頓夜景（Night Sight 模式拍攝）</center></br>
![](../images/110-07.png)<center>曼哈頓夜景（Night Sight 模式拍攝）</center></br>

## 後記
因為我不玩手遊，手機只用來上網及拍照，所以對處理器＆運算能力較不要求，但就平常使用來說，4a 與 5a 的流暢度差不多，並無感覺到明顯的差別 </br>

而拍照除了在 Night Sight 模式下，可看出 5a 在細節上處理得較好之外，也不會因為要將景色拍的更清晰，而讓天空也隨著變亮 </br>
而白天拍照時，若是拍人像，在光線太暗，或是背光時，5a 會自動將照片進行加亮，讓人像拍起來不會太暗 </br>
就相機這點來說，我覺得 5a 有著蠻明顯的升級感 </br>

而因為我之前用的 Pixel 4a 不是 5G 版本，所以再換了 Pixel 5a 後，第一次感受到 5G 的速度：下載 82M，上傳 20M（我的電信商是 Mint）</br>

而家裡的 WiFi 用的是 Verizon Fios 的 300M，測速後：下載 23M，上傳180M，真的有感受到 5G 的速度，也因為 Pixel 5a 的電池容量比 4a 多了 49%，所以就算 5G 上網比較耗電，但我在使用上反而覺得 5a 的耗電量不會像 4a 下降的這麼快，充電的次數也下降許多 </br>

雖然 Pixel 6 也快出了，但剛好手機壞在這個時間點，剛好可以買到 8/18 推出的 Pixel 5a，雖然目前只有美國及日本有上市，但光這個價格（$449），真的覺得對消費者很有吸引力
