---
title: Square（SQ）公佈 2021 Q3 財報，近一年首度虧損
description: 'Square SQ 2021 Q3 第三季財報|美股 Square SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報'
date: 2021-11-09
tags: [Stock, SQ]
categories: Investment
---

Square（股票代號：SQ），從 2021 Q2 財報發布（08/01），到 2021 Q3 財報發布（11/04）期間，股價僅上漲 0.08%（$247.26 → $247.46），同期間 S&P 500 上漲了 6.68%</br>
而 Q3 財報公佈後，Square 隔天股價下跌了 4.07%，讓我們從 Square 2021 第三季財報中分析大跌的原因

複習 Square 2021 Q2 財報：</br>
[Square（SQ）公佈 2021 Q2 財報，股價直接上漲 10%](https://ycjhuo.gitlab.io/blogs/SQ-Earnings-2021-Q2.html)

## 財務指標
[來源](https://investors.squareup.com/financials/quarterly-results/default.aspx)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">QoQ (Q1)</th>
    <th align="center">QoQ (Q2)</th>
    <th align="center">QoQ (Q3)</th>
  </tr>
  <tr>
    <td align="center">營收（億）</td>
    <td align="center">50.58 億</td>
    <td align="center">46.81 億</td>
    <td align="center">38.45 億</td>
    <td align="center">60.09%</td>
    <td align="center">-7.45%</td>
    <td align="center">-17.86%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">19.05%</td>
    <td align="center">24.38%</td>
    <td align="center">29.47%</td>
    <td align="center">-6.39%</td>
    <td align="center">5.33%</td>
    <td align="center">5.1%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">1.34%</td>
    <td align="center">2.67%</td>
    <td align="center">0.6%</td>
    <td align="center">-0.09%</td>
    <td align="center">1.33%</td>
    <td align="center">-2.07%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.08</td>
    <td align="center">0.4</td>
    <td align="center">N/A</td>
    <td align="center">-86.15%</td>
    <td align="center">400%</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：Square Q3 季成長為 -17.86%，已連續二季負成長（Q2 時為 -7.45%），主因與 Q2 相同，均是 Bitcoin 營收下降所致
- 毛利率：所幸隨著利潤最低的 Bitcoin 營收下滑，Square 的毛利率持續回升到 29.47%，已連續二季上升（營收最高時的 Q1，毛利率僅 19.05%）
- 營業利潤率：可惜的是，因為營收下跌，但相關成本＆費用卻沒跟著減少，導致營利率下降至 0.6%，相比 Q2 的歷史新高的 2.67%，下跌了 2.07%
- EPS：隨著 Q2 創下歷史新高的 0.4，本季因營業利潤不高（整季只賺了 2,300 萬），扣掉利息＆其他費用後，總利潤就變成虧損了（約 290 萬）

接著將營收來源細分，逐項來看各個營收的增減變化

## 營收來源
Square 的收入可分為四個項目：
<table style="width:100%">
  <tr>
    <th align="center">收入組成</th>
    <th align="center">營收佔比</th>
    <th align="center">QoQ (Q2)</th>
    <th align="center">QoQ (Q3)</th>
    <th align="center">毛利率 (Q2)</th>
    <th align="center">毛利率 (Q3)</th>
  </tr>
  <tr>
    <td align="center">交易</td>
    <td align="center">33.74%</td>
    <td align="center">27.9%</td>
    <td align="center">5.67%</td>
    <td align="center">44.21%</td>
    <td align="center">41.85%</td>
  </tr>
  <tr>
    <td align="center">訂閱</td>
    <td align="center">18.07%</td>
    <td align="center">22.86%</td>
    <td align="center">1.4%</td>
    <td align="center">81.94%</td>
    <td align="center">80.99%</td>
  </tr>
    <tr>
    <td align="center">硬體</td>
    <td align="center">0.97%</td>
    <td align="center">51.89%</td>
    <td align="center">-14.8%</td>
    <td align="center">-40.43%</td>
    <td align="center">-37.3%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin</td>
    <td align="center">47.22%</td>
    <td align="center">-22.41%</td>
    <td align="center">-33.35%</td>
    <td align="center">2.01%</td>
    <td align="center">2.29%</td>
  </tr>
</table>

- 交易收入：相比 Q2 營收，小幅上升了 5.67%，目前佔總營收比率為 33.74%；毛利率由 44.21% → 41.85%
- 訂閱收入：相比 Q2 營收，僅上升 1.4%，目前佔總營收比率為 18.07%；毛利率由 81.94% → 80.99%
- 硬體收入：減少了 14.8%，目前佔總營收僅 0.97%；毛利率由 -40.43 → -37.3%
- Bitcoin 收入：減少了 33.35%，Q2 季成長為 -22.41%，已連二季下跌，佔總營收比例 47.22%；毛利率由 2.01% → 2.29%

各業務的營收佔比由於 Bitcoin 連二季的大幅減少，變得更為平均，但營收的成長動能相比 Q2 來說，降低不少；毛利率也僅有 Bitcoin 業務小幅上升 0.28% </br>
這些跡象都顯示了 Square 的增長速度正在放緩，下面再來將各業務再分為 Seller vs. Cash App，找出是哪部分業務衰退的更加明顯

## Seller vs. Cash App ecosystem

<table style="width:100%">
  <tr>
    <th align="center">淨利佔比</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">Change（%）</th>
  </tr>
  <tr>
    <td align="center">交易淨利（Seller）</td>
    <td align="center">43.64%</td>
    <td align="center">44.69%</td>
    <td align="center">1.05%</td>
  </tr>
  <tr>
    <td align="center">訂閱淨利（Seller）</td>
    <td align="center">10.96%</td>
    <td align="center">11.76%</td>
    <td align="center">0.81%</td>
  </tr>
  <tr>
    <td align="center">硬體淨利（Seller）</td>
    <td align="center">-1.56%</td>
    <td align="center">-1.24%</td>
    <td align="center">0.32%</td>
  </tr>
  <tr>
    <td align="center">交易淨利（Cash App）</td>
    <td align="center">4.34%</td>
    <td align="center">3.86%</td>
    <td align="center">-0.47%</td>
  </tr>
  <tr>
    <td align="center">訂閱淨利（Cash App）</td>
    <td align="center">35.85%</td>
    <td align="center">34.4%</td>
    <td align="center">-1.46%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin 淨利（Cash App）</td>
    <td align="center">4.84%</td>
    <td align="center">3.72%</td>
    <td align="center">-1.12%</td>
  </tr>
</table>

假設 Seller 跟 Cash App 在同樣業務的毛利率一樣：
- 交易淨利：Seller 本季淨利成長了 1.05%（Q2 季成長為 3%）；Cash App 則是 -0.47%
- 訂閱淨利：Seller 成長了 0.81%；Cash App 則是 -1.46%
- 硬體淨利：與 Q2 相比，Seller 成長了 0.32%，但仍處於虧損狀態
- Bitcoin 淨利：本季佔總體淨利的 3.72%，與 Q2 相比，減少了 1.12%（Q2 的季成長為 -3%）

Square 的獲利引擎 交易淨利（Seller） 在本季僅成長了 1.05%，成長幅度與上季相比，少了約 60%，而它 Seller 的其它類別成長幅度則更低了 </br>
Cash App 業務在本季則是全面衰退，從 Q2 算起，已連續二季負成長，在沒有推出新功能，以及吸引人的優惠狀況下，我自己也已經很久沒用 Cash App 來進行一般的消費＆投資了 </br>

## 後記
在 Square 的 Q3 財報期間（08/01 - 11/04），比特幣價格 39,201.95 → 61,452.23，上漲了 57%，同時成交量也成長了 27% </br>
但 Cash App 的比特幣營收卻仍持續下降，代表著使用者已越來越少用 Cash App 購買比特幣，而轉為使用 Coinbase 及其他加密貨幣交易所 </br>
至於原先預期 Square 的交易收入能隨著疫情漸減＆經濟復甦獲得大幅成長，但實際上卻僅是小幅度的成長 </br>


在各項業務成長速度均下降的現在，花費 290 億美金（全股票交易）收購 BNPL（先買後付）平台 Afterpay，成效仍須時間來驗證 </br>
在 8/1 因這個消息而衝動買入 Square 的投資者，買入價位是 $272.38，與今日（11/09）的股價相比，下跌了 17.3% </br>

我雖然在這次財報期間也有持續加碼 SQ，但卻是在 9 月底 - 10 月初這時股價低迷的時候，買入成本約 241，與當前股價相比約下跌 4% </br>
在財報未見好轉的情況下，除非股價有大幅度下跌，不然不會考慮繼續買入 SQ
