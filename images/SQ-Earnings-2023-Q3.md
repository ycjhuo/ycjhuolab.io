---
title: Block（SQ）2023 Q3 財報｜即將轉虧為盈，股價暴漲 48%
description: 'Block SQ 2023 Q3 第三季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報|Block 收購 Afterpay|Block 收購先買後付平台|Block buy now pay later (BNPL)'
date: 2023-12-04
tags: [Stock, SQ]
categories: Investment
---

支付科技公司 Block（股票代號：SQ），在 11/2 發布了 2023 Q3 財報

在 Q2 - Q3 財報期間（8/3 - 11/2），Block 股價下跌 40%（$73.55 → $43.98），同期的 S&P 500 指數僅下跌 4%

Block 股價從 2021 年達到高峰後（$275）就一路走低，在十月底達到谷底（$39.22），跌幅達到 86%

但在公布 Q3 財報後，Block 彷彿回到了 2021 年，股價快速上漲，截至目前（12/1），股價上漲了 48%

究竟 Block 的第三季財報有什麼魔力，讓股價產生這麼明顯的變化呢？

下面就來看看 Block（SQ）在 2023 Q3 的財報

## Block 財務指標
[來源](https://s29.q4cdn.com/628966176/files/doc_financials/2023/q3/Block_3Q23_Shareholder-Letter.pdf)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q2</th>
    <th align="center">2023 Q3</th>
    <th align="center">Q1（季成長）</th>
    <th align="center">Q2（季成長）</th>
    <th align="center">Q3（季成長）</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">55.3 億</td>
    <td align="center">56.2 億</td>
    <td align="center">7.3%</td>
    <td align="center">10.9%</td>
    <td align="center">1.5%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">33.7%</td>
    <td align="center">33.8%</td>
    <td align="center">-1.3%</td>
    <td align="center">-0.6%</td>
    <td align="center">0.1%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-2.4%</td>
    <td align="center">-0.2%</td>
    <td align="center">2.8%</td>
    <td align="center">-2.3%</td>
    <td align="center">2.2%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">-0.2</td>
     <td align="center">-0.05</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：連續 6 季成長，本季營收為 56.2 億，季成長從 11% → 1.5%，年成長則是從 26% → 24%
- 毛利率：與上季持平，維持在 34%，從 2022 一直維持至今，Block 並不會因擔心營收的成長放緩而犧牲毛利率
- 營業利潤率：比上季成長 2%，回升到 -0.18%，主要是業務＆行政支出在本季減少了 11% & 12% 所致
- EPS：從上季的 -0.2 回升到 -0.05，已整整二年處於虧損狀態，下個季度將可實現獲利

本季財報的各項表現都與 Q1 類似，都是因為大幅減少營運開銷來提升營益率與 EPS，而處於即將由虧轉盈的階段，僅有營收的部分有點不同 <br/>
Q1 的季成長為 7.3%，但本季僅為 1.5%，不過若以年成長來看，二者都差不多在 24%

在 Q1 財報發佈到 Q2 的這段時間，Block 股價位於 $60.5 - $73.6 這個區間，若與現在股價相比，還有至少 % 以上的漲幅空間

## Block 各項表現

<table style="width:100%">
  <tr>
    <th align="center">毛利成長率</th>
    <th align="center">總毛利（季）</th>
    <th align="center">總毛利（年）</th>
    <th align="center">Cash App（季）</th>
    <th align="center">Cash App（年）</th>
    <th align="center">Square（季）</th>
    <th align="center">Square（年）</th>
  </tr>
  <tr>
    <td align="center">Q1</td>
    <td align="center">3.3%</td>
    <td align="center">32%</td>
    <td align="center">9.8%</td>
    <td align="center">49%</td>
    <td align="center">-3.9%</td>
    <td align="center">16%</td>
  </tr>
  <tr>
    <td align="center">Q2</td>
    <td align="center">8.8%</td>
    <td align="center">27%</td>
    <td align="center">4%</td>
    <td align="center">37%</td>
    <td align="center">15%</td>
    <td align="center">18%</td>
  </tr>
  <tr>
    <td align="center">Q3</td>
    <td align="center">1.7%</td>
    <td align="center">21%</td>
    <td align="center">1.6%</td>
    <td align="center">27%</td>
    <td align="center">1.2%</td>
    <td align="center">15%</td>
  </tr>
</table>

本季的毛利為 19 億，季成長從 8.8% → 1.7%，年成長則是 27% → 21%

細分毛利來源的話：
- 消費者服務 Cash App 佔了 53%，季成長從 4% → 1.6%，年成長 37% → 27%
- 商家服務 Square 貢獻了 47%，季成長從 15% → 1.2%，年成長 18% → 15%

不管是年成長或是季成長，均呈現下滑的趨勢，難道 Block 的成長速度要趨於平緩了嗎？


所幸，Block 在八月推出面向消費者的 Square Go，消費者可直接在 App 上預訂美髮/美容等服務，而提供這些服務的商家，當然也都是使用 Block Square 服務的客戶 <br/>

Block 透過這個 App，直接連接了買賣雙方，也造成消費者＆商家的歡迎，目前在 App 上的成交金額已接近 50 萬

截至九月， Cash App 實現了 2,200 萬月活使用者與 5,500 萬的月交易量（年成長 11%）

## 後記
本季在財報開頭，Block 罕見的附上了股東信，想必是因為股價屢破新低，管理層要給投資者增添信心，裡面重述了公司的價值是為客戶創造價值，而不是在意幾個財報季的表現，希望投資者將眼光放遠，而信內的三個重點如下：
1. 預計於 2026 實現「Rule of 40」的目標，達成 15% 的毛利成長率 與 20% 營業利潤率（調整後）
2. 將花費 10 億回購股票，來抵銷因發放給員工的股權所導致的股票稀釋
3. 把公司人數上限設為 1.2 萬人 直到業務量的成長超過公司的成長

長期面，有第 1 點作為公司成長的方向＆目標
短期面，有第 2 點的股票回購（10 億等於是目前 Block 市值的 2.5%）帶動股價成長，以及第 3 點的裁員控制公司成本

這些直接改善了公司的獲利，股價以及成本，也難怪股價直接跳漲，讓投資人對於 Block 重拾信心 <br />

[上篇](https://ycjhuo.gitlab.io/SQ-Earnings-2023-Q2.html) 才提到希望 Block 股價繼續下跌，讓我能持續加碼/攤平自己在 Block 上的虧損，沒想到 Q3 財報公布後，股價漲得又急又快，讓我放緩了加碼的腳步

在 9 - 10 月，我共買入 126 股 Block，均價約在 $45 左右，若以目前股價($65)計算，約成長了 45% <br />
主帳號的虧損也由 $35,390 → $29,135，縮小了 18%（不含最近買入） <br />

![SQ Block 2023 Q3 Earnings](../images/229-01.png)
![SQ Block 2023 Q3 Earnings](../images/229-02.png)

本季財報也預計了 Q4 的毛利為 19.6 億 - 19.8 億，表示季成長會位在 3.2% - 4.3%，比本季的 1.5% 高出不少，也難怪 Block 股價目前仍沒有降溫的跡象

::: warning
點擊觀看更多： <br />
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::