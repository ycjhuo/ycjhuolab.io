const extendsNetworks = {
  instagram: {
    sharer: 'https://www.instagram.com/ycjhuo/',
    type: 'popup',
    icon: '/Instagram.png',
  },
}

module.exports = {
  plugins: [
    ['sitemap', {hostname: 'https://ycjhuo.gitlab.io/'}],
    ['reading-progress'],
    ['minimal-analytics', {ga: 'UA-111664664-1'}],
    ['google-analytics-4', {gtag: 'G-9N82TZGCXL'}],
    ['google-adsense', {ad_client: 'ca-pub-1016984133618195'}],
    ['disqus', {'shortname': 'gitlab-blog'}],
    ['reading-time', {excludes: ['/about', '/tag/.*']}],
    ['vuepress-plugin-baidu-autopush'],
    ['social-share', {networks: ['facebook', 'line', 'instagram', 'email'], email: 'y.c.jhuo@gmail.com', 
                      autoQuote: false, isPlain: false, extendsNetworks}],
    ['vuepress-plugin-container', {type: 'tip', defaultTitle: { '/': 'TIP', '/zh/': '提示', }, }, ],
//  ['container', {type: 'right', defaultTitle: '', }], ['container', {type: 'theorem', before: info => `<div class="theorem"><p class="title">${info}</p>`, after: '</div>', }],
//  ['vuepress-plugin-mailchimp', {endpoint: 'https://gmail.us1.list-manage.com/subscribe/post?u=5233e0513c2acdcdac16b4906&amp;id=cea48c452f'}],
  ]
};
