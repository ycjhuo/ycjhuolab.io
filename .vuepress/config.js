module.exports = {
  title: "YC's Weekly Journal",
  description: '紐約工程師的 Python 教學 | 美股財報分析 | 旅遊飯店開箱',
  dest: 'public',
  head: [
    [ 'link', { rel: 'icon', href: '/favicon.ico' }],
    [ 'meta', { name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=no'}]
  ],
  theme: 'reco',
  // plugins
  plugins: [require('./plugins.js')],
  themeConfig: {
    // 取消 404 騰訊公益
    noFoundPageByTencent: false,
    nav: require("./nav.js"),
    sidebar: 'auto',
    type: 'blog',
    blogConfig: {
        category: { location: 2, text: 'Category' },
        tag: { location: 3, text: 'Tag' },
        socialLinks: [
            { icon: 'reco-mail', link: 'mailto:y.c.jhuo@gmail.com' },
            { icon: 'fa-instagram', link: 'https://www.instagram.com/ycjhuo/' }
          ],
    },
    /*
    'friendLink': [
      {
        'title': '午后南杂',
        'desc': 'Enjoy when you can, and endure when you must.',
        'email': '1156743527@qq.com',
        'link': 'https://www.recoluan.com'
      },
      {
        'title': 'vuepress-theme-reco',
        'desc': 'A simple and beautiful vuepress Blog & Doc theme.',
        'avatar': 'https://vuepress-theme-reco.recoluan.com/icon_vuepress_reco.png',
        'link': 'https://vuepress-theme-reco.recoluan.com'
      }
    ],
    */

//  'logo': '/logo.png',
    'search': true,
    'searchMaxSuggestions': 10,
    'lastUpdated': 'Last Updated',
    'author': 'YC JHUO',
//  'authorAvatar': '/avatar.jpg',
//  'record': 'xxxx',
    'startYear': '2020'
  },
  'markdown': {
    'lineNumbers': true
  }
}